def match(i,j,shift,x,h):
    f    = "ff" * i*j
    z    = "00" * i*shift
    f    = f+z

    n = len(f)
    z1=''
    z2=''
    
    for i in range(16):
        if i+16 < n:            
            z1 = f[n-i-16-1] + z1
        if i < n:            
            z2 = f[n-i-1] + z2

    if not z1: z1="0"
    if not z2: z2="0"
    
    mask = "D(0x%sUL,0x%sUL)"%(z1,z2)
    
    return "(%s & %s) == (%s & %s)"%(mask,h,mask,x)

def check3(ind):
    return [
        "uint64_t index = 2*i+6+%d;"%(ind),
        "uint64_t z     = ((uint64_t *)x)[index];",
        "*cont          = SCM_PACK((index << 2) + 2);",
        "return SCM_PACK(z);"
    ]

def check2(j,ind,x,cont):
    if (j > 0):
        return check1(j,ind,x,cont)
    
    index = "i+yoffset + %d"%(ind)
    return [
        "{",
        [
            "uint128_t y = x[i+1+%d];"%(ind>>3),
            "if (%s)"%(match(2,1,ind & 0x7,"y","h2")),
            "{",
            check3(ind),
            "}",
            "else %s;"%(cont)
        ] ,
        "}"
    ]

def check1(j,ind,x,cont):
    label1 = "l" + str(j) + "_" + str(ind)
    label2 = "c" + str(j) + "_" + str(ind)
    
    return [
        "if (%s)"%(match(1,j,ind,x,"h1")),
        "{",
            [
                'printf("branch 1 %d\n");'%(j),
                *check2(j>>1, ind  , x, "goto " + label1),
            ],
        "}",
        "else"
        "{",
            [
                label2 + ":",
                'printf("branch 2 %d\n");'%(j),
                *check2(j>>1, ind + j, x, cont)
            ],
        "}",
        cont+";",

        label1 + ":",
        "if (%s)"%(match(1,j,ind + j,x,"h1")),
        "{",
            [
                'printf("branch e %d\n");'%(j),
                "goto " + label2 + ";"
            ],
        "}"        
    ]
        
def check0():            
    return [
        "if (%s)"%(match(1,16,0,"xx","h1")),
        "{",
        check1(8,0,"xx","continue"),
        "}"
    ]

def getString():
    l = check0()
    
    def get(l,offset):
        s = ''
        for x in l:
            if isinstance(x,list):
                s += get(x,offset+1)
            else:
                s += " "*offset*4 + x + "\n"

        return s

    with open("src/hash/macro.c","w") as f:
        f.write(get(l,0))
