(define-module (ice-9 type-dispatcher)
  #:declarative? #f
  #:use-module (rnrs bytevectors)
  #:use-module (persist persistance)
  #:use-module (system foreign)
  #:use-module (ice-9 stis stis)
  #:export (define-type-dispatcher))

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))  

(define flagtable
  #(#f
    #nil
    "#<XXX UNUSED LISP FALSE -- DO NOT USE -- SHOULD NEVER BE SEEN XXX>"
    '()
    #t
    "#<XXX UNUSED BOOLEAN 0 -- DO NOT USE -- SHOULD NEVER BE SEEN XXX>"
    "#<XXX UNUSED BOOLEAN 1 -- DO NOT USE -- SHOULD NEVER BE SEEN XXX>"
    "#<XXX UNUSED BOOLEAN 2 -- DO NOT USE -- SHOULD NEVER BE SEEN XXX>"
    "#<unspecified>"
    "#<undefined>"
    "#<eof>"
    "#<unbound>"))

(define flaglength (vector-length flagtable))

(define tc7-list
  '((#:symbol        #x05)
    (#:variable      #x07)
    (#:vector        #x0d)
    (#:wvect         #x0f)
    (#:string        #x15)
    (#:number        #x17)
    (#:hashtable     #x1d)    
    (#:pointer       #x1f)    
    (#:fluid         #x25)
    (#:stringbuf     #x27)
    (#:dynamic-state #x2d)    
    (#:frame         #x2f)    
    (#:keyword       #x35)
    (#:atomic-box    #x37)    
    (#:syntax        #x3d)
    (#:values        #x3f)
    (#:program       #x45)
    (#:vm-cont       #x47)    
    (#:bytevector    #x4d)
    (#:unused-4f     #x4f)
    (#:weak-set      #x55)
    (#:weak-table    #x57)
    (#:array         #x5d)    
    (#:bitvector     #x5f)
    (#:unused-65     #x65)
    (#:unused-67     #x67)
    (#:unused-6d     #x6d)
    (#:unused-6f     #x6f)
    (#:unused-75     #x75)
    (#:smob          #x77)
    (#:port          #x7d)
    (#:stis          #x7f)))

(define keys-map     (make-hash-table))
(define tc7-keys-map (make-hash-table))

(for-each (lambda (x)
	    (hashq-set! keys-map     (car x) (cadr x))
	    (hashq-set! tc7-keys-map (car x) #t))
	  tc7-list)

;; Extra fields that is outside the general tc7 codes
(hashq-set! keys-map #:char    #t)
(hashq-set! keys-map #:pair    #t)
(hashq-set! keys-map #:struct  #t)
(hashq-set! keys-map #:flag    #t)
(hashq-set! keys-map #:bogus   #t)
(hashq-set! keys-map #:integer #t)

(define wr (@ (guile) write))
(define (pk a x) (wr (list a x)) x)

(define (p16 x y) (format #t "~a: ~a~%" x (number->string y 16)) y)
(define-syntax define-type-dispatcher
  (lambda (x)
    (define (validate keys)
      (for-each
       (lambda (k)
	 (when (not (hashq-ref keys-map k))
	   (error "Nota allowed key in type dispatcher (~a)" k)))
       keys))

	 
    (syntax-case x ()
      ((_ (name exp arg ...) xx (key . code) ...)
       (let ((lookup-table (make-hash-table))
	     (keys         (map (lambda (x)
				  (syntax-case x ()
				    ((a . _)
				     (syntax->datum #'a))))
				#'((key . code) ...))))
	 
	 (define (make-tc7-vals)
	   (let lp ((l keys))
	     (if (pair? l)
		 (if (hashq-ref tc7-keys-map (car l))
		     (cons
		      (syntax-case (hashq-ref lookup-table (car l)) ()
			((code ...)
			 #'(lambda (xx exp arg ...)
			     code ...)))
		      (lp (cdr l)))
		     (lp (cdr l)))
		 '())))

	 (define (make-tc7-keys)
	   (let lp ((l keys))
	     (if (pair? l)
		 (if (hashq-ref tc7-keys-map (car l))
		     (cons	
		      (datum->syntax #'name (hashq-ref keys-map (car l)))
		      (lp (cdr l)))
		     (lp (cdr l)))
		 '())))
	 
	 (validate keys)
	 
	 (for-each
	  (lambda (x)
	    (syntax-case x ()
	      ((a . code)
	       (hashq-set! lookup-table (syntax->datum #'a) #'code))))
	  #'((key . code) ...))
	 
	 (with-syntax ((table          (datum->syntax #'name 'type-table))
		       ((tc7-key ...)  (make-tc7-keys))
		       ((tc7-val ...)  (make-tc7-vals))
		       (bogus
			(aif it (hashq-ref lookup-table #:bogus)
			     #`(lambda (xx exp arg ...)
				 #,@it)
			     #`(lambda (xx exp arg ...)
				 (values))))
		       
		       (pair
			(aif it (hashq-ref lookup-table #:pair)
			     #`(lambda (xx exp arg ...)
				 #,@it)
			     #'(lambda (xx exp arg ...) (values))))
		       
		       (struct
			(aif it (hashq-ref lookup-table #:struct)
			     #`(lambda (xx exp arg ...)
				 #,@it)
			     #'(lambda (xx exp arg ...) (values))))

		       
		       (char
			(aif it (hashq-ref lookup-table #:char)
			     #`(lambda (exp arg ...)
				 #,@it)
			     #'(lambda (exp arg ...) (values))))
		       
		       (flag
			(aif it (hashq-ref lookup-table #:flag)
			     #`(lambda (xx exp arg ...)
				 #,@it)
			     #`(lambda (xx exp arg ...)
				 (values))))

		       (number
			(aif it (hashq-ref lookup-table #:number)
			     #`(lambda (xx exp arg ...)
				 #,@it)
			     #`(lambda (xx exp arg ...)
				 (values))))

		       (integer
			(aif it (hashq-ref lookup-table #:integer)
			     #`(lambda (exp arg ...)
				 #,@it)
			     #`(lambda (exp arg ...)
				 (values)))))

	   #`(begin
	       (define table (make-vector #x80 #f))
	       (vector-set! table tc7-key tc7-val)
	       ...
	       
	       (begin		 
		 (define (name exp arg ...)
		   (let* ((addr (stis-address exp))
			  (tc3  (logand addr 7)))
		     (cond		      
		      ((= (logand tc3 3) 2)
		       (integer exp arg ...))
		      
		      ((= tc3 0)
		       (let* ((addr (lookup-1 exp 0))
			      (tc7  (logand addr #x7f)))
			 (cond
			  ((= (logand tc7 1) 0)
			   (pair tc7 exp arg ...))
			  
			  ((= (logand tc7 7) 1)
			   (struct tc7 exp arg ...))
			  
			  ((vector-ref table tc7)
			   =>
			   (lambda (fkn)
			     (fkn (ash addr -7) exp arg ...)))
			  (else
			   (bogus "bogus-tc7" exp arg ...)))))
		      
		      ((= tc3 4)
		       (cond
			((= (logand #x18 addr) #x8)
			 ;; char
			 (char exp arg ...))
			
			((= (logand #x18 addr) 0)
			 (let ((x (ash addr -8)))
			   (if (< x flaglength)	      
			       (flag x exp arg ...)
			       (bogus "bogus-flag" exp arg ...))))
			
			(else
			 (bogus "bogus-tc8" exp arg ...))))

		      (else
		       (bogus "bogus-tc3" exp arg ...)))))))))))))
  
