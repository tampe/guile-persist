(define-module (ice-9 write)
  #:declarative? #f
  #:use-module (system foreign)
  #:use-module (system syntax)
  #:use-module (system vm program)
  #:use-module (persist persistance)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs arithmetic fixnums)
  #:use-module ((rnrs io ports)
                #:prefix rnrs-ports:)
  #:use-module ((oop goops)
		#:select
		(is-a? <object> slot-ref))
  #:use-module (ice-9 soft-suspendable-ports)
  #:use-module (ice-9 soft-port)
  #:use-module (ice-9 control)
  #:use-module (ice-9 iconv)
  #:use-module (ice-9 match)
  #:use-module (ice-9 atomic)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 type-dispatcher)
  #:replace (write display)
  #:export  (get-print-state
	     flags-with-print-state
	     install-write!
	     uninstall-write!))	    



(eval-when (eval load compile)
  (if (module-defined? (resolve-module '(ice-9 stis stis)) 'stis-hash)
      (use-modules 
       ((ice-9 stis stis    ) #:select (stis-address)))
      (use-modules 
       ((guile              ) #:select ((object-address . stis-address))))))
      

;; CONFIG ===========================
(define write-program             #f)
(define use-write-program?        #f)
(define default-fancy?            #f)
(define default-len               32)
(define default-complexity        1024)
(define print-r7rs-symbols?       #f)
(define quote-keywordish-symbols? #f)
(define print-escape-newlines?    #t)
(define print-r6rs-escapes?       #f)
(define highlight-prefix          "{")
(define highlight-suffix          "}")
(define print-keyword-style       'reader)
(define keyword-style?            #t)
(define print-hex-reals?          #f)
(define compress-refs?            #f)
(define default-binary            4)
;; ==================================

(define (lookup-2-scm a b c) (pointer->scm (make-pointer (lookup-2 a b c))))

(define wr (@ (guile) write))
(define (pk a x) (wr (list a x)) x)

(define SCM_CODEPOINT_DOTTED_CIRCLE #x25cc)

(define UC_CCC_NR 0)
(define UC_CATEGORY_MASK_L  #x0000001f)
(define UC_CATEGORY_MASK_LC #x00000007)
(define UC_CATEGORY_MASK_Lu #x00000001)
(define UC_CATEGORY_MASK_Ll #x00000002)
(define UC_CATEGORY_MASK_Lt #x00000004)
(define UC_CATEGORY_MASK_Lm #x00000008)
(define UC_CATEGORY_MASK_Lo #x00000010)
(define UC_CATEGORY_MASK_M  #x000000e0)
(define UC_CATEGORY_MASK_Mn #x00000020)
(define UC_CATEGORY_MASK_Mc #x00000040)
(define UC_CATEGORY_MASK_Me #x00000080)
(define UC_CATEGORY_MASK_N  #x00000700)
(define UC_CATEGORY_MASK_Nd #x00000100)
(define UC_CATEGORY_MASK_Nl #x00000200)
(define UC_CATEGORY_MASK_No #x00000400)
(define UC_CATEGORY_MASK_P  #x0003f800)
(define UC_CATEGORY_MASK_Pc #x00000800)
(define UC_CATEGORY_MASK_Pd #x00001000)
(define UC_CATEGORY_MASK_Ps #x00002000)
(define UC_CATEGORY_MASK_Pe #x00004000)
(define UC_CATEGORY_MASK_Pi #x00008000)
(define UC_CATEGORY_MASK_Pf #x00010000)
(define UC_CATEGORY_MASK_Po #x00020000)
(define UC_CATEGORY_MASK_S  #x003c0000)
(define UC_CATEGORY_MASK_Sm #x00040000)
(define UC_CATEGORY_MASK_Sc #x00080000)
(define UC_CATEGORY_MASK_Sk #x00100000)
(define UC_CATEGORY_MASK_So #x00200000)
(define UC_CATEGORY_MASK_Z  #x01c00000)
(define UC_CATEGORY_MASK_Zs #x00400000)
(define UC_CATEGORY_MASK_Zl #x00800000)
(define UC_CATEGORY_MASK_Zp #x01000000)
(define UC_CATEGORY_MASK_C  #x3e000000)
(define UC_CATEGORY_MASK_Cc #x02000000)
(define UC_CATEGORY_MASK_Cf #x04000000)
(define UC_CATEGORY_MASK_Cs #x08000000)
(define UC_CATEGORY_MASK_Co #x10000000)
(define UC_CATEGORY_MASK_Cn #x20000000)

(define INITIAL_IDENTIFIER_MASK
  (logior UC_CATEGORY_MASK_Lu UC_CATEGORY_MASK_Ll UC_CATEGORY_MASK_Lt
	  UC_CATEGORY_MASK_Lm UC_CATEGORY_MASK_Lo UC_CATEGORY_MASK_Mn
	  UC_CATEGORY_MASK_Nl UC_CATEGORY_MASK_No UC_CATEGORY_MASK_Pd
	  UC_CATEGORY_MASK_Pc UC_CATEGORY_MASK_Po UC_CATEGORY_MASK_Sc
	  UC_CATEGORY_MASK_Sm UC_CATEGORY_MASK_Sk UC_CATEGORY_MASK_So
	  UC_CATEGORY_MASK_Co))

(define SUBSEQUENT_IDENTIFIER_MASK
  (logior INITIAL_IDENTIFIER_MASK
	  UC_CATEGORY_MASK_Nd UC_CATEGORY_MASK_Mc UC_CATEGORY_MASK_Me))

(define flagnames
  #("#f"
    "#nil"
    "#<XXX UNUSED LISP FALSE -- DO NOT USE -- SHOULD NEVER BE SEEN XXX>"
    "()"
    "#t"
    "#<XXX UNUSED BOOLEAN 0 -- DO NOT USE -- SHOULD NEVER BE SEEN XXX>"
    "#<XXX UNUSED BOOLEAN 1 -- DO NOT USE -- SHOULD NEVER BE SEEN XXX>"
    "#<XXX UNUSED BOOLEAN 2 -- DO NOT USE -- SHOULD NEVER BE SEEN XXX>"
    "#<unspecified>"
    "#<undefined>"
    "#<eof>"
    "#<unbound>"))

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

(define escaped-newline-flag         (ash 1 0 ))
(define r6rs-escapes-flag            (ash 1 1 ))
(define write-flag                   (ash 1 2 ))
(define fancy-flag                   (ash 1 3 ))
(define r7rs-symbols-flag            (ash 1 4 ))
(define hex-reals-flag               (ash 1 5 ))
(define write-program-flag           (ash 1 6 ))
(define quote-keywordish-symbol-flag (ash 1 7 ))
(define keyword-style-flag           (ash 1 8 ))
(define before-flag                  (ash 1 9 ))
(define inherited-flag               (ash 1 10))
(define highlight-flag               (ash 1 11))
(define keyword-style-mask           (ash 3 12))
(define keyword-style-flag           (ash 1 14))
(define compress-refs-flag           (ash 1 15))
(define d-shift                      16 )
(define sh-shift                     18 )
(define sh-mask                      (ash 3 sh-shift))
(define sh-d-mask                    3  )
(define convert-mask                 (ash 3 20))
(define utf-flag                     (ash 1 22))
(define binary-mask                  (ash 7 23))
(define binary-shift                 23)
(define comma-flag                   (ash 1 26))
(define quote-flag                   (ash 1 27))
(define python-flag                  (ash 1 28))

(define encoding-mask
  (logior utf-flag
	  convert-mask
	  (ash sh-d-mask d-shift)
	  (ash sh-d-mask sh-shift)))

(define convert-escape  (ash 0 20))
(define convert-replace (ash 1 20))


(define* (make-flags #:key
		     (inherited?                #f                       )
		     (write?                    #t                       )
		     (fancy?                    default-fancy?           )
		     (r6rs-escapes?             print-r6rs-escapes?      )
		     (r7rs-symbols?             print-r7rs-symbols?      )
		     (hex-reals?                print-hex-reals?         )
		     (escape-newlines?          print-escape-newlines?   )
		     (use-write-program?        use-write-program?       )
		     (keyword-style?            keyword-style?           )
		     (quote-keywordish-symbols? quote-keywordish-symbols?)
		     (binary                    default-binary           )
		     (compress-refs?            compress-refs?           ))
  
  (logior
   (ash binary binary-shift)
   (if escape-newlines?          escaped-newline-flag         0)
   (if r6rs-escapes?             r6rs-escapes-flag            0)
   (if write?                    write-flag                   0)
   (if fancy?                    fancy-flag                   0)
   (if r7rs-symbols?             r7rs-symbols-flag            0)
   (if hex-reals?                hex-reals-flag               0)
   (if use-write-program?        write-program-flag           0)
   (if quote-keywordish-symbols? quote-keywordish-symbol-flag 0)
   (if keyword-style?            keyword-style-flag           0)
   (if compress-refs?            compress-refs-flag           0)
   (if inherited?                inherited-flag               0)))

(define i-flags  0)
(define i-buffer 1)
(define i-buffpt 2)
(define i-hash   3)
(define i-pts    4)
(define i-cut    5)
(define i-pos    6)
(define i-maxpos 7)
(define i-maxlen 8)
(define j-cut 0)
(define j-len 1)

(define-inlinable (pstate-flags  pstate) (vector-ref pstate i-flags))
(define-inlinable (pstate-pos    pstate) (vector-ref pstate i-pos))
(define-inlinable (pstate-maxpos pstate) (vector-ref pstate i-maxpos))
(define-inlinable (pstate-len    pstate) (vector-ref
					  (vector-ref pstate i-cut)
					  j-len))
(define-inlinable (pstate-cut    pstate) (vector-ref
					  (vector-ref pstate i-cut)
					  j-cut))
(define-inlinable (pstate-maxlen pstate) (vector-ref pstate i-maxlen))

(define-inlinable (pstate-pos-set! pstate x) (vector-set! pstate i-pos x))
(define-inlinable (pstate-len-set! pstate x) (vector-set!
					      (vector-ref pstate i-cut)
					      j-len x))
(define-inlinable (pstate-cut-set! pstate)   (vector-set!
					      (vector-ref pstate i-cut)
					      j-cut #t))

(define-inlinable (flag? flags flag) (> (logand flags flag) 0))
(define-inlinable (pstate-buffer x) (vector-ref x i-buffer))
(define-inlinable (pstate-hash   x) (vector-ref x i-hash  ))
(define-inlinable (pstate-buffpt x) (vector-ref x i-buffpt))
(define-inlinable (pstate-pts    x) (vector-ref x i-pts))

(define-inlinable (pstate-buffpt-set! x y) (vector-set! x i-buffpt y))

(define (set-flag kind flag flags)
  (cond
   ((= kind 0)
    flags)
   (kind
    (logior flags flag))
   (else
    (logand flags (lognot flag)))))

(define (set-field mask x shift flags)
  (if (< x 0)
      flags
      (logior (logand flags (lognot mask))
	      (ash x shift))))

(define (handle-flags flags inherited? write? binary)
  (if flags
      (set-field binary-mask binary binary-shift
		 (set-flag inherited? inherited-flag
			   (set-flag write? write-flag flags)))
      (make-flags #:write? write? #:inherited? inherited? #:binary binary)))

(define* (make-pstate buffdata hash
		      #:key
		      (binary     -1)
		      (flags      #f)
		      (write?     0 )
		      (inherited? 0 ))
  (vector
   (handle-flags flags inherited? write? binary)
   (car buffdata)
   (cdr buffdata)
   hash
   (cons 0 0)
   (vector #f 0) 0 1024 30))
	  

(define* (pstate-copy pstate
		      #:key
		      (buffdata   #f)
		      (hash       #f)
		      (flags      #f)
		      (write?     0)
		      (binary    -1)
		      (inherited? 0 ))
		      
  (let ((buffer (if buffdata (car buffdata) (pstate-buffer pstate)))
	(buffpt (if buffdata (cdr buffdata) (pstate-buffpt pstate)))
	(hash   (if hash     hash           (pstate-hash   pstate)))
	(flags  (if flags
		    (handle-flags flags inherited? write?
				  (if (< binary 0)
				      (ash (logand binary-mask flags)
					   (- binary-shift))
				      binary))
		    
		    (handle-flags (pstate-flags pstate)  inherited? write?
				  (if (< binary 0)
				      0
				      binary)))))
    
    (vector flags buffer buffpt hash
	    (cons 0 0)
	    (pstate-cut pstate)
	    (pstate-len pstate)		    
	    (pstate-maxpos pstate)
	    (pstate-maxlen pstate))))

(define-inlinable (fancy? flags)
  (flag? flags fancy-flag))

(define-inlinable (highlight? flags)
  (and
   (flag? flags fancy-flag)
   (flag? flags highlight-flag)))

(define-inlinable (write? flags)
  (flag? flags write-flag))

(define-inlinable (pstate-cleanup flags pstate obj)
  (when (not (flag? flags compress-refs-flag))
    (hashq-remove! (pstate-hash pstate) obj)))

(define-inlinable (pstate-register pstate obj)
  (hashq-set! (pstate-hash pstate) obj (pstate-pos pstate)))

(define-inlinable (pstate-before? pstate exp)
  (hashq-ref (pstate-hash pstate) exp))

(define-inlinable (pstate-cut? pstate i)
  (or
   (pstate-cut pstate)
   (if (> i (pstate-maxlen pstate))
       #t       
       (if (> (pstate-pos pstate) (pstate-maxpos pstate))
	   (begin
	     (pstate-cut-set! pstate)
	     #t)
	   #f))))
	

(define-inlinable (pstate++ pstate i)
  (pstate-len-set! pstate (+ i (pstate-len pstate))))

(define-inlinable (pstate-obj++ pstate)
  (pstate-pos-set! pstate (+ 1 (pstate-pos pstate))))

(define buffsize (ash 1 14))
(define nbuff    (- buffsize 600))
(define cleared-bv (make-bytevector buffsize))

(define (clear-bv bv)
  (bytevector-copy! cleared-bv 0 bv 0 buffsize))

(define current-write-port (make-thread-local-fluid #f))
(define-inlinable (get-port) (fluid-ref current-write-port))

(define (make-write-buffer) (make-bytevector buffsize 0))
(define* (make-buffdata #:key (buffer #f) (pt 0))
  (cons (if buffer buffer (make-write-buffer)) pt))

(define-syntax with-buffdata
  (syntax-rules ()
    ((_ (pstate : buffer) code ...)
     (let ((buffer (pstate-buffer pstate)))
       (pstate-buffpt-set! pstate (begin code ...))))

    ((_ (pstate : buffer buffpt) code ...)
     (let ((buffer (pstate-buffer pstate))
	   (buffpt (pstate-buffpt pstate)))
       (pstate-buffpt-set! pstate (begin code ...))))

    ((_ (pstate : buffer buffpt pts) code ...)
     (let ((buffer (pstate-buffer pstate))
	   (buffpt (pstate-buffpt pstate))
	   (pts    (pstate-pts    pstate)))
       (pstate-buffpt-set! pstate (begin code ...))))))

(define-syntax with-buffdata-0
  (syntax-rules ()
    ((_ (pstate : buffer) code ...)
     (let ((buffer (pstate-buffer pstate)))
       (begin code ...)))

    ((_ (pstate : buffer buffpt) code ...)
     (let ((buffer (pstate-buffer pstate))
	   (buffpt (pstate-buffpt pstate)))
       (begin code ...)))

    ((_ (pstate : buffer buffpt pts) code ...)
     (let ((buffer (pstate-buffer pstate))
	   (buffpt (pstate-buffpt pstate))
	   (pts    (pstate-pts    pstate)))
       (begin code ...)))))

(define-syntax-rule (with-shift ((flags : sh d)) code ...)
  (let ((sh (logand (ash flags (- sh-shift)) 3))
	(d  (logand (ash flags (-  d-shift)) 3)))
    code ...))

(define-syntax put-char+
  (lambda (x)
    (syntax-case x ()
      ((_ flags pstate ch)
       (with-syntax ((n (char->integer (syntax->datum #'ch))))
         #'(put-u8* flags pstate n))))))

(define-syntax put-string+
  (lambda (x)
    (syntax-case x ()
      ((_ flags pstate str)
       (let ((l (string->list (syntax->datum #'str))))
       (with-syntax (((n ...) (map char->integer l))
		     ((i ...) (iota (length l)))
		     (N       (length l)))
	 #'(if (eq? (flag? flags sh-mask) 0)
	       (with-buffdata (pstate : buffer ptbuff)
	         (bytevector-u8-set! buffer (+ ptbuff i) n) ...
		 (if (<= nbuff (+ ptbuff N))
		     (begin
		       (put-bytevector (get-port) buffer 0 (+ ptbuff N))
		       0)
		     (+ ptbuff N)))
	       (with-shift ((flags : sh d))
	         (with-buffdata (pstate : buffer ptbuff)
		   (bytevector-u8-set! buffer (+ ptbuff (ash i sh) d) n) ...
		   (if (<= nbuff (+ ptbuff (ash N sh)))
		       (begin
			 (put-bytevector (get-port) buffer 0
					 (+ ptbuff (ash N sh)))
			 (clear-bv buffer)
			 0)
		       (+ ptbuff (ash N sh))))))))))))

(define (put-string++ flags pstate str)
  (define N (string-length str))
  (if (eq? (logand flags sh-mask) 0)
      (with-buffdata (pstate : buffer ptbuff)
        (let lp ((i 0))
	  (when (< i N)	       
	    (bytevector-u8-set! buffer (+ ptbuff i)
				(char->integer (string-ref str i)))
	    (lp (+ i 1))))
	
	(if (<= nbuff (+ ptbuff N))
	    (begin
	      (put-bytevector (get-port) buffer 0 (+ ptbuff N))
	      (clear-bv buffer)
	      0)
	    (+ ptbuff N)))
      
      (with-shift ((flags : sh d))
        (with-buffdata (pstate : buffer ptbuff)
	   (let lp ((i 0))
	     (when (< i N)	       
	       (bytevector-u8-set! buffer (+ ptbuff (ash i sh) d)
				   (char->integer (string-ref str i)))
	       (lp (+ i 1))))
	
	   (if (<= nbuff (+ ptbuff (ash N sh)))
	       (begin
		 (put-bytevector (get-port) buffer 0
				 (+ ptbuff (ash N sh)))
		 (clear-bv buffer)
		 0)
	       (+ ptbuff (ash N sh)))))))

(define-inlinable (put-u8* flags pstate ch)
  (with-buffdata (pstate : buffer ptbuff)
   (if (eq? (logand flags sh-mask) 0)		     
       (begin
	 (bytevector-u8-set! buffer ptbuff ch)
	 (+ ptbuff 1))
       (with-shift ((flags : sh d))
         (bytevector-u8-set! buffer (+ ptbuff d) ch)
	 (+ ptbuff (ash 1 sh))))))

(define-inlinable (put-char* flags pstate ch)
  (put-u8* flags pstate (char->integer ch)))


(define (buffdata-flush port pstate)
  (with-buffdata (pstate : buffer ptbuff)
    (put-bytevector port buffer 0 ptbuff)
    0))

(define-inlinable (maybe-flush pstate pt)
  (with-buffdata (pstate : buffer buffpt)
    (let ((pt (+ buffpt pt)))
      (if (<= nbuff pt)
	  (begin
	    (put-bytevector (get-port) buffer 0 pt)
	    (clear-bv buffer)
	    0)
	  pt))))

(define (put-bytevector* flags pstate bv i count)
  (with-buffdata (pstate : buffer ptbuff)     
    (let lp ((c count) (i i) (pt ptbuff))
      (if (> c 0)
	  (let* ((diff (- nbuff pt))
		 (cc   (if (> diff c) c diff)))
	    (if (<= diff 0)
		(begin
		  (put-bytevector (get-port) buffer 0 pt)
		  (clear-bv buffer)
		  (lp c i 0))
		(begin
		  (if (<= cc 5)
		      (let lp2 ((j 0))
			(when (< j cc)
			  (bytevector-u8-set!
			   buffer (+ pt j)
			   (bytevector-u8-ref bv (+ i j)))
			  (lp2 (+ j 1))))
		      (bytevector-copy! bv i buffer pt cc))
		  (lp (- c cc) (+ i cc) (+ pt cc)))))
	  pt))))

(define-syntax-rule (mk-seq put-string** put-string* c-print-string
			    string-length)
  (begin
    (define (put-string** flags pstate str i count)
      (with-buffdata (pstate : buffer ptbuff pts)     
	(let lp ((c count) (i i) (pt ptbuff))
	  (if (> c 0)
	      (let* ((diff (- nbuff pt)))
		(if (<= diff 0)
		    (begin
		      (put-bytevector (get-port) buffer 0 pt)
		      (clear-bv buffer)
		      (lp c i 0))
		    (begin
		      (set-car! pts pt)
		      (set-cdr! pts c)
		      (c-print-string str i buffer pts flags)
		      (let* ((new-count (cdr pts))
			     (incr      (- c new-count)))
			(lp new-count (+ i incr) (car pts))))))
	      pt))))

    (define (put-string* flags pstate str)
      (put-string** flags pstate str 0 (string-length str)))))

(define-syntax-rule (mk-seq2 put-string** put-string* c-print-string
			    string-length)
  (begin
    (define (put-string** flags pstate str i count)
      (with-buffdata (pstate : buffer ptbuff pts)     
        (let lp ((c count) (i i) (pt ptbuff))
	  (if (> c 0)
	      (let* ((diff (- nbuff pt)))
		(if (<= diff 0)
		    (begin
		      (put-bytevector (get-port) buffer 0 pt)
		      (clear-bv buffer)
		      (lp c i 0))
		    (begin
		      (set-car! pts pt)
		      (set-cdr! pts c)
		      (c-print-string str i buffer pts flags)
		      (let* ((new-count (cdr pts))
			     (incr      (- c new-count)))
			(lp new-count (+ i incr) (car pts))))))
	      pt))))

    (define (put-string* flags pstate str)
      (put-string** flags pstate str 0 (string-length str)))))



(mk-seq2 put-string**           put-string*           c-print-string
	 string-length)

(mk-seq2 put-string-write**     put-string-write*     c-print-string-write
	 string-length)

(mk-seq2 put-string-r7rs**      put-string-r7rs*      c-print-string-r7rs
	 string-length)

(mk-seq2 put-string-extended**  put-string-extended*  c-print-string-extended
	 string-length)

(mk-seq put-bv-u8**            put-bv-u8*             c-print-u8
	array-length)

(mk-seq put-bv-s8**            put-bv-s8*             c-print-s8
	array-length)

(mk-seq put-bv-u16**           put-bv-u16*            c-print-u16
	array-length)

(mk-seq put-bv-s16**           put-bv-s16*            c-print-s16
	array-length)

(mk-seq put-bv-u32**           put-bv-u32*            c-print-u32
	array-length)

(mk-seq put-bv-s32**           put-bv-s32*            c-print-s32
	array-length)

(mk-seq put-bv-u64**           put-bv-u64*            c-print-u64
	array-length)

(mk-seq put-bv-s64**           put-bv-s64*            c-print-s64
	array-length)

(mk-seq put-bv-f64**           put-bv-f64*            c-print-f64
	array-length)

(mk-seq put-bv-f32**           put-bv-f32*            c-print-f32
	array-length)


(define (put-bytevector** flags pstate bv)
  (put-bytevector* flags pstate bv 0 (bytevector-length bv)))

(define r5rs-charnames
  #("space" "newline"))


(define r5rs-charnums
  #(#x20 #x0a))

(define r6rs-charnames
  #("nul" "alarm" "backspace" "tab" "linefeed" "vtab" "page"
    "return" "esc" "delete"))

(define r6rs-charnums
  #(#x00 #x07 #x08 #x09 #x0a #x0b #x0c
	 #x0d #x1b #x7f))

    
(define r7rs-charnames
  #("escape"))

(define r7rs-charnums
  #(#x1b))

(define C0-control-charnames
  #("nul" "soh" "stx" "etx" "eot" "enq" "ack" "bel"
    "bs"  "ht"  "lf"  "vt"  "ff"  "cr"  "so"  "si"
    "dle" "dc1" "dc2" "dc3" "dc4" "nak" "syn" "etb" 
    "can" "em"  "sub" "esc" "fs"  "gs"  "rs"  "us"
    "sp" "del"))

(define C0-control-charnums
  #(#x00 #x01 #x02 #x03 #x04 #x05 #x06 #x07
	 #x08 #x09 #x0a #x0b #x0c #x0d #x0e #x0f
	 #x10 #x11 #x12 #x13 #x14 #x15 #x16 #x17
	 #x18 #x19 #x1a #x1b #x1c #x1d #x1e #x1f
	 #x20 #x7f))

(define alt-charnames
  #("null" "nl" "np"))

(define alt-charnums
  #(#x00 #x0a #x0c))

(define char-nametable (make-vector 256 #f))
(define (mk-nt a b)
  (define n (vector-length a))
  (let lp ((i 0))
    (when (< i n)
      (vector-set! char-nametable (vector-ref a i) (vector-ref b i))
      (lp (+ i 1)))))

(mk-nt alt-charnums        alt-charnames)
(mk-nt C0-control-charnums C0-control-charnames)
(mk-nt r7rs-charnums       r7rs-charnames)
(mk-nt r6rs-charnums       r6rs-charnames)
(mk-nt r5rs-charnums       r5rs-charnames)

(define (char-name ch)
  (define x (char->integer ch))

  (if (< x 256)
      (vector-ref char-nametable x)
      #f))

(define libz (dynamic-link))

(define-syntax-rule (define-foreign name ret string-name args)
  (define name
    (pointer->procedure ret (dynamic-func string-name libz) args)))

(define-foreign uc_is_general_category_withtable
  int "uc_is_general_category_withtable" (list unsigned-long unsigned-long))

(define-foreign uc_combining_class
  int "uc_combining_class" (list unsigned-long))

(define (print-character ch flags pstate)
  (define x (char->integer ch))
  
  (put-string+ flags pstate "#\\")

  (cond
   ((and (not (eq? (uc_combining_class x) UC_CCC_NR))
	 #;(can-put-char flags SCM_CODEPOINT_DOTTED_CIRCLE)
	 #;(can-put-char flags ch))
    
    (put-char* flags pstate (integer->char SCM_CODEPOINT_DOTTED_CIRCLE))
    (put-char* flags pstate ch))
   
   ((and (> (uc_is_general_category_withtable
	     x
	     (logior UC_CATEGORY_MASK_L
		     UC_CATEGORY_MASK_M
		     UC_CATEGORY_MASK_N
		     UC_CATEGORY_MASK_P
		     UC_CATEGORY_MASK_S)) 0)
	 #;(can-put-char flags ch))
    (put-string* flags pstate (list->string (list ch))))

   ((char-name ch)
    =>
    (lambda (str)
      (put-string++ flags pstate str)))

   ((not (flag? flags r6rs-escapes-flag))
    (print-number* (char->integer ch) 16 flags pstate))

   (else
    (put-char+ flags pstate #\x)
    (print-number* (char->integer ch) 16 flags pstate))))

(define-inlinable (print exp flags pstate)
  (if (highlight? flags)
      (begin
	(put-string++ highlight-prefix flags pstate)
	(iprin1       exp              flags pstate)
	(put-string++ highlight-suffix flags pstate))
      
      (iprin1 exp flags pstate)))

(define (print-circref obj flags pstate)
  (let ((n (hashq-ref (pstate-hash pstate) obj)))
    (put-char+ flags pstate #\#)
    (print-number-10* n flags pstate)
    (put-char+ flags pstate #\#)))

(define (print-extended-symbol str flags pstate)
  (put-string+ flags pstate "#{")
  (put-string-extended* flags pstate str)
  (put-string+ flags pstate "}#"))

(define (print-r7rs-extended-symbol str flags pstate)
  (put-char+ flags pstate #\|)
  (put-string-r7rs* flags pstate str)
  (put-char+ flags pstate #\|))

(define (symbol-quoted? str flags)
  (define len (string-length str))
  (if (= len 0)
      #t
      (let lp ((i 0) (count len))
	(if (> count 0)
	    (let ((n (c-symbol-quoted? str i count flags)))
	      (if (eq? n #t)
		  #t
		  (lp (+ i n) (- count n))))
	    #f))))
	      
(define (print-symbol-adv sym flags pstate)
  (let ((str (symbol->string sym)))
    (if (symbol-quoted? str flags)
	(if (flag? flags r7rs-symbols-flag)
	    (print-r7rs-extended-symbol str flags pstate)
	    (print-extended-symbol      str flags pstate))
	
	(put-string* flags pstate str))))

(define (put-symbol* sym flags pstate)
  (let ((str (symbol->string sym)))
    (if (symbol-quoted? str (pstate-flags pstate))
	(if (flag? flags r7rs-symbols-flag)
	    (put-string-r7rs*     flags pstate str)
	    (put-string-extended* flags pstate str))
	
	(put-string* flags pstate str))))


(define (print-symbol x exp flags pstate)
  (if (symbol-interned? exp)
      (print-symbol-adv exp flags pstate)
      (begin
	(put-string+       flags pstate "#<uninterned-symbol ")
	(print-symbol-adv  exp  flags pstate)
	(put-char+         flags pstate #\space)
	(print-number*     (stis-address exp) 16 flags pstate)
	(put-char+         flags pstate #\>))))

(define (print-string x exp flags pstate)
  (if (write? flags)
      (begin
	(put-char+ flags pstate #\")
	(put-string-write* flags pstate exp)
	(put-char+ flags pstate #\"))
	
      (put-string* flags pstate exp)))
  
(define (print-number* n radix flags pstate)
  (put-string++ flags pstate (number->string n radix)))

(define (print-fixnum* n flags pstate)
  (maybe-flush
   pstate 
   (print-fixnum (pstate-buffer pstate)
		 n
		 (pstate-buffpt pstate)
		 flags)))

(define (print-integer* n flags pstate)
  (if (fixnum? n)
      (print-fixnum* n flags pstate)
      (put-string* flags pstate (number->string n 10))))

(define (print-inexact* flags pstate n)
  (if (real? n)
      (maybe-flush
       pstate
       (print-real (pstate-buffer pstate)
		   n
		   (pstate-buffpt pstate)
		   flags))
      
      (let ((x (real-part n))
	    (y (imag-part n)))

	(maybe-flush
	 pstate
	 (print-real (pstate-buffer pstate)
		     x
		     (pstate-buffpt pstate)
		     flags))


	(if (< y 0)
	    (begin
	      (put-string+ flags pstate "-")      
	      (maybe-flush
	       pstate
	       (print-real (pstate-buffer pstate)
			   (- y)
			   (pstate-buffpt pstate)
			   flags)))
	    (begin
	      (put-string+ flags pstate "+")      
	      (maybe-flush
	       pstate
	       (print-real (pstate-buffer pstate)
			   y
			   (pstate-buffpt pstate)
			   flags))))
	
	(put-string+ flags pstate "i"))))

(define (print-number-10* n flags pstate)
  (cond
   ((inexact? n)
    (print-inexact* flags pstate n))
   
   ((integer? n)
    (print-integer* n flags pstate))

   ((rational? n)
    (begin
      (print-integer* (numerator   n) flags pstate)
      (put-char+ flags pstate #\/)
      (print-integer* (denominator n) flags pstate)))
   
   (else
    (put-string* flags pstate (number->string n 10)))))

(define (print-number-16* n flags pstate)
  (put-string++ flags pstate (number->string n 16)))

(define (print-unknown hdr obj flags pstate)
  (put-string+ flags pstate "#<unknown-")
  (put-string++ flags pstate hdr)
  (print-number* (stis-address obj) 16 flags pstate)
  (put-char+ flags pstate #\>))

(define (print-hashtable x h flags pstate)
  (put-string+ flags pstate "#<hash-table ")
  (print-number* (stis-address exp) 16 flags pstate)
  (put-char* flags pstate #\space)
  (print-number-10* (lookup-2 h 2 0) flags pstate) 
  (put-char+ flags pstate #\/)
  (print-number-10* (ash (lookup-2 h 1 0) -8) flags pstate)
  (put-char+ flags pstate #\>))

(define (print-list-adv exp tlr flags pstate adv)
  (define n   0)
  (define sep (if (flag? flags comma-flag) #\, #\space))
  
  (define (circ-ref exp)
    (put-string+ flags pstate " . ")
    (print-circref exp flags pstate))

  (define (cleanup stop)
    (let lp ((exp exp))
      (pstate-cleanup flags pstate exp)
      (when (not (eq? exp stop))
	(lp (cdr exp))))
    (put-string++ flags pstate tlr))
  
  (let lp ((exp exp))
    (define (next exp)
      (cond
       ((pair? exp)
	(put-char* flags pstate sep)
	(lp exp))
       
       ((null? exp)
	(put-string++ flags pstate tlr))

       (else
	(put-string++ flags pstate " . ")
	(if (fancy? flags)
	    (begin
	      (pstate++ pstate 1)
	      (print exp flags pstate))
	  
	    (print exp flags pstate))
	(put-string++ flags pstate tlr))))
      	
       
    (define (fancy)
      (if (fancy? flags)
	  (begin
	    (pstate++ pstate 1)
	    (print (car exp) flags pstate)
	    (set! n (+ n 1))
	    (if (pstate-cut? pstate n)
		(put-string+ flags pstate " ...")
		(next (cdr exp))))
	  
	  (begin
	    (print (car exp) flags pstate)
	    (next (cdr exp)))))
      
      
    (if (pair? exp)
	(let ((x (car exp)))
	  (if adv
	      (if (pstate-before? pstate exp)
		  (begin
		    (circ-ref exp)
		    (cleanup exp))
		  
		  (begin
		    (pstate-register pstate exp)
		    (fancy)))
	      (fancy)))
	(next exp))))
	
	    
(define (print-list hdr exp tlr flags pstate)
  ;; Classical tourtous hare check
  (define adv
    (let lp ((h exp) (t #f))
      (if (pair? h)
	  (if (eq? h t)
	      exp
	      (let ((t  (if t t h))
		    (h2 (cdr h)))
		(if (pair? h2)
		    (if (eq? h2 t)
			exp
			(lp (cdr h2) (cdr t)))
		    #f)))
	  #f)))

  (define sep (if (flag? flags comma-flag) #\, #\space))
  
  (put-string++ flags pstate hdr)

  (if (or adv (fancy? flags))
      (print-list-adv exp tlr flags pstate adv)

      (with-buffdata-0 (pstate : buffer buffpt pst)	      
	(let lp ((exp exp))
	  (when (pair? exp)
	    (set-car! pst (pstate-buffpt pstate))
	    
	    (let ((exp (c-print-list exp buffer pst flags))
		  (e   (cdr pst)))
		
	      (define (sync)
		(let ((buffpt (car pst)))
		  (pstate-buffpt-set!
		   pstate
		   (if (> buffpt nbuff)
		       (begin
			 (put-bytevector (get-port) buffer 0 buffpt)
			 0)
		       buffpt))))

	      (if e
		  (let ((nexp (cdr exp)))
		    (sync)
		    (print (car exp) flags pstate)
		    (cond
		     ((pair? nexp)
		      (put-char* flags pstate sep)
		      (lp exp))
		     
		     ((null? nexp)
		      (values))
		     
		     (else
		      (put-string+ flags pstate " . ")
		      (print nexp flags pstate))))
		    
		  (begin
		    (sync)
		    (when (pair? exp)
		      (lp exp)))))))))

  (put-string++ flags pstate tlr))


	    
(define (print-variable x exp flags pstate)
  (put-string+ flags pstate "#<variable 0x")
  (print-number* (stis-address exp) 16 flags pstate)
  (put-string+ flags pstate " value: ")
  (print (variable-ref exp) flags pstate)
  (put-char+ flags pstate #\>))

(define (print-atomic-box x exp flags pstate)
  (put-string+ flags pstate "#<atomic-box 0x")
  (print-number* (stis-address exp) 16 flags pstate)
  (put-string+ flags pstate " value: " )
  (print (atomic-box-ref exp) flags pstate)
  (put-char+ flags pstate #\>))

(define (print-fluid x exp flags pstate)
  (if (fluid-thread-local? exp)
      (put-string+ flags pstate "#<thread-local-fluid 0x")
      (put-string+ flags pstate "#<fluid 0x"))

  (print-number* (stis-address exp) 16 flags pstate)
  (put-char+ flags pstate #\>))

(define (print-pointer x exp flags pstate)
  (put-string+ flags pstate "#<pointer 0x")
  (print-number* (pointer-address exp) 16 flags pstate)
  (put-char+ flags pstate #\>))

(define (print-stis x exp flags pstate)
  (put-string+ flags pstate "#<stis-stack 0x")
  (print-number* (stis-address exp) 16 flags pstate)
  (put-char+ flags pstate #\>))

(define (print-vector x v flags pstate)
  (if (fancy? flags)
      (print-vector1 x v flags pstate)
      (print-vector2 x v flags pstate)))

(define (print-vector1 x v flags pstate)
  (put-string+ flags pstate "#(")
  
  (let* ((n      0)
	 (len    (vector-length v))
	 (last   (- len 1)))
    
    (let lp ((i 0))
      (define (end)
	(put-char+ flags pstate #\)))

      (define (next)
	(put-char+ flags pstate #\space)
	(lp (+ i 1)))
      
      (if (< i last)
	  (let ((item (vector-ref v i)))	      
	    (print item flags pstate)
	    (set! n (+ n 1))
	    (pstate++ pstate 1)
	    (if (pstate-cut? pstate n)
		(begin
		  (put-string+ flags pstate " ...")
		  (end))
		(next)))
	
	  (begin	    
	    (print (vector-ref v last) flags pstate)
	    (end))))))

(define (print-vector2 x v flags pstate)
  (define N     100)
  (define len  (vector-length v))
  (define last (- len 1))
  (define ch (if (flag? flags comma-flag) #\, #\space))
  
  (put-string+ flags pstate "#(")

  (let lp ((n 0))
    (when (< n len)
      (with-buffdata-0 (pstate : buffer buffpt pts)
        (define (sync)
	  (let ((pt (car pts)))
	    (pstate-buffpt-set! pstate
	       (if (>= pt nbuff)
		   (begin
		     (put-bytevector (get-port) buffer 0 pt)
		     0)
		   pt))))

	(define (next n)
	  (when (< n last)
	    (put-char* flags pstate ch))
	  (lp n))

	(set-car! pts buffpt)

	(let ((it (c-print-vector v n (pstate-buffer pstate) pts flags)))
	  (if (<= it 0)
	      (let ((n (- n it)))
		(sync)
		(print (vector-ref v n) flags pstate)
		(next (+ n 1)))
	      (begin
		(sync)
		(lp (+ n it))))))))

  (put-string+ flags pstate ")"))

(define (print-struct-default s flags pstate)
  (define (applicable? vtable)
    (> (logand (ash 1 3) (struct-ref vtable 1))))

  (define (setter? vtable)
    (> (logand (ash 1 5) (struct-ref vtable 1))))
  
  (let* ((n     0)
	 (vtable (struct-vtable s))
	 (name   (struct-ref vtable 4)))

    (put-string+ flags pstate "#<")

    (if name
	(print name flags (pstate-copy pstate #:write? #f))
          
	(let ((layout (struct-ref vtable 0)))
	  (if (struct-vtable? s)
	      (put-string+ flags pstate "vtable:")
	      (put-string+ flags pstate "struct:"))

	  (print-number* (stis-address vtable) 16 flags pstate)
	  (put-string++ flags pstate layout)))
    
    (put-char* flags pstate #\space)
    
    (when (applicable? vtable)
      (let ((proc (struct-ref s 0)))
	(when proc
	  (put-string+ flags pstate " proc: ")
	  (if (procedure? proc)
	      (proc flags s)
	      (put-string+ flags pstate "(not a procedure)")))

	(when (setter? vtable)
	  (put-string+ flags pstate " setter: ")
	  (let ((setter (struct-ref s 2)))
	    (print exp flags (pstate-copy pstate #:write? #t))))))

    (put-char+ flags pstate #\>)))

;; TODO, fix this pstate thingie
(define (print-struct-adv s flags pstate)
  (let ((vtable (struct-vtable s))
	(pr     (struct-ref    s 3)))
    (if (procedure? pr)
	(pr s (get-port) pstate)
	(print-struct-default s flags pstate))))

(define (print-struct x exp flags pstate)
  (if (is-a? exp <object>)
      (let ((print (if (write? flags)
		       (@@ (oop goops) write)
		       (@@ (oop goops) display))))
	(print exp (get-port)))
      (print-struct-adv exp flags pstate)))

(define (print-port-default p flags pstate)
  (define type (let ((addr (lookup-2 p 3 0)))
		 (if (= 0 addr)
		     "flags"
		     (pointer->string (make-pointer addr)))))
  
  
  (put-string+ flags pstate "#<")
  

  (if (port-closed? p)
        (put-string+ flags pstate "closed ")
	(if (> (lookup-1 p 0))
	    (if (input-port? p)
		(if (output-port? p)
		    (put-string+ flags pstate "input-output ")
		    (put-string+ flags pstate "input "))
		(if (output-port? p)
		      (put-string+ flags pstate "output ")
		      (put-string+ flags pstate "bogus ")))
	    (put-string+ flags pstate "bogus ")))
  
  (put-string++ flags pstate type)
  (put-char+   flags pstate #\space)
  (print-number* (stis-address p) 16 flags pstate)
  (put-char+   flags pstate #\>)
  
  #t)
  
(define (print-port x p flags pstate)
  (define port (get-port))
  
  (let ((pr (lookup-2 p 3 1)))
    (if (= pr 0)
	(print-port-default p flags pstate)
	(let ()
	  (define (m x) (make-pointer (stis-address x)))

	  (define fkn
	    (pointer->procedure int (make-pointer pr) (list '* '* '*)))

	  (let* ((sport (get-port))
		 (port (if (soft-port? sport)
			   (slot-ref sport 'port)
			   sport)))
	    
	    (buffdata-flush flags pstate)
	    
	    (catch #t
	      (lambda ()
		(fkn (m p) (m sport) (m pstate)))
	      (lambda (x)
		(if (soft-port? sport)
		    (catch #t
		      (lambda ()
			(let* ((pstate ((@@ (ice-9 ports) get-print-state)
					port)))
			  (fkn (m p) (m port) (m pstate))))
		      (lambda (y)
			(apply throw y)))
		    (apply throw x)))))))))

(define-syntax-rule (mk-looper fu8 print-integer print-integer-fast M)
  (define (fu8 bv flags pstate)
    (if (fancy? flags)
	(let* ((n (bytevector-length bv))
	       (N (- n M)))

	  (let lp ((i 0))
	    (define (cont)
	      (when (not (= i N))
		(put-char+ flags pstate #\space))
	      (lp (+ i M)))
	  
	    (define (end)
	      (values))
    
	    (if (< i n)
		(begin
		  
		  (let ((di (/
			     (+ 1
				(log10
				 (print-integer flags pstate bv i)))
			     30.0)))
		    
		    (pstate++ pstate di)
		    
		    (if (pstate-cut? pstate 0)
			(begin
			  (put-string+ flags pstate " ...")
			  (end))
			(cont))))
		(end))))

	(print-integer-fast flags pstate bv))))


(mk-looper fu8  print-u8  put-bv-u8* 1)
(mk-looper fs8  print-s8  put-bv-s8* 1)

(mk-looper fu16 print-u16 put-bv-u16* 2)
(mk-looper fs16 print-s16 put-bv-s16* 2)

(mk-looper fu32 print-u32 put-bv-u32* 4)
(mk-looper fs32 print-s32 put-bv-s32* 4)

(mk-looper fu64 print-u64 put-bv-u64* 8)
(mk-looper fs64 print-s64 put-bv-s64* 8)

(mk-looper ff32 print-f32 put-bv-f32* 4)
(mk-looper ff64 print-f64 put-bv-f64* 8)

(define (print-u8 flags pstate bv i)
  (let ((x (bytevector-u8-ref bv i)))
    (print-fixnum* x flags pstate)))
   
(define (print-s8 flags pstate bv i)
  (let ((x (bytevector-s8-ref bv i)))
    (print-fixnum* x flags pstate)))
   

(define (print-u16 flags pstate bv i)
  (let ((x (bytevector-u16-ref bv i (native-endianness))))
    (print-fixnum* x flags pstate)))

(define (print-s16 flags pstate bv i)
  (let ((x (bytevector-s16-ref bv i (native-endianness))))
    (print-fixnum* x flags pstate)))

(define (print-u32 flags pstate bv i)
  (print-fixnum* (bytevector-u32-ref bv i (native-endianness))
		 flags pstate))

(define (print-s32 flags pstate bv i)
  (print-fixnum* (bytevector-s32-ref bv i (native-endianness))
		 flags pstate))

(define (print-u64 flags pstate bv i)
  (print-integer* (bytevector-u64-ref bv i (native-endianness))
		  flags pstate))

(define (print-s64 flags pstate bv i)
  (print-integer* (bytevector-s64-ref bv i (native-endianness))
		  flags pstate))

(define (print-f32 flags pstate bv i)
  (print-inexact* (bytevector-ieee-single-ref bv i (native-endianness))
		  flags pstate))

(define (print-f64 flags pstate bv i)
  (print-inexact* (bytevector-ieee-double-ref bv i (native-endianness))
		  flags pstate))

(define ftable (make-hash-table))


(define-syntax q
  (lambda (x)
    (define (conc str x)
      (datum->syntax
       x
       (string->symbol
	(string-append
	 str (symbol->string
	      (syntax->datum x))))))
    
    (syntax-case x ()
      ((_ u8 ...)
       (with-syntax
	   (((f8 ...) (map (lambda (x) (conc "f" x)) #'(u8 ...)))
	    ((v8 ...) (map (lambda (x) (conc "v" x)) #'(u8 ...))))
	 #'(list (list 'u8 f8 ) ... (list 'v8 f8) ...))))))

(for-each
 (lambda (x)
   (hashq-set! ftable (car x) (cadr x)))
 (q u8 s8 u16 s16 u32 s32 u64 s64 f32 f64))
 
(define (print-bytevector x bv flags pstate)
  (define type (array-type bv))
  
  (put-char+    flags pstate #\#)
  (put-string++ flags pstate (symbol->string type))
  (put-char+    flags pstate #\()

  (aif f-it (hashq-ref ftable type)
       (begin
	 (f-it bv flags pstate)
	 (put-char+ flags pstate #\)))
       (error "bytevector format not supflagsed")))


(define (print-smob x s flags pstate)
  (define (m x) (make-pointer (stis-address x)))
  (define port (get-port))

  (buffdata-flush flags pstate)
  
  (catch #t
    (lambda ()      
      (smob-c-print (m s) (m port) (m pstate)))
    (lambda x
      (if (soft-port? port)
	  (catch #t
	    (lambda ()
	      (smob-c-print (m s) (m (slot-ref port 'port)) (m pstate)))
	    (lambda y
	      (throw x)))
	  (throw x)))))


(define (print-array x exp flags pstate)
  ;; TODO FIX THE PREFIX
  ;;((@@ (ice-9 arrays) array-print-prefix) exp flags)
  (let ((ndims (array-rank exp)))
    (let lp ((dim ndims) (l '()) (s (array-dimensions exp)))
      (define (mk fkn)
	(let/ec return
	  (let ((n (car s)))
	    (define (end)
	      (put-char+ flags pstate #\))
	      (return))
		
	    (define (fancy i)
	      (when (fancy? flags)
		(when (pstate-cut? pstate i)
		  (put-string+ flags pstate "...")
		  (end))))
	    
	    (put-char+ flags pstate #\()
	    
	    (when (= n 0)
	      (end))
	    
	    (fkn   0)				
	    (fancy 0)
	    
	    (let iter ((i 1))
	      (if (< i n)
		  (begin
		    (put-char+ flags pstate #\space)
		    (fkn   i)
		    (fancy i)
		    (iter  (+ i 1)))
		  (end))))))
	

      (cond
       ((= dim 0)
	(put-char+ flags pstate #\()
	(print (array-ref exp) flags pstate)
	(put-char+ flags pstate #\)))
       
       ((= dim 1)
	(mk (lambda (i)
	      (print (apply array-ref exp
			    (reverse (cons i l))) flags pstate))))

       (else
	(mk (lambda (i) (lp (- dim 1) (cons i l) (cdr s)))))))))

(define (print-bitvector x exp flags pstate)
  (put-string+ flags pstate "#*")
  (let ((n (bitvector-length exp)))
    (let/ec end
      (let lp ((i 0))
	(if (< i n)
	    (let ((x (bitvector-ref exp i)))
	      (if x
		  (put-char+ flags pstate #\0)
		  (put-char+ flags pstate #\1))

	      (when (fancy? flags)
		(pstate++ pstate (/ 1 30.0))
		(when (pstate-cut? pstate 0)
		  (put-string+ flags pstate "...")
		  (end)))
	      
	      (lp (+ i 1))))))))

(define (print-program x exp flags pstate)
  (define (is-continuation?)
    (> (logand (lookup-1 exp 0) #x800) 0))
  
  (define (is-partial-continuation?)
    (> (logand (lookup-1 exp 0) #x1000) 0))
  
  (define print (if (not (flag? flags write-program-flag))
		    (@@ (system vm program) write-program)
		    write-program))
  (cond
   ((is-continuation?)
    (put-string+ flags pstate "#<continuation ")
    (print-number* (stis-address exp) 16 flags pstate)
    (put-char+ flags pstate #\>))

   ((is-partial-continuation?)
    (put-string+ flags pstate "#<partial-continuation ")
    (print-number* (stis-address exp) 16 flags pstate)
    (put-char+ flags pstate #\>))

   ((not print)
    (put-string+ flags pstate "#<program ")
    (print-number* (stis-address exp) 16 flags pstate)
    (put-char+ flags pstate #\space)
    (print-number* (stis-address (program-code exp)) 16 flags pstate)
    (put-char+ flags pstate #\>))

   (else
    (print exp (get-port)))))

(define (print-number x exp flags pstate)
  (print-number-10* exp flags pstate))

(define (print-integer exp flags pstate)
  (print-fixnum* exp flags pstate))
 
(define (print-pair x exp flags pstate)
  (print-list "(" exp ")" flags pstate))

(define (print-char exp flags pstate)
  (if (write? flags)
      (print-character exp flags pstate)
      (put-char* flags pstate exp)))

(define (print-flag x scm flags pstate)
  (put-string++ flags pstate (vector-ref flagnames x)))

(define (print-keyword x exp flags pstate)
  (put-string+  flags pstate "#:")
  (print-symbol x (keyword->symbol exp) flags pstate))

(define (print-bogus str exp flags pstate)
  (put-char+    flags pstate #\<)
  (put-string++ flags pstate str)
  (put-string+  flags pstate " 0x")
  (print-number* (stis-address exp) 16 flags pstate)
  (put-char+    flags pstate #\>))

(define-syntax-rule (ma x exp flags pstate fkn)
  (begin
    (pstate-obj++ pstate)
    (fkn x exp flags pstate)))

(define-syntax-rule (ma1 exp flags pstate fkn)
  (begin
    (pstate-obj++ pstate)
    (fkn exp flags pstate)))


(define-syntax-rule (mk x exp flags pstate fkn)
  (begin
    (if (pstate-before? pstate exp)
	(print-circref exp flags pstate)
	(begin
	  (pstate-register pstate exp)
	  (pstate-obj++ pstate)
	  (pstate-cleanup flags pstate exp)
	  (fkn x exp flags pstate)))))


(define-type-dispatcher (iprin1 exp flags pstate) x
  (#:char
   (ma1  exp flags pstate print-char))
  (#:pair
   (mk x exp flags pstate print-pair))
  (#:struct
   (mk x exp flags pstate print-struct))
  (#:flag
   (ma x exp flags pstate print-flag))
  (#:bogus 
   (ma x exp flags pstate print-bogus))
  (#:integer
   (ma1  exp flags pstate print-integer))

  ;; tc7
  (#:symbol
   (ma x exp flags pstate print-symbol))

  (#:variable
   (mk x exp flags pstate print-variable))
  (#:vector
   (mk x exp flags pstate print-vector))
  #;(#:wvect
   (mk x exp flags pstate print-wvect))
  (#:string
   (ma x exp flags pstate print-string))
  (#:number
   (ma x exp flags pstate print-number))
  (#:hashtable
   (ma x exp flags pstate print-hashtable))
  (#:pointer
   (ma x exp flags pstate print-pointer))
  (#:fluid
   (ma x exp flags pstate print-fluid))
  #;(#:stringbuf
  (mk tc7-stringbuf print-stringbuf))
  #;(#:dynamic-state
  (ma x exp flags hash print-dynamic-state))
  #;(#:frame
  (ma x exp flags hash print-frame))
  (#:keyword
   (ma x exp flags pstate print-keyword))
  (#:atomic-box
   (mk x exp flags pstate print-atomic-box))
  #;(#:syntax
   (ma x exp flags hash print-syntax))
  #;(#:values
   (mk x exp flags hash print-values))
  (#:program
   (mk x exp flags pstate print-program))
  #;(#:vm-cont
  (ma x exp flags hash print-vm-cont))
  (#:bytevector
   (ma x exp flags pstate print-bytevector))
  #;(#:unused-4f     #x4f)
  #;(#:weak-set
  (ma x exp flags hash print-weak-set))
  #;(#:weak-table
  (ma x exp flags hash print-weak-table))
  (#:array
   (mk x exp flags pstate print-array))
  (#:bitvector
   (ma x exp flags pstate print-bitvector))
  #;(#:unused-65     #x65)
  #;(#:unused-67     #x67)
  #;(#:unused-6d     #x6d)
  #;(#:unused-6f     #x6f)
  #;(#:unused-75     #x75)
  (#:smob
   (mk x exp flags pstate print-smob))
  (#:port
   (mk x exp flags pstate print-port))
  (#:stis
   (ma x exp flags pstate print-stis)))

;; Single threaded
(define write-buffers (make-thread-local-fluid '()))
(define write-hashes  (make-thread-local-fluid '()))

(define-syntax-rule (with-pstate-data (buffdata hash) code ...)
  (let* ((buffers  (fluid-ref write-buffers))
	 
	 (buffdata (make-buffdata
		    #:buffer
		    (if (pair? buffers)
			(car buffers)
			(let ((buff (make-write-buffer)))
			  (fluid-set! write-buffers (cons buff buffers))
			  buff))))
	 
	 (hashes   (fluid-ref write-hashes))

	 (hash     (if (pair? hashes)
		       (car hashes)
		       (let ((hashtab (make-hash-table)))
			 (fluid-set! write-hashes (cons hashtab hashes))
			 hashtab))))
    (with-fluids ((write-buffers (cdr (fluid-ref write-buffers)))
		  (write-hashes  (cdr (fluid-ref write-hashes))))
	code ...)))

(define port->pstate-maps (make-thread-local-fluid #f))

(define (get-port-pstate port)
  (aif it (fluid-ref port->pstate-maps)
       (aif it2 (hash-ref it port)
	    (fluid-ref it2)
	    (begin
	      (hash-set! it port (make-fluid #f))
	      #f))
       (let ((h (make-weak-key-hash-table)))
	 (hash-set! h port (make-fluid #f))
	 (fluid-set! port->pstate-maps h)
	 #f)))

(define-syntax-rule (with-pstate (port : pstate) code ...)
  (with-fluids (((hash-ref (fluid-ref port->pstate-maps) port) pstate))
    code ...))

(define (get-encoding port flags)
  (define p (if (soft-port? port) (slot-ref port 'port) port))
  (define detailed (lookup-2-scm p 2 9))
  (define conv     (lookup-2-scm p 2 8))
  
  (let* ((enc (if detailed
		  detailed
		  (string->symbol (port-encoding p)))))
    (logior
     (logand (lognot encoding-mask) flags)
     
     (case conv
       ((substitute) convert-replace)
       ((escape    ) convert-escape)
       (else
	convert-replace))
     
     (case enc
       ((UTF-8)      utf-flag)
       ((ISO-8859-1) 0)
       ((UTF-16)     (logior (ash 1 sh-shift)
			     (ash 1 d-shift)))
       ((UTF-32)     (logior (ash 2 sh-shift)
			     (ash 3 d-shift)))
       ((UTF-16LE)   (ash 1 sh-shift))
       ((UTF-32LE)   (ash 2 sh-shift))
       ((UTF-16BE)   (logior (ash 1 sh-shift)
			     (ash 1 d-shift)))
       ((UTF-32BE)   (logior (ash 2 sh-shift)
			     (ash 3 d-shift)))
       (else
	(error "Unsupported flags encoding"))))))

(define-syntax-rule (mk write pp)
  (define* (write exp
		  #:optional (port (current-output-port))
		  #:key
		  (inherit? #t)
		  (binary    0))
    
    (define-syntax-rule (mk pstate flush? line)
      (let* ((pstate line))
	(with-pstate (port : pstate)
	   (dynamic-wind
	      (lambda () (values))
	      (lambda ()
		(with-fluids ((current-write-port port))		       
	          (print exp (get-encoding port (pstate-flags pstate))
			 pstate)))
	      (lambda ()
		(hash-clear! (pstate-hash pstate))
		(when flush?
		  (buffdata-flush port pstate)
		  (values))
		(clear-bv (pstate-buffer pstate)))))))

      
    (aif pstate (get-port-pstate port)    
	 (cond
	  ((eq? inherit? #f)
	   (with-pstate-data (buffdata hash)
	     (mk pstate #t
		 (make-pstate 
		  buffdata hash
		  #:flags (make-flags #:write? pp #:inherited? #f
				      #:binary binary)))))
	  
	  ((flag? (pstate-flags pstate) inherited-flag)
	   (mk pstate #f
	       (pstate-copy pstate #:write? pp #:binary binary)))
	  
	  ((eq? inherit? #t)
	   (mk pstate #t (pstate-copy pstate
				      #:write? pp #:inherited? #t
				      #:binary binary)))
	  
	  (else
	   (with-pstate-data (buffdata hash)
	     (mk pstate #t
		 (pstate-copy pstate
			      #:write?   pp
			      #:hash     hash
			      #:binary   binary
			      #:buffdata buffdata)))))

	 
	 (with-pstate-data (buffdata hash)
	   (mk pstate #t
	       (make-pstate buffdata hash
			    #:flags
			    (make-flags #:write?     pp
					#:binary     binary
					#:inherited? inherit?)))))))
    

(mk write   #t)
(mk display #f)

(define (get-print-state flags)
  (aif it (hashq-ref table flags)
       (fluid-ref it)
       #f))

(define (flags-with-print-state flags pstate)
  (define fl (aif it (hashq-ref table flags)
		  it
		  (let ((fl (make-fluid #f)))
		    (hashq-set! table flags fl)
		    fl)))
  (fluid-set! fl pstate)
  
  flags)


(define flags-bindings
  '(((guile) write display)))

(define write-installed? #f)
(define saved-flags-bindings #f)

(define (install-write!)
  (set! write-installed? #t)
  (unless saved-flags-bindings
    (set! saved-flags-bindings (make-hash-table))
    (let ((suspendable-flagss (resolve-module '(ice-9 write))))
      (for-each
       (match-lambda
	 ((mod . syms)
          (let ((mod (resolve-module mod)))
            (for-each (lambda (sym)
                        (hashq-set! saved-flags-bindings sym
                                    (module-ref mod sym))
                        (module-set! mod sym
                                     (module-ref suspendable-flagss sym)))
                      syms))))
       flags-bindings))))

(define (uninstall-write!)
  (set! write-installed? #f)
  (when saved-flags-bindings
    (for-each
     (match-lambda
       ((mod . syms)
        (let ((mod (resolve-module mod)))
          (for-each (lambda (sym)
                      (let ((saved (hashq-ref saved-flags-bindings sym)))
                        (module-set! mod sym saved)))
                    syms))))
     flags-bindings)
    (set! saved-flags-bindings #f)))
