(define-module (ice-9 stis-hash)
  #:use-module (ice-9 pretty-print)
  #:use-module (oop goops)
  #:use-module ((persist persistance)
		#:select
		(lookup-1
		 stis-cache-b-search
		 stis-cache-b-add!))
  #:use-module (rnrs arithmetic fixnums)
  #:declarative? #f
  #:export (make-stis-hash-table
            make-stis-hash-type
            with-stis-hash

            stis-hash-for-each
            stis-hash-fold
            stis-hash-map

            stis-hash-ref
            stis-hash-set!
            stis-hash-remove!
            stis-hash-clear!
	    
            stis-c-ref
            stis-c-set!
            stis-c-remove!

	    make-stis-cache
	    stis-cache-ref
	    stis-cache-add!

	    transfer-hh
	    expand-a
	    empty-hash-slot?
	    <HH>))

(eval-when (eval load compile)
  (define do-stis #t)
  (if (and do-stis
	   (module-defined? (resolve-module '(ice-9 stis stis)) 'stis-hash))
      (use-modules 
       ((ice-9 stis stis    ) #:select (stis-b-search
					stis-hash
					stis-hashq
					stis-address
					stis-hashv))
	 
       ((system vm  vm      ) #:select (stis-bv-ref
					stis-bv-set!
					
					stis-b-remove!
					stis-b-make
					stis-b-add!
					stis-b-alist-ref
					stis-b-alist-set!)))
      (use-modules 
       ((persist persistance) #:select (stis-bv-ref
			 		stis-bv-set!
					  
					stis-b-make
					stis-b-search
					stis-b-remove!
					stis-b-add!
					stis-b-alist-ref
					stis-b-alist-set!))
	 
       ((guile              ) #:select (hash hashq hashv)))))

	
(eval-when (eval load compile)
  (if (and do-stis
	   (module-defined? (resolve-module '(ice-9 stis stis)) 'stis-hash))
      (values)
      (begin
	(define! 'stis-address  object-address)
	(define! 'stis-hash     (lambda (x)   (hash  x (ash 1 60))))
	(define! 'stis-hashv    (lambda (x)   (hashv x (ash 1 60))))
	(define! 'stis-hashq    (lambda (x)   (hashq x (ash 1 60)))))))


(define-syntax-rule (aif it p . x) (let ((it p)) (if it . x)))

(define* (-make-stis-hash-table #:optional (n 32) (m 2))
  (set! m (min 4 (max m 2)))
  (let ((h (stis-b-make
	    (ash
	     (let lp ((s 1))
	       (if (>= s n)
		   s
		   (lp (ash s 1)))) (- m))
	    m)))
    h))

(define* (make-stis-cache #:optional (n 1024) (m 4))
  (-make-stis-hash-table n m))

  
(define (stis-d-remove! v k h eq)
  (define ret #f)
  
  (define (lp l)
    (if (pair? l)
	(let ((it (car l)))
	  (if (eq k (car it))
              (begin
                (set! ret it)
                (cdr l))
              
	      (let* ((l1 (cdr l))
		     (l2 (lp l1)))
		(it (eq? l1 l2)
		    l
		    (cons it l2)))))
	'()))

  (let* ((l1 (stis-b-alist-ref v h))
	 (l2 (lp l1)))
    (when (not (eq? l1 l2))        
      (stis-b-alist-set! v h l2)))
  ret)

(define-inlinable (stis-c-remove! v k h eq)
  (define (slow-rem) (stis-d-remove! v k h eq))
  (aif it (stis-b-search v h)
       (if (pair? it)
	   (if (eq (car it) k)
	       it
	       (slow-rem))
	   (slow-rem))
       (values)))


(define-inlinable (stis-hashx-remove! hash eq v k)
  (stis-c-remove! v k (hash k) eq))

(define *removers* (make-hash-table))

(define (stis-d-ref v k default h eq)
  (let ((l (stis-b-alist-ref v h)))
    (let lp ((ll l))
      (if (pair? ll)
	  (let ((it (car ll)))
	    (if (eq k (car it))
		(cdr it)
		(lp (cdr ll))))
	  default))))
  

;; stis-b-search returns wither a candidate pair, #t (has cons list) #f no match
(define-inlinable (stis-c-ref v k default h eq)  
  (define (slow-ref) (stis-d-ref v k default h eq))
  (aif it (stis-b-search v h)
       (if (pair? it)
	   (if (eq (car it) k)
	       (cdr it)
	       (slow-ref))
	   (slow-ref))
       default))

(define-inlinable (stis-cache-ref- cache k default h eq)
  (aif it (stis-cache-b-search cache h)
       (if (pair? it)
	   (if (eq (car it) k)
	       (cdr it)
	       default)
	   default)
       default))

(define-inlinable (stis-cache-add- cache k handle h eq)
  (let ()
    (define (add)
      (stis-cache-b-add! cache h handle))
	       
    (aif it (stis-cache-b-search cache h)
	 (if (pair? it)
	     (if (eq (car it) k)
		 (values)
		 (add))
	     (add))
	 (add))))

(define-inlinable (ha x)
  (let ((xx (stis-address x)))
    (cond
     ((eq? (logand xx 3) 2)
      (ash xx -2))

     ((symbol? x)
      (ash xx -3))
    
     (else
      (stis-hashv x)))))

(define (stis-cache-add! cache x v)
  (let ((h (ha x)))
    (stis-cache-add- (stis-bv-ref cache) x (cons x v) h eqv?)))


(define (stis-cache-ref cache x default)
  (let ((h (ha x)))
    (stis-cache-ref- (stis-bv-ref cache) x default h eqv?)))


(define-inlinable (stis-hashx-ref hash eq v k default)
  (stis-c-ref v k default (hash k) eq))

(define *reffers* (make-hash-table))


(define (stis-d-set! v k val h eq)
  (let ((l (stis-b-alist-ref v h)))
    (let lp ((ll l))
      (if (pair? ll)
	  (let ((it (car ll)))
	    (if (eq k (car it))
		(begin
		  (set-cdr! it val)
		  #f)
		(lp (cdr ll))))
	  (let* ((it (cons k val))
		 (l  (cons it l)))
	    (stis-b-alist-set! v h l)
	    it)))))

(define-inlinable (stis-c-set! v k val h eq)
  (define (slow-set!) (stis-d-set! v k val h eq))
  (aif it (stis-b-search v h)
       (if (pair? it)
	   (if (eq (car it) k)
	       (begin
		 (set-cdr! it val)
		 #f)
	       (slow-set!))
	   (slow-set!))
       
       (let ((it (cons k val)))
	 (stis-b-add! v h it)
	 it)))

(define-inlinable (stis-hashx-set! hash eq v k val)
  (stis-c-set! v k val (hash k) eq))

(define *setters* (make-hash-table))

(define (stis-d-het! v k val h eq)
  (let ((l (stis-b-alist-ref v h)))
    (stis-b-alist-set! v h (cons val l))
    (values)))

(define-inlinable (stis-c-het! v k val h eq)
  (define (slow-het!) (stis-d-het! v k val h eq))
  (aif it (stis-b-search v h)
       (if (pair? it)
           (slow-het!)
           (stis-b-add! v h val))
       (stis-b-add! v h val)))

(define-inlinable (stis-hashx-het! hash eq v k val)
  (stis-c-het! v k val (hash k) eq))

(define *hetters* (make-hash-table))


(define-syntax-rule (make-stis-hash-type kind ehash eq)
  (begin
    (define (ref h k d) (stis-hashx-ref ehash eq h k d))
    (define mref
      (lambda (x)
	(syntax-case x ()
	  ((_ h k d) #'(stis-hashx-ref ehash eq h k d)))))
    (define (set h k v) (stis-hashx-set! ehash eq h k v))
    (define mset
      (lambda (x)
	(syntax-case x ()
	  ((_ h k d) #'(stis-hashx-set! ehash eq h k d)))))
    
    (define (het h k v) (stis-hashx-het! ehash eq h k v))
    (define mhet
      (lambda (x)
	(syntax-case x ()
	  ((_ h k v) #'(stis-hashx-het! ehash eq h k v)))))
    
    (define (rem h k  ) (stis-hashx-remove! ehash eq h k))
    (define mrem
      (lambda (x)
	(syntax-case x ()
	  ((_ h k) #'(stis-hashx-remove! ehash eq h k)))))
    
    (hash-set! *reffers*  kind                   ref )
    (hash-set! *reffers*  (symbol->keyword kind) mref)
    
    (hash-set! *setters*  kind                   set )
    (hash-set! *setters*  (symbol->keyword kind) mset)
    
    (hash-set! *hetters*  kind                   het )
    (hash-set! *hetters*  (symbol->keyword kind) mhet)
    
    (hash-set! *removers* kind                   rem )
    (hash-set! *removers* (symbol->keyword kind) mrem)
    
    (values)))

(make-stis-hash-type 'fastq (lambda (x) x) eq?    )

(make-stis-hash-type 'fastv
		     (lambda (x)
		       (if (or (fixnum? x) (symbol? x))
			   x
			   (stis-hashv x)))
		     eqv?)

(make-stis-hash-type 'fast
		     (lambda (x)
		       (if (or (fixnum? x) (symbol? x))
			   x
			   (stis-hash x)))
		     equal?)

(make-stis-hash-type 'hashq stis-hashq     eq?    )
(make-stis-hash-type 'hashv stis-hashv     eqv?   )
(make-stis-hash-type 'hash  stis-hash      equal? )

(define-class <HH> () offset kind m nm km a na ka da dm name)

(define (transfer-hh from to)
  (define (set k) (slot-set! to k (slot-ref from k)))
  (for-each set '(kind m nm km a na ka da dm name)))
  

(define i-kind 1)
(define i-m    2)
(define i-nm   3)
(define i-km   4)
(define i-a    5)
(define i-na   6)
(define i-ka   7)
(define i-da   8)
(define i-dm   9)

(define* (make-stis-hash-table kind #:optional (n 32) (m 2) #:key (name #f))
  (let ((map (-make-stis-hash-table n m))
        (ar  (make-vector n))
        (o   (make <HH>)))

    (slot-set!   o 'offset 0)
    (struct-set! o i-kind kind)
    (struct-set! o i-m    map)
    (struct-set! o i-nm   n)
    (struct-set! o i-km   0)
    (struct-set! o i-a    ar)
    (struct-set! o i-na   n)
    (struct-set! o i-ka   0)
    (struct-set! o i-da   0)
    (struct-set! o i-dm   m)

    (slot-set! o 'name name)

    o))
    

(define-method (write (o <HH>) port)
  (aif it (slot-ref o 'name)
       (format port
	       "<stis-hash:~a (~a) n=~a/~a>"
	       it
	       (slot-ref o 'kind)
	       (slot-ref o 'km)
	       (slot-ref o 'nm))
       (format port
	       "<stis-hash (~a) n=~a/~a>"     
	       (slot-ref o 'kind)
	       (slot-ref o 'km)
	       (slot-ref o 'nm))))

(define-method (display (o <HH>) port) (write o port))

(eval-when (load eval compile)
           (define (pp x) (pretty-print (syntax->datum x)) x))
(define-syntax expand
  (lambda (x)
    (syntax-case x ()
      ((_ h (k m mm) code ...)
       #'(let* ((o  (slot-ref h 'offset))
		(k  (struct-ref h (+ o i-kind)))
		(m  (struct-ref h (+ o i-m)))
		(mm (stis-bv-ref m)))
	   code ...))
      
      ((_ h (t k m mm nm km a na ka da) code ...)
       #'(expand1 h (t m mm nm km a na ka da)
	   (let* ((o (slot-ref h 'offset))
		  (k (struct-ref h (+ o i-kind))))
	     (dynamic-wind
		 (lambda ()
		   (set! t  0)
		   (set! m  (struct-ref h (+ o i-m)))
		   (set! mm (stis-bv-ref m))
		   (set! nm (struct-ref h (+ o i-nm)))
		   (set! km (struct-ref h (+ o i-km)))
		   (set! a  (struct-ref h (+ o i-a)))
		   (set! na (struct-ref h (+ o i-na)))
		   (set! ka (struct-ref h (+ o i-ka)))
		   (set! da (struct-ref h (+ o i-da))))
		 (lambda () code ...)
		 (lambda ()
		   (when (> t 0)
		     (struct-set! h (+ o i-km) km)
		     (struct-set! h (+ o i-ka) ka)
		     (when (> t 1)
		       (struct-set! h (+ o i-nm) nm)
		       (struct-set! h (+ o i-m)  m)
		       (struct-set! h (+ o i-a)  a)
		       (struct-set! h (+ o i-na) na)
		       (struct-set! h (+ o i-da) da)))))))))))


(define-syntax expand1
  (lambda (x)
    (syntax-case x ()
      ((_ h () code ...)
       #'(let () code ...))

      ((_ h (a . aa) . code)
       #'(let ((a #f))
	   (expand1 h aa . code))))))

(define-syntax define-ref
  (lambda (x)
    (syntax-case x ()
      ((f u . a)
       (eq? (syntax->datum #'u) '_)
       #'(values))
      
      ((f (mac ref) kind m)
       (with-syntax ((fref (datum->syntax #'ref (gensym "fref"))))
         #'(begin
             (define-syntax fref (hash-ref *reffers* mac))
             (define-syntax ref
	       (lambda (x)
		 (syntax-case x ()
		   ((_ k  ) #'(fref m k #f))
		   ((_ k d) #'(fref m k  d))
		   (_
		    #'(lambda* (k #:optional (d #f))
			       (fref m k d))))))
	     (when (not (eq? (symbol->keyword kind) mac))
	       (error
		(format
		 #f "macro accessor with wrong ref kind ~a != ~a" kind mac))))))


  
      ((f ref kind m)
       (with-syntax ((fref (datum->syntax #'ref (gensym "fref"))))
         #'(begin
             (define fref (hash-ref *reffers* kind))
             (define-syntax ref
	       (lambda (x)
		 (syntax-case x ()
		   ((_ k  ) #'(fref m k #f))
		   ((_ k d) #'(fref m k  d))
		   (_
		    #'(lambda* (k #:optional (d #f))
		        (fref m k d))))))))))))

(define-inlinable (empty-hash-slot? x) (eq? (cdr x) none))

(define (transfer-m h kind nm a ka)
  (define N   (* nm 2))
  (define m2  (-make-stis-hash-table N (slot-ref h 'dm)))
  (define mm2 (stis-bv-ref m2))
  (let ((hset (hash-ref *hetters* kind)))
    (let lp ((i 0))
      (if (< i ka)
          (let ((it (vector-ref a i)))
            (if (eq? (cdr it) none)
                (lp (+ i 1))
                (begin
                  (hset mm2 (car it) it)
                  (lp (+ i 1)))))
          (values m2 N))))) 

(define (transfer-a a na ka)
  (let* ((N  (* na 2))
         (a2 (make-vector N)))
    (let lp ((i 0) (j 0))
      (if (< i ka)
          (let ((it (vector-ref a i)))
            (if (eq? (cdr it) none)
                (lp (+ i 1) j)
                (begin
                  (vector-set! a2 j (vector-ref a i))
                  (lp (+ i 1) (+ j 1)))))
          (values a2 N)))))
            
(define-syntax-rule (add-a h t kind m mm nm km a na ka da it)
  (begin
    (set! t (logior t 1))
    (set! km (+ km 1))
    (vector-set! a ka it)
    (set! ka (+ ka 1))  
    
    (when (> (* 2 km) nm)
      (set! t 2)
      (call-with-values (lambda ()  (transfer-m h kind nm a ka))
        (lambda (m2 nm2)
          (set! m  m2 )
	  (set! mm (stis-bv-ref m))
          (set! nm nm2))))

    (when (= ka na)
      (set! t 2)
      (call-with-values (lambda ()  (transfer-a a na ka))
        (lambda (a2 na2)
          (set! a  a2 )
          (set! ka (- ka da))
          (set! da 0)
          (set! na na2))))))

(define-syntax define-set
  (lambda (x)
    (syntax-case x ()
      ((f u . a)
       (eq? (syntax->datum #'u) '_)
       #'(values))
      
      ((f (mac set) h t kind m mm nm km a na ka da)
       (with-syntax ((fset (datum->syntax #'ref (gensym "fset"))))
	 #'(begin			
	     (define-syntax fset (hash-ref *setters* mac))
             (define (set k v)
               (aif it (fset mm k v)
                    (add-a t kind m mm nm km a na ka da it)
                    #f)
               (values))
	     (when (not (eq? (symbol->keyword kind) mac))
	       (error
		(format
		 #f "macro accessor with wrong set kind ~a != ~a" kind mac))))))

      ((f set h t kind m mm nm km a na ka da)
       (with-syntax ((fset (datum->syntax #'ref (gensym "fset"))))
         #'(begin
             (define fset (hash-ref *setters* kind))
             (define (set k v)
               (aif it (fset mm k v)
                    (add-a h t kind m mm nm km a na ka da it)
                    #f)
               (values))))))))
  
(define (clear-a a na ka)
  (let lp ((i 0) (j 0))
    (if (< i ka)
        (let ((it (vector-ref a i)))
          (if (eq? (cdr it) none)
              (lp (+ i 1) j)
              (begin
                (vector-set! a j it)
                (lp (+ i 1) (+ j 1)))))
        (let lp ((j j))
          (when (< j ka)
            (begin
              (vector-set! a j #f)
              (lp (+ j 1))))))))

(define-syntax-rule (rem-a t a na ka da it)
  (begin
    (set! t (logior t 4))
    (set! da (+ da 1))
    (set-cdr! it none)

    (when (> (* 2 da) (max 100 ka))
      (clear-a a na ka)
      (set! ka (- ka da))
      (set! da 0))))

(define none (list 'none))

(define-syntax define-rem
  (lambda (x)
    (syntax-case x ()
      ((f u . a)
       (eq? (syntax->datum #'u) '_)
       #'(values))
      
      ((f (mac rem) t kind m mm nm km a na ka da)
       (with-syntax ((frem (datum->syntax #'ref (gensym "frem"))))
         #'(begin
	     (define-syntax frem (hash-ref *removers* mac))
             (define (rem k) (frem mm kind))
	     (when (not (eq? (symbol->keyword kind) mac))
	       (error
		(format
		 #f "macro accessor with wrong rem kind ~a != ~a" kind mac))))))



      ((f rem t kind m mm nm km a na ka da)
       (with-syntax ((frem (datum->syntax #'ref (gensym "frem"))))
         #'(begin
             (define frem (hash-ref *removers* kind))
             (define (rem k) (frem mm k))))))))

(define-syntax with1
  (syntax-rules (_)
    ((_ (H : ref) code)
     (expand H (kind m mm)
      (define-ref ref kind mm)
      code))

    ((_ (H : ref set) code)
     (expand H (t kind m mm nm km a na ka da)
      (define-ref ref kind mm)
      (define-set set H t kind m mm nm km a na ka da)
      code))

    ((_ (H : ref set rem) code)
     (expand H (t kind m mm nm km a na ka da)
      (define-ref ref kind mm)
      (define-set set H t kind m mm nm km a na ka da)
      (define-rem rem t kind m mm nm km a na ka da)
      code))))

            
(define-syntax with-stis-hash
  (syntax-rules ()
    ((_ () . code)
     (begin . code))
    ((_ (x . y) . code)     
     (with1 x (with-stis-hash y . code)))))


(define* (stis-hash-ref h k #:optional (d #f))
  (with-stis-hash ((h : ref)) (ref k d)))

(define (stis-hash-set! h k v)
  (with-stis-hash ((h : _ set)) (set k v)))

(define (stis-hash-remove! h k)
  (with-stis-hash ((h : _ _ rem)) (rem k)))

(define-syntax-rule (expand-a (h : a ka) code ...)
  (let* ((o  (slot-ref h 'offset))
	 (a  (struct-ref h i-a))
	 (ka (struct-ref h i-ka)))
    code ...))

(define (stis-hash-for-each f h)
  (expand-a (h : a ka)
    (let lp ((i 0))
      (when (< i ka)
        (let ((it (vector-ref a i)))
          (when (not (eq? (cdr it) none))
            (f (car it) (cdr it)))
          (lp (+ i 1)))))))


(define (stis-hash-fold f s h)
  (expand-a (h : a ka)
    (let lp ((i 0) (s s))
      (if (< i ka)
          (let ((it (vector-ref a i)))
            (if (not (eq? (cdr it) none))
                (lp (+ i 1) (f (car it) (cdr it) s))
                (lp (+ i 1) s)))
          s))))

(define (stis-hash-map f h)
  (reverse!
   (expand-a (h : a ka)
     (let lp ((i 0) (s '()))
       (if (< i ka)
           (let ((it (vector-ref a i)))
             (if (not (eq? (cdr it) none))
                 (lp (+ i 1) (cons (f (car it) (cdr it)) s))
                 (lp (+ i 1) s)))
           s)))))

(define (stis-hash-clear! h)
  (with-stis-hash ((h : _ _ rem))
    (expand-a (h : a ka)
      (let lp ((i 0))
	(if (< i ka)
	    (let ((r (vector-ref a i)))
	      (vector-set! r i #f)
	      (when (not (eq? (cdr r) none))
		(rem (car r)))
	      (lp (+ i 1)))
	    (begin
	      (slot-set! h 'km 0)
	      (slot-set! h 'ka 0)
	      (slot-set! h 'da 0)))))))
	      
