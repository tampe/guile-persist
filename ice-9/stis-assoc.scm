(define-module (ice-9 stis-assoc)
  #:use-module (ice-9 pretty-print)
  #:use-module (oop goops)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs arithmetic fixnums)
  
  #:export (make-stis-ahash-table
            make-stis-ahash-type
            with-stis-ahash

            stis-ahash-for-each
            stis-ahash-fold
            stis-ahash-map

            stis-ahash-ref
            stis-ahash-set!
            stis-ahash-remove!

	    stis-ahash-clear!))

(eval-when (eval load compile)
  (define do-stis #t)
  (if (and do-stis
	   (module-defined? (resolve-module '(ice-9 stis stis)) 'stis-hash))
      (use-modules 
       ((ice-9 stis stis    ) #:select (stis-a-search
					stis-hash
					stis-hashq
					stis-hashv))
	 
       ((system vm  vm      ) #:select (stis-bv-ref
					stis-bv-set!
					
					stis-a-make
					stis-a-add!
					stis-a-ref
					stis-a-set!
					stis-a-re-search)))
      (use-modules 
       ((persist persistance) #:select (stis-bv-ref
					stis-bv-set!
					
					stis-a-make
					stis-a-search
					stis-a-add!
					stis-a-ref
					stis-a-set!
					stis-a-re-search)))))

(eval-when (eval load compile)
  (if (and do-stis
	   (module-defined? (resolve-module '(ice-9 stis stis)) 'stis-hash))
      (values)
      (begin
	(define! 'stis-hash     (lambda (x)   (hash  x (ash 1 60))))
	(define! 'stis-hashv    (lambda (x)   (hashv x (ash 1 60))))
	(define! 'stis-hashq    (lambda (x)   (hashq x (ash 1 60)))))))

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

(define C (+ 32 (* 8 16)))

(define-syntax awhen
  (syntax-rules ()
    ((_ (it i ...) p . a)
     (call-with-values (lambda () p)
       (lambda (it i ...)
	 (when it . a))))
    ((_ it . l)
     (awhen (it) . l))))

(define empty (list 'empty))


(define-syntax-rule (make-check check it v return)
  (define-inlinable (check it k d eq fail)
    (let ((u (cdr it)))
      (if (eq (car u) k)
	  (let ((v (cdr u)))
	    (if (eq? v empty)
		(fail (car it))
		return))
	  (fail (car it))))))

(make-check check  it v v )
(make-check check2 it v it)

(define-inlinable (stis-a-re-ref a k d h n m eq check)
  (define (fail m) (afail a k d h n eq m check))
  (aif it (stis-a-re-search a k n m)
       (check it k d eq fail)
       d))

(define (afail a k d h n eq m check)
  (let lp ((i (+ m 1)) (k (logand (+ m 1) #xf)))
    (define (fail m) (lp (+ i 1) (+ k 1)))
    (if (< i n)
	(if (< k 16)
	    (let ((it (stis-a-ref a k)))
	      (check it k d eq fail))
	    (stis-a-re-ref a k d h n m eq check))
	d)))
	    
(define-inlinable (stis-aa-href a k d h n eq)
  (define (fail m) (afail a k d h n eq m check2))
  (aif it (stis-a-search a h n)
       (check it k d eq fail)
       d))

(define-inlinable (stis-aa-ref a k d h n eq)
  (define (fail m) (afail a k d h n eq m check))
  (aif it (stis-a-search a h n)
       (check it k d eq fail)
       d))

(define-inlinable (stis-aa-set! a k v h n eq)
  (aif it (stis-aa-href a k #f h n eq)
       (let ((it (cdr it))
	     (v  (cdr it)))
	 (if (eq? v empty)
	     (begin
	       (set-cdr! it v)
	       (values 1 1))
	     (values 0 0)))

       (begin
	 (stis-a-add! a h n (cons* n k v))
	 (values 1 0))))


(define-inlinable (stis-aa-remove! a k h n eq)
  (aif it (stis-aa-href a k #f h n eq)
       (let ((it (cdr it))
	     (v  (cdr it)))
	 (if (eq? v empty)
	     0
	     (begin
	       (set-cdr! it empty)
	       1)))
       0))


(define-inlinable (stis-ahashx-ref hash eq a k default n)
  (stis-aa-ref a k default (hash k) n eq))

(define *reffers* (make-hash-table))


(define-inlinable (stis-ahashx-href hash eq a k default n)
  (stis-aa-href a k default (hash k) n eq))

(define *hreffers* (make-hash-table))


(define-inlinable (stis-ahashx-set! hash eq a k v n)
  (stis-aa-set! a k v (hash k) n eq))

(define *setters* (make-hash-table))


(define-inlinable (stis-ahashx-remove! hash eq a k n)
  (stis-aa-remove! a k (hash k) n eq))

(define *removers* (make-hash-table))


(define-syntax-rule (make-stis-ahash-type kind hash eq)
  (begin
    (define (ref h k d n) (stis-ahashx-ref hash eq h k d n))
    (define mref
      (lambda (x)
	(syntax-case x ()
	  ((_ h k d n) #'(stis-ahashx-ref hash eq h k d n)))))
    
    (define (hre h k d n) (stis-ahashx-href hash eq h k d n))
    
    (define (set h k v n) (stis-ahashx-set! hash eq h k v n))
    (define mset
      (lambda (x)
	(syntax-case x ()
	  ((_ h k d n) #'(stis-ahashx-set! hash eq h k d n)))))

    (define (rem h k n )  (stis-ahashx-remove! hash eq h k n))
    (define mrem
      (lambda (x)
	(syntax-case x ()
	  ((_ h k n) #'(stis-ahashx-remove! hash eq h k n)))))

    (hash-set! *reffers*  kind ref)
    (hash-set! *reffers*  (symbol->keyword kind) mref)
    
    (hash-set! *setters*  kind set)
    (hash-set! *setters*  (symbol->keyword kind) mset)
    
    (hash-set! *hreffers* kind hre)
    
    (hash-set! *removers* kind rem)
    (hash-set! *removers* (symbol->keyword kind) mrem)

    (values)))

(make-stis-ahash-type 'fastq
		      (lambda (x) x)
		      eq?)

(make-stis-ahash-type 'fastv
		      (lambda (x)
			(if (or (fixnum? x) (symbol? x))
			    x
			    (stis-hashv x)))
		      eqv?)

(make-stis-ahash-type 'fast
		      (lambda (x)
			(if (or (fixnum? x) (symbol? x))
			    x
			    (stis-hash x)))
		      equal?)

(make-stis-ahash-type 'hashq stis-hashq eq?    )
(make-stis-ahash-type 'hashv stis-hashv eqv?   )
(make-stis-ahash-type 'hash  stis-hash  equal? )


(define-class <A> () kind a N n d name)

(define i-kind 0)
(define i-a    1)
(define i-N    2)
(define i-n    3)
(define i-d    4)
(define i-name 5)

(define* (make-stis-ahash-table kind #:optional (N 32) #:key (name #f))
  (define (f n)
    (let lp ((nn 32))
      (if (< nn n) 
	  (lp (* 2 nn))
	  nn)))

  (set! N (f N))
	  
  (let ((o (make <A>)))
    (struct-set! o i-kind kind)
    (struct-set! o i-a
		 (stis-a-make (+ (ash N -4)
				 (if (> (logand N #xf) 0)
				     1
				     0))))
					      
    (struct-set! o i-N    N)
    (struct-set! o i-n    0)
    (struct-set! o i-d    0)
    (struct-set! o i-name name)

    o))

(define-method (write (o <A>) port)
  (aif it (slot-ref o 'name)
        (format port
                "<stis-ahash:~a (~a) n=~a/~a>"
                it
                (slot-ref o 'kind)
                (slot-ref o 'n)
                (slot-ref o 'N))
        (format port
                "<stis-ahash (~a) n=~a/~a>"                
                (slot-ref o 'kind)
                (slot-ref o 'n)
                (slot-ref o 'N))))

(define-method (display (o <A>) port) (write o port))


(define-syntax expand
  (lambda (x)
    (syntax-case x ()
      ((_ h (a aa k n) . code)
       #'(expand0 h (a aa k n) . code))
      
      ((_ h (t a aa k n N d) code ...)
       #'(expand0 h (a aa k n N d)
           (let* ((t  0))
	     (dynamic-wind
		 (lambda ()
		   (set! t  0)
		   (set! a  (struct-ref h i-a))
		   (set! aa (stis-bv-ref a))
		   (set! n  (struct-ref h i-n))
		   (set! N  (struct-ref h i-N))
		   (set! d  (struct-ref h i-d)))
		 (lambda () code ...)
		 (lambda ()
		   (when (> t 0)
		     (struct-set! h i-n n)
		     (when (> t 1)
		       (struct-set! h i-d  d)
		       (struct-set! h i-N  N)))))))))))

(define-syntax expand0 
  (lambda (x)
    (syntax-case x ()
      ((_ h (a aa) code ...)
       #'(let* ((a  (struct-ref h i-a))
		(aa (stis-bv-ref a)))
	   code ...))

      ((_ h (u v a . aa) . code)
       (with-syntax ((i-a (datum->syntax #'a (string->symbol
                                              (string-append
                                               "i-"
                                               (symbol->string
                                                (syntax->datum #'a)))))))
         #'(let ((a (struct-ref h i-a)))
             (expand0 h (u v . aa) . code)))))))

(define-syntax define-ref
  (lambda (x)
    (syntax-case x (_)
      ((f _ . a)
       #'(values))
      
      ((f (mac ref) kind aa n)
       (with-syntax ((fref (datum->syntax #'ref (gensym "fref"))))
         #'(begin
	     (define-syntax fref (hash-ref *reffers* mac))
             (define-syntax ref
	       (lambda (x)
		 (syntax-case x ()
		   ((_ k  )
		    #'(fref aa k #f n))
		   
		   ((_ k d)
		    #'(fref aa k  d n))

		   (_
		    #'(lambda* (k #:optional (d #f)) (fref aa k d n))))))

	     (when (not (eq? (symbol->keyword kind) mac))
	       (error
		(format
		 #f "macro accessor with wrong ref kind ~a != ~a" kind mac))))))


      ((f ref kind aa n)
       (with-syntax ((fref (datum->syntax #'ref (gensym "fref"))))
         #'(begin
	     (define fref (hash-ref *reffers* kind))	
             (define-syntax ref
	       (lambda (x)
		 (syntax-case x ()
		   ((_ k  )
		    #'(fref aa k #f n))

		   ((_ k d)
		    #'(fref aa k  d n))

		   (_
		    #'(lambda* (k #:optional (d #f)) (fref aa k d n))))))))))))
  
(define (check-n a aa n N)
  (if (= n N)
      (let* ((NN  (* 2 N))
	     (K   (* C (ash N  -4)))
	     (KK  (* C (ash NN -4)))
	     (aaa (make-bytevector KK 0)))
	(bytevector-copy! aa 0 aaa 0 K)
	(stis-bv-set! a aaa)
	NN)
      #f))
             
(define (check-d a n N d)
  (if (and (> n 8) (> (ash d 1) n))
      (let lp ((i 0) (ii 0))
	(if (< i n)
	    (let ((x (stis-a-ref a i)))
	      (if (eq? (cddr x) empty)
		  (lp (+ i 1) ii)
		  (if (= i ii)
		      (lp (+ i 1) (+ ii 1))
		      (begin
			(stis-a-set! a ii (cons ii (cdr x)))
			(lp (+ i 1) (+ ii 1))))))
	    (let lp ((iii ii))
	      (if (< iii n)
		  (begin
		    (stis-a-set! a iii #f)
		    (lp (+ iii 1)))
		  (if (= i ii)
		      #f
		      ii)))))
      #f))

(define-syntax define-set
  (lambda (x)
    (syntax-case x (_)
      ((f _ . a)
       #'(values))

      ((f (mac set) t kind a aa n N d)
       (with-syntax ((fset (datum->syntax #'ref (gensym "fset"))))
         #'(begin
	     (define-syntax fset (hash-ref *setters* mac))
             (define-syntax set
	       (lambda (x)
		 (syntax-case x ()
		   ((_ k d)
		    #'(fset aa k d n))

		   (_
		    #'(lambda* (k d) (fset aa k d n))))))

	     (when (not (eq? (symbol->keyword kind) mac))
	       (error
		(format
		 #f "macro accessor with wrong set kind ~a != ~a" kind mac))))))

	       

      ((f set t kind a aa n N d)
       (with-syntax ((fset (datum->syntax #'ref (gensym "fset"))))
         #'(begin
             (define fset (hash-ref *setters* kind))
             (define (set k v)
	       (call-with-values (lambda () (fset aa k v n))
		 (lambda (nn mm)
		   (when (= nn 1)
		     (set! n (+ n 1))
		     (set! t (logior t 1))
		     (awhen NN (check-n a aa n N)
		       (set! N NN)
		       (set! t 2)))

		   (when (= mm 1) 
		     (set! d (- d 1))
		     (set! t 2)))))))))))

(define-syntax define-rem
  (lambda (x)
    (syntax-case x (_)
      ((f _ . a)
       #'(values))

      ((f (mac rem) t kind a n N d)
       (with-syntax ((frem (datum->syntax #'ref (gensym "frem"))))
         #'(begin
	     (define-syntax frem (hash-ref *removers* mac))
             (define-syntax set
	       (lambda (x)
		 (syntax-case x ()
		   ((_ k n)
		    #'(frem aa k n))

		   (_
		    #'(lambda* (k) (frem aa k n))))))

	     (when (not (eq? (symbol->keyword kind) mac))
	       (error
		(format
		 #f "macro accessor with wrong rem kind ~a != ~a" kind mac))))))


      ((f rem t kind a n N d)
       (with-syntax ((frem (datum->syntax #'ref (gensym "frem"))))
         #'(begin
             (define frem (hash-ref *removers* kind))
             (define (rem k)
	       (let ((mm  (frem a k n)))
		 (when (= mm 1) 
		   (set! d (+ d 1))
		   (set! t (logior t 2))
		   (awhen nn (check-d a n N d)
		     (set! t 2)
		     (set! n nn)))))))))))

(define-syntax with1
  (syntax-rules (_)
    ((_ (H : ref) code)
     (expand H (a aa kind n)
      (define-ref ref kind aa n)
      code))
    
    ((_ (H : ref set) code)
     (expand H (t a aa kind n N d)
      (define-ref ref kind aa n)
      (define-set set t kind a aa n N d)
      code))

    ((_ (H : ref set rem) code)
     (expand H (a aa t kind n N d)
      (define-ref ref kind aa n)
      (define-set set t kind a aa n N d)
      (define-rem rem t kind aa n N d)
      code))))

(define-syntax with-stis-ahash
  (syntax-rules ()
    ((_ () . code)
     (begin . code))
    ((_ (x . y) . code)     
     (with1 x (with-stis-ahash y . code)))))


(define* (stis-ahash-ref h k #:optional (d #f))
  (with-stis-ahash ((h : ref)) (ref k d)))

(define (stis-ahash-set! h k v)
  (with-stis-ahash ((h : _ set)) (set k v)))

(define (stis-ahash-remove! h k)
  (with-stis-ahash ((h : _ _ rem)) (rem k)))


(define-syntax-rule (expand-a (h : a n) code ...)
  (let ((a  (struct-ref h i-a))
        (n  (struct-ref h i-n)))
    code ...))

(define (stis-ahash-for-each f h)
  (expand-a (h : a n)
    (let lp ((i 0))
      (if (< i n)
	  (let ((x (stis-a-ref a i)))
	    (when (not (eq? (car x) empty))
	      (f (cadr x) (cddr x)))
	    (lp (+ i 1)))))))

(define (stis-ahash-fold f s h)
  (expand-a (h : a n)
    (let lp ((i 0) (s s))
      (if (< i n)
	  (let ((x (stis-a-ref a i)))
	    (if (eq? (car x) empty)
		(lp (+ i 1) s)
		(lp (+ i 1) (f (cadr x) (cddr x) s))))
	    s))))

(define (stis-ahash-map f h)
  (reverse! (stis-ahash-fold (lambda (k v s) (cons (f k v) s)) '() h)))

(define* (stis-ahash-clear! h)
  (expand-a (h : a n)
    (let lp ((i 0))
      (when (< i n)
	(stis-a-set! a i #f)
	(lp (+ i 1))))

    (struct-set! a i-n 0)
    (struct-set! a i-d 0)))

