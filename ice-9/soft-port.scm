(define-module (ice-9 soft-port)
  #:use-module (rnrs bytevectors)
  #:use-module (ice-9 ports internal)
  #:use-module (ice-9 format)
  #:use-module (oop goops)
  
  #:export (soft-port-read soft-port-write
                           soft-writer
                           soft-port?
			   soft-peek-byte
			   
                           soft-port-read-buffer
                           soft-port-write-buffer
                           soft-port-auxiliary-write-buffer
                           
                           soft-port-mode
                           
                           soft-input-port?
                           soft-output-port?
                           soft-port-closed?
                           soft-port-random-access?
                           
                           soft-seek
                           soft-close-port
                           
                           soft-expand-port-read-buffer!
                           soft-port-read-buffering

                           %soft-port-encoding
                           soft-port-decode-char

                           soft-accept
                           soft-connect
                           soft-getsockopt

			   soft-port-filename
                           soft-setvbuf
			   soft-port-poll

			   soft-unread-char
			   soft-port-clear-stream-start-for-bom-write
			   soft-port-clear-stream-start-for-bom-read
			   soft-specialize-port-encoding!
			   soft-port-encode-chars
			   soft-port-encode-char
			   soft-port-conversion-strategy
			   soft-port-line-buffered?

			   soft-buffered?
			   %soft-port-property
			   soft-port-line
			   soft-port-column

			   make-port-buffer
			   
                           make-soft-input/output-port
                           make-soft-input-port
                           make-soft-output-port
                           ))

(define-syntax-rule (aif it p a b) (let ((it p)) (if it a b)))

(define-class <soft-port> ()
  name
  flags
  read
  write
  read-buffer
  write-buffer
  aux-buffer
  seek
  close
  port
  buffering?
  pstate)

(define n 0)
(define-syntax mk<lass>
  (lambda (x)
    (syntax-case x ()
      ((_ x ...)
       (with-syntax (((s ...) (map (lambda (stx)
				     (datum->syntax stx
				       (string->symbol
					(string-append
					 "n-"
					 (symbol->string
					  (syntax->datum stx))))))
				   #'(x ...))))
	 #'(begin
	     (define-class <soft-port> () x ...)
	     (define s (let ((nn n))
			      (set! n (+ n 1))
			      nn)) ...))))))

(mk<lass>
 name
 flags
 read
 write
 read-buffer
 write-buffer
 aux-buffer
 seek
 close
 port
 buffering?
 pstate)

  

(define (soft-port? sport)
  (is-a? sport <soft-port>))

(define (soft-writer o port)
  (aif it (struct-ref o n-name)
       (format port
	       "<soft-port(~a): ~a>"
	       it
	       (soft-port-mode o))
       
       (format port
	       "<soft-port: ~a>"
	       (soft-port-mode o))))

(define-method (write (o <soft-port>) port) (soft-writer o port))
(define-method (display (o <soft-port>) port) (write o port))


(define READ   1)
(define WRITE  2)
(define CLOSED 4)
(define RANDOM 8)

(define flagmap #("0" "r" "w" "r+"))

(define (soft-port-mode port)
  (let ((flags (struct-ref port n-flags)))
    (vector-ref flagmap (logand flags 3))))

(define (soft-port-read port)
  (struct-ref port n-read))

(define (soft-port-write port)
  (struct-ref port n-write))

(define (soft-port-read-buffer port)
  (aif it (struct-ref port n-read-buffer)
       it
       (port-read-buffer (slot-ref port 'port))))

(define (soft-port-write-buffer port)
  (aif it (struct-ref port n-write-buffer)
       it
       (port-write-buffer (struct-ref port n-port))))


(define (soft-seek port n kind)
  (let ((seek (struct-ref port n-seek)))
    (seek n kind)))

(define (soft-input-port?  port)
  (> (logand (struct-ref port n-flags) READ)
     0))

(define (soft-output-port?  port)
  (> (logand (struct-ref port n-flags) WRITE)
     0))

(define (soft-port-random-access? port)
  (> (logand (struct-ref port n-flags) RANDOM)
     0))

(define (soft-port-closed? port)
  (> (logand (struct-ref port n-flags) CLOSED)
     0))

(define (soft-close-port port)
  (let ((flags (struct-ref port n-flags)))
    (if (> (logand flags CLOSED) 0)
        #f
        (begin
          (struct-set! port n-flags (logior flags  CLOSED))
	  (aif cl (struct-ref port n-close)
	       (cl)
	       (close-port (struct-ref port n-port)))))))

(define* (soft-expand-port-read-buffer! port size #:optional (putback? #f))
  (aif it (struct-ref port n-read-buffer)
       (let* ((buf     it)
              (bv1     (vector-ref buf 0))
              (newbuf  (make-port-buffer size (vector-ref buf 3)))
              (bv2     (vector-ref newbuf 0))
              (cur     (vector-ref buf 1))
              (n       (vector-ref buf 2))
              (m       (- n cur)))
    
         (if putback?
             (begin 
               (bytevector-copy! bv1 cur bv2 (- size m)  m)
               (vector-set! newbuf 1 (- size m))
               (vector-set! newbuf 2 size))
          
             (begin
               (bytevector-copy! bv1 cur bv2 0 (- n cur))
               (vector-set! newbuf 1 0)
               (vector-set! newbuf 2 (- n cur))))

         (struct-set! port n-read-buffer newbuf)
         
         newbuf)

       (expand-port-read-buffer!
        (struct-ref port n-port) size putback?)))
  
(define (soft-port-read-buffering port)
  (aif it (struct-ref port n-read-buffer)
       (bytevector-length (vector-ref it 0))
       (port-read-buffering (struct-ref port n-port))))

(define (soft-port-auxiliary-write-buffer port)
  (aif it (struct-ref port n-aux-buffer)
       it
       (port-auxiliary-write-buffer (struct-ref port n-port))))


(define (%soft-port-encoding port)
  (%port-encoding (struct-ref port n-port)))

(define (soft-port-decode-char port bv cur input-size)
  (port-decode-char 
   (struct-ref port n-port)
   bv cur input-size))

(define (soft-accept port flags)
  (accept (struct-ref port n-port) flags))

(define (soft-connect port . l)
  (apply connect (struct-ref port n-port) l))

(define (soft-getsockopt port . l)
  (apply getsockopt (struct-ref port n-port) l))

(define* (soft-setvbuf port sym #:optional (size (ash 1 12)))
  (if (or (struct-ref port n-read-buffer)
          (struct-ref port n-write-buffer))
      (begin
       (when (struct-ref port n-read-buffer)
         (case sym
           ((none)
            (vector-set! (struct-ref port n-read-buffer)  0
                         (make-bytevector 0)))

           ((line)
            (vector-set! (struct-ref port n-read-buffer)  0
                         (make-bytevector size)))
          
           ((block)
            (vector-set! (struct-ref port n-read-buffer)  0
                         (make-bytevector size)))))

       (when (struct-ref port n-write-buffer)
         (case sym
           ((none)
            (vector-set! (struct-ref port n-write-buffer) 0 (make-bytevector 0))
            (vector-set! (struct-ref port n-aux-buffer)   0
			 (make-bytevector 0)))
         
           ((line)
            (vector-set! (struct-ref port n-write-buffer) 0
			 (make-bytevector size))
            (vector-set! (struct-ref port n-aux-buffer)   0
                         (make-bytevector size)))
          
           ((block)
            (vector-set! (struct-ref port n-write-buffer) 0
			 (make-bytevector size))
            (vector-set! (struct-ref port n-aux-buffer)   0
			 (make-bytevector size))
            ))))

      (aif port2 (struct-ref port n-port)
           (setvbuf port2 sym)
           (values))))

(define (make-port-buffer size eof?)
  (vector (make-bytevector size) 0 0 eof? (cons 0 0)))

(define (f read-buffer)
  (if read-buffer
      (if (eq? read-buffer #t)
	  (make-port-buffer (ash 1 12) #t)
	  read-buffer)
      #f))

(define (soft-port-poll port a)
  (port-poll (struct-ref port n-port) a))

(define* (make-soft-input/output-port
          name read! write! close
          #:key
	  (buffering?   #f)
          (strategy     #f)
          (encoding     #f)
          (flags        #f)
          (seek         #f)
          (subport      #f)
          (read-buffer  #f)
          (write-buffer #f)
          (aux-buffer   #f))
          
  (define read
    (if read!
        (lambda (port a b c) (read! a b c))
        (let ((read (port-read subport)))
          (lambda (port a b c) (read subport a b c)))))
  
  (define write
    (if write!
        (lambda (port a b c) (write! a b c))
        (let ((write (port-write subport)))
          (lambda (port a b c) (write subport a b c)))))
    
  (set! subport (if subport
		    (if (soft-port? subport)
			(slot-ref subport 'port)
			subport)
		    (%make-void-port "rw")))

  
  (set! seek (if seek
                 seek
                 (if subport
                     (lambda (n kind) ((@ (guile) seek) subport n kind))
                     #f)))
  
  (set! read-buffer  (f read-buffer))  
  (set! write-buffer (f write-buffer))  
  (set! aux-buffer   (f aux-buffer))
                         
  (let ((ret (make <soft-port>)))
    (slot-set! ret 'name         name)
    (slot-set! ret 'flags        (logior (if flags flags 0)
                                         (logior READ WRITE)))
    (slot-set! ret 'read         read)
    (slot-set! ret 'write        write)
    (slot-set! ret 'read-buffer  read-buffer)
    (slot-set! ret 'write-buffer write-buffer)
    (slot-set! ret 'aux-buffer   aux-buffer)
    (slot-set! ret 'seek         seek)
    (slot-set! ret 'close        close)
    (slot-set! ret 'port         subport)
    (slot-set! ret 'buffering?   buffering?)
    
    (when encoding
      (set-port-encoding! (slot-ref ret 'port) encoding))

    (when strategy
      (set-port-conversion-strategy! (slot-ref ret 'port) strategy))
    
    ret))



(define* (make-soft-output-port
          name write! close
          #:key
	  (buffering?   #f)
          (strategy     #f)
          (encoding     #f)
          (flags        #f)
          (seek         #f)
          (subport      #f)
          (write-buffer #f)
          (aux-buffer   #f))
            
  (define write
    (if write!
        (lambda (port a b c) (write! a b c))
        (let ((write (port-write subport)))
          (lambda (port a b c) (write subport a b c)))))
  
  (set! subport (if subport
		    (if (soft-port? subport)
			(slot-ref subport 'port)
			subport)
		    (%make-void-port "w")))

  (set! seek (if seek
                 seek
                 (if subport
                     (lambda (n kind) ((@ (guile) seek) subport n kind))
                     #f)))
  
  (set! write-buffer (f write-buffer))  
  (set! aux-buffer   (f aux-buffer))
                         
  (let ((ret (make <soft-port>)))
    (slot-set! ret 'name         name)
    (slot-set! ret 'flags        (logand
                                  (logior (if flags flags 0)  WRITE)
                                  (lognot READ)))
    (slot-set! ret 'read         #f)
    (slot-set! ret 'write        write)
    (slot-set! ret 'read-buffer  #f)
    (slot-set! ret 'write-buffer write-buffer)
    (slot-set! ret 'aux-buffer   aux-buffer)
    (slot-set! ret 'seek         seek)
    (slot-set! ret 'close        close)
    (slot-set! ret 'port
               (if (soft-port? subport)
                   (slot-ref subport 'port)
                   (if subport
                       subport
                       (%make-void-port "w"))))
    (slot-set! ret 'buffering?   buffering?)
    (slot-set! ret 'pstate       (make-fluid #f))
    
    (when encoding
      (set-port-encoding! (slot-ref ret 'port) encoding))

    (when strategy
      (set-port-conversion-strategy! (slot-ref ret 'port) strategy))
    
    ret))
         
(define* (make-soft-input-port
          name read! close
          #:key
	  (buffering?   #f)
          (strategy     #f)
          (encoding     #f)
          (flags        #f)
          (seek         #f)
          (subport      #f)
          (read-buffer  #f))
          
  (define read
    (if read!
        (lambda (port a b c) (read! a b c))
        (let ((read (port-read subport)))
          (lambda (port a b c) (read subport a b c)))))
  
  (set! subport (if subport
		    (if (soft-port? subport)
			(slot-ref subport 'port)
			subport)
		    (%make-void-port "r")))

  (set! seek (if seek
                 seek
                 (if subport
                     (lambda (n kind) ((@ (guile) seek) subport n kind))
                     #f)))
  
  (set! read-buffer  (f read-buffer))  
                         
  (let ((ret (make <soft-port>)))
    (slot-set! ret 'name         name)    
    (slot-set! ret 'flags        (logand
                                  (logior (if flags flags 0)
                                          READ)
                                  (lognot WRITE)))
    
    (slot-set! ret 'read         read)
    (slot-set! ret 'write        #f)
    (slot-set! ret 'read-buffer  read-buffer)
    (slot-set! ret 'write-buffer #f)
    (slot-set! ret 'aux-buffer   #f)
    (slot-set! ret 'seek         seek)
    (slot-set! ret 'close        close)
    (slot-set! ret 'buffering?   buffering?)
    (slot-set! ret 'port
               (if (soft-port? subport)
                   (slot-ref subport 'port)
                   (if subport
                       subport
                       (%make-void-port "r"))))

    (when encoding
      (set-port-encoding! (slot-ref ret 'port) encoding))

    (when strategy
      (set-port-conversion-strategy! (slot-ref ret 'port) strategy))
    
    ret))

(define (soft-unread-char delim port)
  (unread-char delim (struct-ref port n-port)))

(define (soft-specialize-port-encoding! port enc)
  (specialize-port-encoding! (struct-ref port n-port) enc))

(define (soft-port-clear-stream-start-for-bom-write port a)
  (port-clear-stream-start-for-bom-write (struct-ref port n-port) a))

(define (soft-port-clear-stream-start-for-bom-read port)
  (port-clear-stream-start-for-bom-read (struct-ref port n-port)))

(define (soft-port-encode-chars port a b c d)
  (port-encode-chars (struct-ref port n-port) a b c d))

(define (soft-port-encode-char port a b)
  (port-encode-char (struct-ref port n-port) a b))

(define (soft-port-line-buffered? port) #t)

(define (soft-port-conversion-strategy port)
  (port-conversion-strategy (struct-ref port n-port)))

(define (soft-buffered? port)
  (struct-ref port n-buffering?))

(define (soft-port-filename port)
  (port-filename (struct-ref port n-port)))

(define (%soft-port-property port s)
  (%port-property (struct-ref port n-port) s))

(define (soft-port-line port)
  (car (vector-ref (soft-port-read-buffer port) 4)))

(define (soft-port-column port)
  (cdr (vector-ref (soft-port-read-buffer port) 4)))

