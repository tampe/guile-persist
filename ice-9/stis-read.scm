(define-module (ice-9 stis-read)
  #:use-module (system foreign)
  #:use-module (system syntax)
  #:use-module (oop goops)
  #:use-module (ice-9 match)
  #:use-module (ice-9 soft-port)
  #:use-module (ice-9 binary-ports)
  #:use-module (ice-9 supervectors c)
  #:use-module (ice-9 supervectors typeinfo)
  #:use-module (ice-9 supervectors core)
  #:use-module (rnrs bytevectors)
  #:use-module (rnrs io ports)
  #:use-module (persist persistance)
  #:replace (read))


(define  read-bits-mask          3)
(define  read-bits-shift         0)
(define  read-bins-mask          (ash 7 2))
(define  read-bins-shift         2)
(define  read-sh-mask            (ash 3 5))
(define  read-sh-shift           5)
(define  read-d-mask             (ash 3 7))
(define  read-d-shift            7)
(define  read-real-flag          (ash 1 9))
(define  read-sgn-flag           (ash 1 10))
(define  utf8-flag               (ash 1 11))
(define  inherited-flag          (ash 1 12))
(define  compress-refs-flag      (ash 1 13))
(define  record-positions-flag   (ash 1 14))
(define  end-flag                (ash 1 15))
(define  r7rs-symbols-flag       (ash 1 16))
(define  square-brackets-flag    (ash 1 17))
(define  curly-infix-flag        (ash 1 18))
(define  keyword-style-mask      (ash 3 19))
(define  keyword-style-prefix    0)
(define  complex-flag            (ash 1 22))
(define  binary-flag             (ash 1 23))
(define  r6rs-escapes-flag       (ash 1 24))
(define  hungry-eol-escapes-flag (ash 1 25))
(define  read-neoteric-flag      (ash 1 26))

(define numeric-mask
  (logior
   read-real-flag
   read-sgn-flag
   read-bits-mask
   read-bins-mask))

(define encoding-mask
  (logior
   utf8-flag
   read-d-mask
   read-sh-mask))

#|
(define bitfield:case-insensitive?   2)
(define bitfield:keyword-style       4)
(define bitfield:r6rs-escapes?       6)
(define bitfield:square-brackets?    8)
(define bitfield:hungry-eol-escapes? 10)
(define bitfield:curly-infix?        12)
(define bitfield:r7rs-symbols?       14)
(define read-option-bits             16)

(define read-option-mask         #b11)
(define read-option-inherit      #b11)
(define read-options-inherit-all (1- (ash 1 read-option-bits)))

(define keyword-style-hash-prefix 0)
(define keyword-style-prefix      1)
(define keyword-style-postfix     2)

(define (compute-reader-options port)
  (let ((options      (read-options))
        (port-options (or (%port-property port 'port-read-options)
                          read-options-inherit-all)))
    
    (define-syntax-rule (option field exp)
      (let ((port-option (logand port-options (ash read-option-mask field))))
        (if (= port-option (ash read-option-inherit field))
            exp
            port-option)))
    
    (define (bool key field)
      (option field
              (if (memq key options) (ash 1 field) 0)))
    
    (define (enum key values field)
      (option field
              (ash (assq-ref values (and=> (memq key options) cadr)) field)))
    
    (logior (bool 'positions bitfield:record-positions?)
            (bool 'case-insensitive bitfield:case-insensitive?)
            (enum 'keywords '((#f . 0) (prefix . 1) (postfix . 2))
                  bitfield:keyword-style)
            (bool 'r6rs-hex-escapes bitfield:r6rs-escapes?)
            (bool 'square-brackets bitfield:square-brackets?)
            (bool 'hungry-eol-escapes bitfield:hungry-eol-escapes?)
            (bool 'curly-infix bitfield:curly-infix?)
            (bool 'r7rs-symbols bitfield:r7rs-symbols?))))
|#

(define-syntax-rule (aif it a b c) (let ((it a)) (if it b c)))

(define (lookup-2-scm a b c) (pointer->scm (make-pointer (lookup-2 a b c))))

(define-syntax I
  (lambda (x)
    (syntax-case x ()
      ((_ z)
       (datum->syntax x (char->integer (syntax->datum #'z))))
      ((_ z y ...)
       (with-syntax (((u ...) (map (lambda (z)
				     (datum->syntax
				      x (char->integer
					 (syntax->datum z))))
				   (cons #'z #'(y ...)))))
		    #''(u ...))))))

(define buffsize (ash 1 14))
(define nbuff    (- buffsize 600))
		    

(define current-read-port (make-thread-local-fluid #f))
(define-inlinable (get-port) (fluid-ref current-read-port))

(define (make-read-buffer) (make-bytevector buffsize 0))
(define* (make-buffdata #:key (buffer #f) (pt 0) (end pt))
  (define s (make-string buffsize #\a))
  (define b (butify s buffsize))
  (bytevector-copy! cleared-bv 0 b 0 buffsize)
  
  (vector (if buffer buffer (make-read-buffer))
	  pt end
	  (make-vector      buffsize)
	  b s))

(define* (make-flags #:key
		     (inherited? #f)
		     (syntax?    #f))
  
  (logior
   (if inherited? inherited-flag        0)
   (if syntax?    record-positions-flag 0)))

(define i-flags    0)
(define i-buffer   1)
(define i-buffpt   2)
(define i-end      3)
(define i-hash     4)
(define i-pts      5)
(define i-pos      6)
(define i-vector   7)
(define i-bvector  8)
(define i-eof      9)
(define i-neo     10)
(define i-seek    11)
(define i-str     12)

(define-inlinable (pstate-flags   x) (vector-ref x i-flags))
(define-inlinable (pstate-pos     x) (vector-ref x i-pos))
(define-inlinable (pstate-buffer  x) (vector-ref x i-buffer))
(define-inlinable (pstate-hash    x) (vector-ref x i-hash  ))
(define-inlinable (pstate-pt      x) (vector-ref x i-buffpt))
(define-inlinable (pstate-end     x) (vector-ref x i-end))
(define-inlinable (pstate-pts     x) (vector-ref x i-pts))
(define-inlinable (pstate-vector  x) (vector-ref x i-vector))
(define-inlinable (pstate-bvector x) (vector-ref x i-bvector))
(define-inlinable (pstate-eof     x) (vector-ref x i-eof))
(define-inlinable (pstate-neo     x) (vector-ref x i-neo))
(define-inlinable (pstate-seek    x) (vector-ref x i-seek))
(define-inlinable (pstate-str     x) (vector-ref x i-str))

(define-inlinable (flag? flags flag) (> (logand flags flag) 0))
(define-inlinable (pstate-pt-set!     x y) (vector-set! x i-buffpt y))
(define-inlinable (pstate-end-set!    x y) (vector-set! x i-end    y))
(define-inlinable (pstate-eof-set!    x y) (vector-set! x i-eof    y))
(define-inlinable (pstate-neo-set!    x y) (vector-set! x i-neo    y))
(define-inlinable (pstate-seek-set!   x y) (vector-set! x i-seek   y))
(define-inlinable (pstate-buffer-set! x y) (vector-set! x i-buffer y))

(define-inlinable (neoteric++ pstate)
  (pstate-neo-set! pstate (+ 1 (pstate-neo pstate))))
(define-inlinable (neoteric-- pstate)
  (pstate-neo-set! pstate (- (pstate-neo pstate) 1)))

(define (set-flag kind flag flags)
  (cond
   ((= kind 0)
    flags)
   (kind
    (logior flags flag))
   (else
    (logand flags (lognot flag)))))

(define (set-field mask x shift flags)
  (if (< x 0)
      flags
      (logior (logand flags (lognot mask))
	      (ash x shift))))

(define (handle-flags flags inherited?)
  (if flags
      (set-flag inherited? inherited-flag flags)
      (make-flags #:inherited? inherited?)))

(define* (make-pstate buffdata hash
		      #:key
		      (flags      #f)
		      (inherited? 0 ))
  (vector
   (handle-flags flags inherited?)
   (vector-ref buffdata 0)
   (vector-ref buffdata 1)
   (vector-ref buffdata 2)
   hash
   (cons 0 0)
   (cons 0 0)
   (vector-ref buffdata 3)
   (vector-ref buffdata 4)
   #f
   0 0
   (vector-ref buffdata 5)))

	  

(define* (pstate-copy pstate
		      #:key
		      (buffdata   #f)
		      (hash       #f)
		      (flags      #f)
		      (inherited? 0 ))
		      
  (let ((buffer (if buffdata (vector-ref buffdata 0) (pstate-buffer  pstate)))
	(buffpt (if buffdata (vector-ref buffdata 1) (pstate-pt      pstate)))
	(buffe  (if buffdata (vector-ref buffdata 2) (pstate-end     pstate)))
	(vec    (if buffdata (vector-ref buffdata 3) (pstate-vector  pstate)))
	(bvec   (if buffdata (vector-ref buffdata 4) (pstate-bvector pstate)))
	(s      (if buffdata (vector-ref buffdata 5) (pstate-bvector pstate)))
	(eof    (pstate-eof pstate))
	(neo    (pstate-neo pstate))
	(pos    (pstate-pos pstate))
	(pts    (pstate-pts pstate))
	(hash   (if hash     hash                    (pstate-hash   pstate)))
	(flags  (if flags
		    (handle-flags flags inherited?)		    
		    (handle-flags (pstate-flags pstate) inherited?))))
    
    (vector flags buffer buffpt buffe hash
	    pts pos
	    vec bvec eof neo (pstate-seek pstate) s)))

(define cleared-bv (make-bytevector buffsize))

(define (clear-bv pstate)
  (define buffer (pstate-buffer pstate))
  (define pt     (pstate-pt     pstate))
  (define end    (pstate-end    pstate))
  (define n      (- end pt))

  #;(seek (get-port) (- n) SEEK_CUR)
  
  (bytevector-copy! cleared-bv 0 buffer 0 end))


(define e-default (if (eq? (native-endianness) 'big) end-flag 0))

(define-syntax with-bytevector
  (syntax-rules ()
    ((_ (pstate : buf pt end pts pos bv) code ...)
     (let ((pts (pstate-pts     pstate))	   
	   (pos (pstate-pos     pstate))
	   (pt  (pstate-pt      pstate))
	   (end (pstate-end     pstate))
	   (buf (pstate-buffer  pstate))
	   (bv  (pstate-bvector pstate)))
       code ...))))

(define-syntax with-vector
  (syntax-rules ()
    ((_ (pstate : buf pts pos v) code ...)
     (let ((pts (pstate-pts    pstate))
	   (pos (pstate-pos    pstate))
	   (buf (pstate-buffer pstate))
	   (v   (pstate-vector pstate)))
       code ...))))

(define-syntax with-buffer
  (syntax-rules ()
    ((_ (pstate : buf pt end) code ...)
     (let ((buf (pstate-buffer pstate))
	   (pt  (pstate-pt     pstate))
	   (end (pstate-end    pstate)))
       (call-with-values (lambda () (begin code ...))
	 (lambda (pt end)
	   (pstate-pt-set!  pstate pt)
	   (pstate-end-set! pstate end)))))))

(define-syntax with-buffdata
  (syntax-rules ()
    ((_ (pstate : buf pt end pts pos) code ...)
     (let ((buf (pstate-buffer pstate))
	   (pt  (pstate-pt     pstate))
	   (pts (pstate-pts    pstate))
	   (pos (pstate-pos    pstate))	   
	   (end (pstate-end    pstate)))
       code ...))))

(define-syntax-rule (with-pos (pos : x y) code ...)
  (let ((x (car pos))
	(y (cdr pos)))
    code ...))

(define-inlinable (revert-pos pos x y)
  (set-car! pos x)
  (set-cdr! pos y))
			      

(define (sync pstate)
  (with-buffer (pstate : buffer pt end)
    (if (or (>= pt nbuff) (>= pt end))
	(if (pstate-eof pstate)
	    (values pt end)
	    (let ((port (get-port)))
	      (if (eof-object? (lookahead-u8 port))
		  (begin
		    (pstate-eof-set! pstate #t)
		    (values pt end))
		  (let* ((m (- end      pt))
			 (M (- buffsize m )))

		    (when (> m 0)
		      (bytevector-copy! buffer pt buffer 0 m))
		    
		    (let* ((n (get-bytevector-n! port buffer m M))
			   (P (+ m n))
			   (N (- end P)))
		      (pstate-seek-set! pstate (ftell port))
		      (when (> N 0)
			(bytevector-copy! cleared-bv 0  buffer P N))
		    
		      (values 0 P))))))
	
	(values pt end))))
	
   


(define-inlinable (peek-n flags pstate n)
  (if (flag? flags utf8-flag)
      (bytevector-u8-ref (pstate-buffer pstate) (+ n (pstate-pt pstate)))
      (let ((Sh (ash (logand flags read-sh-mask) (- (read-sh-shift)))))
	(if (= Sh 0)
	    (bytevector-u8-ref (pstate-buffer pstate) (+ n (pstate-pt pstate)))
	    (let ((N (* n (ash 1 Sh)))
		  (D (ash (logand flags read-d-mask) (- (read-d-shift)))))
	      (bytevector-u8-ref (pstate-buffer pstate)
				 (+ N D)))))))

(define (peek-1 flags pstate) (peek-n flags pstate 0))
(define (peek-2 flags pstate) (peek-n flags pstate 1))
(define (peek-3 flags pstate) (peek-n flags pstate 2))

(define-inlinable (skip-n flags pstate n)
  (if (flag? flags utf8-flag)
      (pstate-pt-set! pstate (+ n (pstate-pt pstate)))
      (let ((Sh (ash (logand flags read-sh-mask) (- (read-sh-shift)))))
	(let ((N (* n (ash 1 Sh))))
	  (pstate-pt-set! pstate (+ (pstate-pt pstate) N))))))

(define (skip-1 flags pstate) (skip-n flags pstate 1))
(define (skip-2 flags pstate) (skip-n flags pstate 2))
(define (skip-3 flags pstate) (skip-n flags pstate 3))

(define-inlinable (end-n flags pstate n)
  (define pt  (pstate-pt  pstate))
  (define end (pstate-end pstate))
  (if (>= (+ pt n) end)
      #t
      (let ((i (peek-n flags pstate n)))
	(if (or (eq? i 0) (memq i (I #\newline #\return #\tab #\space)))
	    #t
	    #f))))

(define (end-1 flags pstate) (end-n flags pstate 0))
(define (end-2 flags pstate) (end-n flags pstate 1))
(define (end-3 flags pstate) (end-n flags pstate 2))

(define-syntax-rule (with-sh-d (flags : Sh D) code ...)
  (let ((Sh (ash (logand flags read-sh-mask) (- read-sh-shift)))
	(D  (ash (logand flags read-d-mask ) (- read-d-shift))))
    code ...))
			
(define (match-string flags pstate str)
  (define buffer (pstate-buffer pstate))
  (define pt     (pstate-pt     pstate))
  (define n      (string-length str))
  
  (with-sh-d (flags : Sh D)
    (define N (ash 1 Sh))
    (let lp ((i 0) (pt pt))
      (if (< i n)
	  (if (eq? (bytevector-u8-ref buffer (+ pt D))
		   (char->integer (string-ref str i)))
	      (lp (+ i 1) (+ pt N))
	      #f)
	  (pstate-pt-set! pstate pt)))))
    	     
	      
;; A fast copy from 1 byte to 4 bytes that does not stall the system
;; and is fiber friendly
(define (copy2 from i to j n)
  (define N  (ash n -12))
  (define M  (- (logand n (- (ash 1 12) 1)) 1))
  (define v1 (mk-bin0 from 0 0 0 0                     0 1))
  (define v2 (mk-bin0 to   0 0 0 (logior e-default 2)  0 1))
  (when (> n 0)
    (let lp ((i 0) (j 0))
      (if (< i N)
	  (begin
	    (loop-general-copy #f v1 j (+ j 4096) 1 v2 j (+ j 4096) 1)
	    (lp (+ i 1) (+ j 4096)))
	  (loop-general-copy #f v1 j (+ j M) 1 v2 j (+ j M) 1)))))

(define (make-bv float sgn size i val)
  (if float
      (if (= size 2)
	  (make-f32vector i val)
	  (make-f64vector i val))
      (if sgn
	  (case size
	    ((0) (make-s8vector  i val))
	    ((1) (make-s16vector i val))
	    ((2) (make-s32vector i val))
	    ((3) (make-s64vector i val)))
	  (case size
	    ((0) (make-u8vector  i val))
	    ((1) (make-u16vector i val))
	    ((2) (make-u32vector i val))
	    ((3) (make-u64vector i val))))))


(define (end? pstate)
  (and (pstate-eof pstate)
       (= (pstate-pt pstate) (pstate-end pstate))))

(define (read-spaces flags pstate)
  (define pts    (pstate-pts    pstate))
  (define pos    (pstate-pos    pstate))
  (define buffer (pstate-buffer pstate))
  (let lp ((found #f))
    (sync pstate)
    (set-car! pts (pstate-pt pstate))
    (let ((fnd (c-read-spaces buffer (pstate-end pstate) pts pos flags)))
      (pstate-pt-set! pstate (car pts))
      (let ((e (cdr pts)))
	(if (= e 0)
	    (lp (or found fnd))
	    (or found fnd))))))

(define (read-bytevector i flags pstate)
  (define j (peek-1 flags pstate))
  (skip-1 flags pstate)

  (if (memq j (I #\u #\s #\f))
      ((read-srfi-4-vector j) flags pstate)
      (error "wrong type of bytevector in #vx... (read)")))

;; TODO Fix this for utf16 utf32
(define (read-number* flags pstate)
  (define (sstring->number str flags)
    (define one (if (flag? flags read-real-flag) 1.0 1))
    (define b   (case (ash (logand flags read-bins-mask) (- read-bins-shift))
		  ((0) 10)
		  ((1) 2)
		  ((2) 4)
		  ((3) 8)
		  ((4) 16)
		  ((5) 32)))
    (* one (string->number str b)))

  (sync pstate)
  (with-bytevector (pstate : buffer pt end pts pos bv)
    (define seek-pos (pstate-seek pstate))
    (define oldbuf   #f)
    (define (undo)
      (seek (get-port) seek-pos SEEK_SET)
     
      (pstate-buffer-set! pstate (if oldbuf oldbuf buffer))
      (pstate-end-set!    pstate end)
      (pstate-pt-set!     pstate pt))
   
    (define (fail)
      (read-symbol flags pstate))
  
    (define (revert)
      (undo)
      (if (flag? flags binary-flag)
	  #f
	  (fail)))

    (define (synct pt3)
      (when (not oldbuf)
	(set! oldbuf (bytevector-copy buffer)))
      (pstate-pt-set! pstate pt3)
      (sync pstate))

    
    (let lp ((i 0) (bv bv) (pt2 pt) (n (bytevector-length bv)) (state 0))
      (set-car! pts pt)
      (let ((state (c-numerati buffer nbuff pts flags state)))
	(let ((pt3 (car pts))
	      (e   (cdr pts)))
	  (let lp2 ((e e))
	    (case e
	      ((0 1)
	       (let* ((m (- pt3 pt2))
		      (M (+ m i))
		      (N (min M n))
		      (k (- M N)))
		 (bytevector-copy! buffer pt2 bv i N)
		 (let* ((p (+ i   (- m k)))
			(q (+ pt2 (- m k)))
			(s (make-string M #\a)))	      

		   (if (> k 0)
		       (begin
			 (copy-str bv     0 s 0 p)
			 (copy-str buffer q s p k))
		       (copy-str bv 0 s 0 M))
		 
		   (aif num (sstring->number s flags)
			(begin
			  (pstate-pt-set! pstate pt3)
			  (set-car! pos (+ (car pos) M))
			  num)
			(revert)))))
		     
	    	  ;; complex (+)

	      ((4)
	       (if (flag? flags binary-flag)
		   (revert)
		   (aif a (lp2 1)
			(aif b (read-number (logior flags
						    binary-flag
						    complex-flag
						    read-real-flag)
					    pstate)
		     
			     (make-rectangular a b)
			     
			     (revert))
			(revert))))
	    
	      ;; rational (/)
	      ((5)
	       (if (flag? flags binary-flag)
		   (revert)
		   (aif a (lp2 0)
			(aif b (begin
				 (skip-1 flags pstate)
				 (read-number (logior flags binary-flag)
					      pstate))
			     (if (integer? b)
				 (/ a b)
				 (revert))
			     (revert))
			(revert))))
	  
	      ;; i
	      ((6)
	       (if (flag? flags complex-flag)
		   (aif b (lp2 1)
			b
			#f)
		   (revert)))

	      ((2 3 7)
	       (revert))

	      ((8)
	       (let* ((m (- pt3 pt2))
		      (M (+ m i))
		      (N (min M n))
		      (k (- M N)))
		 (if (> k 0)
		     (let* ((n2   (* n 2))
			  (bv2  (make-bytevector n2 0)))
		       (bytevector-copy! bv 0 bv2 0 i)
		       (bytevector-copy! buffer pt bv i M)
		       (synct pt3)
		       (lp (+ i M) bv2 (pstate-pt pstate) n2 state))
		     (begin
		       (bytevector-copy! buffer pt bv i M)
		       (synct pt3)
		       (lp (+ i M) bv (pstate-pt pstate)  n state))))))))))))
	   
  
(define (read-symbol flags pstate)
  (string->symbol (read-string0 flags pstate 0 #t)))

(define (butify x n)
    (pointer->bytevector (make-pointer (c-butify x)) n 0))
  
;; A fast copy from bytevector to string
(define (copy-str from i to j n)
  (bytevector-copy! from i (butify to n) j n))

(define (read-number+ flags pstate)
  (skip-1 flags pstate)
  (let ((x (read-number0 flags pstate)))
    (if (number? x)
	x
	(string->symbol (string-append "+" (symbol->string x))))))

(define (read-number- flags pstate)
  (skip-1 flags pstate)
  (let ((x (read-number0 flags pstate)))
    (if (number? x)
	(- x)
	(string->symbol (string-append "-" (symbol->string x))))))

(define (read-number0 flags pstate)
  (let ((ch (peek-1 flags pstate)))
    (cond
     ((eq? ch (I #\#))
      (let ((ch (peek-2 flags pstate)))
	(if (memq ch (I #\i #\e #\b #\B #\o #\O #\d #\D #\x #\X #\I
			#\E #\y #\Y))
	    (begin
	      (skip-2 flags pstate)
	      (read-number-and-radix ch flags pstate))
	    (read-symbol flags pstate))))

     ((eq? ch (I #\+))
      (read-number+ flags pstate))

     ((eq? ch (I #\-))
      (read-number- flags pstate))

     (else
      (read-number flags pstate)))))

	  
(define (read-number flags pstate)
  (define bits (ash (logand flags read-bins-mask) (- read-bins-shift)))  
  (sync pstate)
  (with-buffdata (pstate : buffer pt end pts pos)
    (define (undo)
      (pstate-pt-set! pstate pt))
   
    (define (fail)
      (read-symbol flags pstate))
  
    (define (revert)
      (if (flag? flags binary-flag)
	  #f
	  (begin
	    (undo)
	    (fail))))

    (set-car! pts pt)
    (c-numerati buffer nbuff pts flags 0)
    
    (let ((e (cdr pts)))
      (let lp2 ((e e))
	(case e
	  ((0)
	   ;; Found an integer
	   (set-car! pts pt)
	   (let ((num (c-read-integer buffer pts pos
				      (logior
				       flags
				       (ash 3 read-bits-shift)))))
	     (pstate-pt-set! pstate (car pts))
	     (if (eq? (cdr pts) 0)
		 (revert)
		 num)))

	  ;; Found a real 
	  ((1)
	   (set-car! pts pt)
	   (let ((num (c-read-double buffer pts pos flags)))
	     (pstate-pt-set! pstate (car pts))
	     (if (= (cdr pts) 0)
		 (revert)
		 num)))

	  ;; complex (+)
	  ((4)
	   (if (flag? flags binary-flag)
	       #f
	       (aif a (lp2 1)
		    (aif b (read-number (logior flags
						binary-flag
						complex-flag
						read-real-flag)
					pstate)
			 (make-rectangular a b)			 
			 (revert))
		    (revert))))

	  ;; rational (/)
	  ((5)
	   (if (flag? flags binary-flag)
	       #f
	       (aif a (lp2 0)
		    (aif b (begin
			     (skip-1 flags pstate)
			     (read-number (logior flags binary-flag) pstate))
			 (if (integer? b)
			     (/ a b)
			     (revert))
			 (revert))
		    (revert))))
	  
	  ;; i
	  ((6)
	   (if (flag? flags complex-flag)
	       (aif b (lp2 1)
		    b
		    #f)
	       (revert)))
	
	  ;; Buffer too small (a large integer/real)
	  ((8)	   
	   (read-number* flags pstate))

	  ;; Malformed number => symbol
	  ((2 3 7)
	   (fail)))))))
	
	
	
	 
	 
(define (read-number-and-radix i flags pstate)
  (case (integer->char i)
    ((#\e #\E)
     (read-number flags pstate))

    ((#\b #\B)
     (read-number (logior flags (ash 1 read-bins-shift)) pstate))

    ((#\o #\O)
     (read-number (logior flags (ash 3 read-bins-shift)) pstate))

    ((#\x #\X)
     (read-number (logior flags (ash 4 read-bins-shift)) pstate))

    ((#\y #\Y)
     (read-number (logior flags (ash 5 read-bins-shift)) pstate))

    ((#\i #\I)
     (read-number (logior flags read-real-flag) pstate))))


(define (read-parenthesized end flags pstate vector?)
  (read-spaces flags pstate)
  (let lp ((l '()))
    (cond
     ((eq? (peek-1 flags pstate) end)
      (skip-1 flags pstate)
      (if vector?
	  (list->vector (reverse! l))
	  (reverse! l)))

     (else
      (let ((x (read0 flags pstate)))
	(if (read-spaces flags pstate)
	    (lp (cons x l))
	    (lp (cons x l))))))))
	

(define (read-unquote flags pstate)
  (list 'unquoteflags 
	(read-subexpression flags pstate "unquote expression")))

(define (read-unquote-splicing flags pstate)
  (list 'unquote-splicing
	(read-subexpression flags pstate "unquote splicing expression")))

(define (read-comment flags pstate)
  (pk 'read-comment))

(define (read-unsyntax-splicing flags pstate)
  (list 'unsyntax-splicing
	(read-subexpression flags pstate "unsyntax-splicing expression")))

(define (read-unsyntax flags pstate)
  (list 'unsyntax
	(read-subexpression flags pstate "unsyntax expression")))

(define (read-nil i flags pstate)
  (if (match-string flags pstate "il")
      (if (read-spaces flags pstate)
	  #nil
	  (error "#nil does not end with a white space (read)"))
      (error "#nil is not matched (read)")))

(define (read-character flags pstate)
  (pk 'read-character))

(define (read-bitvector flags pstate)
  (pk 'read-bitvector))

(define (read-extended-symbol i flags pstate)
  (string->symbol (read-string0 flags pstate 3 #t)))

(define (read-subexpression flags pstate what)
  (read-spaces flags pstate)
  (if (pstate-eof pstate)
      (error (string-append "unexpected end of input while reading " what))
      (read0 flags pstate)))

(define (read-syntax-expression i flags pstate)
  (list 'syntax (read-subexpression flags pstate
				    "syntax expression")))

(define (read-quasisyntax-expression i flags pstate)
  (list 'quasi-syntax (read-subexpression flags pstate
					  "quasi syntax expression")))

(define (read-keyword i flags pstate)
  (symbol->keyword (string->symbol (read-string0 flags pstate 0 #t))))

(define (read-quoted-subexpression flags pstate)
  (skip-1 flags pstate)
  (list 'quote (read-subexpression flags pstate "quoted expression")))

(define (read-quasi-quoted-subexpression flags pstate)
  (skip-1 flags pstate)
  (list 'quasiquote (read-subexpression flags pstate "quasiquoted expression")))

(define (read-r7rs-string flags pstate)
  (string->symbol (read-string0 flags pstate 2 #t)))

(define (read-array flags pstate type tag rank shape bits)
  (pk 'read-array))

(define (read-false i flags pstate)
  (if (end-1 flags pstate)
      #f
      (if (match-string flags pstate "alse")
	  (if (end-1 flags pstate)
	      #f
	      (error "#true does not end correctly (read)"))	  
	  (error "#F does not end correctly (read)"))))
  
(define (read-true i flags pstate)
  (if (end-1 flags pstate)
      #t
      (if (match-string flags pstate "rue")
	  (if (end-1 flags pstate)
	      #t
	      (error "#true does not end correctly (read)"))	       
	  (error "#T does not end correctly (read)"))))

(define (read-false-or-srfi-4-vector i flags pstate)
  ;; We read #f we must alow us to parse  #false, #f32 or #f64
  (let ((i (peek-3 flags pstate)))
    (if (or (eq? i (I #\3))
	    (eq? i (I #\6))
	    (eq? i (I #\8)))

	(begin
	  (skip-2 flags pstate)
	  ((read-srfi-4-vector #\f) 0 flags pstate))
	
	(begin
	  (skip-2 flags pstate)
	  (if (match-string flags pstate "alse")
	      (if (end-1 flags pstate)
		  #f
		  (error "#false does not end correctly (in read)"))
	      (if (end-1 flags pstate)
		  #f
		  (error "#f does not continue correctly (in read)")))))))


(define (sharp-custom i flags pstate)
  (let ((ch (integer->char i)))
    (aif proc (read-hash-procedure ch)
	 (proc ch (get-port))
	 (error "Unknown # object: ~S" i))))

(define (read-string0 flags pstate kind sym)
  (with-bytevector (pstate : buffer pt end pst pos strbuff1)
    (let lp ((i       0)
	     (strbuff strbuff1)
	     (n       (bytevector-length strbuff1)) (N 1)
	     (first?  sym))
      (define (grow i)
	(let* ((n2       (* 2  n))
	       (m        (* n2 N))
	       (strbuff* (make-bytevector m 0)))
	  (bytevector-copy! strbuff 0 strbuff* 0 (* i N))
	  (lp i strbuff* n2 N #f)))

      (define (enlarge i)
	(let* ((n2       (* 4 n))
	       (strbuff* (make-bytevector n2 0)))	 
	  (copy2 strbuff 0 strbuff* 0 i)
	  (lp i strbuff* n 4 #f)))

      (sync pstate)

      (let ()
	(define pt    (pstate-pt pstate))
	(define count (- nbuff pt))
	(set-car! pst i)
	(set-cdr! pst count)      
	(let ((d  (c-read-string strbuff (- n 32) buffer pt pst
				 pos flags N kind)))
	  (let ((pt (+ pt d))
		(i  (car pst))
		(e  (cdr pst)))
	    (pstate-pt-set! pstate pt);

	    (case e
	      ((1)
	       ;;Finished
	       (cond
		(first?
		 (substring/shared (pstate-str pstate) 0 i))
	       
		((= N 1)
		 (let ((ret (make-string i #\a)))
		   (copy-str strbuff 0 ret 0 i)
		   ret))

		(else
		 (let ((ret (make-string i #\⊕)))
		   (copy-str strbuff 0 ret 0 (* i 4))
		   ret))))


	      ((2)
	       ;; enlarge
	       (enlarge i))
	      
	      ((3)
	       (cond
		((>= i (- n 32))
		 (grow i))
		(else
		 (lp i strbuff n N first?))))
	      
	      ((4)
	       ;; hungry eol
	       ;; TODO
	       (values)))))))))

(define (read-string flags pstate)
  (skip-1 flags pstate)
  (let ((s (read-string0 flags pstate 1 #f)))
    (skip-1 flags pstate)
    s))

(define (read-vector1 flags pstate)
  (with-vector (pstate : buffer pst pos vctbuff1)
    (let lp ((i 0) (vctbuff vctbuff1) (n (vector-length vctbuff1)))
      (define (grow i)
	(let* ((n2       (* 2 n))
	       (vctbuff* (make-vector n2 #f)))
	  (vector-copy! vctbuff 0 vctbuff* 0 i)
	  (lp i vctbuff* n2)))
      
      (set-car! pst (pstate-pt pstate))
      (let ((d (c-read-vector buffer vctbuff pst pos n flags)))
	(let ((i (+ i d))
	      (e (cdr pst)))
	  (sync pstate)	     
	  (case e
	    ((0)
	     (read-spaces flags pstate)
	     (let ((d (read0 flags pstate)))
	       (vector-set! vctbuff i d)
	       (lp (+ i 1) vctbuff n)))
	    
	    ((1)
	     (grow i))
	    	    
	    ((2)
	     (let ((i (+ i d)))
	       (let ((ret (make-vector i #f)))
		 (vector-copy! vctbuff 0 ret 0 i)
		 ret)))))))))
(define (read-vector flags pstate)
  (skip-1 flags pstate)
  (read-parenthesized (I #\)) flags pstate #t))

(define (read-fixnum-10 flags pstate)
  (define pts    (pstate-pts    pstate))
  (define pos    (pstate-pos    pstate))
  (define buffer (pstate-buffer pstate))
  (set-car! pts (pstate-pt pstate))
  (let ((num (c-read-integer buffer pts pos flags)))
    (pstate-pt-set! pstate (car pts))
    (if (eq? (cdr pts) 0)
	(error "not a fixnum")
	num)))

(define (read-srfi-4-vector type)
  (lambda (i flags pstate)    
    (define (read-rank)
      (read-fixnum-10 flags pstate))

    (define (read-tag)
      (let lp ((i (peek-1 flags pstate)) (chars '()))
        (if (memq i (I #\( #\@ #\:))
	    (if (null? chars)
		#t
		(string->symbol
		 (list->string
		  (reverse! chars))))
	    (begin
	      (skip-1 flags pstate)
	      (lp (peek-1 flags pstate) (cons (integer->char i) chars))))))

    (define (read-dimension)
      (let* ((i    (peek-1 flags pstate))
	     (lbnd (if (eq? i (I #\@))
		       (begin
			 (skip-1 flags pstate)
			 (read-fixnum-10 flags pstate))
                       0))
	     (i    (peek-1 flags pstate))
	     (len  (if (eq? i (I #\:))
		       (begin
			 (skip-1 flags pstate)
			 (read-fixnum-10 flags pstate))
		       #f)))
	(if len
	    (list lbnd (+ lbnd (1- len)))
	    lbnd)))

    (define (read-shape alt)
      (define i (peek-1 flags pstate))	
      (if (memq i (I #\@ #\:))
	  (let ((head (read-dimension))
		(tail (read-shape '())))
	    (cons head tail))
	  alt))

    (let* ((rank  (read-rank))
	   (tag   (read-tag))
	   (i     (peek-1 flags pstate)))
      (if (eq? i (I #\())
	  (let ((nbits (if (eq? tag #t)
			   0
			   (case tag
			     ((x) 4)
			     ((o) 3)
			     ((y) 5)
			     ((b) 1)
			     (else #f)))))

	    (if nbits
		(begin
		  (skip-1 flags pstate)
		  (cond
		   ((eq? type #\u)
		    (read-bytevector* flags pstate #f #f rank nbits))
		   ((eq? type #\s)
		    (read-bytevector* flags pstate #f #t rank nbits))
		   ((eq? type #\f)
		    (read-bytevector* flags pstate #t #f rank nbits))
		   ((eq? type #\c)
		    (error "got array type c - can't be a bytevector"))
		   (else
		    (error "BUG unkown array type ~a" type))))
		(read-array flags pstate type rank tag 0 0)))
	  
	  (let ((shape (read-shape '())))
	    (skip-1 flags pstate)
	    (read-array flags pstate type rank tag shape 0))))))
      
      

    
(define (read-bytevector* flags pstate float sgn size nbits)
  (define N #f)
  (set! size (case size
	       ((8)  0)
	       ((16) 1)
	       ((32) 2)
	       ((64) 3)
	       (else (error "unknown size ~a (read)~%" size))))

  (set! N (ash 1 size))
  
  (set! flags (logior (logand flags (lognot numeric-mask))
		      (if  float read-real-flag 0)
		      (if  sgn   read-sgn-flag  0)
		      (ash size  read-bits-shift)
		      (ash nbits read-bins-shift)))

  (with-bytevector (pstate : buffer pt end pst pos bvctbuff1)
    (let lp ((i 0) (bvctbuff bvctbuff1) (n (bytevector-length bvctbuff1)))
      (define (grow i)
	(let* ((n2        (* 2 n))
	       (bvctbuff* (make-bytevector n2 #f)))
	  (bytevector-copy! bvctbuff 0 bvctbuff* 0 i)
	  (lp i bvctbuff* n2)))
      
      (set-car! pst (pstate-pt pstate))

      (let ((d (c-read-bytevector buffer nbuff bvctbuff i n pst pos flags)))
	(pstate-pt-set! pstate (car pst))
	(sync pstate)
	(let ((i (+ i d))
	      (e (cdr pst)))
	  (case e
	    ((0)
	     (read-spaces flags pstate)
	     (let ((d (read-number* flags pstate)))
	       (cond
		(float
		 (if (eq? size 2)
		     (bytevector-ieee-single-set! bvctbuff i d
						  (native-endianness))
		     (bytevector-ieee-double-set! bvctbuff i d
						  (native-endianness))))

		(sgn
		 (case size
		   ((0) (bytevector-s8-set!  bvctbuff i d))
		   ((1) (bytevector-s16-set! bvctbuff i d (native-endianness)))
		   ((2) (bytevector-s32-set! bvctbuff i d (native-endianness)))
		   ((3) (bytevector-s64-set! bvctbuff i d
					     (native-endianness)))))

		(else
		 (case size
		   ((0) (bytevector-u8-set!  bvctbuff i d))
		   ((1) (bytevector-u16-set! bvctbuff i d (native-endianness)))
		   ((2) (bytevector-u32-set! bvctbuff i d (native-endianness)))
		   ((3) (bytevector-u64-set! bvctbuff i d
					     (native-endianness))))))
	       
	       (lp (+ i 1) bvctbuff n)))
	    
	    ((2 4)
	     (grow i))

	    ((3)
	     (lp i bvctbuff n))
	    	    
	    ((1)
	     (let* ((n   (* i N))
		    (ret (make-bv float sgn size i 0)))
	       (bytevector-copy! bvctbuff 0 ret 0 n)		 
	       ret))))))))

(define (read-list flags pstate)
  (skip-1 flags pstate)
  (read-parenthesized (I #\)) flags pstate #f))

(define (read-list1 flags pstate)
  (skip-1 flags pstate)
  (define pts    (pstate-pts    pstate))
  (define pos    (pstate-pos    pstate))
  (define buffer (pstate-buffer pstate))
  
  (let lp ((x '()))
    (set-car! pts (pstate-pt pstate))    
    (let ((x (c-read-list buffer x pts pos flags)))
      (let ((e (cdr pts)))
	(sync pstate)    
	  (case e
	    ((0)
	     ;; We need to read it in schm land
	     (read-spaces flags pstate)
	     (let ((ch (peek-1 flags pstate)))
	       (cond
		((eq? ch (I #\)))
		 (skip-1 flags pstate)
		 (reverse! x))
		
		((eq? ch (I #\.))
		 (skip-1 flags pstate)
		 (read-spaces flags pstate)
		 (let ((cd (read0 flags pstate)))
		   (read-spaces flags pstate)
		   (when (not (eq? (peek-1 flags pstate) (I #\))))
		     (error "wrong right parnathesis in reader"))
		   (skip-1 flags pstate)
		   (let lp ((x x) (res cd))
		     (if (pair? x)
			 (lp (cdr x) (cons (car x) res))
			 res))))

		(else
		 (let ((d (read0 flags pstate)))
		   (read-spaces flags pstate)
		   (lp (cons d x)))))))
	    	       	    
	    ((1)
	     ;; need a synch
	     (lp x)))))))


(define table-2 (make-vector 256 sharp-custom))

(define-syntax set-2
  (syntax-rules ()
    ((_ (ch ...) lam)
     (let ((l lam))
       (begin (set-2 ch l) ...)))
    ((_ ch lam)
     (vector-set! table-2 (char->integer ch) lam))))

(define (skip-2* lam)
  (lambda (i flags pstate)
    (skip-2 flags pstate)
    (lam i flags pstate)))

(set-2 #\\  (skip-2* read-character)              )
(set-2 #\(  (skip-2* read-vector)                 )
(set-2 #\s  (skip-2* (read-srfi-4-vector #\s))    )
(set-2 #\u  (skip-2* (read-srfi-4-vector #\u))    )
(set-2 #\c  (skip-2* (read-srfi-4-vector #\c))    )
(set-2 #\f  read-false-or-srfi-4-vector           )
(set-2 #\v  (skip-2* read-bytevector)             )
(set-2 #\*  (skip-2* read-bitvector)              )
(set-2 #\t  (skip-2* read-true)                   )
(set-2 #\T  (skip-2* read-true)                   )
(set-2 #\F  (skip-2* read-false)                  )
(set-2 #\:  (skip-2* read-keyword)                )
(set-2 #\{  (skip-2* read-extended-symbol)        )
(set-2 #\'  (skip-2* read-syntax-expression)      )
(set-2 #\`  (skip-2* read-quasisyntax-expression) )
(set-2 #\n  (skip-2* read-nil)                    )

(set-2 (#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\@)
       read-array)

(set-2 (#\i #\e #\b #\B #\o #\O #\d #\D #\x #\X #\I #\E #\y #\Y)
       (skip-2* read-number-and-radix))

(set-2 #\,
  (lambda (i flags pstate)
    (if (eq? (I #\@) (peek-3 flags pstate))
	(begin (skip-3 flags pstate) (read-unsyntax-splicing flags pstate))
	(begin (skip-2 flags pstate) (read-unsyntax          flags pstate)))))

(define (read-sharp flags pstate)
  (let ((i (peek-2 flags pstate)))
    (if (< i 256)	
	((vector-ref table-2 i) i flags pstate)
	(sharp-custom i flags pstate))))
  


(define table-1 (make-vector 256 read-symbol))

(define-syntax set-1
  (syntax-rules ()
    ((_ (ch ...) lam)
     (begin (set-1 ch lam) ...))
    ((_ ch lam)
     (vector-set! table-1 (char->integer ch) lam))))

(set-1 #\; read-comment)

(set-1 #\{
  (lambda (flags pstate)
    (cond
     ((flag? flags curly-infix-flag)
      (neoteric++ pstate)
      (let ((expr (read-parenthesized (I #\}) flags pstate #f)))
	(neoteric-- pstate)
	expr))
     (else
      (read-symbol flags pstate)))))

(set-1 #\[
   (lambda (flags pstate)
    (cond
     ((flag? flags square-brackets-flag)
      (read-parenthesized (I #\]) flags pstate #f))
     ((flag? flags curly-infix-flag)
      ;; The syntax of neoteric expressions requires that '[' be a
      ;; delimiter when curly-infix is enabled, so it cannot be part
      ;; of an unescaped symbol.  We might as well do something
      ;; useful with it, so we adopt Kawa's convention:  [...] =>
      ;; ($bracket-list$ ...)
      ;; FIXME: source locations for this cons
      (cons '$bracket-list$ (read-parenthesized (I #\]) flags pstate #f)))
     (else
      (read-symbol flags pstate)))))

(set-1 #\( read-list)
(set-1 #\" read-string)
(set-1 #\|
  (lambda (flags pstate)
    (if (flag? flags r7rs-symbols-flag)
	(string->symbol (read-r7rs-string flags pstate))
	(read-symbol flags pstate))))

(set-1 #\' read-quoted-subexpression)

(set-1 #\` read-quasi-quoted-subexpression)

(set-1 #\,
  (lambda (flags pstate)
    (cond
     ((eq? (I #\@ ) (peek-2 flags pstate))
      (skip-2 flags pstate)
      (read-unquote-splicing flags pstate))
     (else
      (skip-1 flags pstate)
      (read-unquote flags pstate)))))

(set-1 #\# read-sharp)

(set-1 #\)
   (lambda (flags pstate)
     (error "unexpected \")\"")))

(set-1 #\}
  (lambda (flags pstate)
    (if (flag? flags curly-infix-flag)
	(error "unexpected \"}\"")
	(read-symbol flags pstate))))

(set-1 #\]
  (lambda (flags pstate)       
    (if (flag? flags square-brackets-flag)
	(error "unexpected \"]\"")
	(read-symbol flags pstate))))

(set-1 #\:
  (lambda (flags pstate)
    (if (eq? (logand flags keyword-style-mask) keyword-style-prefix)
	(begin
	  (skip-1 flags pstate)
	  (let ((sym (read-symbol flags pstate)))
	    (symbol->keyword sym)))
	(read-symbol flags pstate))))

(set-1 (#\0 #\1 #\2 #\3 #\4 #\5 #\6 #\7 #\8 #\9 #\.) read-number)
(set-1 #\+  read-number+)
(set-1 #\-  read-number-)

(define ia (char->integer #\a))

(define-inlinable (read000 flags pstate)
  (read-spaces flags pstate)
  (if (end? pstate)
      eof-object
      (let* ((i (peek-1 flags pstate))
	     (i (if (< i 256) i ia)))
	((vector-ref table-1 i) flags pstate))))

(define-inlinable (read00 flags pstate)
  (if (> (logand read-neoteric-flag flags) 0)
      (read-neoteric flags pstate)
      (read000 flags pstate)))


(define (read0 flags pstate)
  (define (annotate line column datum)
    ;; Usually when reading compound expressions consisting of multiple
    ;; syntax objects, like lists, the "leaves" of the expression are
    ;; annotated but the "root" isn't.  Like in (A . B), A and B will be
    ;; annotated but the pair won't.  Therefore the usually correct
    ;; thing to do is to just annotate the result.  However in the case
    ;; of reading ( . C), the result is the already annotated C, which
    ;; we don't want to re-annotate.  Therefore we avoid re-annotating
    ;; already annotated objects.
    (if (syntax? datum)
        datum
	(let ((filename (port-filename (get-port))))
	  (datum->syntax #f ; No lexical context.
			 datum
			 #:source (vector filename line (1- column))))))

  (if (> (logand flags record-positions-flag) 0)
      (let* ((pos (pstate-pos pstate))
	     (m   (car pos))
	     (n   (cdr pos))
	     (x   (read00 flags pstate)))
	(annotate n m x))
      (read00 flags pstate)))

(define read-buffers (make-thread-local-fluid '()))
(define read-hashes  (make-thread-local-fluid '()))

(define-syntax-rule (with-pstate-data (buffdata hash) code ...)
  (let* ((buffers  (fluid-ref read-buffers))
	 
	 (buffdata (make-buffdata
		    #:buffer
		    (if (pair? buffers)
			(let ((buff (car buffers)))
			  (bytevector-copy! cleared-bv 0 buff 0 buffsize)
			  buff)
			
			(let ((buff (make-read-buffer)))
			  (fluid-set! read-buffers (cons buff buffers))
			  buff))))
	 
	 (hashes   (fluid-ref read-hashes))

	 (hash     (if (pair? hashes)
		       (car hashes)
		       (let ((hashtab (make-hash-table)))
			 (fluid-set! read-hashes (cons hashtab hashes))
			 hashtab))))
    (with-fluids ((read-buffers (cdr (fluid-ref read-buffers)))
		  (read-hashes  (cdr (fluid-ref read-hashes))))
	code ...)))

(define port->pstate-maps (make-thread-local-fluid #f))

(define (get-port-pstate port)
  (aif it (fluid-ref port->pstate-maps)
       (aif it2 (hash-ref it port)
	    (fluid-ref it2)
	    (begin
	      (hash-set! it port (make-fluid #f))
	      #f))
       (let ((h (make-weak-key-hash-table)))
	 (hash-set! h port (make-fluid #f))
	 (fluid-set! port->pstate-maps h)
	 #f)))

(define-syntax-rule (with-pstate (port : pstate) code ...)
  (with-fluids (((hash-ref (fluid-ref port->pstate-maps) port) pstate))
    code ...))

(define (get-encoding port flags)
  (define p (if (soft-port? port) (slot-ref port 'port) port))
  (define detailed (lookup-2-scm p 2 9))
  (define conv     (lookup-2-scm p 2 8))
  
  (let* ((enc (if detailed
		  detailed
		  (string->symbol (port-encoding p)))))
    (logior
     (logand (lognot encoding-mask) flags)
          
     (case enc
       ((UTF-8)      utf8-flag)
       ((ISO-8859-1) 0)
       ((UTF-16)     (logior (ash 1 read-sh-shift)
			     (ash 1 read-d-shift)))
       ((UTF-32)     (logior (ash 2 read-sh-shift)
			     (ash 3 read-d-shift)))
       ((UTF-16LE)   (ash 1 read-sh-shift))
       ((UTF-32LE)   (ash 2 read-sh-shift))
       ((UTF-16BE)   (logior (ash 1 read-sh-shift)
			     (ash 1 read-d-shift)))
       ((UTF-32BE)   (logior (ash 2 read-sh-shift)
			     (ash 3 read-d-shift)))
       (else
	(error "Unsupported flags encoding"))))))

(define* (read #:optional (port (current-input-port))
               #:key
	       (syntax?  #f)
               (inherit? #t))
    
  (define-syntax-rule (mk pstate flush? line)
    (let* ((pstate line))
      (with-pstate (port : pstate)
        (with-fluids ((current-read-port port))
	  (dynamic-wind
	      (lambda () (values))
	      (lambda ()
		(sync pstate)
		(read0 (get-encoding port (pstate-flags pstate))
		       pstate))
	      (lambda ()
		(hash-clear! (pstate-hash pstate))
		(clear-bv pstate)))))))

      
  (aif pstate (get-port-pstate port)    
       (cond
	((eq? inherit? #f)
	 (with-pstate-data (buffdata hash)
	   (mk pstate #t
	       (make-pstate 
		buffdata hash
		#:flags (make-flags #:inherited? #f #:syntax? syntax?)))))
	  
	((flag? (pstate-flags pstate) inherited-flag)
	 (mk pstate #f
	     (pstate-copy pstate)))
	  
	((eq? inherit? #t)
	 (mk pstate #t (pstate-copy pstate
				    #:inherited? #t)))
				      
	  
	(else
	 (with-pstate-data (buffdata hash)
	   (mk pstate #t
	       (pstate-copy pstate
			    #:hash     hash
			      #:buffdata buffdata)))))

       
       (with-pstate-data (buffdata hash)
	 (mk pstate #t
	     (make-pstate buffdata hash
			  #:flags
			  (make-flags #:inherited? inherit?
				      #:syntax?    syntax?))))))

(define* (read-syntax #:optional (port (current-input-port))
		      #:key      (inherit? #f))
  (read port #:syntax? #t #:inherit? inherit?))

(define flags-bindings
  '(((ice-9 read) read read-syntax)))

(define read-installed? #f)
(define saved-flags-bindings #f)

(define (install-read!)
  (set! read-installed? #t)
  (unless saved-flags-bindings
    (set! saved-flags-bindings (make-hash-table))
    (let ((suspendable-flagss (resolve-module '(ice-9 stis-read))))
      (for-each
       (match-lambda
	 ((mod . syms)
          (let ((mod (resolve-module mod)))
            (for-each (lambda (sym)
                        (hashq-set! saved-flags-bindings sym
                                    (module-ref mod sym))
                        (module-set! mod sym
                                     (module-ref suspendable-flagss sym)))
                      syms))))
       flags-bindings))))

(define (uninstall-read!)
  (set! read-installed? #f)
  (when saved-flags-bindings
    (for-each
     (match-lambda
       ((mod . syms)
        (let ((mod (resolve-module mod)))
          (for-each (lambda (sym)
                      (let ((saved (hashq-ref saved-flags-bindings sym)))
                        (module-set! mod sym saved)))
                    syms))))
     flags-bindings)
    (set! saved-flags-bindings #f)))
