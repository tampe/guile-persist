;;; Ports, implemented in Scheme
;;; Copyright (C) 2016, 2019 Free Software Foundation, Inc.
;;;
;;; This library is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; This library is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with this program.  If not, see
;;; <http://www.gnu.org/licenses/>.

;;; Commentary:
;;;
;;; We would like to be able to implement green threads using delimited
;;; continuations.  When a green thread would block on I/O, it should
;;; suspend and arrange to be resumed when it can make progress.
;;;
;;; The problem is that the ports code is written in C.  A delimited
;;; continuation that captures a C activation can't be resumed, because
;;; Guile doesn't know about the internal structure of the C activation
;;; (stack frame) and so can't compose it with the current continuation.
;;; For that reason, to implement this desired future, we have to
;;; implement ports in Scheme.
;;;
;;; If Scheme were fast enough, we would just implement ports in Scheme
;;; early in Guile's boot, and that would be that.  However currently
;;; that's not the case: character-by-character I/O is about three or
;;; four times slower in Scheme than in C.  This is mostly bytecode
;;; overhead, though there are some ways that compiler improvements
;;; could help us too.
;;;
;;; Note that the difference between Scheme and C is much less for
;;; batched operations, like read-bytes or read-line.
;;;
;;; So the upshot is that we need to keep the C I/O routines around for
;;; performance reasons.  We can still have our Scheme routines
;;; available as a module, though, for use by people working with green
;;; threads.  That's this module.  People that want green threads can
;;; even replace the core bindings, which enables green threading over
;;; other generic routines like the HTTP server.
;;;

(define-module (ice-9 soft-suspendable-ports)
  #:use-module (rnrs bytevectors)
  #:use-module (oop goops)
  #:use-module (ice-9 ports internal)
  #:use-module (ice-9 match)
  #:use-module (ice-9 soft-port)
  #:export (current-read-waiter
            current-write-waiter

	    soft-suspendable-ports-installed?
            install-soft-suspendable-ports!
            uninstall-soft-suspendable-ports!))

(define %port-closed? (@ (guile) port-closed?))

(define %%port? (@ (guile) port?))
(define (%port? port)
  (or (%%port? port) (is-a? port <port>)))

(define %input-port?  (@ (guile) input-port?))
(define %output-port? (@ (guile) output-port?))

(define (port? x) (or (%port? x) (soft-port? x)))

(define (default-read-waiter  port)
  (if (soft-port? port)
      (soft-port-poll port "r")
      (port-poll port "r")))

(define (default-write-waiter port)
  (if (soft-port? port)
      (soft-port-poll port "w")
      (port-poll port "w")))


(define (soft-default-read-waiter  port) (soft-port-poll port "r"))
(define (soft-default-write-waiter port) (soft-port-poll port "w"))

(define current-read-waiter  (make-parameter default-read-waiter))
(define current-write-waiter (make-parameter default-write-waiter))

(define (wait-for-readable port) ((current-read-waiter) port))
(define (wait-for-writable port) ((current-write-waiter) port))

(define (read-bytes port dst start count)
  (cond
   (((port-read port) port dst start count)
    => (lambda (read)
         (unless (<= 0 read count)
           (error "bad return from port read function" read))
         read))
   (else
    (wait-for-readable port)
    (read-bytes port dst start count))))

(define (soft-read-bytes port dst start count)
  (cond
   (((soft-port-read port) port dst start count)
    => (lambda (read)
         (unless (<= 0 read count)
           (error "bad return from port read function" read))
         read))
   (else
    (wait-for-readable port)
    (soft-read-bytes port dst start count))))

(define (write-bytes port src start count)
  (cond
   (((port-write port) port src start count)
    => (lambda (written)
         (unless (<= 0 written count)
           (error "bad return from port write function" written))
         (when (< written count)
           (write-bytes port src (+ start written) (- count written)))))
   (else
    (wait-for-writable port)
    (write-bytes port src start count))))

(define (soft-write-bytes port src start count)
  (cond
   (((soft-port-write port) port src start count)
    => (lambda (written)
         (unless (<= 0 written count)
           (error "bad return from port write function" written))
         (when (< written count)
           (soft-write-bytes port src (+ start written) (- count written)))))
   (else
    (wait-for-writable port)
    (soft-write-bytes port src start count))))

(define (flush-input port)
  (let* ((buf (port-read-buffer port))
         (cur (port-buffer-cur buf))
         (end (port-buffer-end buf)))
    (when (< cur end)
      (set-port-buffer-cur! buf 0)
      (set-port-buffer-end! buf 0)
      (seek port (- cur end) SEEK_CUR))))

(define (soft-flush-input port)
  (let* ((buf (soft-port-read-buffer port))
         (cur (port-buffer-cur buf))
         (end (port-buffer-end buf)))
    (when (< cur end)
      (set-port-buffer-cur! buf 0)
      (set-port-buffer-end! buf 0)
      (soft-seek port (- cur end) SEEK_CUR))))

(define (%flush-output port)
  (let* ((buf (port-write-buffer port))
         (cur (port-buffer-cur buf))
         (end (port-buffer-end buf)))
    (when (< cur end)
      ;; Update cursors before attempting to write, assuming that I/O
      ;; errors are sticky.  That way if the write throws an error,
      ;; causing the computation to abort, and possibly causing the port
      ;; to be collected by GC when it's open, any subsequent close-port
      ;; or force-output won't signal *another* error.
      (set-port-buffer-cur! buf 0)
      (set-port-buffer-end! buf 0)
      (write-bytes port (port-buffer-bytevector buf) cur (- end cur)))))

(define (soft-flush-output port)
  (let* ((buf (soft-port-write-buffer port))
         (cur (port-buffer-cur buf))
         (end (port-buffer-end buf)))
    (when (< cur end)
      ;; Update cursors before attempting to write, assuming that I/O
      ;; errors are sticky.  That way if the write throws an error,
      ;; causing the computation to abort, and possibly causing the port
      ;; to be collected by GC when it's open, any subsequent close-port
      ;; or force-output won't signal *another* error.
      (set-port-buffer-cur! buf 0)
      (set-port-buffer-end! buf 0)
      (soft-write-bytes port (port-buffer-bytevector buf) cur (- end cur)))))

(define utf8-bom #vu8(#xEF #xBB #xBF))
(define utf16be-bom #vu8(#xFE #xFF))
(define utf16le-bom #vu8(#xFF #xFE))
(define utf32be-bom #vu8(#x00 #x00 #xFE #xFF))
(define utf32le-bom #vu8(#xFF #xFE #x00 #x00))

(define-syntax-rule (mk1 clear-stream-start-for-bom-read
			 peek-byte fill-input
			 port-clear-stream-start-for-bom-read
			 %port-encoding
			 specialize-port-encoding!)
			 
  (define (clear-stream-start-for-bom-read port io-mode)
    (define (maybe-consume-bom bom)
      (and (eq? (peek-byte port) (bytevector-u8-ref bom 0))
	   (call-with-values (lambda ()
			       (fill-input port (bytevector-length bom)))
	     (lambda (buf cur buffered)
	       (and (<= (bytevector-length bom) buffered)
		    (let ((bv (port-buffer-bytevector buf)))
		      (let lp ((i 1))
			(if (= i (bytevector-length bom))
			    (begin
			      (set-port-buffer-cur! buf (+ cur i))
			      #t)
			    (and (eq? (bytevector-u8-ref bv (+ cur i))
				      (bytevector-u8-ref bom i))
				 (lp (1+ i)))))))))))
    (when (and (port-clear-stream-start-for-bom-read port)
	       (eq? io-mode 'text))
      (case (%port-encoding port)
	((UTF-8)
	 (maybe-consume-bom utf8-bom))
	((UTF-16)
	 (cond
	  ((maybe-consume-bom utf16le-bom)
	   (specialize-port-encoding! port 'UTF-16LE))
	  (else
	   (maybe-consume-bom utf16be-bom)
	   (specialize-port-encoding! port 'UTF-16BE))))
	((UTF-32)
	 (cond
	  ((maybe-consume-bom utf32le-bom)
	   (specialize-port-encoding! port 'UTF-32LE))
	  (else
	   (maybe-consume-bom utf32be-bom)
	   (specialize-port-encoding! port 'UTF-32BE))))))))

(mk1 soft-clear-stream-start-for-bom-read
     soft-peek-byte
     soft-fill-input
     soft-port-clear-stream-start-for-bom-read
     %soft-port-encoding
     soft-specialize-port-encoding!)

(mk1 clear-stream-start-for-bom-read
     port-peek-byte
     fill-input
     port-clear-stream-start-for-bom-read
     %port-encoding
     specialize-port-encoding!)


(define-syntax-rule (mk2 fill-input
			 clear-stream-start-for-bom-read
			 port-read-buffer
			 port-read-buffering
			 input-port?
			 port-random-access?
			 flush-output
			 expand-port-read-buffer!
			 read-bytes)
			 
  (define* (fill-input port #:optional (minimum-buffering 1) (io-mode 'text))
    (clear-stream-start-for-bom-read port io-mode)
    (let* ((buf (port-read-buffer port))
	   (cur (port-buffer-cur buf))
	   (buffered (max (- (port-buffer-end buf) cur) 0)))
      (cond
       ((or (<= minimum-buffering buffered) (port-buffer-has-eof? buf))
	(values buf cur buffered))
       (else
	(unless (input-port? port)
	  (error "not an input port" port))
	(when (port-random-access? port)
	  (%flush-output port))
	(let ((bv (port-buffer-bytevector buf)))
	  (cond
	   ((< (bytevector-length bv) minimum-buffering)
	    (expand-port-read-buffer! port minimum-buffering)
	    (fill-input port minimum-buffering))
	   (else
	    (when (< 0 cur)
	      (bytevector-copy! bv cur bv 0 buffered)
	      (set-port-buffer-cur! buf 0)
	      (set-port-buffer-end! buf buffered))
	    (let ((buffering (max (port-read-buffering port)
				  minimum-buffering)))
	      (let lp ((buffered buffered))
		(let* ((count (- buffering buffered))
		       (read (read-bytes port bv buffered count)))
		  (cond
		   ((zero? read)
		    (set-port-buffer-has-eof?! buf #t)
		    (values buf 0 buffered))
		   (else
		    (let ((buffered (+ buffered read)))
		      (set-port-buffer-end! buf buffered)
		      (if (< buffered minimum-buffering)
			  (lp buffered)
			  (values buf 0 buffered))))))))))))))))

(mk2 fill-input
     clear-stream-start-for-bom-read
     port-read-buffer
     port-read-buffering
     %input-port?
     port-random-access?
     flush-output
     expand-port-read-buffer!
     read-bytes)

(mk2 soft-fill-input
     soft-clear-stream-start-for-bom-read
     soft-port-read-buffer
     soft-port-read-buffering
     soft-input-port?
     soft-port-random-access?
     soft-flush-output
     soft-expand-port-read-buffer!
     soft-read-bytes)
		

(define* (force-output #:optional (port (current-output-port)))
  (unless (and (%output-port? port) (not (%port-closed? port)))
    (error "not an open output port" port))
  (%flush-output port))

(define* (soft-force-output #:optional (port (current-output-port)))
  (unless (and (soft-output-port? port) (not (soft-port-closed? port)))
    (error "not an open output port" port))
  (soft-flush-output port))

(define close-port
  (let ((%close-port (@ (guile) close-port)))
    (lambda (port)
      (cond
       ((%port? port)
	(cond
	 ((%port-closed? port) #f)
	 (else
	  (when (%output-port? port) (%flush-output port))
	  (%close-port port))))

       ((soft-port? port)
	(cond
	 ((soft-port-closed? port) #f)
	 (else
	  (when (soft-output-port? port) (soft-flush-output port))
	  (soft-close-port port))))))))

(define-inlinable (port-peek-bytes port count kfast kslow)
  (let* ((buf (port-read-buffer port))
	 (cur (port-buffer-cur buf))
	 (buffered (- (port-buffer-end buf) cur)))
    (if (<= count buffered)
	(kfast buf (port-buffer-bytevector buf) cur buffered)
	(call-with-values (lambda () (fill-input port count))
	  (lambda (buf cur buffered)
	    (kslow buf (port-buffer-bytevector buf) cur buffered))))))

(define-inlinable (soft-peek-bytes port count kfast kslow)
  (let* ((buf (soft-port-read-buffer port))
	 (cur (port-buffer-cur buf))
	 (buffered (- (port-buffer-end buf) cur)))
    (if (<= count buffered)
	(kfast buf (port-buffer-bytevector buf) cur buffered)
	(call-with-values (lambda () (soft-fill-input port count))
	  (lambda (buf cur buffered)
	    (kslow buf (port-buffer-bytevector buf) cur buffered))))))

(define (port-peek-byte port)
  (port-peek-bytes port 1
		   (lambda (buf bv cur buffered)
		     (bytevector-u8-ref bv cur))
		   (lambda (buf bv cur buffered)
		     (and (> buffered 0)
			  (bytevector-u8-ref bv cur)))))

(define (soft-peek-byte port)
  (soft-peek-bytes port 1
		   (lambda (buf bv cur buffered)
		     (bytevector-u8-ref bv cur))
		   (lambda (buf bv cur buffered)
		     (and (> buffered 0)
			  (bytevector-u8-ref bv cur)))))

(define (peek-byte port)
  (if (%port? port)
      (port-peek-byte port)
      (soft-peek-byte port)))

(define* (lookahead-u8 port)
  (define (fast-path buf bv cur buffered)
    (bytevector-u8-ref bv cur))
  (define (slow-path buf bv cur buffered)
    (if (zero? buffered)
        the-eof-object
        (fast-path buf bv cur buffered)))
  (if (%port? port)
      (port-peek-bytes port 1 fast-path slow-path)
      (soft-peek-bytes port 1 fast-path slow-path)))

(define* (get-u8 port)
  (define (fast-path buf bv cur buffered)
    (set-port-buffer-cur! buf (1+ cur))
    (bytevector-u8-ref bv cur))
  (define (slow-path buf bv cur buffered)
    (if (zero? buffered)
        (begin
          (set-port-buffer-has-eof?! buf #f)
          the-eof-object)
        (fast-path buf bv cur buffered)))
  (if (%port? port)
      (port-peek-bytes port 1 fast-path slow-path)
      (soft-peek-bytes port 1 fast-path slow-path)))

(define-syntax-rule (mk3 get-bytevector-n!
			 port-read-buffer
			 fill-input
			 port-random-access?
			 flush-output
			 port-clear-stream-start-for-bom-read
			 read-bytes
			 port-read-buffering)
			 
  (define (get-bytevector-n! port bv start count)
    (define (port-buffer-take! pos buf cur to-copy)
      (bytevector-copy! (port-buffer-bytevector buf) cur
			bv pos to-copy)
      (set-port-buffer-cur! buf (+ cur to-copy))
      (+ pos to-copy))
    (define (take-already-buffered)
      (let* ((buf (port-read-buffer port))
	     (cur (port-buffer-cur buf))
	     (buffered (max (- (port-buffer-end buf) cur) 0)))
	(port-buffer-take! start buf cur (min count buffered))))
    (define (buffer-and-fill pos)
      (call-with-values (lambda () (fill-input port 1 'binary))
	(lambda (buf cur buffered)
	  (if (zero? buffered)
	      ;; We found EOF, which is marked in the port read buffer.
	      ;; If we haven't read any bytes yet, clear the EOF from the
	      ;; buffer and return it.  Otherwise return the number of
	      ;; bytes that we have read.
	      (if (= pos start)
		  (begin
		    (set-port-buffer-has-eof?! buf #f)
		    the-eof-object)
		  (- pos start))
	      (let ((pos (port-buffer-take! pos buf cur
					    (min (- (+ start count) pos)
						 buffered))))
		(if (= pos (+ start count))
		    count
		    (buffer-and-fill pos)))))))
    (define (fill-directly pos)
      (when (port-random-access? port)
	(%flush-output port))
      (port-clear-stream-start-for-bom-read port)
      (let lp ((pos pos))
	(let ((read (read-bytes port bv pos (- (+ start count) pos))))
	  (cond
	   ((= (+ pos read) (+ start count))
	    count)
	   ((zero? read)
	    ;; We found EOF.  If we haven't read any bytes yet, return
	    ;; EOF.  Otherwise save the EOF in the port read buffer.
	    (if (= pos start)
		the-eof-object
		(begin
		  (set-port-buffer-has-eof?! (port-read-buffer port) #t)
		  (- pos start))))
	   (else (lp (+ pos read)))))))
    (let ((pos (take-already-buffered)))
      (cond
       ((= pos (+ start count))
	count)
       ((< (- (+ start count) pos) (port-read-buffering port))
	(buffer-and-fill pos))
       (else (fill-directly pos))))))

(mk3 port-get-bytevector-n!
     port-read-buffer
     fill-input
     port-random-access?
     flush-output
     port-clear-stream-start-for-bom-read
     read-bytes
     port-read-buffering)

(mk3 soft-get-bytevector-n!
     soft-port-read-buffer
     soft-fill-input
     soft-port-random-access?
     soft-flush-output
     soft-port-clear-stream-start-for-bom-read
     soft-read-bytes
     soft-port-read-buffering)

(define (unbuf-get-bytevector-n! port bv start count)
  (define (port-buffer-take! pos buf cur to-copy)
      (bytevector-copy! (port-buffer-bytevector buf) cur
			bv pos to-copy)
      (set-port-buffer-cur! buf (+ cur to-copy))
      (+ pos to-copy))
  (define (take-already-buffered)
      (let* ((buf (soft-port-read-buffer port))
	     (cur (port-buffer-cur buf))
	     (buffered (max (- (port-buffer-end buf) cur) 0)))
	(port-buffer-take! start buf cur (min count buffered))))

    (let ((pos (take-already-buffered)))
      (cond
       ((= pos (+ start count))
	count)
       (else
	(+ (- pos start)
	   (soft-read-bytes port bv pos (- (+ start count) pos)))))))

  

(define (get-bytevector-n! port bv start count)
  (if (%port? port)
      (port-get-bytevector-n! port bv start count)
      (if (soft-buffered? port)
	  (soft-get-bytevector-n! port bv start count)
	  (unbuf-get-bytevector-n! port bv start count))))

(define (get-bytevector-n port count)
  (let* ((bv (make-bytevector count))
         (result (get-bytevector-n! port bv 0 count)))
    (cond ((eof-object? result)
           result)
          ((= result count)
           bv)
          (else
           (let ((bv* (make-bytevector result)))
             (bytevector-copy! bv 0 bv* 0 result)
             bv*)))))
  

(define (get-bytevector-some port)
  (if (%port? port)
      (call-with-values (lambda () (fill-input port 1 'binary))
	(lambda (buf cur buffered)
	  (if (zero? buffered)
	      (begin
		(set-port-buffer-has-eof?! buf #f)
		the-eof-object)
	      (let ((result (make-bytevector buffered)))
		(bytevector-copy! (port-buffer-bytevector buf) cur
				  result 0 buffered)
		(set-port-buffer-cur! buf (+ cur buffered))
		result))))
      (call-with-values (lambda () (soft-fill-input port 1 'binary))
	(lambda (buf cur buffered)
	  (if (zero? buffered)
	      (begin
		(set-port-buffer-has-eof?! buf #f)
		the-eof-object)
	      (let ((result (make-bytevector buffered)))
		(bytevector-copy! (port-buffer-bytevector buf) cur
				  result 0 buffered)
		(set-port-buffer-cur! buf (+ cur buffered))
		result))))))

(define (get-bytevector-some! port bv start count)
  (if (%port? port)
      (if (zero? count)
	  0
	  (call-with-values (lambda () (fill-input port 1 'binary))
	    (lambda (buf cur buffered)
	      (if (zero? buffered)
		  (begin
		    (set-port-buffer-has-eof?! buf #f)
		    the-eof-object)
		  (let ((transfer-size (min count buffered)))
		    (bytevector-copy! (port-buffer-bytevector buf) cur
				      bv start transfer-size)
		    (set-port-buffer-cur! buf (+ cur transfer-size))
		    transfer-size)))))
      (if (zero? count)
	  0
	  (call-with-values (lambda () (soft-fill-input port 1 'binary))
	    (lambda (buf cur buffered)
	      (if (zero? buffered)
		  (begin
		    (set-port-buffer-has-eof?! buf #f)
		    the-eof-object)
		  (let ((transfer-size (min count buffered)))
		    (bytevector-copy! (port-buffer-bytevector buf) cur
				      bv start transfer-size)
		    (set-port-buffer-cur! buf (+ cur transfer-size))
		    transfer-size)))))))

(define (get-bytevector-all port)
  (let lp ((p 0) (s 256) (bv (make-bytevector 1024)))
    (let ((read (get-bytevector-n! port bv p s)))
      (if (or (= read 0) (< read (- s p)))
	  (let ((ret (make-bytevector (+ read p) 0)))
	    (bytevector-copy! bv 0 ret 0 (+ read p))
	    ret)
	  (let* ((s  (* 2 s))
		 (bv2 (make-bytevector s 0)))
	    (bytevector-copy! bv 0 bv2 0 (+ p read))
	    (lp (+ p read) s bv2))))))

(define (put-u8 port byte)
  (if (%port? port)
      (let* ((buf (port-write-buffer port))
	     (bv (port-buffer-bytevector buf))
	     (end (port-buffer-end buf)))
	(unless (<= 0 end (bytevector-length bv))
	  (error "not an output port" port))
	(when (and (eq? (port-buffer-cur buf) end) (port-random-access? port))
	  (flush-input port))
	(cond
	 ((= end (bytevector-length bv))
	  ;; Multiple threads racing; race to flush, then retry.
	  (%flush-output port)
	  (put-u8 port byte))
	 (else
	  (bytevector-u8-set! bv end byte)
	  (set-port-buffer-end! buf (1+ end))
	  (when (= (1+ end) (bytevector-length bv)) (%flush-output port)))))
      (let* ((buf (soft-port-write-buffer port))
	     (bv (port-buffer-bytevector buf))
	     (end (port-buffer-end buf)))
	(unless (<= 0 end (bytevector-length bv))
	  (error "not an output port" port))
	(when (and (eq? (port-buffer-cur buf) end)
		   (soft-port-random-access? port))
	  (soft-flush-input port))
	(cond
	 ((= end (bytevector-length bv))
	  ;; Multiple threads racing; race to flush, then retry.
	  (soft-flush-output port)
	  (put-u8 port byte))
	 (else
	  (bytevector-u8-set! bv end byte)
	  (set-port-buffer-end! buf (1+ end))
	  (when (= (1+ end) (bytevector-length bv)) (soft-flush-output port)
		))))))

(define-syntax-rule (mk5 put-bytevector
			 port-write-buffer
			 port-random-access?
			 flush-input
			 flush-output
			 write-bytes)
			 
(define* (put-bytevector port src #:optional (start 0)
                         (count (- (bytevector-length src) start)))
  (unless (<= 0 start (+ start count) (bytevector-length src))
    (error "invalid start/count" start count))
  (let* ((buf (port-write-buffer port))
         (bv (port-buffer-bytevector buf))
         (size (bytevector-length bv))
         (cur (port-buffer-cur buf))
         (end (port-buffer-end buf))
         (buffered (max (- end cur) 0)))
    (when (and (eq? cur end) (port-random-access? port))
      (flush-input port))
    (cond
     ((<= size count)
      ;; The write won't fit in the buffer at all; write directly.
      ;; Write directly.  Flush write buffer first if needed.
      (when (< cur end) (%flush-output port))
      (write-bytes port src start count))
     ((< (- size buffered) count)
      ;; The write won't fit into the buffer along with what's already
      ;; buffered.  Flush and fill.
      (%flush-output port)
      (set-port-buffer-end! buf count)
      (bytevector-copy! src start bv 0 count))
     (else
      ;; The write will fit in the buffer, but we need to shuffle the
      ;; already-buffered bytes (if any) down.
      (set-port-buffer-cur! buf 0)
      (set-port-buffer-end! buf (+ buffered count))
      (bytevector-copy! bv cur bv 0 buffered)
      (bytevector-copy! src start bv buffered count)
      ;; If the buffer completely fills, we flush.
      (when (= (+ buffered count) size)
        (%flush-output port)))))))
  
(mk5 port-put-bytevector
     port-write-buffer
     port-random-access?
     flush-input
     flush-output
     write-bytes)

(mk5 soft-put-bytevector
     soft-port-write-buffer
     soft-port-random-access?
     soft-flush-input
     soft-flush-output
     soft-write-bytes)

(define* (unbuf-put-bytevector port src #:optional (start 0)
			       (count (- (bytevector-length src) start)))

  (unless (<= 0 start (+ start count) (bytevector-length src))
    (error "invalid start/count" start count))    
  
  (let* ((buf      (port-write-buffer port))
         (bv       (port-buffer-bytevector buf))
         (size     (bytevector-length bv))
         (cur      (port-buffer-cur buf))
         (end      (port-buffer-end buf)))
    
    (when (and (eq? cur end) (port-random-access? port))
      (flush-input port))
    
    (when (< cur end) (%flush-output port))
    (write-bytes port src start count)))
    
(define (put-bytevector port . l)
  (if (%port? port)
      (apply port-put-bytevector port l)
      (if (soft-buffered? port)
	  (apply soft-put-bytevector port l)
	  (apply unbuf-put-bytevector port l))))

(define (decoding-error subr port)
  ;; GNU definition; fixme?
  (define EILSEQ 84)
  (throw 'decoding-error subr "input decoding error" EILSEQ port))

(define-inlinable (decode-utf8 bv start avail u8_0 kt kf)
  (cond
   ((< u8_0 #x80)
    (kt (integer->char u8_0) 1))
   ((and (<= #xc2 u8_0 #xdf) (<= 2 avail))
    (let ((u8_1 (bytevector-u8-ref bv (1+ start))))
      (if (= (logand u8_1 #xc0) #x80)
          (kt (integer->char
               (logior (ash (logand u8_0 #x1f) 6)
                       (logand u8_1 #x3f)))
              2)
          (kf))))
   ((and (= (logand u8_0 #xf0) #xe0) (<= 3 avail))
    (let ((u8_1 (bytevector-u8-ref bv (+ start 1)))
          (u8_2 (bytevector-u8-ref bv (+ start 2))))
      (if (and (= (logand u8_1 #xc0) #x80)
               (= (logand u8_2 #xc0) #x80)
               (case u8_0
                 ((#xe0) (>= u8_1 #xa0))
                 ((#xed) (>= u8_1 #x9f))
                 (else #t)))
          (kt (integer->char
               (logior (ash (logand u8_0 #x0f) 12)
                       (ash (logand u8_1 #x3f) 6)
                       (logand u8_2 #x3f)))
              3)
          (kf))))
   ((and (<= #xf0 u8_0 #xf4) (<= 4 avail))
    (let ((u8_1 (bytevector-u8-ref bv (+ start 1)))
          (u8_2 (bytevector-u8-ref bv (+ start 2)))
          (u8_3 (bytevector-u8-ref bv (+ start 3))))
      (if (and (= (logand u8_1 #xc0) #x80)
               (= (logand u8_2 #xc0) #x80)
               (= (logand u8_3 #xc0) #x80)
               (case u8_0
                 ((#xf0) (>= u8_1 #x90))
                 ((#xf4) (>= u8_1 #x8f))
                 (else #t)))
          (kt (integer->char
               (logior (ash (logand u8_0 #x07) 18)
                       (ash (logand u8_1 #x3f) 12)
                       (ash (logand u8_2 #x3f) 6)
                       (logand u8_3 #x3f)))
              4)
          (kf))))
   (else (kf))))

(define (bad-utf8-len bv cur buffering first-byte)
  (define (ref n)
    (bytevector-u8-ref bv (+ cur n)))
  (cond
   ((< first-byte #x80) 0)
   ((<= #xc2 first-byte #xdf)
    (cond
     ((< buffering 2) 1)
     ((not (= (logand (ref 1) #xc0) #x80)) 1)
     (else 0)))
   ((= (logand first-byte #xf0) #xe0)
    (cond
     ((< buffering 2) 1)
     ((not (= (logand (ref 1) #xc0) #x80)) 1)
     ((and (eq? first-byte #xe0) (< (ref 1) #xa0)) 1)
     ((and (eq? first-byte #xed) (< (ref 1) #x9f)) 1)
     ((< buffering 3) 2)
     ((not (= (logand (ref 2) #xc0) #x80)) 2)
     (else 0)))
   ((<= #xf0 first-byte #xf4)
    (cond
     ((< buffering 2) 1)
     ((not (= (logand (ref 1) #xc0) #x80)) 1)
     ((and (eq? first-byte #xf0) (< (ref 1) #x90)) 1)
     ((and (eq? first-byte #xf4) (< (ref 1) #x8f)) 1)
     ((< buffering 3) 2)
     ((not (= (logand (ref 2) #xc0) #x80)) 2)
     ((< buffering 4) 3)
     ((not (= (logand (ref 3) #xc0) #x80)) 3)
     (else 0)))
   (else 1)))

(define-syntax-rule (mk10 peek-char-and-next-cur/utf8
			  fill-input
			  port-conversion-strategy)
			  
  (define (peek-char-and-next-cur/utf8 port buf cur first-byte)
    (if (< first-byte #x80)
	(values (integer->char first-byte) buf (+ cur 1))
	(call-with-values (lambda ()
			    (fill-input port
					(cond
					 ((<= #xc2 first-byte #xdf) 2)
					 ((= (logand first-byte #xf0) #xe0) 3)
					 (else 4))))
	  (lambda (buf cur buffering)
	    (let ((bv (port-buffer-bytevector buf)))
	      (define (bad-utf8)
		(let ((len (bad-utf8-len bv cur buffering first-byte)))
		  (when (zero? len) (error "internal error"))
		  (if (eq? (port-conversion-strategy port) 'substitute)
		      (values #\xFFFD buf (+ cur len))
		      (decoding-error "peek-char" port))))
	      (decode-utf8 bv cur buffering first-byte
			   (lambda (char len)
			     (values char buf (+ cur len)))
			   bad-utf8)))))))

(mk10 peek-char-and-next-cur/utf8
      fill-input
      port-conversion-strategy)

(mk10 soft-peek-char-and-next-cur/utf8
      soft-fill-input
      soft-port-conversion-strategy)
		
(define (peek-char-and-next-cur/iso-8859-1 port buf cur first-byte)
  (values (integer->char first-byte) buf (+ cur 1)))

(define (soft-peek-char-and-next-cur/iso-8859-1 port buf cur first-byte)
  (values (integer->char first-byte) buf (+ cur 1)))


(define-syntax-rule (mk11 peek-char-and-next-cur/iconv
			  fill-input
			  port-conversion-strategy
			  port-decode-char)
  (define (peek-char-and-next-cur/iconv port)
    (let lp ((prev-input-size 0))
      (let ((input-size (1+ prev-input-size)))
	(call-with-values (lambda () (fill-input port input-size))
	  (lambda (buf cur buffered)
	    (cond
	     ((< buffered input-size)
	      ;; Buffer failed to fill; EOF, possibly premature.
	      (cond
	       ((zero? prev-input-size)
		(values the-eof-object buf cur))
	       ((eq? (port-conversion-strategy port) 'substitute)
		(values #\xFFFD buf (+ cur prev-input-size)))
	       (else
		(decoding-error "peek-char" port))))
	     ((port-decode-char port (port-buffer-bytevector buf)
				cur input-size)
	      => (lambda (char)
		   (values char buf (+ cur input-size))))
	     (else
	      (lp input-size)))))))))

(mk11 peek-char-and-next-cur/iconv
      fill-input
      port-conversion-strategy
      port-decode-char)

(mk11 soft-peek-char-and-next-cur/iconv
      soft-fill-input
      soft-port-conversion-strategy
      soft-port-decode-char)

(define (peek-char-and-next-cur port)
  (define (have-byte buf bv cur buffered)
    (let ((first-byte (bytevector-u8-ref bv cur)))
      (case (%port-encoding port)
        ((UTF-8)
         (peek-char-and-next-cur/utf8 port buf cur first-byte))
        ((ISO-8859-1)
         (peek-char-and-next-cur/iso-8859-1 port buf cur first-byte))
        (else
         (peek-char-and-next-cur/iconv port)))))
  (port-peek-bytes port 1 have-byte
		   (lambda (buf bv cur buffered)
		     (if (< 0 buffered)
			 (have-byte buf bv cur buffered)
			 (values the-eof-object buf cur)))))

(define (soft-peek-char-and-next-cur port)
  (define (have-byte buf bv cur buffered)
    (let ((first-byte (bytevector-u8-ref bv cur)))
      (case (%soft-port-encoding port)
        ((UTF-8)
         (soft-peek-char-and-next-cur/utf8 port buf cur first-byte))
        ((ISO-8859-1)
         (soft-peek-char-and-next-cur/iso-8859-1 port buf cur first-byte))
        (else
         (soft-peek-char-and-next-cur/iconv port)))))
  (soft-peek-bytes port 1 have-byte
		   (lambda (buf bv cur buffered)
		     (if (< 0 buffered)
			 (have-byte buf bv cur buffered)
			 (values the-eof-object buf cur)))))

(define* (port-peek-char port)
  (define (slow-path)
    (call-with-values (lambda () (peek-char-and-next-cur port))
      (lambda (char buf cur)
        char)))
  (define (fast-path buf bv cur buffered)
    (let ((u8 (bytevector-u8-ref bv cur))
          (enc (%port-encoding port)))
      (case enc
        ((UTF-8) (decode-utf8 bv cur buffered u8 (lambda (char len) char)
                              slow-path))
        ((ISO-8859-1) (integer->char u8))
        (else (slow-path)))))
  (port-peek-bytes port 1 fast-path
              (lambda (buf bv cur buffered) (slow-path))))

(define* (soft-peek-char port)
  (define (slow-path)
    (call-with-values (lambda () (soft-peek-char-and-next-cur port))
      (lambda (char buf cur)
        char)))
  (define (fast-path buf bv cur buffered)
    (let ((u8 (bytevector-u8-ref bv cur))
          (enc (%soft-port-encoding port)))
      (case enc
        ((UTF-8) (decode-utf8 bv cur buffered u8 (lambda (char len) char)
                              slow-path))
        ((ISO-8859-1) (integer->char u8))
        (else (slow-path)))))
  (soft-peek-bytes port 1 fast-path
		   (lambda (buf bv cur buffered) (slow-path))))

(define* (peek-char #:optional (port (current-input-port)))
  (if (%port? port)
      (port-peek-char port)
      (soft-peek-char port)))

(define-inlinable (advance-port-position! pos char)
  ;; FIXME: this cond is a speed hack; really we should just compile
  ;; `case' better.
  (cond
   ;; FIXME: char>? et al should compile well.
   ((<= (char->integer #\space) (char->integer char))
    (set-port-position-column! pos (1+ (port-position-column pos))))
   (else
    (case char
      ((#\alarm) #t)                    ; No change.
      ((#\backspace)
       (let ((col (port-position-column pos)))
         (when (> col 0)
           (set-port-position-column! pos (1- col)))))
      ((#\newline)
       (set-port-position-line! pos (1+ (port-position-line pos)))
       (set-port-position-column! pos 0))
      ((#\return)
       (set-port-position-column! pos 0))
      ((#\tab)
       (let ((col (port-position-column pos)))
         (set-port-position-column! pos (- (+ col 8) (remainder col 8)))))
      (else
       (set-port-position-column! pos (1+ (port-position-column pos))))))))


(define-syntax-rule (mk20 port-read-char
			  peek-char-and-next-cur
			  %port-encoding
			  port-peek-bytes
			  )
  (define* (port-read-char port)
    (define (finish buf char)
      (advance-port-position! (port-buffer-position buf) char)
      char)
    
    (define (slow-path)
      (call-with-values (lambda () (peek-char-and-next-cur port))
	(lambda (char buf cur)
        (set-port-buffer-cur! buf cur)
        (if (eq? char the-eof-object)
            (begin
              (set-port-buffer-has-eof?! buf #f)
              char)
            (finish buf char)))))
    
    (define (fast-path buf bv cur buffered)
      (let ((u8 (bytevector-u8-ref bv cur))
	    (enc (%port-encoding port)))
      (case enc
        ((UTF-8)
         (decode-utf8 bv cur buffered u8
                      (lambda (char len)
                        (set-port-buffer-cur! buf (+ cur len))
                        (finish buf char))
                      slow-path))
        ((ISO-8859-1)
         (set-port-buffer-cur! buf (+ cur 1))
         (finish buf (integer->char u8)))
        (else (slow-path)))))
    
  (port-peek-bytes port 1 fast-path
		   (lambda (buf bv cur buffered) (slow-path)))))

(mk20 port-read-char
      peek-char-and-next-cur
      %port-encoding
      port-peek-bytes)

(mk20 soft-read-char
      soft-peek-char-and-next-cur
      %soft-port-encoding
      soft-peek-bytes)

(define* (read-char #:optional (port (current-input-port)))
  (if (%port? port)
      (port-read-char port)
      (soft-read-char port)))

(define-inlinable (port-fold-chars/iso-8859-1 port proc seed)
  (let* ((buf (port-read-buffer port))
         (cur (port-buffer-cur buf)))
    (let fold-buffer ((buf buf) (cur cur) (seed seed))
      (let ((bv (port-buffer-bytevector buf))
            (end (port-buffer-end buf)))
        (let fold-chars ((cur cur) (seed seed))
          (cond
           ((= end cur)
            (call-with-values (lambda () (fill-input port))
              (lambda (buf cur buffered)
                (if (zero? buffered)
                    (call-with-values (lambda () (proc the-eof-object seed))
                      (lambda (seed done?)
                        (if done? seed (fold-buffer buf cur seed))))
                    (fold-buffer buf cur seed)))))
           (else
            (let ((ch (integer->char (bytevector-u8-ref bv cur)))
                  (cur (1+ cur)))
              (set-port-buffer-cur! buf cur)
              (advance-port-position! (port-buffer-position buf) ch)
              (call-with-values (lambda () (proc ch seed))
                (lambda (seed done?)
                  (if done? seed (fold-chars cur seed))))))))))))

(define-inlinable (soft-port-fold-chars/iso-8859-1 port proc seed)
  (let* ((buf (soft-port-read-buffer port))
         (cur (port-buffer-cur buf)))
    (let fold-buffer ((buf buf) (cur cur) (seed seed))
      (let ((bv (port-buffer-bytevector buf))
            (end (port-buffer-end buf)))
        (let fold-chars ((cur cur) (seed seed))
          (cond
           ((= end cur)
            (call-with-values (lambda () (soft-fill-input port))
              (lambda (buf cur buffered)
                (if (zero? buffered)
                    (call-with-values (lambda () (proc the-eof-object seed))
                      (lambda (seed done?)
                        (if done? seed (fold-buffer buf cur seed))))
                    (fold-buffer buf cur seed)))))
           (else
            (let ((ch (integer->char (bytevector-u8-ref bv cur)))
                  (cur (1+ cur)))
              (set-port-buffer-cur! buf cur)
              (advance-port-position! (port-buffer-position buf) ch)
              (call-with-values (lambda () (proc ch seed))
                (lambda (seed done?)
                  (if done? seed (fold-chars cur seed))))))))))))

(define-inlinable (port-fold-chars port proc seed)
  (case (%port-encoding port)
    ((ISO-8859-1) (port-fold-chars/iso-8859-1 port proc seed))
    (else
     (let lp ((seed seed))
       (let ((ch (port-read-char port)))
         (call-with-values (lambda () (proc ch seed))
           (lambda (seed done?)
             (if done? seed (lp seed)))))))))

(define-inlinable (soft-port-fold-chars port proc seed)
  (case (%soft-port-encoding port)
    ((ISO-8859-1) (soft-port-fold-chars/iso-8859-1 port proc seed))
    (else
     (let lp ((seed seed))
       (let ((ch (soft-read-char port)))
         (call-with-values (lambda () (proc ch seed))
           (lambda (seed done?)
             (if done? seed (lp seed)))))))))


(define-syntax-rule (mk30 read-delimited
			  port-fold-chars
			  unread-char)
  (define* (read-delimited delims #:optional (port (current-input-port))
			   (handle-delim 'trim))

    ;; Currently this function conses characters into a list, then uses
    ;; reverse-list->string.  It wastes 2 words per character but it still
    ;; seems to be the fastest thing at the moment.
    (define (finish delim chars)
      (define (->string chars)
	(if (and (null? chars) (not (char? delim)))
	    the-eof-object
	    (reverse-list->string chars)))
      (case handle-delim
	((trim) (->string chars))
	((split) (cons (->string chars) delim))
	((concat)
	 (->string (if (char? delim) (cons delim chars) chars)))
	((peek)
	 (when (char? delim) (unread-char delim port))
	 (->string chars))
	(else
	 (error "unexpected handle-delim value: " handle-delim))))
    (define-syntax-rule (make-folder delimiter?)
      (lambda (char chars)
	(if (or (not (char? char)) (delimiter? char))
	    (values (finish char chars) #t)
	    (values (cons char chars) #f))))
    (define-syntax-rule (specialized-fold delimiter?)
      (port-fold-chars port (make-folder delimiter?) '()))
    (case (string-length delims)
      ((0) (specialized-fold (lambda (char) #f)))
      ((1) (let ((delim (string-ref delims 0)))
	     (specialized-fold (lambda (char) (eqv? char delim)))))
      (else => (lambda (ndelims)
		 (specialized-fold
		  (lambda (char)
		    (let lp ((i 0))
		      (and (< i ndelims)
			   (or (eqv? char (string-ref delims i))
			       (lp (1+ i))))))))))))

(mk30 port-read-delimited
      port-fold-chars
      unread-char)

(mk30 soft-read-delimited
      soft-port-fold-chars
      soft-unread-char)

(define* (read-delimited delims #:optional (port (current-input-port))
			 (handle-delim 'trim))
  (if (%port? port)
      (port-read-delimited delims port handle-delim)
      (soft-read-delimited delims port handle-delim)))

  
(define* (read-line #:optional (port (current-input-port))
                    (handle-delim 'trim))
  (read-delimited "\n" port handle-delim))


(define* (%read-line port)
  (read-line port 'split))

(define* (put-string port str #:optional (start 0)
                     (count (- (string-length str) start)))
  (if (%port? port)
      (let* ((aux (port-auxiliary-write-buffer port))
	     (pos (port-buffer-position aux))
	     (line (port-position-line pos)))
	(set-port-buffer-cur! aux 0)
	(port-clear-stream-start-for-bom-write port aux)
	(let lp ((encoded 0))
	  (when (< encoded count)
	    (let ((encoded (+ encoded
			      (port-encode-chars port aux str
						 (+ start encoded)
						 (- count encoded)))))
	      (let ((end (port-buffer-end aux)))
		(set-port-buffer-end! aux 0)
		(port-put-bytevector port (port-buffer-bytevector aux) 0 end)
		(lp encoded)))))
	(when (and (not (eqv? line (port-position-line pos)))
		   (port-line-buffered? port))
	  (%flush-output port)))


        (let* ((aux (soft-port-auxiliary-write-buffer port))
	       (pos (port-buffer-position aux))
	       (line (port-position-line pos)))
	  (set-port-buffer-cur! aux 0)
	  (soft-port-clear-stream-start-for-bom-write port aux)
	  (let lp ((encoded 0))
	    (when (< encoded count)
	      (let ((encoded (+ encoded
				(soft-port-encode-chars port aux str
							(+ start encoded)
							(- count encoded)))))
		(let ((end (port-buffer-end aux)))
		  (set-port-buffer-end! aux 0)
		  (soft-put-bytevector port (port-buffer-bytevector aux) 0 end)
		  (lp encoded)))))
	  (when (and (not (eqv? line (port-position-line pos)))
		     (soft-port-line-buffered? port))
	    (soft-flush-output port)))))

(define* (put-char port char)
  (if (%port? port)
      (let ((aux (port-auxiliary-write-buffer port)))
	(set-port-buffer-cur! aux 0)
	(port-clear-stream-start-for-bom-write port aux)
	(port-encode-char port aux char)
	(let ((end (port-buffer-end aux)))
	  (set-port-buffer-end! aux 0)
	  (port-put-bytevector port (port-buffer-bytevector aux) 0 end))
	(when (and (eqv? char #\newline) (port-line-buffered? port))
	  (%flush-output port)))

        (let ((aux (soft-port-auxiliary-write-buffer port)))
	  (set-port-buffer-cur! aux 0)
	  (soft-port-clear-stream-start-for-bom-write port aux)
	  (soft-port-encode-char port aux char)
	  (let ((end (port-buffer-end aux)))
	    (set-port-buffer-end! aux 0)
	    (soft-put-bytevector port (port-buffer-bytevector aux) 0 end))
	  (when (and (eqv? char #\newline) (soft-port-line-buffered? port))
	    (soft-flush-output port)))))

(define (write-char char port) (put-char port char))

(define accept
  (let ((%accept (@ (guile) accept)))
    (lambda* (port #:optional (flags 0))
      (if (%port? port)
	  (let lp ()
	    (or (%accept port flags)
		(begin
		  (wait-for-readable port)
		  (lp))))
	  (let lp ()
	    (or (soft-accept port flags)
		(begin
		  (wait-for-readable port)
		  (lp))))))))

(define connect
  (let ((%connect (@ (guile) connect)))
    (lambda (port sockaddr . args)
      (if (%port? port)
	  (unless (apply %connect port sockaddr args)
	    ;; Clownshoes semantics; see connect(2).
	    (wait-for-writable port)
	    (let ((err (getsockopt port SOL_SOCKET SO_ERROR)))
	      (unless (zero? err)
		(scm-error 'system-error "connect" "~A"
			   (list (strerror err)) #f))))
	  (unless (apply soft-connect port sockaddr args)
	    ;; Clownshoes semantics; see connect(2).
	    (wait-for-writable port)
	    (let ((err (soft-getsockopt port SOL_SOCKET SO_ERROR)))
	      (unless (zero? err)
		(scm-error 'system-error "connect" "~A"
			   (list (strerror err)) #f))))))))

(define port-read
  (let ((port-read% (@ (ice-9 ports internal) port-read)))
    (lambda (port)
      (if (%port? port)
	  (port-read%     port)
	  (soft-port-read port)))))

(define port-write
  (let ((port-write% (@ (ice-9 ports internal) port-write)))
    (lambda (port)
      (if (%port? port)
	  (port-write%     port)
	  (soft-port-write port)))))

(define port-filename
  (let ((port-filename% (@ (guile) port-filename)))
    (lambda (port)
      (if (%port? port)
	  (port-filename%     port)
	  (soft-port-filename port)))))

(define %port-property
  (let ((%port-property% (@ (guile) %port-property)))
    (lambda (port s)
      (if (%port? port)
	  (%port-property%     port s)
	  (%soft-port-property port s)))))

(define port-line
  (let ((port-line% (@ (guile) port-line)))
    (lambda (port)
      (if (%port? port)
	  (port-line%     port)
	  (soft-port-line port)))))

(define port-column
  (let ((port-column% (@ (guile) port-column)))
    (lambda (port)
      (if (%port? port)
	  (port-column%     port)
	  (soft-port-column port)))))

(define input-port?
  (lambda (port)
    (if (%port? port)
	(%input-port?     port)
	(soft-input-port? port))))

(define output-port?
  (lambda (port)
    (if (%port? port)
	(%output-port?     port)
	(soft-output-port? port))))

(define port-closed?
  (lambda (port)
    (if (%port? port)
	(%port-closed?     port)
	(soft-port-closed? port))))

(define  fl (make-fluid #f))
(define  %write)

(define format (@ (ice-9 format) format))
(define simple-format
  (lambda x (apply (@ (ice-9 format) format) x)))

(define saved-port-bindings #f)
(define newline
  (let ((%nl (@ (guile) newline)))
    (lambda* (#:optional (port (current-output-port)))
      (if (%port? port)
	  (%nl port)
	  (put-char port #\newline)))))

(define port-bindings
  '(((guile)
     format simple-format newline
     read-char write-char peek-char force-output close-port
     accept connect port? port-filename)

    ((ice-9 ports)
     %port-property port-line port-column
     input-port? output-port? port-closed?)
    
    ((ice-9 ports internal)
     port-read port-write)

    ((ice-9 binary-ports)
     get-u8 lookahead-u8 get-bytevector-n get-bytevector-n!
     get-bytevector-some get-bytevector-some! get-bytevector-all
     open-bytevector-output-port
     put-u8 put-bytevector)

    ((ice-9 textual-ports)
     put-char put-string)

    ((ice-9 rdelim)
     %read-line read-line read-delimited)))


(define soft-suspendable-ports-installed? #f)

(define (install-soft-suspendable-ports!)
  (set! soft-suspendable-ports-installed? #t)
  (unless saved-port-bindings
    (set! saved-port-bindings (make-hash-table))
    (let ((suspendable-ports (resolve-module '(ice-9 soft-suspendable-ports))))
      (for-each
       (match-lambda
         ((mod . syms)
          (let ((mod (resolve-module mod)))
            (for-each (lambda (sym)
                        (hashq-set! saved-port-bindings sym
                                    (module-ref mod sym))
                        (module-set! mod sym
                                     (module-ref suspendable-ports sym)))
                      syms))))
       port-bindings))))

(define (uninstall-soft-suspendable-ports!)
  (set! soft-suspendable-ports-installed? #f)
  (when saved-port-bindings
    (for-each
     (match-lambda
       ((mod . syms)
        (let ((mod (resolve-module mod)))
          (for-each (lambda (sym)
                      (let ((saved (hashq-ref saved-port-bindings sym)))
                        (module-set! mod sym saved)))
                    syms))))
     port-bindings)
    (set! saved-port-bindings #f)))

(define* (open-bytevector-output-port #:optional (transcoder #f))
  (define bv (make-bytevector (ash 1 10)))
  (define pt 0)
  (define ma 0)
  (define (write! src start count)
    (define n (+ start count))
    (define m (bytevector-length bv))

    (when (> n m)
      (let lp ((mm m))
	(if (> n mm)
	    (lp (* 2 mm))
	    (let ((bv2 (make-bytevector mm 0)))
	      (bytevector-copy! bv 0 bv2 0 pt)
	      (set! bv bv2)))))
    
    (bytevector-copy! src start bv pt count)
    (set! pt (+ pt count))
    (set! ma (max ma pt))
    
    count)

  (define (seek n kind)
    (cond
     ((eq? kind SEEK_CUR)
      (set! pt (+ pt n)))

     ((eq? kind SEEK_SET)
      (set! pt n))

     ((eq? kind SEEK_END)
      (set! pt (max 0 (- ma n))))))

  (letrec ((get
	    (lambda ()
	      (soft-flush-output self)
	      (let ((ret (make-bytevector ma)))
		(bytevector-copy! bv 0 ret 0 ma)
		ret)))
	   (self
	    (make-soft-output-port "bv" write! #f
				   #:seek         seek
				   #:aux-buffer   #t
				   #:write-buffer #t
				   #:buffering?   #t)))
    (values self get)))
