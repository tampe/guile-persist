#include <stdio.h>
#include <gmp.h>
#include <endian.h>
#include "hash.c"
#include "mpz.c"

#define DOSWAP (__BYTE_ORDER == __BIG_ENDIAN)

#define SWAP(i,n) do {t = v[i]; v[i] = v[n-i-1]; v[n-i-1] = t;} while (0)

uint64_t swap64(uint64_t x)
{
  uint64_t y = x;
  char *v = (char *)&y;
  char t;

  SWAP(0,8);
  SWAP(1,8);
  SWAP(2,8);
  SWAP(3,8);

  return y;
}

uint32_t swap32(uint64_t x)
{
  uint32_t y = x;
  char *v = (char *)&y;
  char t;

  SWAP(0,4);
  SWAP(1,4);

  return y;
}

uint16_t swap16(uint64_t x)
{
  uint16_t y = x;
  char *v = (char *)&y;
  char t;

  SWAP(0,2);

  return y;
}

uint64_t read64(u_char *v, int sw)
{
  uint64_t *x;

  x = (uint64_t *)v;
  if (sw && DOSWAP) return swap64(*x);
  return *x;
}

uint64_t read32(u_char *v, int sw)
{
  uint32_t *x;
  x = (uint32_t *)v;
  if (sw && DOSWAP) return swap32(*x);
  return *x;
}

uint64_t read16(u_char *v, int sw)
{
  uint16_t *x;
  x = (uint16_t *)v;
  if (sw && DOSWAP) return swap16(*x);
  return *x;
}


int get_n(uint64_t x)
{
  if (x < 1UL<<(8*1)) return 1;
  if (x < 1UL<<(8*2)) return 2;
  if (x < 1UL<<(8*4)) return 4;
  return 8;
}

uint64_t read_c_integer(uint8_t *v, int N, int sw)
{
  if (N == 8)
    {
      return read64(v,sw);
    }
  else if (N ==  1)
    {
      return v[0];
    }
  else if (N == 2)
    {
      return read16(v,sw);
    }
  else if (N == 4)
    {
      return read32(v,sw);
    }
  else if (N == 3)
    {
      uint32_t xx = read32(v,sw);
      return xx & ((1UL << 3*8) - 1);
    }
  else if (N == 5 || N == 6 || N == 7)
    {
      uint64_t xx = read64(v,sw);
      return xx & ((1UL << N*8) - 1);
    }
  
  return 0;
}

SCM scm_read_c_integer(uint8_t *v, uint64_t N)
{
  return scm_from_int64(read_c_integer(v, N,1));
}

SCM read_integer(uint8_t *v, int N)
{
  if (N <= 8) return scm_read_c_integer(v,N);



  SCM x = scm_ash(scm_from_int(1),scm_from_int(8*N-1));
  
  mpz_t *mp = MY_I_BIG_MPZ(x);

  int i = 0;
  
 loop:
  if (N <= 0) return x;

  (*mp)->_mp_d[i] = read_c_integer(v, N<8 ? N : 8, 0);
  
  v += 8;
  N -= 8;
  i ++;
  
  goto loop;

}


SCM read_double(u_char *v)
{
  uint64_t x = read64(v,1);
  return scm_from_double((double) x);
}

SCM read_single(u_char *v)
{
  uint32_t x = read32(v,1);
  return scm_from_short((float) x);
}

SCM read_complex(u_char *v)
{
  SCM x1 = read_double(v  );
  SCM x2 = read_double(v+8);

  return scm_make_rectangular(x1,x2);
}

SCM read_rational(u_char *v, int N1, int N2)
{
  SCM x1 = read_integer(v,N1);
  SCM x2 = read_integer(v+N1,N2);
  return scm_divide(x1,x2);
}

void add_u64(uint8_t *v, uint64_t xx)
{
  if (DOSWAP) xx  = swap64(xx);
  *((uint64_t *) v) = xx;
}

void add_u32(u_char *v, uint64_t xx)
{
  if (DOSWAP) xx  = swap32(xx);
  *((uint32_t *) v) = xx;
}

void add_u16(u_char *v, uint16_t xx)
{
  if (DOSWAP) xx  = swap16(xx);
  *((uint16_t *) v) = xx;
}

SCM peek(SCM stack)
{
  return SCM_CAR(stack);
}
SCM pop(SCM *stack, SCM *store)
{
  SCM x = *stack;
  SCM z = SCM_CAR(x);
  *stack = SCM_CDR(x);

  if (store)
    {
      SCM_SETCDR(x,*store);
      SCM_SETCAR(x,SCM_BOOL_F);
      *store = x;
    }
  
  return z;		
}

void push(SCM x, SCM *stack, SCM *store)
{
  if (!store || SCM_NULLP(*store))
    *stack = scm_cons(x, *stack);
  else
    {
      SCM y = *store;
      *store = SCM_CDR(y);
      
      SCM_SETCAR(y,x);
      SCM_SETCDR(y,*stack);
      *stack = y;	
    }    
}

void write_u64(uint8_t *v, uint64_t x, int sw)
{
  if (sw && DOSWAP) x = swap64(x);
  *((uint64_t *)v) = x;
}

void write_u32(uint8_t *v, uint64_t x, int sw)
{
  if (sw && DOSWAP) x = swap32(x);
  *((uint32_t *)v) = (uint32_t) x;
}

void write_u16(uint8_t *v, uint64_t x, int sw)
{
  if (sw && DOSWAP) x = swap16(x);
  *((uint16_t *)v) = (uint16_t) x;
}

void write_u8(uint8_t *v, uint64_t x)
{
  *v = (uint8_t)x;
}

uint64_t write_nn(uint8_t *v, uint64_t *i, uint64_t N)
{
  int u = N % 8;
  if (u == 3)
    {
      N++;
    }
  else if (u > 4 && u < 8)
    {
      N += 8-u;
    }
  
  v += *i;
  
  if (N < (1 << 8) - 1)
    {
      write_u8(v,N);
      *i = *i + 1;
    }
  else
    {
      write_u8 (v  , 0xffff   );
      write_u16(v+1, N     , 1);
      *i = *i + 3;
    }

  return N;
}

#define L(X) ((uint64_t) (X))

uint64_t read_nnn(uint8_t *v, uint64_t *i)
{
  uint64_t ret = v[0];
  if (ret < (1<<8) - 4)
    {
      *i += 1;
    }
  else if (ret == (1<<8) - 4)
    {
      *i  += 2;
      ret = v[1];
    }
  else if (ret == (1<<8) - 3)
    {
      *i += 3;
      ret = v[1] + (L(v[2])<<8);
    }
  else if (ret == (1<<8) - 2)
    {
      *i += 5;
      ret = v[1] + (L(v[2])<<8) + (L(v[3])<<(8*2)) + (L(v[4])<<(8*3));      
    }
  else
    {
      *i += 9;
      ret = v[1] + (L(v[2])<<8) + (L(v[3])<<(8*2)) + (L(v[4])<<(8*3));
      ret += (L(v[5])<<(8*4)) + (L(v[6])<<(8*5)) + (L(v[7])<<(8*6)) + (L(v[8])<<(8*7));      
    }

  return ret;
}
int write_nnn(uint8_t *v, uint64_t N)
{
  if (N < (1<<8) - 4)
    {
      v[0] = N;
      return 1;
    }
  else if (N < (1<<8))
    {
      v[0] = (1<<8)-4;
      v[1] = N;

      return 2;
    }
  else if (N < (1<<(8*2)))
    {
       v[0] = (1<<8)-3;
       v[1] = N & 0xff;
       v[2] = N >> 8;
       return 3;
    }
  else if (N < (1UL<<(8*4)))
    {
       v[0] = (1<<8)-2;
       v[1] = N & 0xff;
       v[2] = (N >> (8*1)) && 0xff;
       v[3] = (N >> (8*2)) && 0xff;
       v[4] = (N >> (8*3)) && 0xff;
       return 5;
    }
  else
    {
      v[0] = (1<<8)-2;
      v[1] = N & 0xff;
      v[2] = (N >> (8*1)) && 0xff;
      v[3] = (N >> (8*2)) && 0xff;
      v[4] = (N >> (8*3)) && 0xff;
      v[5] = (N >> (8*4)) && 0xff;
      v[6] = (N >> (8*5)) && 0xff;
      v[7] = (N >> (8*6)) && 0xff;
      v[8] = (N >> (8*7)) && 0xff;
      return 9;
    }
}

int write_c_integer(uint8_t *v, uint64_t N, uint64_t x, int sw)
{
  int u = N % 8;
  if (u == 3)
    N += 1;
  else if (u > 4 && u < 8)
    N += 8-u;
  
  if (N == 1)
    {
      *v = (uint8_t) x;
    }
  else if (N == 2)
    {
      write_u16(v, x, sw);
    }
  else if (N == 4)
    {
      write_u32(v, x, sw);
    }
  else if (N == 8)
    {
      write_u64(v, x, sw);
    }

  return N;
}

int write_integer(uint8_t *v, uint64_t N, SCM x)
{
  if (N <= 8)
    {
      return write_c_integer(v, N, scm_to_uint64(x), 1);
    }
  else
    {
      int u = N % 8;
      if (u == 3)
	N += 1;
      else if (u > 4 && u < 8)
	N += 8-u;

      mpz_t *mp = MY_I_BIG_MPZ(x);
      
      int i = 0;
      int k = N;
      while(k > 0)
	{
	  write_c_integer(v, (k<8 ? k : 8), (uint64_t) (*mp)->_mp_d[i], 0);

	  v += 8;
	  k -= 8;
	  i++;     
	}
    }

  return N;
}

#define PEEK_F()  peek(fstack)
#define POP_A()   pop(&astack, &store)
#define POP_F()   pop(&fstack, &store)
#define POP_FF()  pop(&fstack, 0)
#define PUSH_A(xx) push(xx, &astack, &store)
#define PUSH_F(xx) push(xx, &fstack, &store)
#define CONTENTS(_bv) ((u_char *) SCM_BYTEVECTOR_CONTENTS(_bv))

