void * tags [0x100];
void * tags3[0x100];

#define DE_UNPACK(data)				\
  int _do_lam  = GET_I(data ,9);		\
  SCM map3     = GET_S(data, 10);		\

#define READTAG					\
do {						\
  tag = v[i];					\
  i++;						\
 } while (0)

#define READ_NN					\
{						\
  N = *(v+i);					\
  i += 1;					\
  if (N >= (1 << 8) - 1)			\
    {						\
      N = read16(v + i,1);			\
      i += 2;					\
    }						\
}

#define READ_N						\
  do {							\
    N = tag >> 4;					\
    if (N >= (1 << 4) - 4)				\
      {							\
	int K = N - (1 << 4) + 4;			\
	K = 1 << K;					\
	N = read_c_integer(v + i, K, 1);		\
	i += K;						\
      }							\
  } while (0)

#define REG(ddoreg,x)				\
  if (ddoreg)						\
    {							\
      scm_hashq_set_x(map,scm_from_uint64(j),x);	\
      j++;						\
    }
  

#define DOPAIR					\
  {						\
    vv = (SCM *)1;				\
    vk = 0;					\
    vn = 0;					\
  }

#define AT_STARTUP						\
  if (scm_is_eq(t,t_bv))					\
    {								\
      POP_F();							\
      wx = POP_F();						\
      wk = scm_to_long(POP_F());					\
      wt = scm_to_int(POP_F());						\
      if (wt)								\
	{								\
      	  wn = scm_to_uint64(scm_string_length(wx));			\
									\
	  int qq = wt & 8;						\
	  if (qq)							\
	    {								\
	      wn *= 4;							\
	      ww = (u_char *) scm_i_string_wide_chars (wx);		\
	    }								\
	  else								\
	    {								\
	      ww = (u_char *) scm_i_string_chars (wx);			\
	    }								\
	}								\
      else								\
	{								\
	  ww = CONTENTS(wx);						\
	  wn = scm_to_uint64(scm_bytevector_length(wx));		\
	}								\
      NEW_F(goto bv_loop, goto bv_loop, ;);				\
    }

#define NEW_F(FINISH,NEXT,STARTUP)				\
  if (SCM_CONSP(fstack))					\
    {								\
      SCM t = SCM_CAR(fstack);					\
								\
      /* bytevectors? */					\
      STARTUP;							\
								\
      if (scm_is_eq(t,t_vec))					\
	{							\
	  POP_F();						\
	  vx = POP_F();						\
	  vk = scm_to_long(POP_F());				\
	  vv = (SCM *)SCM_I_VECTOR_ELTS(vx);			\
	  vn = scm_to_long(scm_vector_length(vx));		\
	  vt = t_vec;						\
	}							\
      else if (scm_is_eq(t,t_struct))				\
	{							\
	  POP_F();						\
	  vx = POP_F();						\
	  vk = scm_to_long(POP_F());				\
	  vv = (SCM *) SCM_STRUCT_DATA(vx);			\
	  vn = SCM_STRUCT_SIZE(vx);				\
	  vt = t_struct;					\
	}							\
      else if (scm_is_eq(t,t_hash_q))				\
	{							\
	  POP_F();						\
	  vx = POP_F();						\
	  vk = scm_to_long(POP_F());				\
	  vv = (SCM *)SCM_I_VECTOR_ELTS(vx);			\
	  vn = scm_to_long(scm_vector_length(vx));		\
	  vt = t_hash_q;					\
	}							\
      else if (scm_is_eq(t,t_hash_e))				\
	{							\
	  POP_F();						\
	  vx = POP_F();						\
	  vk = scm_to_long(POP_F());				\
	  vv = (SCM *)SCM_I_VECTOR_ELTS(vx);			\
	  vn = scm_to_long(scm_vector_length(vx));		\
	  vt = t_hash_e;					\
	}							\
      else if (scm_is_eq(t,t_hash_v))				\
	{							\
	  POP_F();						\
	  vx = POP_F();						\
	  vk = scm_to_long(POP_F());				\
	  vv = (SCM *)SCM_I_VECTOR_ELTS(vx);			\
	  vn = scm_to_long(scm_vector_length(vx));		\
	  vt = t_hash_v;					\
	}							\
      else							\
	DOPAIR;							\
								\
      NEXT;							\
    }								\
  else								\
    FINISH;	     

#define NEW_F_FIRST NEW_F(goto loop, goto loop, AT_STARTUP)
#define NEW_F_LAST  NEW_F(goto out_finish , goto atom,;)

/* push the current vector data info to the stack */
#define PUSH_OLD					\
  if (vk < vn)						\
    {							\
      PUSH_F(scm_from_long(vk));			\
      PUSH_F(vx);					\
      PUSH_F(vt);					\
      vk=vn=0;						\
    }

SCM deserialize0(SCM bv, SCM ii, SCM jj, SCM nn, SCM fstack,
		 SCM map, SCM sflags, int setup_maps, SCM data, SCM state)
{  
  if (setup_maps)
    {
      tags [ scm_to_int ( i_int_p     )] = && li_int_p;
      tags [ scm_to_int ( i_int_n     )] = && li_int_n;
      tags [ scm_to_int ( i_bv        )] = && li_bv;
      tags [ scm_to_int ( i_str       )] = && li_str;
      tags [ scm_to_int ( i_wstr      )] = && li_wstr;
      tags [ scm_to_int ( i_sym       )] = && li_sym;
      tags [ scm_to_int ( i_wsym      )] = && li_wsym;
      tags [ scm_to_int ( i_keyw      )] = && li_keyw;
      tags [ scm_to_int ( i_wkeyw     )] = && li_wkeyw;
      tags [ scm_to_int ( i_char      )] = && li_char;
      tags [ scm_to_int ( i_ref       )] = && li_ref;
      tags [ scm_to_int ( i_vector    )] = && li_vector;
      tags [ scm_to_int ( i_struct    )] = && li_struct;
      tags [ scm_to_int ( i_lambda    )] = && li_lambda;
      tags [ scm_to_int ( i_imp       )] = && li_imp;
      
      tags [ 0xf | (scm_to_int ( j_null      ) << 4)] = && lj_null;
      tags [ 0xf | (scm_to_int ( j_double    ) << 4)] = && lj_double;
      tags [ 0xf | (scm_to_int ( j_single    ) << 4)] = && lj_single;
      tags [ 0xf | (scm_to_int ( j_true      ) << 4)] = && lj_true;
      tags [ 0xf | (scm_to_int ( j_false     ) << 4)] = && lj_false;      
      tags [ 0xf | (scm_to_int ( j_complex   ) << 4)] = && lj_complex;
      tags [ 0xf | (scm_to_int ( j_rational_p) << 4)] = && lj_rational_p;
      tags [ 0xf | (scm_to_int ( j_rational_n) << 4)] = && lj_rational_n;
      tags [ 0xf | (scm_to_int ( j_bigint_p  ) << 4)] = && lj_bigint_p;
      tags [ 0xf | (scm_to_int ( j_bigint_n  ) << 4)] = && lj_bigint_n;
      tags [ 0xf | (scm_to_int ( j_pair      ) << 4)] = && lj_pair;
      tags [ 0xf | (scm_to_int ( j_hashq     ) << 4)] = && lj_hashq;
      tags [ 0xf | (scm_to_int ( j_hashv     ) << 4)] = && lj_hashv;
      tags [ 0xf | (scm_to_int ( j_hash      ) << 4)] = && lj_hash ;
      
      tags [ 0xff ] = &&level_lk;

      return SCM_BOOL_F;
    }

  int c_state = scm_to_int(state);
  int first   = c_state & 1;
  int final   = c_state & 2;

  uint64_t flags  = scm_to_uint64(sflags);

  // Bytevector that comes in
  u_char   *v   = CONTENTS(bv);
  uint64_t i    = scm_to_long(ii);
  uint64_t n    = scm_to_long(nn);
  uint64_t nb   = scm_to_long(scm_bytevector_length(bv));
  
  // Id counter fro references
  uint64_t j    = scm_to_long(jj);

  // Tag unpacking
  int      tag = 0;
  uint64_t N   = 0;

  // Active SCM
  SCM     x   = SCM_BOOL_F;

  // Cons storage for fast cons allocations
  SCM   store = SCM_EOL;

  // Iterating over bytevectors
  SCM      wx  = SCM_BOOL_F;
  u_char  *ww  = NULL;  
  uint64_t wk  = 0;
  uint64_t wn  = 0;
  int      wt  = 0;
  
  // Iterating over vectors struct and objects
  SCM      vx  = SCM_BOOL_F;
  SCM     *vv  = NULL;
  uint64_t vk  = 0;
  uint64_t vn  = 0;
  SCM      vt  = SCM_BOOL_F;
  // To mke sure we hit scheme land time to time
  int k = 0;

  int neg = 0;
  
  SCM narrow_char = scm_integer_to_char(scm_from_int(10));
  SCM wide_char   = scm_integer_to_char(scm_from_int(1000));


  DE_UNPACK(data)
    
  if (first)
    {
      flags = read64(v,1);
      i+=8;
    }
 
  int do_str  = flags & 1 << 1;
  int do_sym  = flags & 1 << 2;
  int do_ref  = flags & 1 << 3;
  int do_bv   = flags & 1 << 4;
  int do_key  = flags & 1 << 5;
  int do_vec  = flags & 1 << 6;
  int do_pair = flags & 1 << 7;
  int do_lam  = (flags & 1 << 8) && _do_lam;

  if (scm_is_false(map)) map = scm_make_hash_table(scm_from_int(1<<8));

  NEW_F_FIRST;  
 loop:
  if (i >= n)
    {
      PUSH_OLD;
      goto out;      
    }
  
  DBG(printf("loop %ld < %ld\n",i,n));
  READTAG;
  DBG(printf("tag %d (N = %d)\n",tag & 0xf, tag >> 4));
  if ((0xf & tag) == 15)
    goto *(tags[ tag ]);
  else
    goto *(tags[ tag & 0xf ]);
  
  // ======================= LEVEL_I
 li_int_p:
  DBG(printf("fixnum >= 0\n"));
  READ_N;
  DBG(printf("N = %ld\n",N));  
  x = scm_from_uint64(N);
  goto atom;
  
 li_int_n:
  DBG(printf("fixnum < 0\n"));
  READ_N;
  DBG(printf("N = %ld\n",N));
  x = scm_from_uint64(N);
  x = scm_product(scm_from_int(-1),x);
  goto atom;
  
 li_bv:
  DBG(printf("bv\n"));  
  READ_N;

  wx = scm_c_make_bytevector(N);

  wt = 0;
  ww = CONTENTS(wx);
  wn = N;
  wk = 0;
  uint64_t diff;
  int do_out = 0;
  
 bv_loop:
  diff = wn - wk;
  if (diff ==  0        ) goto bv_finish;
  if (k + diff > nchunk ) diff = nchunk-k;    
  if (i + diff >=  n    ) diff = n - i;
  
  memcpy(ww + wk, v + i, diff);
  
  if (diff != wn - wk) do_out = 1;

  DBG(printf("copied diff=%ld/%ld bytes type %d\n",diff,wn-wk,wt));
  wk += diff;
  k  += diff;
  i  += diff;

  if (do_out) goto bv_out;
  
 bv_finish:
  {
    int do_it = 0;
    switch (wt & 7)
      {   
      case 0:
	do_it = do_bv;
	x     = wx;
	break;
      case 1:
	x     = wx;
	do_it = do_str;
	break;
      case 2:
	do_it = do_sym;
	x     = wx;
	x     = scm_string_to_symbol(x);
	break;
      case 3:
	do_it = do_key;
	x     = wx;
	x     = scm_string_to_symbol(x);
	x     = scm_symbol_to_keyword(x);
	break;
      }

    REG(do_it,x);
    goto atom;
  }

 bv_out:
  PUSH_OLD;
  PUSH_F(scm_from_int  (wt));
  PUSH_F(scm_from_long (wk));
  PUSH_F(wx);
  PUSH_F(t_bv); 
  
  goto out;
  
 li_str:
  DBG(printf("string\n"));
  READ_N;
  wx = scm_c_make_string(N,narrow_char);

  wt = 1;  
  ww = (u_char *) scm_i_string_chars (wx);
  wn = N;
  wk = 0;
  goto bv_loop;
  
 li_wstr:
  DBG(printf("string\n"));
  READ_N;
  
  wx = scm_c_make_string(N/4,wide_char);

  wt = 1 | 8;
  ww = (u_char *) scm_i_string_wide_chars (wx);
  wn = N;
  wk = 0;
  goto bv_loop;

 li_sym:
  DBG(printf("symbol\n"));
  READ_N;  
  
  wx = scm_c_make_string(N,narrow_char);

  wt = 2;
  ww = (u_char *) scm_i_string_chars (wx);
  wn = N;
  wk = 0;
  goto bv_loop;

  
 li_wsym:
  DBG(printf("wide symbol\n"));
  READ_N;  
  
  wx = scm_c_make_string(N,wide_char);

  wt = 2 | 8;
  ww = (u_char *) scm_i_string_wide_chars (wx);
  wn = N;
  wk = 0;
  
  goto bv_loop;

 li_keyw:
  DBG(printf("symbol\n"));
  READ_N;  
  
  wx = scm_c_make_string(N,narrow_char);

  wt = 3;
  ww = (u_char *) scm_i_string_chars (wx);
  wn = N;
  wk = 0;

  goto bv_loop;

  
 li_wkeyw:
  DBG(printf("wide symbol\n"));
  READ_N;  
  
  wx = scm_c_make_string(N,wide_char);

  wt = 3 | 8;
  ww = (u_char *) scm_i_string_wide_chars (wx);
  wn = N;
  wk = 0;
  goto bv_loop;

 li_char:
  READ_N;
  PUSH_F(scm_integer_to_char(scm_from_long(N)));
  goto atom;
  
 li_ref:
  READ_N;
  if (scm_is_true (map))
    {
      SCM x = scm_hashq_ref(map,scm_from_uint64(N),SCM_BOOL_F);

      if (scm_is_true(x))
	goto atom;
      else
	goto out_err;
    }
  else
    goto out_err;
  
 li_vector:
  {
      READ_N;
      SCM NN = scm_from_long(N);
      x = scm_make_vector(NN,SCM_BOOL_F);
      REG(do_vec,x);	

      PUSH_OLD;
      vx  = x;
      vv  = (SCM *) SCM_I_VECTOR_ELTS(x);
      vk  = 0;
      vn  = N;
      vt  = t_vec;
    }
    
    if (vk == vn) goto atom_vn;
    goto loop;

 lj_hashq:
    vt = t_hash_q;
    goto li_hash_x;

 lj_hashv:
    vt = t_hash_v;
    goto li_hash_x;

 lj_hash:
    vt = t_hash_e;
    
 li_hash_x:
    {
      uint64_t N = 2*read_c_integer(v+i,8,1);
      i += 8;
      
      SCM NN = scm_from_uint64(N);
      
      x = scm_make_vector(NN,SCM_BOOL_F);
      REG(do_ref,x);	

      PUSH_OLD;
      vx  = x;
      vv  = (SCM *) SCM_I_VECTOR_ELTS(x);
      vk  = 0;
      vn  = N;
    
    
      if (vk == vn) goto atom;
      goto loop;
    }

 li_struct:
    {
      READ_N;
      SCM jj = scm_from_long(N);
      SCM vtable = scm_hashq_ref(map,jj,SCM_BOOL_F);
      if (scm_is_true(vtable) && SCM_STRUCTP(vtable))
	{
	  int nn = SCM_VTABLE_SIZE(vtable);
	  x  = scm_words (SCM_UNPACK(vtable) | scm_tc3_struct, nn + 1);
	  REG(do_ref,x);	
	  
	  PUSH_OLD;
	  vx  = x;
	  vv  = (SCM *) SCM_STRUCT_DATA(x);
	  vk  = 0;
	  vn  = nn;
	  vt  = t_struct;	  
	}
      else
	scm_misc_error("atom-deserilizer",
		       "struct refferenced map has not a correct vtable",
		       SCM_EOL);
      if (vk == vn) goto atom_vn;
      goto loop;
    }

 li_lambda:
    {
      if (!do_lam)
	{
	  scm_misc_error
	    ("atom-deserialize",
	     "atom deserializer is not allowed to deserialize lambdas",
	     SCM_EOL);
	}
      
      READ_N;
      int nfree = N;

       
      uint64_t addr = read_nnn(v+i, &i);
      uint64_t jj   = read_nnn(v+i, &i);

      SCM path = scm_hashq_ref(map3, scm_from_uint64(jj), SCM_BOOL_F);

      if (scm_is_false(path) || !scm_is_string(path))
	scm_misc_error("atom-deserializer",
		       "could not find path",
		       SCM_EOL);
      
       
      PUSH_OLD;

      SCM code = scm_call_2(find_fkn_adress,path,scm_from_uint64(addr));

      vx = gp_make_null_procedure(scm_from_int(nfree), code);
      vv = SCM_PROGRAM_FREE_VARIABLES(vx);
      REG(do_ref,vx);
       
      vn = nfree;
      vk = 0;
      vt = t_lambda;
      
      if (vk == vn) goto atom_vn;
      goto loop;	        
    }
    
 lj_bigint_p:
    DBG(printf("+bigint\n"));
    READ_NN;

    if (N + i > nb || N > (1<<12)+10)
      {
	goto out_err;
      }

    x = read_integer(v+i,N);
    i += N;
    k += N/8 + 1;
    goto atom;

 lj_bigint_n:
    DBG(printf("-bigint\n"));
    READ_NN;
    if (N + i > nb || N  > (1<<12)+10)
      {
	goto out_err;
      }

    x = scm_product(scm_from_int(-1),read_integer(v+i,N));
    i += N;
    k += N/8 + 1;
    goto atom_vn;

 lj_rational_n:
  neg = 1;
  
 lj_rational_p:
  {
    READ_NN;
    int N1 = N;
  
    READ_NN;
    int N2 = N;

    if (N1 + N2 + i > nb || N1 > (1>>12)+10 || N2 > (1>>12)+10)
      {
	goto out_err;
      }
    
    x = read_rational(v+i,N1,N2);
    if (neg)
      {
	neg = 0;
	x = scm_product(scm_from_int(-1),x);
      }
    
    i += N1 + N2;
    k += (N1 + N2)/8 + 1;
    goto atom;
  }

  
 li_imp:
    READ_N;
    x = SCM_PACK(N);
    goto atom;
    
    // ===================================== LEVEL J
  
 lj_null:
  DBG(printf("EOL\n"));
  x = SCM_EOL;
  goto atom;
  
 lj_complex:
  x = read_double(v);
  i += 16;
  k += 16;
  goto atom;
  
 lj_pair:
  DBG(printf("pair"));
  PUSH_OLD;
  PUSH_F(t_pair);
  REG(do_pair,fstack);  
  goto loop;
  
 lj_double:
  x = read_double(v);
  i += 8;
  k += 8;
  goto atom;
  
 lj_single:
  x = read_single(v);
  i += 4;
  k += 4;
  goto atom;
  
 lj_true:
  x = SCM_BOOL_T;
  goto atom;

 lj_false:
  x = SCM_BOOL_F;
  goto atom;
  
 level_lk:
  READTAG;
  goto *(tags3 [tag]);
  
 atom:
  if (vn)
    {
      // Vector/struct/class
      vv[vk] = x;
      vk++;
      
    atom_vn:
      if (vk == vn)
	{
	  if (scm_is_eq(vt,t_vec))
	    x = vx;
	  
	  else if (scm_is_eq(vt,t_struct))
	    x = vx;
	  
	  else if (scm_is_eq(vt,t_lambda))
	    x = vx;
	  
	  else if (scm_is_eq(vt,t_hash_q))
	    x = make_hash_table_q(vx);
	  
	  else if (scm_is_eq(vt,t_hash_e))
	    x = make_hash_table_e(vx);
	  
	  else if (scm_is_eq(vt,t_hash_v))
	    x = make_hash_table_v(vx);
	  
	  else
	    scm_misc_error("atom-deserializer",
			   "in vector like compound generation, cant find type",
			   SCM_EOL);
	  
	  NEW_F_LAST;
	}

      goto loop;
    }

  if (SCM_CONSP(fstack))
    {
      //PAIR
      SCM tx = PEEK_F();
      if (scm_is_eq(tx,t_pair))
	{
	  // CAR POSITION
	  DBG(printf("set car\n"));
	  SCM_SETCAR(fstack,x);
	  goto loop;
	}
      else
	{
	  DBG(printf("set cdr\n"));
	  // CDR position
	  tx = fstack;
	  POP_FF();
	  
	  SCM_SETCDR(tx,x);
	  x = tx;
	  
	  NEW_F_LAST;
	}
    }

  goto out_finish;
  
    
  
 out:
  return scm_list_7(SCM_BOOL_T,
		    SCM_BOOL_T,
		    scm_from_uint64(i),
		    scm_from_uint64(j),
		    fstack,
		    map,
		    scm_from_uint64(flags));

 out_finish:
  return scm_list_7((final ? SCM_BOOL_F : SCM_BOOL_T),
		    SCM_BOOL_T,
		    scm_from_uint64(i),
		    scm_from_uint64(j),
		    (final ? x : fstack),
		    map,
		    scm_from_int(flags));

 out_err:
  return scm_list_7(SCM_BOOL_F,
		    SCM_BOOL_F,
		    scm_from_uint64(i),
		    scm_from_uint64(j),
		    fstack,
		    map,
		    scm_from_uint64(flags));
}

void deserialize_init()
{
  SCM x = SCM_BOOL_F;
  deserialize0(x, x, x, x, x, x, x, 1, x, x);
}

SCM_DEFINE(atom_c_deserializer, "atom-c-deserialize", 9, 0, 0,	   
	   (SCM bv, SCM i, SCM j, SCM n, SCM stack,
	    SCM map, SCM flags, SCM data, SCM final),
	   "atom-deserialize")
  
#define FUNC_NAME s_atom_c_deserialize
{
  return
    deserialize0(bv, i, j, n, stack, map, flags, 0, data, final);
}
#undef FUNC_NAME
