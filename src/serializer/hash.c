int get_hash_kind(SCM h, uint64_t *nn)
{  
  uint64_t n = SCM_HASHTABLE_N_BUCKETS(h);
  SCM      m = scm_from_uint64(n);
  
  int isq = 1;
  int ise = 1;
  int isv = 1;

  *nn = 0;
  SCM v = SCM_HASHTABLE_VECTOR(h);
  
  for (uint64_t i = 0; i < n; i++)
    {
      SCM l = SCM_SIMPLE_VECTOR_REF (v,i);
      while (1)
	{
	  if (  SCM_NULLP(l) ) break;
	  if ( !SCM_CONSP(l) ) return 3;
	  SCM y = SCM_CAR(l);

	  if (!SCM_CONSP(y)) return 3;
	  
	  SCM key = SCM_CAAR(l);

	  int kind = 7;
	  if (scm_is_eq(key,kind_of_hash))
	    {
	      kind = scm_to_int(SCM_CADR(l));	      
	    }

	  isq = isq && (kind & 1);
	  isv = isv && (kind & 2);
	  ise = ise && (kind & 4);
	  
	  isq = isq && scm_to_int(scm_hashq(key, m)) == i;
	  isv = isv && scm_to_int(scm_hashv(key, m)) == i;
	  ise = ise && scm_to_int(scm_hash (key, m)) == i;

	  (*nn) ++;
	  l = SCM_CDR(l);
	}      
    }
  
  if (isq) return 0;
  if (isv) return 1;
  if (ise) return 2;
  
  return 3;
}

SCM get_hash_data_as_vector(SCM h, uint64_t nhash)
{
  uint64_t n    = SCM_HASHTABLE_N_BUCKETS(h);
  SCM v         = SCM_HASHTABLE_VECTOR(h);
  SCM ret       = scm_make_vector(scm_from_uint64(2*nhash),SCM_BOOL_F);
  SCM *vec      = (SCM *) SCM_I_VECTOR_ELTS(ret);
  uint64_t j = 0;
  for (uint64_t i = 0; i < n; i++)
    {
      SCM l = SCM_SIMPLE_VECTOR_REF (v,i);
      for (; SCM_CONSP(l); l = SCM_CDR(l))	 
	{
	  SCM y = SCM_CAR(l);
	  vec[j++] = SCM_CAR(y);
	  vec[j++] = SCM_CDR(y);
	}
    }

  return ret;
}

SCM make_hash_table_q(SCM v)
{
  uint64_t n = scm_c_vector_length(v);

  SCM h = scm_make_hash_table(scm_from_uint64(n/2));

  SCM *vec = (SCM *)SCM_I_VECTOR_ELTS(v);
  
  for (uint64_t i = 0; i < n; i += 2)
  {
    scm_hashq_set_x(h,vec[i],vec[i+1]);
  }

  return h;
}

SCM make_hash_table_v(SCM v)
{
  uint64_t n = scm_c_vector_length(v);

  SCM h = scm_make_hash_table(scm_from_uint64(n/2));

  SCM *vec = (SCM *)SCM_I_VECTOR_ELTS(v);
  
  for (uint64_t i = 0; i < n; i += 2)
  {
    scm_hashv_set_x(h,vec[i],vec[i+1]);
  }

  return h;
}

SCM make_hash_table_e(SCM v)
{
  uint64_t n = scm_c_vector_length(v);

  SCM h = scm_make_hash_table(scm_from_uint64(n/2));

  SCM *vec = (SCM *)SCM_I_VECTOR_ELTS(v);
  
  for (uint64_t i = 0; i < n; i += 2)
  {
    scm_hash_set_x(h,vec[i],vec[i+1]);
  }

  return h;
}
#define STRINGBUF_HEADER_SIZE   2U
#define STRINGBUF_CONTENTS(buf) ((void *)				\
                                 SCM_CELL_OBJECT_LOC (buf,		\
						      STRINGBUF_HEADER_SIZE))
#define SH_STRING_TAG       (scm_tc7_string + 0x100)

#define STRING_STRINGBUF(str) (SCM_CELL_OBJECT_1(str))
#define STRING_START(str)     ((size_t)SCM_CELL_WORD_2(str))
#define STRING_LENGTH(str)    ((size_t)SCM_CELL_WORD_3(str))
#define IS_SH_STRING(str)   (SCM_CELL_TYPE(str)==SH_STRING_TAG)

#define SH_STRING_STRING(sh) (SCM_CELL_OBJECT_1(sh))

#define STRINGBUF_WIDE_CHARS(buf) ((scm_t_wchar *) STRINGBUF_CONTENTS (buf))
#define STRINGBUF_CHARS(buf)    ((unsigned char *) STRINGBUF_CONTENTS (buf))
static inline void
get_str_buf_start (SCM *str, SCM *buf, size_t *start)
{
  *start = STRING_START (*str);
  if (IS_SH_STRING (*str))
    {
      *str = SH_STRING_STRING (*str);
      *start += STRING_START (*str);
    }
  *buf = STRING_STRINGBUF (*str);
}

uint8_t *
i_string_chars (SCM str)
{
  SCM buf;
  size_t start;
  get_str_buf_start (&str, &buf, &start);
  return (uint8_t *) (STRINGBUF_CHARS (buf) + start);
}

