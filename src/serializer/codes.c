// lower bits 1-4
#include <stdio.h>

// These index combines with a size 
SCM i_int_p    = SCM_BOOL_F;
SCM i_int_n    = SCM_BOOL_F;
SCM i_bv       = SCM_BOOL_F;
SCM i_str      = SCM_BOOL_F;
SCM i_wstr     = SCM_BOOL_F;
SCM i_sym      = SCM_BOOL_F;
SCM i_wsym     = SCM_BOOL_F;
SCM i_keyw     = SCM_BOOL_F;
SCM i_wkeyw    = SCM_BOOL_F;
SCM i_char     = SCM_BOOL_F;
SCM i_ref      = SCM_BOOL_F;
SCM i_vector   = SCM_BOOL_F;
SCM i_imp      = SCM_BOOL_F;
SCM i_struct   = SCM_BOOL_F;
SCM i_lambda   = SCM_BOOL_F;

// Lower bits #xf Higher bits 5-8
SCM j_null       = SCM_BOOL_F;
SCM j_rational_p = SCM_BOOL_F;
SCM j_rational_n = SCM_BOOL_F;
SCM j_complex    = SCM_BOOL_F;
SCM j_pair       = SCM_BOOL_F;
SCM j_next       = SCM_BOOL_F;
SCM j_double     = SCM_BOOL_F;
SCM j_single     = SCM_BOOL_F;
SCM j_true       = SCM_BOOL_F;
SCM j_false      = SCM_BOOL_F;
SCM j_struct     = SCM_BOOL_F;
SCM j_class      = SCM_BOOL_F;
SCM j_bigint_p   = SCM_BOOL_F;
SCM j_bigint_n   = SCM_BOOL_F;
SCM j_hashq      = SCM_BOOL_F;
SCM j_hashv      = SCM_BOOL_F;
SCM j_hash       = SCM_BOOL_F;

SCM t_vec        = SCM_BOOL_F;
SCM t_bv         = SCM_BOOL_F;
SCM t_pair       = SCM_BOOL_F;
SCM t_type       = SCM_BOOL_F;
SCM t_struct     = SCM_BOOL_F;
SCM t_hash_q     = SCM_BOOL_F;
SCM t_hash_v     = SCM_BOOL_F;
SCM t_hash_e     = SCM_BOOL_F;
SCM t_lambda     = SCM_BOOL_F;

int nchunk     = 1<<15;

uint64_t abort_addr            = 0;
uint64_t cont_addr             = 0;
SCM      find_program_file     = SCM_BOOL_F;
SCM      find_mapped_elf_image = SCM_BOOL_F;
SCM      find_fkn_adress       = SCM_BOOL_F;
SCM      kind_of_hash          = SCM_BOOL_F;

SCM_DEFINE(atom_c_serializer_info, "atom-c-serialize-info", 6, 0, 0,
	   (SCM a, SCM b, SCM c, SCM d, SCM e, SCM f),
	   "serializer-info")
  
#define FUNC_NAME atom_c_serializr_info
{
  abort_addr            = scm_to_uint64(a);
  cont_addr             = scm_to_uint64(b);
  find_program_file     = c;
  find_mapped_elf_image = d;
  find_fkn_adress       = e;
  kind_of_hash          = f;
  
  return SCM_UNDEFINED;
}
#undef FUNC_NAME

void serializer_init()
{
  // These index combines with a size
  i_int_p    = scm_from_int(0);
  i_int_n    = scm_from_int(1);
  i_bv       = scm_from_int(2);
  i_str      = scm_from_int(3);
  i_wstr     = scm_from_int(4);
  i_sym      = scm_from_int(5);
  i_wsym     = scm_from_int(6);
  i_keyw     = scm_from_int(7);
  i_wkeyw    = scm_from_int(8);
  i_char     = scm_from_int(9);
  i_ref      = scm_from_int(10);
  i_vector   = scm_from_int(11);
  i_imp      = scm_from_int(12);
  i_struct   = scm_from_int(13);
  i_lambda   = scm_from_int(14);
  
  // Lower bits #xf Higher bits 5-8
  j_null       = scm_from_int(1);
  j_true       = scm_from_int(2);
  j_false      = scm_from_int(3);
  j_bigint_p   = scm_from_int(4);
  j_bigint_n   = scm_from_int(5);
  j_rational_p = scm_from_int(6);
  j_rational_n = scm_from_int(7);
  j_complex    = scm_from_int(8);
  j_double     = scm_from_int(9);
  j_single     = scm_from_int(10);  
  j_pair       = scm_from_int(11);
  j_hashq      = scm_from_int(12);
  j_hashv      = scm_from_int(13);
  j_hash       = scm_from_int(14);
  
  t_type     = scm_list_1(scm_from_int(1));
  t_vec      = scm_list_1(scm_from_int(2));
  t_pair     = scm_list_1(scm_from_int(3));
  t_struct   = scm_list_1(scm_from_int(4));
  t_bv       = scm_list_1(scm_from_int(5));
  t_hash_q   = scm_list_1(scm_from_int(6));
  t_hash_v   = scm_list_1(scm_from_int(7));
  t_hash_e   = scm_list_1(scm_from_int(8));
  t_lambda   = scm_list_1(scm_from_int(9));
}



//#define DBG(X) X
#define DBG(X) ;
