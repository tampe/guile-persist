 /* FIXME: In Guile 3.2, replace this union with just a "size" member.
     Digits are always allocated inline.  */
 

struct my_bignum1
{
  scm_t_bits tag;
  union {
    mpz_t mpz;
    struct {
      int zero;
      int size;
      mp_limb_t *limbs;
    } z;
  } u;
  mp_limb_t limbs[];
};


#define MY_I_BIG_MPZ(x) ((mpz_t *) (SCM_CELL_OBJECT_LOC((x),1)))
#define MY_I_BIG_MPZ1(x) (((struct my_bignum1 *)x)->u.mpz)

mpz_t *big_mpz (SCM x)
{
  mpz_t *retp;
  retp = MY_I_BIG_MPZ(x);
  return retp;
}

mpz_srcptr big_mpz_c (SCM x)
{
  mpz_t *retp;
  retp = MY_I_BIG_MPZ(x);
  return (mpz_srcptr) retp;
}

SCM mybig()
{
  SCM y = scm_from_uint64(1000L*1000*1000*1000*1000);
  return scm_product(y,y);  
}

SCM mybig0()
{
  SCM r = mybig0();
  SCM x = mybig0();
  mpz_mul_ui(*big_mpz(r), *big_mpz(x), 0);
  scm_remember_upto_here_1 (x);
  return r;
}

SCM biggit(SCM x)
{
  if (SCM_I_INUMP (x))
    {
      SCM result = mybig();
      SCM zero   = mybig0();
      mpz_add_ui(*big_mpz(result),big_mpz_c(zero),scm_to_uint64(x));
      scm_remember_upto_here_1 (zero);
      return result;
    }
  else
    return x;
}

#define UNARY(scm_mp_fac,mpz_fac_ui)		\
SCM scm_mp_fac(SCM x)				\
{						\
  x = biggit(x);				\
  SCM result = mybig();				\
  mpz_fac_ui(*big_mpz(result),big_mpz_c(x));	\
  return  scm_list_1(result);			\
}

#define UNARYUI(scm_mp_fac,mpz_fac_ui)		\
SCM scm_mp_fac(ulong x)				\
{						\
  SCM result = mybig();				\
  mpz_fac_ui(*big_mpz(result), x);		\
  return  scm_list_1(result);	\
}

UNARYUI(scm_mp_fac      , mpz_fac_ui);
UNARYUI(scm_mp_2fac     , mpz_2fac_ui);
UNARYUI(scm_mp_primorial, mpz_primorial_ui);
UNARYUI(scm_mp_fib      , mpz_fib_ui);
UNARYUI(scm_mp_lucas    , mpz_lucnum_ui);

UNARY(scm_nextprime, mpz_nextprime);


#define IBINARY(scm_jacobi,mpz_jacobi)					\
  int scm_jacobi(SCM x, SCM y)						\
  {									\
    x = biggit(x);							\
    y = biggit(y);							\
  									\
    int res = mpz_jacobi(big_mpz_c(x), big_mpz_c(y));			\
    scm_remember_upto_here_2 (x,y);					\
  									\
    return res;								\
  }


#define BINARY(scm_jacobi,mpz_jacobi)					\
  SCM scm_jacobi(SCM x, SCM y)						\
  {									\
    x = biggit(x);							\
    y = biggit(y);							\
									\
    SCM r = mybig();							\
									\
    mpz_jacobi(*big_mpz(r),big_mpz_c(x), big_mpz_c(y));			\
    scm_remember_upto_here_2 (x,y);					\
  									\
    return scm_list_1(r);						\
  }


#define BINARY_UI(scm_jacobi,mpz_jacobi)				\
  SCM scm_jacobi(SCM x, unsigned long  y)				\
  {									\
    SCM r = mybig();							\
    x = biggit(x);							\
    									\
    mpz_jacobi(*big_mpz(r),big_mpz_c(x), y);				\
    scm_remember_upto_here_1 (x);					\
  									\
    return scm_list_1(r);						\
  }

#define BINARYOUT_UI(scm_jacobi,mpz_jacobi)				\
  SCM scm_jacobi(unsigned long  y)					\
  {									\
    SCM r1 = mybig();							\
    SCM r2 = mybig();							\
    									\
    mpz_jacobi(*big_mpz(r1),*big_mpz(r2), y);				\
  									\
    return scm_list_2(r1,r2);						\
  }

#define BINARY_UIUI(scm_jacobi,mpz_jacobi)				\
  SCM scm_jacobi(unsigned long x, unsigned long  y)			\
  {									\
    SCM r = mybig();							\
									\
    mpz_jacobi(*big_mpz(r),x, y);					\
    scm_remember_upto_here_1 (x);					\
  									\
    return scm_list_1(r);						\
  }


IBINARY(scm_jacobi   , mpz_jacobi);
IBINARY(scm_legendre , mpz_legendre)
IBINARY(scm_kronecker, mpz_kronecker);


BINARY_UI(scm_bin_ui  , mpz_bin_ui);
BINARY_UIUI(scm_bin_uiui, mpz_bin_uiui);
BINARYOUT_UI(scm_fib2_ui , mpz_fib2_ui);
BINARYOUT_UI(scm_lucas2_ui , mpz_lucnum2_ui);

BINARY(scm_mod_invert, mpz_invert);

