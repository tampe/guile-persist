#include <string.h>

SCM scm_list_6(SCM x1, SCM x2, SCM x3, SCM x4, SCM x5, SCM x6)
{
  return scm_cons(x1,scm_list_5(x2,x3,x4,x5,x6));
}

#define PUSH(x) push(x,&stack,&store)
#define POP()   pop(&stack,&store)

#define MAYBE_LARGER(iii)						\
  if(iii > K)								\
  {									\
    K   = 4*K;								\
    if (iii > K) K = iii*2;						\
									\
    SCM bv2 = scm_make_bytevector(scm_from_int(K),scm_from_int(0));	\
    scm_bytevector_copy_x(bv , scm_from_int(0),				\
			  bv2, scm_from_int(0), scm_from_int(i));	\
    bv  = bv2;								\
    v   = CONTENTS(bv2);						\
  }

#define ADD_U8(xx)				\
  do {						\
    MAYBE_LARGER(i+1)				\
    v[i] = (uint8_t) (xx);			\
    i++;					\
  } while (0);


#define ADD_F(xx) ADD_U8(0xf | (scm_to_int(xx) << 4))

#define ADD_U64(xx)				\
  do {						\
    MAYBE_LARGER(i+8)				\
    add_u64(v+i,xx);				\
    i += 8;					\
  } while (0);

#define TAGHANDLE(nnn)				\
  do {						\
    itag    = scm_to_int(tag);			\
    int  mm = 0;				\
    int  q  = 0;				\
    if (nnn < 12)				\
      {						\
	mm = nnn;				\
      }						\
    else if (nnn < 1<<8)			\
      {						\
	q  = 1;					\
	mm = 12;				\
      }						\
    else if (nnn < 1<<16)			\
      {						\
	q  = 2;					\
	mm = 13;				\
      }						\
    else if (nnn < 1L<<32)			\
      {						\
	q  = 4;					\
	mm = 14;				\
      }						\
    else					\
      {						\
	q  = 8;					\
	mm = 15;				\
      }						\
    int tugg = itag | (mm << 4);		\
    ADD_U8(tugg);				\
    long nm = nnn;				\
    for(; q > 0; q--)				\
      {						\
	ADD_U8(nm & 0xff);			\
	nm = nm >> 8;				\
      }						\
  } while (0)


#define ATOM(SUCC)					\
    do {						\
    if (SCM_IMP(x))					\
      {							\
        DBG(printf("atom\n"));				\
	if (SCM_I_INUMP(x))				\
	  {							\
	    long nn = scm_to_uint64(x);				\
	    DBG(printf("small number %ld\n",nn));		\
	    if (nn < 0)						\
	      {							\
		tag = i_int_n;					\
		nn = -nn;					\
	      }							\
	    else						\
	      {							\
		tag = i_int_p;					\
	      }							\
	    TAGHANDLE(nn);					\
	    SUCC;						\
	  }							\
      								\
	if (SCM_NULLP(x))					\
	  {							\
	    ADD_F(j_null);					\
	    SUCC;						\
	  }							\
								\
	if (scm_is_eq(x,SCM_BOOL_F))				\
	  {							\
	    ADD_F(j_false);					\
	    SUCC;						\
	  }							\
								\
	if (scm_is_eq(x,SCM_BOOL_T))				\
	  {							\
	    ADD_F(j_true);					\
	    SUCC;						\
	  }							\
								\
	if (SCM_CHARP(x))					\
	  {							\
	    tag = i_char;					\
	    uint64_t nn  = scm_to_ulong(scm_char_to_integer(x));	\
	    TAGHANDLE(nn);					\
	    SUCC;						\
	  }						\
							\
	if (! scm_is_eq(x,SCM_UNDEFINED))		\
	  {						\
	    uint64_t nn  = (uint64_t) SCM_UNPACK(x);	\
	    tag = i_imp;				\
	    TAGHANDLE(nn);				\
	    SUCC;					\
	  }						\
	goto out_error;					\
     }							\
  } while (0);						

#define DB(x) ((uint64_t) x)
#define INEXACT(SUCC)				\
  if (SCM_INEXACTP(x))				\
    {						\
      DBG(printf("inexact\n"));			\
      if (SCM_REALP(x))				\
	{						\
	  DBG(printf("real %f\n",SCM_REAL_VALUE(x)));	\
	  ADD_F(j_double);				\
	  						\
	  uint64_t nnl  = DB(SCM_REAL_VALUE(x));	\
	  ADD_U64(nnl);					\
	}					\
      else					\
	{					\
	  DBG(printf("complex\n"));		\
	  ADD_F(j_complex);			\
						\
	  uint64_t nnl  = DB(SCM_COMPLEX_REAL(x));	\
	  ADD_U64(nnl);				\
						\
	  nnl  = DB(SCM_COMPLEX_IMAG(x));	\
	  ADD_U64(nnl);				\
	}					\
      						\
      SUCC;					\
    }
#define E_REF scm_hash_ref
#define E_SET scm_hash_set_x
#define Q_REF scm_hashq_ref
#define Q_SET scm_hashq_set_x

#define REF_HANDLER(MAP,REF,SET,test,NEXT,P)			\
  if (test)							\
    {								\
      DBG(printf("try ref %d\n",P));				\
      SCM jj = REF(MAP,x,SCM_BOOL_F);				\
      if (scm_is_true(jj))					\
	{							\
	  DBG(printf("found ref\n"));					\
	  SCM  tag = i_ref;						\
	  uint64_t nn  = scm_to_int64(jj);				\
	  TAGHANDLE(nn);						\
	  NEXT;							\
	}							\
      else if (P)						\
	{							\
	  SET(MAP,x,scm_from_int64(j));				\
	  j++;							\
	}							\
    }								

#define REF_HANDLER_Q(test,NEXT,P)		\
  REF_HANDLER(map,Q_REF,Q_SET,test,NEXT,P);		

#define REF_HANDLER_E(test,NEXT,P)		\
  REF_HANDLER(map2,E_REF,E_SET,test,NEXT,P);		

  
#define PUSH_VEC(vx,kk_tag)			\
  PUSH(SCM_PACK(vx));				\
  PUSH(scm_from_long(nn));			\
  PUSH(scm_from_long(k));			\
  PUSH(kk_tag);					\
  PUSH(t_type);


#define GET_I(data,i)							\
  (  scm_is_true(data)							\
     ?  scm_is_true(scm_hashq_ref(data,scm_from_int(i),SCM_BOOL_F))	\
     :  0)

#define GET_S(data,i)							\
  (  scm_is_true(data)							\
     ?  scm_hashq_ref(data,scm_from_int(i),SCM_BOOL_F)			\
     :  SCM_BOOL_F)

#define UNPACK(data)			\
  SCM map     = GET_S(data, 0);		\
  SCM map2    = GET_S(data, 1);		\
  int do_str  = GET_I(data, 2);		\
  int do_sym  = GET_I(data, 3);		\
  int do_ref  = GET_I(data, 4);		\
  int do_bv   = GET_I(data, 5);		\
  int do_key  = GET_I(data, 6);		\
  int do_vec  = GET_I(data, 7);		\
  int do_pair = GET_I(data, 8);		\
  int do_lam  = GET_I(data ,9);		\
  SCM map3    = GET_S(data, 10);	\
  SCM map4    = GET_S(data, 11);	\

#define DOSTRING(I_STR,I_WSTR)					\
  do {								\
    nn = scm_to_uint64(scm_string_length (x));			\
    int qq = scm_to_int(scm_string_bytes_per_char (x));		\
								\
    if (qq == 1)						\
      {								\
	tag = I_STR;						\
	ww = (u_char *) i_string_chars (x);			\
      }								\
    else							\
      {								\
	tag = I_WSTR;						\
        ww = (u_char *) i_string_chars (x);			\
	nn *= 4;						\
      }								\
								\
    TAGHANDLE(nn);						\
								\
    goto bv0;							\
								\
  } while (0)


#define REFFER(NEXT)				\
  do {						\
    if (SCM_CONSP(x))				\
      {						\
	REF_HANDLER_Q(do_ref,NEXT,0);		\
      }						\
						\
    if (scm_is_symbol(x))			\
      {						\
	REF_HANDLER_Q(do_sym,NEXT,0);		\
      }						\
						\
    if (scm_is_string(x))			\
      {						\
	REF_HANDLER_E(do_str,NEXT,0);		\
      }						\
						\
    if (SCM_I_IS_VECTOR(x))			\
      {						\
	REF_HANDLER_E(do_ref,NEXT,0);		\
      }						\
						\
    if (scm_is_bytevector(x))			\
      {						\
	REF_HANDLER_E(do_bv,NEXT,0);		\
      }						\
						\
    if (scm_is_keyword(x))			\
      {						\
	REF_HANDLER_E(do_key,NEXT,0);		\
      }						\
  } while (0)


SCM_DEFINE(atom_c_serializer, "atom-c-serialize", 6, 0, 0,	   
	   (SCM bv, SCM xii, SCM jj, SCM stack, SCM data, SCM first),
	   "serializer")
  
#define FUNC_NAME atom_c_serialize
{
  SCM     store = SCM_EOL;
  
  SCM      x     = SCM_BOOL_F;
  u_char * v     = CONTENTS(bv);
  SCM      tag   = 0;
  uint64_t nn    = 0;
  uint64_t K     = scm_to_uint64(scm_bytevector_length(bv));
  uint64_t i     = scm_to_uint64(xii);
  uint64_t ii    = i;
  uint64_t j     = scm_to_uint64(jj);
  uint64_t k     = 0;
  SCM      y     = SCM_BOOL_F;
  SCM      tp    = SCM_BOOL_F;
  int     itag   = 0;
  u_char  *ww    = NULL;

  const SCM    *vv  = NULL;

  SCM zero_scm = scm_from_int(0);
  
  UNPACK(data);

  if (scm_is_true(first))
    {
      uint64_t flags = 1;
      flags |= (do_str  ? 1 << 1 : 0);
      flags |= (do_sym  ? 1 << 2 : 0);
      flags |= (do_ref  ? 1 << 3 : 0);
      flags |= (do_bv   ? 1 << 4 : 0);
      flags |= (do_key  ? 1 << 5 : 0);
      flags |= (do_vec  ? 1 << 6 : 0);
      flags |= (do_pair ? 1 << 7 : 0);
      flags |= (do_lam  ? 1 << 8 : 0);

      ADD_U64(flags);
    }

 loop:
  DBG(printf("loop %ld\n",i));
  if (SCM_CONSP(stack))
    x = POP();
  else
    goto out_final;

 loop0:
  if (i - ii > nchunk)
    {
      PUSH(x);
      goto out;
    }

  // Make sure to bring up vector and bytevector data if they are pushed
  // the stack for later useage
  
  if (scm_is_eq(x, t_type))
    {
      DBG(printf("do_continuation\n"));
      
      tp  = POP();
      if (scm_is_eq(tp,t_vec    ))  goto do_vec;
      if (scm_is_eq(tp,t_bv     ))  goto do_bv;
      
    do_vec:
      DBG(printf("do_vec\n"));
      
      k  = scm_to_long(POP());
      nn = scm_to_long(POP());
      vv = (SCM *) SCM_UNPACK(POP());

      goto v_loop;
      
    do_bv:
      DBG(printf("do_bv\n"));
      
      k  =  scm_to_long(POP());
      nn =  scm_to_long(POP());
      ww =  (u_char *) SCM_UNPACK(POP());

      goto bv_loop;
    }
      
  ATOM(goto loop);
    
  if (SCM_CONSP(x))
    {
      DBG(printf("pair *\n"));
      
      REF_HANDLER_Q(do_ref, goto loop, 1);
      
      ADD_F(j_pair);
      
      y = x;
      x = SCM_CAR(y);

      ATOM   (goto pair_2);
      INEXACT(goto pair_2);
      REFFER (goto pair_2);
      
      PUSH(SCM_CDR(y));
      goto loop0;

    pair_2:
      x = SCM_CDR(y);
      goto loop0;
    }

  if (scm_is_number(x))
    {
      DBG(printf("heap number\n");)
      
      INEXACT(goto loop);
      
      if (scm_is_integer(x))
	{
	  // bignums
	  DBG(printf("bignum\n"));
	  uint64_t len = scm_to_uint64(scm_integer_length (x));
	  uint64_t N   = len / 8UL + (len % 8UL > 0 ? 1 : 1);
	  
	  if (N > (1<<12))
	    scm_misc_error("atom-serializer",
			   "can't handle huge bignum max 200 bytes allowed",
			   SCM_EOL);

	  if (scm_is_true(scm_negative_p(x)))
	    {
	      x = scm_difference(scm_from_int(0),x);
	      ADD_F(j_bigint_n);
	    }
	  else
	    {
	      ADD_F(j_bigint_p);
	    }

	  N = write_nn(v,&i,N);
	  MAYBE_LARGER(N+i);
	  
	  N = write_integer(v+i, N, x);
	  i += N;

	  goto loop;
	}

      //Rational
      DBG(printf("rational\n"));
      if (scm_is_true(scm_negative_p(x)))
	{
	  ADD_F(j_rational_n);
	  x = scm_difference(zero_scm, x);
	}
      else
	ADD_F(j_rational_p);
      
      SCM upper = SCM_FRACTION_NUMERATOR(x);
      SCM lower = SCM_FRACTION_DENOMINATOR(x);

      uint64_t  N1 = scm_to_uint64(scm_integer_length (upper)) / 8UL;
      uint64_t  N2 = scm_to_uint64(scm_integer_length (lower)) / 8UL;
      
      if (N1 > 1<<12 || N2 > 1<<12) goto out_error;

      MAYBE_LARGER(N1 + N2 + i);
	
      N1 = write_nn(v,&i,N1);
      N2 = write_nn(v,&i,N2);
      N1 = write_integer(v+i   , N1, upper);
      N2 = write_integer(v+i+N1, N2, lower);
      i += N1 + N2;

      goto loop;
    }

  if (scm_is_symbol(x))
    {
      DBG(printf("symbol\n"));
      REF_HANDLER_Q(do_sym,goto loop,1)
	
      x = scm_symbol_to_string (x);
      DOSTRING(i_sym,i_wsym);
    }

  if (scm_is_string(x))
    {
      DBG(printf("string\n"));
      REF_HANDLER_E(do_str,goto loop,1);
      DOSTRING(i_str,i_wstr);
    }

  if (SCM_I_IS_VECTOR(x))
    {
      DBG(printf("vector *\n"));

      tag = i_vector;
      nn  = scm_to_long(scm_vector_length(x));

      TAGHANDLE(nn);
      
      REF_HANDLER_Q(do_ref,goto loop,1);

      k   = 0;
      vv  = SCM_I_VECTOR_ELTS(x);
      
    v_loop:
      if (i - ii >  nchunk ) goto v_out;
      if (k      >= nn     ) goto loop;
      
      x = vv[k];
      
      ATOM    (goto v_loop0);
      INEXACT (goto v_loop0);
      REFFER  (goto v_loop0);
      
      k++;
      PUSH_VEC(vv,t_vec);
      goto loop0;

    v_loop0:
      k++;      
      goto v_loop;
      
    v_out:
      PUSH_VEC(vv,t_vec);
      goto out;
    }

    
  if (scm_is_bytevector(x))
    {
      int diff;
      
      DBG(printf("bytevector\n"));
      REF_HANDLER_E(do_bv, goto loop,1);
      
      tag = i_bv;
      ww  = CONTENTS(x);

      nn  = SCM_BYTEVECTOR_LENGTH(x);

      TAGHANDLE(nn);
    bv0:            
      
      k = 0;
      
    bv_loop:
      diff = nn - k;
      if (nchunk < (i-ii) + diff) diff = nchunk - (i - ii);
      
      MAYBE_LARGER(i + diff);
      memcpy(v+i,ww+k,diff);
      i += diff;
      k += diff;
      
      if (k >= nn) goto loop;

      PUSH_VEC(ww,t_bv);
      goto out;
    }

  if (SCM_STRUCTP(x))
    {
      DBG(printf("struct *\n"));
      SCM vtable = SCM_STRUCT_VTABLE(x);
      SCM jj     = scm_hashq_ref(map3,vtable,SCM_BOOL_F);

      if (scm_is_true(jj))
	{
	  REF_HANDLER_Q(do_ref,goto loop,1);
	  
	  tag = i_struct;	  
	  int jjj = scm_to_uint(jj);
	  TAGHANDLE(jjj);
	  nn  = SCM_STRUCT_SIZE(x);
	  
	  k   = 0;
	  vv  = (SCM *) SCM_STRUCT_DATA(x);
      
	  goto v_loop;
	}
      else
	{
	  scm_misc_error("atom-serializer",
			 "you must register a struct that shall serialize",
			 SCM_EOL);
	}		       
    }
  
  if (scm_is_keyword(x))
    {
      DBG(printf("keyword\n"));
      REF_HANDLER_Q(do_key,goto loop,1);
      x = scm_keyword_to_symbol(x);
      x = scm_symbol_to_string (x);
      
      DOSTRING(i_keyw,i_wkeyw);
    }


  if (SCM_PROGRAM_P(x))
    {
      if (!do_lam)
	scm_misc_error("atom-serialize",
		       "found a procedure that can't be serialized",
		       SCM_EOL);

      REF_HANDLER_Q(do_ref,goto loop,1);

      int       prim     = SCM_PROGRAM_IS_PRIMITIVE(x);
      int       nfree    = prim ? 0 : SCM_PROGRAM_NUM_FREE_VARIABLES(x);
      ;         vv       = prim ? (SCM *) NULL :SCM_PROGRAM_FREE_VARIABLES(x);
      uint64_t  addr     = (uint64_t) SCM_PROGRAM_CODE(x);
      
      if (addr ==cont_addr)
	{
	  scm_misc_error("atom-serializer",
			 "can't serialize a continuation",
			 SCM_EOL);
	}
      else if (addr == abort_addr)
	{
	  scm_misc_error("atom-serializer",
			 "can't serialize an abort",
			 SCM_EOL);
	}
      else
	{
	  SCM       elf      = scm_call_1(find_mapped_elf_image,
					  scm_from_uint64(addr));
	  
	  uint64_t  elfaddr  = scm_to_uint64(gp_find_elf_relative_adress(elf));
	  
	  int64_t   rel_addr = addr - elfaddr;
	  
	  SCM       path     = scm_call_1(find_program_file,
					  scm_from_uint64(elfaddr));

	  if (rel_addr < 0)
	    {
	      scm_misc_error("atom-serialize",
			     "relative addres is negative",
			     SCM_EOL);
	    }
	  
	  SCM jpth      = scm_hash_ref(map4,path,SCM_BOOL_F);

	  if (scm_is_false(jpth))
	    {
	      scm_misc_error
		("atom-serializer",
		 "lambda module: ~a is not in the equal map (whitelist)",
		 scm_list_1(path));
	    }
	  
	  tag = i_lambda;
	  TAGHANDLE(nfree);

	  MAYBE_LARGER(8*2);
		    
	  i += write_nnn(v+i, rel_addr);

	  uint64_t jj = scm_to_uint64(jpth);
	  i += write_nnn(v+i, jj);
	  
	  nn = nfree;
	  k  = 0;

	  goto v_loop;
	}
    }
  
  if (SCM_HASHTABLE_P(x))
    {
      DBG(printf("hashtable\n"));
      
      uint64_t nhash;
      
      int kind = get_hash_kind(x, &nhash);

      if (kind >= 3)
	{
	  scm_misc_error("atom-serializer",
			 "encountered a hastable that was not serializable",
			 SCM_EOL);
	}
      
      if (kind == 0)
	{
	  DBG(printf("eq hash %ld\n",nhash));	  
	  ADD_F(j_hashq);
	}
      else if (kind == 1)
	{
	  DBG(printf("eqv hash %ld\n", nhash));	  
	  ADD_F(j_hashv);
	}
      else
	{
	  DBG(printf("equal hash %ld\n",nhash));	  
	  ADD_F(j_hash);
	}

      i += write_integer(v+i, 8, scm_from_uint64(nhash));
      
      x = get_hash_data_as_vector(x, nhash);

      nn  = 2*nhash;      
      k   = 0;
      vv  = (SCM *) SCM_I_VECTOR_ELTS(x);
      
      goto v_loop;
    }
  
 out:    
  return scm_list_6(SCM_BOOL_T,SCM_BOOL_T,
		    bv,scm_from_long(i),scm_from_long(j),stack);
  
 out_final:    
  return scm_list_6(SCM_BOOL_F,SCM_BOOL_T,
		    bv,scm_from_long(i),scm_from_long(j),stack);
  
 out_error:
  return scm_list_6(SCM_BOOL_F,SCM_BOOL_F,
		    bv,scm_from_long(i),scm_from_long(j),stack);

}
#undef FUNC_NAME

  
  
