/*
*****************************************************
*****************************************************
                    P R I N T E R
****************************************************
*/

#define MKBV_(int8_t, print_s8_, print_signed_number)			\
  inline void print_s8_(uint8_t *buf, size_t *bufpt, size_t len, int8_t *bv, \
			size_t *count, uint64_t flags, int Sh, int D)	\
  {									\
    size_t  bp  = *bufpt;						\
    size_t  cnt = *count;						\
    size_t  n   = 0;							\
  									\
    int sep = (flags & comma_flag) ? ',' : ' ';				\
    uint64_t nbin = (flags & binary_mask) >> binary_shift;		\
  									\
    while (bp <= len && n < cnt)					\
      {									\
	int8_t x = ((int8_t *)bv)[n];					\
      									\
	if (nbin)							\
	  bp += print_signed_number##_binary(x, buf + bp, flags,	\
					     Sh, D, 0);			\
	else								\
	  bp += print_signed_number(x, buf + bp, flags, Sh, D, 0);	\
									\
	n  += 1;							\
									\
	if (n != cnt)							\
	  {								\
	    bp += put_char(sep, buf + bp, Sh, D);			\
	  }								\
      }									\
									\
    *bufpt  = bp;							\
    *count -= n;							\
  }

MKBV_( int8_t , print_s8_ , print_int64 );
MKBV_(uint8_t , print_u8_ , print_uint64);

MKBV_( int16_t, print_s16_, print_int64 );
MKBV_(uint16_t, print_u16_, print_uint64);

MKBV_( int32_t, print_s32_, print_int64 );
MKBV_(uint32_t, print_u32_, print_uint64);

MKBV_( int64_t, print_s64_, print_int64 );
MKBV_(uint64_t, print_u64_, print_uint64);

MKBV_(float   , print_f32_, print_float  );
MKBV_(double  , print_f64_, print_double );

#define MKBV(typer,print_u8,scheme,print_u8_)				\
  SCM_DEFINE(print_u8, scheme, 5, 0, 0,					\
	     (SCM bv_, SCM start_, SCM buf_,				\
	      SCM bufpt_count, SCM flags_),				\
	     "***")							\
  {									\
    size_t bufpt    = scm_to_size_t(SCM_CAR(bufpt_count));		\
    size_t count    = scm_to_size_t(SCM_CDR(bufpt_count));		\
    size_t len      = scm_c_bytevector_length(buf_)-600;		\
    size_t start    = scm_to_size_t(start_);				\
    uint64_t flags  = scm_to_uint64(flags_);				\
    uint8_t *buf    = (uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_);	\
    typer   *bv     = (typer *) SCM_BYTEVECTOR_CONTENTS(bv_);		\
									\
    int Sh = 1;								\
    int D  = 0;								\
    MKShD();								\
									\
    if (Sh == 0)							\
      print_u8_(buf, &bufpt, len, bv + start,				\
		&count, flags, 0, 0);					\
    else								\
      print_u8_(buf, &bufpt, len, bv + start,				\
		&count, flags, Sh, D);					\
									\
    SCM_SETCAR(bufpt_count, scm_from_size_t(bufpt));			\
    SCM_SETCDR(bufpt_count, scm_from_size_t(count));			\
    									\
    return SCM_UNSPECIFIED;						\
  }


MKBV(uint8_t , print_u8 , "c-print-u8" , print_u8_ );
MKBV( int8_t , print_s8 , "c-print-s8" , print_s8_ );

MKBV(uint16_t, print_u16, "c-print-u16", print_u16_);
MKBV( int16_t, print_s16, "c-print-s16", print_s16_);

MKBV(uint32_t, print_u32, "c-print-u32", print_u32_);
MKBV( int32_t, print_s32, "c-print-s32", print_s32_);

MKBV(uint64_t, print_u64, "c-print-u64", print_u64_);
MKBV( int64_t, print_s64, "c-print-s64", print_s64_);

MKBV(float   , print_f32, "c-print-f32", print_f32_);
MKBV(double  , print_f64, "c-print-f64", print_f64_);

/*
**********************************************
**********************************************
                  R E A D E R
**********************************************
*/

typedef uint64_t read_t(char *buf, size_t *bufpt, SCM pos, int *e,
			uint64_t flags);

// BYTVECTOR READER LOOP PART
#define MKRBV(read_s8_, int_t, skip, buf_t, read)				\
  inline size_t read_s8_(buf_t *buf_, size_t *bufpt, size_t buflen,	\
			 uint8_t *bv_, size_t n, size_t len, SCM pos,	\
			 int *e, uint64_t flags)			\
  {									\
    buf_t   *buf = (buf_t *) buf_ + *bufpt;				\
    buflen -= *bufpt;							\
    int_t   *bv  = ((int_t *) bv_) + n;					\
    len -= n;								\
    n=0;								\
    size_t   d, bp  = 0;						\
    int64_t ch;								\
    int ee=0;								\
   									\
    while (bp < buflen && n < len)					\
      {									\
        d  = skip(buf,bp,100,pos);					\
	bp += d;							\
									\
	if (d < 0)							\
	  {								\
	    *e = 0;							\
	    *bufpt += bp;						\
	    return n;							\
	  }								\
									\
        ch = buf[bp];							\
									\
	if (ch == ')')							\
	  {								\
	    *e = 1;							\
	    *bufpt += bp + d;						\
	    return n;							\
	  }								\
									\
	d = 0;								\
	int_t x = read(buf+bp,&d,pos,&ee,flags);			\
									\
	if (ee == 0)							\
	  {								\
	    *e = 0;							\
	    *bufpt += bp;						\
	    return n;							\
	  }								\
									\
	bv[n] = x;							\
        n  += 1;							\
	bp += d;							\
      }	         							\
    									\
    *bufpt += bp;							\
    if (bp >= buflen && n >= len)					\
      {									\
	*e = 2;								\
      }									\
    else if (bp > buflen)						\
      {									\
	*e = 3;								\
      }									\
    else								\
      *e = 4;								\
									\
    return n;								\
  }



#define MKMKRBV(tag,skip,buf_t,b)					\
  MKRBV(read##b##bv_s8##tag ,   int8_t, skip, buf_t,read##b##integer_s##tag); \
  MKRBV(read##b##bv_u8##tag ,  uint8_t, skip, buf_t,read##b##integer_u##tag); \
  MKRBV(read##b##bv_s16##tag,  int16_t, skip, buf_t,read##b##integer_s##tag); \
  MKRBV(read##b##bv_u16##tag, uint16_t, skip, buf_t,read##b##integer_u##tag); \
  MKRBV(read##b##bv_s32##tag,  int32_t, skip, buf_t,read##b##integer_s##tag); \
  MKRBV(read##b##bv_u32##tag, uint32_t, skip, buf_t,read##b##integer_u##tag); \
  MKRBV(read##b##bv_s64##tag,  int64_t, skip, buf_t,read##b##integer_s##tag); \
  MKRBV(read##b##bv_u64##tag, uint64_t, skip, buf_t,read##b##integer_u##tag); \
  MKRBV(read##b##bv_f32##tag, float   , skip, buf_t,read##b##float##tag); \
  MKRBV(read##b##bv_f64##tag, double  , skip, buf_t,read##b##double##tag);


#define MKMKMKRBV(b)					\
MKMKRBV(_utf8     , skip_utf8     , uint8_t ,b);	\
MKMKRBV(_utf16    , skip_utf16    , uint16_t,b);	\
MKMKRBV(_utf16_rev, skip_utf16_rev, uint16_t,b);	\
MKMKRBV(_utf32    , skip_utf32    , uint32_t,b);	\
MKMKRBV(_utf32_rev, skip_utf32_rev, uint32_t,b);

MKMKMKRBV(_);
MKMKMKRBV(_b_);


#define READBV3(tag,tag2,tag3,tag4,tag5)				\
  {									\
    return read##tag2##bv_##tag5##tag(buf,bufpt,buflen,bv,n,len,pos,e,flags); \
  }

#define READBV2(tag,tag2)					\
  {								\
  if (real)							\
    {								\
      if (rank == 3)						\
	{							\
	  READBV3(tag, tag2, double, , f64);			\
	}							\
      else if (rank == 2)					\
	{							\
	  READBV3(tag, tag2, float , , f32);			\
	}							\
      else							\
	scm_misc_error("read","wrong real rank", SCM_EOL);	\
    }								\
								\
  sgn = flags & read_sgn_flag;					\
								\
  if (sgn)							\
    {								\
      if (rank == 0)						\
	{							\
	  READBV3(tag,tag2,integer,_s,s8);			\
	}							\
      else if (rank == 1)					\
	{							\
	  READBV3(tag,tag2,integer,_s,s16);			\
	}							\
      else if (rank == 2)					\
	{							\
	  READBV3(tag,tag2,integer,_s,s32);			\
	}							\
      else if (rank == 3)					\
	{							\
	  READBV3(tag,tag2,integer,_s,s64);			\
	}							\
    }								\
  else								\
    {								\
      if (rank == 0)						\
	{							\
	  READBV3(tag,tag2,integer,_u,u8);			\
	}							\
      else if (rank == 1)					\
	{							\
	  READBV3(tag,tag2,integer,_u,u16);			\
	}							\
      else if (rank == 2)					\
	{							\
	  READBV3(tag,tag2,integer,_u,u32);			\
	}							\
      else if (rank == 3)					\
	{							\
	  READBV3(tag,tag2,integer,_u,u64);			\
	}							\
    }								\
}

#define READBV(tag)					\
  {							\
    if (nbits)						\
      {							\
	READBV2(tag,_b_);				\
      }							\
    else						\
      {							\
	READBV2(tag,_);					\
      }							\
  }


// Main bytvector dispatcher code
size_t c_read_bytevector_(uint8_t *buf_, size_t *bufpt, size_t buflen,
			  uint8_t *bv, size_t n, size_t len,
			  int *e, SCM pos, uint64_t flags)
{
  int Sh,D,real,sgn,rank,nbits;
  Sh    = (flags & read_sh_mask) >> read_sh_shift;
  rank  = (flags & read_bits_mask ) >> read_bits_shift;
  real  = flags & read_real_flag;
  nbits = (flags & read_bins_mask ) >> read_bins_shift;
  
  if (Sh == 0)
    {
      uint8_t *buf = buf_;
      READBV(_utf8);
    }
  else if  (Sh == 1)
    {
      D  = (flags & read_d_mask ) >> read_d_shift;
      uint16_t *buf = (uint16_t *)buf_;
      if (D == 1)
	{
	 if (is_me_big)
	   {
	     READBV(_utf16);
	   }
	 else
	   {
	     READBV(_utf16_rev);
	   }
	}
      else
	{
	  if (is_me_big)
	    {
	      READBV(_utf16_rev);
	    }
	  else
	    {
	      READBV(_utf16);
	    }
	}
    }
  else if  (Sh == 2)
    {
      uint32_t *buf = (uint32_t *)buf_;
      D  = (flags & read_d_mask ) >> read_d_shift;
      if (D == 3)
	{
	  if (is_me_big)
	    {
	      READBV(_utf32);
	    }
	  else
	    {
	      READBV(_utf32_rev);
	    }
	}
      else
	{
	  if (is_me_big)
	    {
	      READBV(_utf32_rev);
	    }
	  else
	    {
	      READBV(_utf32);
	    }
	}
    }

  scm_misc_error("read", "could not find dispatcher",SCM_EOL);
  return 0;
}


SCM_DEFINE(c_read_bytevector, "c-read-bytevector", 8, 0, 0,
	   (SCM buf_, SCM buflen_,
	    SCM bv_, SCM n_, SCM len_,
	    SCM bufpt_e, SCM pos, SCM flags_),
	   "find a most probable a list match")
#define FUNC_NAME c_read_bytevector_s
{
  uint8_t   *bv    = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(bv_ ));
  uint8_t   *buf   = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_));
  size_t    buflen = scm_to_size_t(buflen_);
  size_t    len    = scm_to_size_t(len_);
  size_t    n      = scm_to_size_t(n_);
  uint64_t  flags  = scm_to_uint64(flags_);
  int       e      = 0;
  size_t    bufpt  = scm_to_size_t(SCM_CAR(bufpt_e));

  n = c_read_bytevector_(buf, &bufpt, buflen, bv, n, len, &e, pos, flags);
  
  SCM_SETCAR(bufpt_e, scm_from_size_t(bufpt));
  SCM_SETCDR(bufpt_e, scm_from_size_t(e));
  
  return scm_from_size_t(n);
}
#undef FUNC_NAME

