#undef DB
#define DB(X)

int dispatch_number(SCM x, uint8_t *buf, uint64_t flags, int Sh, int D)
{
  uint64_t nbin = (flags && binary_mask) >> binary_shift;
  
  if (SCM_INEXACTP (x))
    {
      SCM *v = (SCM *)x;
      double y1 = ((double *)v)[1];
      if (SCM_REALP(x))
	{
	  if (nbin)
	    return print_double_binary(y1, buf, flags, Sh, D, 1);
	  else
	    return print_double(y1, buf, flags, Sh, D, 0);	    
	}
      else
	{
	  int n = 0;
	  double y2 = ((double *)v)[2];
	  if (nbin)
	    {
	      n  = print_double_binary(y1, buf, flags, Sh, D, 1);
	      n += put_char('+',buf+n, Sh, D);
	      n += print_double_binary(y2, buf + n, flags, Sh, D, 1);
	      n += put_char('i',buf+n, Sh, D);
	    }
	  else
	    {
	      n  = print_double(y1, buf, flags, Sh, D, 0);
	      n += put_char('+',buf+n, Sh, D);
	      n += print_double(y2, buf + n, flags, Sh, D, 0);
	      n += put_char('i',buf+n, Sh, D);
	    }

	  return n;
	}
    }

  return -1;
}


#define MKNUMERATI(tag, int_t, SWAP, N)					\
  int numerati_##tag (uint8_t *buf_, size_t len, size_t *buffpt,	\
		      int *e, uint64_t flags, int state)		\
  {									\
    int  bins   = (flags & read_bins_mask) >> read_bins_shift;		\
    int  M      = (1 << bins) - 10;					\
    int_t *buf  = (int_t *) buf_;					\
    size_t pt   = *buffpt/N;						\
    int flt     = state & 1;						\
    int exp     = state & 2;						\
    int num     = state & 4;						\
    int complex = flags & read_complex_flag;				\
									\
    for (;pt*N < len; pt += 1)						\
      {									\
	int64_t ch2 = 0, ch = SWAP(buf[pt]);				\
	if (ch == '.')							\
	  {								\
	    if (!flt)							\
	      {								\
		flt = 1;						\
		continue;						\
	      }								\
	    else							\
	      {								\
		*e = 2;							\
		goto out;						\
	      }								\
	  }								\
	else if (bins < 4 ? (ch == 'e' || ch == 'E')			\
		          : (ch == 'z' || ch == 'Z'))			\
	  {								\
	    if (!exp)							\
	      {								\
		num = 0;						\
		exp = 2;						\
		continue;						\
	      }								\
	    else							\
	      {								\
		*e = 3;							\
		goto out;						\
	      }								\
	  }								\
	else if (ch == '+' || ch == '-')				\
	  {								\
	    if (!num)							\
	      continue;							\
									\
	    *e = 4;							\
	    goto out;							\
	  }								\
	else if(complex && ch == 'i')					\
	  {								\
	    *e = 6;							\
	    goto out;							\
	  }								\
	else								\
	  {								\
	    ch2 = ch - '0';						\
	    if (ch2 >= 0 && ch2 <= 9)					\
	      {								\
		num = 4;						\
		continue;						\
	      }								\
	    								\
	    if (bins >= 4)						\
	      {								\
	        ch2 = ch - 'a';						\
		if (ch2 >= 0 && ch2 < M)				\
		  {							\
		    num = 4;						\
		    continue;						\
		  }							\
									\
		ch2 = ch - 'A';						\
		if (ch2 >= 0 && ch2 < M)				\
		  {							\
		    num = 4;						\
		    continue;						\
		  }							\
	       }							\
	       if (ch == '/' && num)					\
		 {							\
		   *e=5;						\
		   goto out;						\
		 }							\
	       if (ch == 0 || ch == ' ' || ch == '\n' || ch == '\r'	\
		   || ch == '\t')					\
		 *e = ((exp || flt || (flags & read_real_flag)) ? 1 : 0); \
	       else							\
		 *e = 7;						\
									\
	       goto out;						\
	   }							        \
       }								\
									\
    *e = 8;								\
  out:									\
    *buffpt = pt*N;							\
    return exp | flt;							\
  }


MKNUMERATI(utf8     , uint8_t , REV0 , 1);
MKNUMERATI(utf16    , uint16_t, REV0 , 2);
MKNUMERATI(utf16_rev, uint16_t, rev16, 2);
MKNUMERATI(utf32    , uint32_t, REV0 , 4);
MKNUMERATI(utf32_rev, uint32_t, rev32, 4);


int numerati(uint8_t *buf_, size_t len, size_t *buffpt, int *e, uint64_t flags, int state)
{
  int Sh,D;
  MKRShD()						\

  if (Sh == 0)
    {
      return numerati_utf8(buf_,len,buffpt,e,flags,state);
    }
  else if (Sh == 1)
    {
      if (D == 1)
	{
	  if (is_me_big)
	    return numerati_utf16(buf_,len,buffpt,e,flags,state);
	  else
	    return numerati_utf16_rev(buf_,len,buffpt,e,flags,state);
	}
      else
	{
	  if (is_me_big)
	    return numerati_utf16_rev(buf_,len,buffpt,e,flags,state);
	  else
	    return numerati_utf16(buf_,len,buffpt,e,flags,state);
	}
    }
  else if (Sh == 2)
    {
      if (D == 3)
	{
	  if (is_me_big)
	    return numerati_utf32(buf_,len,buffpt,e,flags,state);
	  else
	    return numerati_utf32_rev(buf_,len,buffpt,e,flags,state);
	}
      else
	{
	  if (is_me_big)
	    return numerati_utf32_rev(buf_,len,buffpt,e,flags,state);
	  else
	    return numerati_utf32(buf_,len,buffpt,e,flags,state);
	}
    }
  else
    scm_misc_error("read","not a valid encoding",SCM_EOL);

  return 0;
}

SCM_DEFINE(c_numerati, "c-numerati", 5, 0, 0,
	   (SCM buf_, SCM len_, SCM bufpt_e, SCM flags_, SCM state),
	   "find a most probable a list match")
#define FUNC_NAME print_fixnum_c_
{
  size_t    bp    = scm_to_size_t(SCM_CAR(bufpt_e));
  uint8_t   *buf  = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_));
  int64_t   len   = scm_to_size_t(len_);
  uint64_t  flags = scm_to_uint64(flags_);
  int       e     = 0;

  int res = numerati(buf, len, &bp, &e, flags, scm_to_int(state));

  SCM_SETCAR(bufpt_e, scm_from_size_t(bp));
  SCM_SETCDR(bufpt_e, scm_from_int(e));

  return scm_from_int(res);
}

#undef FUNC_NAME

SCM_DEFINE(print_real, "print-real", 4, 0, 0,
	   (SCM bv, SCM x_, SCM bufpt, SCM flags_),
	   "find a most probable a list match")
#define FUNC_NAME print_real_c
{
  size_t    bp    = scm_to_size_t(bufpt);					
  uint8_t  *pt    = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(bv)) + bp;
  double*   xx    = (double *)x_;
  uint64_t  flags = scm_to_uint64(flags_);

  int Sh = 0;
  int D  = 0;
  MKShD();

  uint64_t nbin = (flags & binary_mask) >> binary_shift;
  
  if (nbin)
    {
      if (Sh == 0)
	return scm_from_int(print_double_binary(xx[1], pt, flags, 0 , 0, 1));
      else
	return scm_from_int(print_double_binary(xx[1], pt, flags, Sh, D, 1));
    }
  else
    {
      if (Sh == 0)
	return scm_from_int(print_double(xx[1], pt, flags, 0 , 0, 0));
      else
	return scm_from_int(print_double(xx[1], pt, flags, Sh, D, 0));
    }
  
}
#undef FUNC_NAME



SCM_DEFINE(print_fixnum_, "print-fixnum", 4, 0, 0,
	   (SCM bv, SCM x_, SCM bufpt, SCM flags_),
	   "find a most probable a list match")
#define FUNC_NAME print_fixnum_c_
{
  size_t    bp    = scm_to_size_t(bufpt);
  uint8_t   *pt   = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(bv)) + bp;
  int64_t   x     = scm_to_int64(x_);
  uint64_t  flags = scm_to_uint64(flags_);

  int Sh = 1;
  int D  = 0;
  MKShD();

  uint64_t nbin = (flags & binary_mask) >> binary_shift;
  if (nbin)
    {
      if (Sh == 0)
	return scm_from_int(print_int64_binary(x, pt, flags, 0 , 0, 1));
      else
	return scm_from_int(print_int64_binary(x, pt, flags, Sh, D, 1));
    }
  else
    {
      if (Sh == 0)
	return scm_from_int(print_int64(x, pt, flags, 0 , 0, 0));
      else
	return scm_from_int(print_int64(x, pt, flags, Sh, D, 0));
    }    
}
#undef FUNC_NAME
