int dispatch_char(SCM x, uint8_t *buf, uint64_t flags, int Sh, int D)
{
  return -1;
}

int dispatch(SCM x, uint8_t *buf, uint64_t flags, int Sh, int D, int nbin)
{
  int tc3 = SCM_ITAG3(x);

  if ((tc3 & 3) == scm_tc2_int)
    {
      if (nbin)
	return print_int64_binary(scm_to_int64(x), buf, flags, Sh , D, 1);
      else
	return print_int64(scm_to_int64(x), buf, flags, Sh , D, 0);
	
    }

  if (tc3 == 0)
    {
      int tc7 = SCM_TYP7(x);
     
      if (tc7 == scm_tc7_number)
	{
	  return dispatch_number(x, buf, flags, Sh, D);
	}
      else if (tc7 ==  scm_tc7_string)
	{
	  return dispatch_string(x, buf, 64, flags, Sh, D);
	}
      else if (tc7 == scm_tc7_symbol)
	{
	  return dispatch_symbol(x, buf, 64, flags, Sh, D);
	}
      else if (tc7 == scm_tc7_keyword)
	{
	  int d = put_string("#:",buf,Sh,D);
	  return dispatch_symbol(scm_keyword_to_symbol(x), buf + d, 64, flags, Sh, D);
	}
    }

  if (tc3 == scm_tc3_imm24)
    {
      int tc16 = SCM_TYP16(x);
      if (tc16 == scm_tc8_flag)
	{
	  if (scm_is_eq(x, SCM_BOOL_T))
	    {
	      return put_string("#t",buf,Sh,D);
	    }
	  else if (scm_is_eq(x, SCM_BOOL_F))
	    {
	      return put_string("#f",buf,Sh,D);
	    }
	}
      else if (tc16 == scm_tc8_char)
	{
	  return dispatch_char(x, buf, flags, Sh, D);
	}
    }
  
  return -1;
}
