#include <quadmath.h>
#define DB(X)
long double mypow[1100];
__float128  mypowq[1100];
int  MANBITS32  = 0;
int  MANBITS64  = 0;
int  MANBITS128 = 0;
int  DIG32;
int  DIG64;
int  DIG128;
typedef struct
{
  __float128 d;
  int exp;
} exp_t;

typedef struct
{
  double d;
  int exp;
} fexp_t;

typedef struct
{
  long double d;
  int exp;
} lexp_t;

exp_t  emap2[3000];
lexp_t emap[3000];
fexp_t fmap[500];



#define RUNX(i)						\
  if (unlikely(!val)) goto out;				\
  							\
  ch = (val / M[i]);						\
  pt[nn + D] = '0' + ch;					\
  nn += N;							\
  val -= ch*M[i];						\
  m++;								\


#define PP(n10,E,print_uint64,prefix)				\
  if (exp < 0 && -exp + m <= n10)				\
    {								\
      int offset = -exp+1;					\
      mymemmove(pt + n + (offset<<Sh),pt + n, m, Sh, D);	\
      mymemset(pt + n, '0', offset, Sh, D);			\
      pt[n+N+D]='.';						\
      n += (m + offset)<<Sh;					\
    }								\
  else if (exp == 0)						\
    {								\
      mymemmove(pt + n + 2*N, pt + n + N, m - 1, Sh, D);	\
      pt[n+N+D] = '.';						\
      if (m - 1 == 0)						\
	{							\
	  pt[n+2*N+D] = '0';					\
	  m++;							\
	}							\
      n += (m + 1) << Sh;					\
    }								\
  else if (exp > 0 && m + exp < 10 && m <= exp+1)		\
    {								\
      int offset = exp - m + 1;					\
      mymemset(pt + n + (m<<Sh), '0', offset, Sh, D);		\
      n += (m + offset) << Sh;					\
      pt[n+D] = '.';						\
      n+= N;							\
      pt[n+D] = '0';						\
      n+=N;							\
    }								\
  else								\
    {								\
      mymemmove(pt + n + 2*N,pt + n + N, m-1, Sh, D);		\
      pt[n+N+D] = '.';						\
      if (m - 1 == 0)						\
	{							\
	  pt[n+2*N+D] = '0';					\
	  m++;							\
	}							\
								\
      n += (m + 1)<<Sh;						\
      pt[n + D] = E;						\
      n += N;							\
      if (exp < 0)						\
	{							\
	  pt[n + D] = '-';					\
	  n  += N;						\
	  exp = -exp;						\
	}							\
								\
      nexp = print_uint64(exp, pt+n, flags, Sh, D, prefix);	\
    }

int print_ldouble(__float128 d, uint8_t *pt, uint64_t flags,
		  int Sh, int D, int prefix)
{  
  int N = 1 << Sh;
  int n = 0, m = 0;

  if (d == 0)
    {
      pt[n     + D] = '0';
      pt[n+  N + D] = '.';
      pt[n+2*N + D] = '0';

      return 3*N;
    }
  
  if (unlikely(d < 0))
    {
      d = -d;
      pt[(n>>Sh)+D] = '-';
      n+=N;
    }

  int exp2;
  frexpq(d,&exp2);

  int        exp   = exp2+17;
  __float128 dd    = emap2[exp2+1023].d;

  d   *= dd;
  uint128_t val  = (uint128_t)d;
  
  int digit100 = val % 100;
  int digit    = digit100 % 10;

  if (unlikely(digit100<20))
    val -= digit100;
  else
    {
      if (unlikely(digit >= 5))
	val+=10-digit;
      else
	val-=digit;
    }

  int64_t ch;

  int ddd = 0;
  int ppp = 1;
  for(;val;m++, val /= 10, n += N)
    {
      ch = (val % 10);
      if (ppp && !ch)
	ddd++;
      else
	ppp = 0;
      
      pt[n + D] = '0' + ch;					
      val -= ch;
    }

  n  -= N*m;
  m  -= ddd;

  for (int i = 0; i < m/2; i++)
    {
      ch = pt[n-i*N+D];
      pt[n-i*N+D] = pt[n-(m-i-1+ddd)*N+D];
      pt[n-(m-i-1+ddd)*N+D] = ch;     
    }
  
  n += N*m;
  
  int nexp = 0;

  DB(printf("m = %d\n",m));

  PP(10, 'e', print_uint64, 0);

  return n + nexp;
}


int print_double(double d, uint8_t *pt, uint64_t flags, int Sh, int D, int prefix)
{
  uint64_t M[18];
  M[ 1] = 10;
  M[ 2] = 100;
  M[ 3] = 1000UL;
  M[ 4] = 1000UL*10;
  M[ 5] = 1000UL*100;
  M[ 6] = 1000UL*1000;
  M[ 7] = 1000UL*1000*10;
  M[ 8] = 1000UL*1000*100;
  M[ 9] = 1000UL*1000*1000;
  M[10] = 1000UL*1000*1000*10;
  M[11] = 1000UL*1000*1000*100;
  M[12] = 1000UL*1000*1000*1000;
  M[13] = 1000UL*1000*1000*1000*10;
  M[14] = 1000UL*1000*1000*1000*100;
  M[15] = 1000UL*1000*1000*1000*1000;
  M[16] = 1000UL*1000*1000*1000*1000*10;
  M[17] = 1000UL*1000*1000*1000*1000*100;
  
  int N   = 1 << Sh;
  int n = 0, m = 0;

  if (d == 0)
    {
      pt[n     + D] = '0';
      pt[n+  N + D] = '.';
      pt[n+2*N + D] = '0';

      return 3*N;
    }
  
  if (unlikely(d < 0))
    {
      d = -d;
      pt[(n>>Sh)+D] = '-';
      n+=N;
    }

  int exp2;
  frexp(d,&exp2);
  exp2 += 1023;
  int         exp   = emap[exp2].exp;
  long double dd    = emap[exp2].d;

  dd *= d;
  uint64_t val  = (uint64_t)dd;

  DB(printf ("val:\n%lu\n100000000000000000\nexp = %d\n",val,exp));

  int digit100 = val % 100;
  int digit    = digit100 % 10;

  if (unlikely(digit100<20))
    val -= digit100;
  else
    {
      if (unlikely(digit >= 5))
	val+=10-digit;
      else
	val-=digit;
    }


  if (unlikely(val < 100000000000000000UL))
    {
      val *=10;
      exp --;
    }
  if (unlikely(val < 100000000000000000UL))
    {
      val *=10;
      exp--;
    }

  DB(printf ("val = %lu, exp %d : exp2 %d\n",val,exp,exp2));

  int64_t ch;
  
  int nn = n;
  RUNX(17);
  RUNX(16);
  RUNX(15);
  RUNX(14);
  RUNX(13);
  RUNX(12);
  RUNX(11);
  RUNX(10);
  RUNX( 9);
  RUNX( 8);
  RUNX( 7);
  RUNX( 6);
  RUNX( 5);
  RUNX( 4);
  RUNX( 3);
  RUNX( 2);
  
  int nexp = 0;
  
 out:
  DB(printf("m = %d\n",m));

  PP(10, 'e', print_uint64, 0);

  return n + nexp;
}

#define DB(X)

int print_ldouble_binary(__float128 d, uint8_t *pt, uint64_t flags,
			int Sh, int D, int prefix)
{
  int nbits = (flags & binary_mask) >> binary_shift;  
  int N   = 1<<Sh;
  int n   = 0;
  int m   = 0;
  
  if (d == 0)
    {
      pt[n     + D] = '0';
      pt[n+  N + D] = '.';
      pt[n+2*N + D] = '0';

      return 3*N;
    }

  if (unlikely(d < 0))
    {
      d = -d;
      pt[n+D] = '-';
      n+=N;
    }

  if (prefix)
    {
      static const char binpre[6] = {'b','b','q','o','x','y'};
      n += put_char('#'          ,pt + n, Sh, D);
      n += put_char(binpre[nbits],pt + n, Sh, D);
    }

  int       exp;
  uint128_t man = ldexpq(frexpq(d,&exp), MANBITS128);
  
  int z = MANBITS128 % nbits;
  if (z > 0) man <<= (nbits - z);
  
  if (exp >= 0)
    {
      int u   = (exp % nbits);
      man    <<= u;
      exp    /= nbits;
    }
  else
    {
      exp = - exp;
      
      int u = nbits  -  (exp % nbits);
      if (u == nbits)
	{
	  exp /= nbits;
	}
      else
	{
	  man <<= u;      
	  exp  /= nbits;
	  exp  += 1;
	}

      exp = - exp;
    }


  int nn = print_uint128_binary_cut(man, pt + n, flags, Sh, D, 0, 1);
  m = (nn >> Sh);
  
  int nexp = 0;

  PP(10,'Z',print_uint64_binary,0);
  return n + nexp;
}

int print_double_binary(double d, uint8_t *pt, uint64_t flags,
			int Sh, int D, int prefix)
{
  int nbits = (flags & binary_mask) >> binary_shift;  
  int N   = 1<<Sh;
  int n   = 0;
  int m   = 0;
  
  if (d == 0)
    {
      pt[n     + D] = '0';
      pt[n+  N + D] = '.';
      pt[n+2*N + D] = '0';

      return 3*N;
    }

  if (unlikely(d < 0))
    {
      d = -d;
      pt[n+D] = '-';
      n+=N;
    }

  if (prefix)
    {
      static const char binpre[6] = {'b','b','q','o','x','y'};
      n += put_char('#'          ,pt + n, Sh, D);
      n += put_char(binpre[nbits],pt + n, Sh, D);
    }

  int      exp;
  uint64_t man = ldexp(frexp(d,&exp),MANBITS64);
  
  int z = MANBITS64 % nbits;
  if (z > 0) man <<= (nbits - z);
  
  if (exp >= 0)
    {
      int u   = (exp % nbits);
      man   <<= u;
      exp    /= nbits;
    }
  else
    {
      exp = - exp;
      
      int u = nbits  -  (exp % nbits);
      if (u == nbits)
	{
	  exp /= nbits;
	}
      else
	{
	  man <<= u;      
	  exp  /= nbits;
	  exp  += 1;
	}

      exp = - exp;
    }


  int nn = print_uint64_binary_cut(man, pt + n, flags, Sh, D, 0, 1);
  m = (nn >> Sh);
  
  int nexp = 0;

  PP(10,'Z',print_uint64_binary,0);
  return n + nexp;
}

int print_float_binary(float d, uint8_t *pt, uint64_t flags,
		       int Sh, int D, int prefix)
{
  int nbits = (flags & binary_mask) >> binary_shift;
  int N   = 1<<Sh;
  int n   = 0;
  int m   = 0;
  
  if (d == 0)
    {
      pt[n     + D] = '0';
      pt[n+  N + D] = '.';
      pt[n+2*N + D] = '0';

      return 3*N;
    }

  if (unlikely(d < 0))
    {
      d = -d;
      pt[n+D] = '-';
      n+=N;
    }

  if (prefix)
    {
      static const char binpre[6] = {'b','b','q','o','x','y'};
      n += put_char('#'          ,pt + n, Sh, D);
      n += put_char(binpre[nbits],pt + n, Sh, D);
    }

  int exp;
  uint64_t man = ldexpf(frexpf(d,&exp), MANBITS32);

  int z = MANBITS32 % nbits;
  if (z > 0) man <<= (nbits - z);
  
  if (exp >= 0)
    {
      int u   = (exp % nbits);
      man   <<= u;
      exp    /= nbits;
    }
  else
    {
      exp = - exp;
      
      int u = nbits  -  (exp % nbits);
      if (u == nbits)
	{
	  exp /= nbits;
	}
      else
	{
	  man <<= u;      
	  exp  /= nbits;
	  exp  += 1;
	}

      exp = - exp;
    }


  int nn = print_uint64_binary_cut(man, pt + n, flags, Sh, D, 0, 1);
  m = (nn >> Sh);
  
  int nexp = 0;

  PP(10, 'Z', print_uint64_binary, 0);
  
  return n + nexp;
}

int print_float(float d, uint8_t *pt, uint64_t flags,
		int Sh, int D, int prefix)
{
  uint64_t M[9];
  M[ 1] = 10;
  M[ 2] = 100;
  M[ 3] = 1000UL;
  M[ 4] = 1000UL*10;
  M[ 5] = 1000UL*100;
  M[ 6] = 1000UL*1000;
  M[ 7] = 1000UL*1000*10;
  M[ 8] = 1000UL*1000*100;

  int N = 1<<Sh;
  int n = 0, m = 0;

  if (d == 0)
    {
      pt[n     + D] = '0';
      pt[n+  N + D] = '.';
      pt[n+2*N + D] = '0';

      return 3*N;
    }
  
  if (unlikely(d < 0))
    {
      d = -d;
      pt[D] = '-';
      n += N;
    }

  int exp2;
  frexpf(d,&exp2);
  
  exp2 += 250;

  int    exp   = fmap[exp2].exp;
  double dd    = fmap[exp2].d;

  double ddd   = d * dd;
  uint64_t val = (uint32_t)ddd;

  int64_t ch;

  DB(printf ("val:\n%lu\n10000000\nexp = %d\n",val,exp));

  int digit    = val % 10;
  exp--;
  
  {
    if (unlikely(digit >= 5))
      val+=10-digit;
    else
      val-=digit;
  }


  if (unlikely(val < 10000000))
    {
      val *=10;
      exp --;
    }
  if (unlikely(val < 10000000))
    {
      val *=10;
      exp--;
    }

  DB(printf ("val = %lu, exp %d : exp2 %d\n",val,exp,exp2));

  int nn = n;
  RUNX( 7);
  RUNX( 6);
  RUNX( 5);
  RUNX( 4);
  RUNX( 3);
  RUNX( 2);
  RUNX( 1);
  
  int nexp = 0;
  
 out:
  DB(printf("m = %d\n",m));

  PP(10, 'e', print_uint64, 0);  

  return n + nexp;
}


#define MKRFLOAT(read_double, int_t, float_type, REV,			\
		 read_integer, M1, M, uint64_t,mypow)			\
  float_type read_double(int_t *buf, size_t *bufpt, SCM pos ,int *e,	\
			 uint64_t flags)				\
  {									\
    int i,j,jj,ii,j1,j2,j3,ex,sgn = 0;					\
    									\
    int64_t  ch, ch2;							\
    uint64_t exp, x1, x2 ;						\
    									\
    float_type x;							\
    									\
    buf += *bufpt;							\
    size_t bp = 0;							\
    ch = REV(buf[bp]);							\
    if (ch == '-')							\
      {									\
	bp += 1;							\
	sgn = 1;							\
      }									\
    									\
    i = 0;								\
    while (i < 100)							\
      {									\
	ch = REV(buf[bp+i]);						\
	if (ch != '0') break;						\
	i++;								\
      }									\
    									\
    if (i == 100)							\
      {									\
	*e = 0;								\
	return 0;							\
      }									\
    									\
    if (i)								\
      {									\
	bp += i;							\
	incr_pos(pos,i);						\
      }									\
    									\
    j = 0;								\
    while (j <= M)							\
      {									\
	ch = REV(buf[bp+j]);						\
	ch2 = ch - '0';							\
	if (ch2 <  0 || ch2 >= 10) break;				\
	j++;								\
      }									\
    									\
    jj = j;								\
    if (j == M)								\
      {									\
	while (jj < 100)						\
	  {								\
	    ch = REV(buf[bp+jj]);					\
	    ch2 = ch - '0';						\
	    if (ch2 <  0 || ch2 > 9) break;				\
	  }								\
									\
	if (jj == 100)							\
	  {								\
	    *e = 0;							\
	    return 0;							\
	  }								\
      }									\
    									\
    exp = 1;								\
    x2  = 0;								\
    for (x1 = 0, i = j-1; i >= 0; i--, exp *= 10)			\
      {									\
	ch   = REV(buf[bp+i]);						\
	ch2  = ch - '0';						\
	x1  += ch2*exp;							\
      }									\
    									\
    if (jj)								\
      {									\
        bp += jj;							\
	j1  = jj - j;							\
	incr_pos(pos,jj);						\
      }									\
    									\
    ch = buf[bp];							\
									\
    j2 = 0;								\
    j3 = 0;								\
    if (ch == '.')							\
      {									\
        bp ++;								\
	incr_pos(pos,1);						\
									\
	i = 0;								\
	for(; i < 100; i++)						\
	  {								\
	    ch  = REV(buf[bp+i]);					\
	    ch2 = ch - '0';						\
	    if (ch2 != 0) break;					\
	  }								\
									\
	if (i == 100)							\
	  {								\
	    *e = 0;							\
	    return 0;							\
	  }								\
	    								\
	if (i)								\
	  {								\
	    j2 = i;							\
	    incr_pos(pos,i);						\
	    bp += i;							\
	    jj += i;							\
	  }								\
	   								\
	for(i = 0; jj < M; i++, jj++)					\
	  {								\
	    ch = REV(buf[bp+i]);					\
	    ch2 = ch - '0';						\
	    if (ch2 < 0 || ch2 > 9)  break;				\
	  }								\
									\
	ii=i;								\
	if (jj == M)							\
	  {								\
	    for(; jj < 100; jj++,ii++)					\
	      {								\
		ch = REV(buf[bp+ii]);					\
		ch2 = ch - '0';						\
		if (ch2 <  0 || ch2 > 9) break;				\
	      }								\
	    								\
	    if (jj == 100)						\
	      {								\
		*e = 1;							\
		return 0;						\
	      }								\
	  }								\
									\
	if (i)								\
	  {								\
	    j  = i;							\
            exp = 1;							\
            for (i = j-1; i >= 0; i--, exp *= 10)			\
	      {								\
                ch  = REV(buf[bp+i]);					\
                ch2 = ch - '0';					        \
		x2 += ch2 * exp;					\
	      }								\
	    j3  = j;							\
	  }								\
									\
        if (ii)								\
	  {								\
	    bp  += ii;							\
	    incr_pos(pos, ii);						\
	  }								\
      }							                \
									\
    ch = REV(buf[bp]);							\
    									\
    ex = 0;								\
    if (ch == 'E' || ch == 'e')						\
      {									\
	bp ++;								\
	incr_pos(pos,1);						\
	int e2 = 0;							\
	ex = read_integer(buf,&bp,pos,&e2,flags|3);			\
	if (!e2)							\
	  {								\
	    *e = 0;							\
	    return 0;							\
	  }								\
      }									\
									\
    int ex1 = j1     + ex;						\
    int ex2 = -j2-j3 + ex;						\
									\
    if (ex2 < -M1)							\
      {									\
	x = powf(10,-20);						\
      }									\
    else if (ex1 > M1)							\
      {									\
	x = powf(10, 20);						\
      }									\
    else								\
      x = (x1 ? (x1*mypow[500 + ex1]) : 0) +				\
  	  (x2 ? (x2*mypow[500 + ex2]) : 0);				\
    									\
    									\
    *bufpt += bp;							\
    *e = 1;								\
    return sgn?-x:x;							\
  }    

#define MKMKRFLOAT(tag, int_t, REV)					\
  MKRFLOAT(read_qdouble##tag, int_t, __float128 , REV,			\
	   read_integer_s##tag, 499, (DIG128+1), uint128_t,		\
	   mypowq);							\
  MKRFLOAT(read_double##tag, int_t, double , REV,			\
	   read_integer_s##tag, 500, 19, uint64_t, mypow);		\
  MKRFLOAT(read_float##tag , int_t, float, REV,				\
	   read_integer_s##tag, 100, 10, uint64_t, mypow);

MKMKRFLOAT(_utf8     ,  uint8_t, REV0 );
MKMKRFLOAT(_utf16    , uint16_t, REV0 );
MKMKRFLOAT(_utf16_rev, uint16_t, rev16);
MKMKRFLOAT(_utf32    , uint32_t, REV0 );
MKMKRFLOAT(_utf32_rev, uint32_t, rev32);


#define MKRFLOATBIN(read_double_binary, int_t, REV, float_type,		\
		    M, M1, read_b_integer, uint64_t, ldexp)		\
  									\
  float_type read_double_binary(int_t *buf, size_t *bufpt, SCM pos, int *e, \
				uint64_t flags)				\
  {									\
    uint64_t nbin  = (flags & read_bins_mask) >> read_bins_shift;	\
    int N  = 1<<nbin;							\
    int MM = (M + 8)/nbin;						\
    int i,j,jj,sh,j0,j1,j2,j3,ex,sgn = 0;				\
    									\
    int64_t  ch, ch2;							\
    uint64_t man, x1, x2 ;						\
    									\
    float_type x;							\
    									\
    buf += *bufpt;							\
    size_t bp = 0;							\
    for (j = 0; j < 100; j++)						\
      {									\
	ch = REV(buf[bp]);						\
									\
	if (ch == '-')							\
	  {								\
	    bp += 1;							\
	    sgn = sgn ? 0 : 1;						\
	    incr_pos(pos,1);						\
	    continue;							\
	  }								\
	else if (ch == '+')						\
	  {								\
	    bp++;							\
	    incr_pos(pos,1);						\
	    continue;							\
	  }								\
									\
	break;								\
      }									\
    									\
    if (j == 100)							\
      {									\
	*e = 0;								\
	return 0;							\
      }									\
    									\
    i = 0;								\
    while (i < 100)							\
      {									\
	ch = REV(buf[bp+i]);						\
	if (ch != '0') break;						\
	i++;								\
      }									\
    									\
    if (i == 100)							\
      {									\
	*e = 0;								\
	return 0;							\
      }									\
    									\
    if (i)								\
      {									\
	bp += i;							\
	incr_pos(pos,i);						\
      }									\
    									\
    j = 0;								\
    while (j < MM)							\
      {									\
	ch = REV(buf[bp+j]);						\
	ch2 = ch - '0';							\
	if (ch2 <  0) break;						\
	if (ch2 >= 10 && N > 10)					\
	  {								\
	    ch2 = ch - 'a' + 10;					\
	    if (ch2 < 10 || ch2 >= N)					\
	      {								\
	        ch2 = ch - 'A' + 10;					\
		if (ch2 < 10 || ch2 >= N)				\
		  {							\
		    break;						\
		  }							\
	      }							        \
	  }								\
									\
	j++;								\
      }									\
    									\
    jj = j;								\
    if (j == MM)							\
      {									\
	while (jj < 100)						\
	  {								\
	    ch = REV(buf[bp+jj]);					\
	    ch2 = ch - '0';						\
	    if (ch2 <  0) break;					\
	    if (ch2 >= 10 && N > 10)					\
	      {								\
		ch2 = ch - 'a' + 10;					\
		if (ch2 < 10 || ch2 >= N)				\
		  {							\
		    ch2 = ch - 'A' + 10;				\
		    if (ch2 < 10 || ch2 >= N) break;			\
		  }							\
	      }								\
	    jj++;							\
	  }								\
									\
	if (jj == 100)							\
	  {								\
	    *e = 0;							\
	    return 0;							\
	  }								\
      }									\
    									\
    sh  = nbin*(j-1);							\
    j1  = (jj-j)*nbin;							\
    x2  = 0;								\
    for (x1 = 0, i = 0; i < j; i++)					\
      {									\
	ch = REV(buf[bp+i]);						\
	ch2 = ch - '0';							\
	if (ch2 >= 10) ch2 = ch - 'A' + 10;				\
	if (ch2 >= N ) ch2 = ch - 'a' + 10;				\
	x1  |= ch2 << sh;						\
	sh  -= nbin;							\
      }									\
    									\
    j0 = i*nbin;							\
    									\
    if (jj)								\
      {									\
        bp += jj;							\
	j1  = jj - j;							\
	incr_pos(pos,jj);						\
      }									\
    									\
    ch = REV(buf[bp]);							\
    									\
    j2 = 0;								\
    j3 = 0;								\
    if (ch == '.')							\
      {									\
        bp ++;								\
	incr_pos(pos,1);						\
									\
	{								\
	  i = 0;						        \
	  for(; i < 100; i++)						\
	    {								\
	      ch = REV(buf[bp+i]);					\
	      ch2 = ch - '0';						\
	      if (ch2 != 0) break;					\
	    }								\
	  								\
	  if (i == 100)							\
	    {								\
	      *e = 0;							\
	      return 0;							\
	    }								\
	  								\
	  if (i)							\
	    {								\
	      j2 = i*nbin;						\
	      incr_pos(pos,i);						\
	      bp += i;							\
	      jj += i;							\
	    }								\
	}								\
									\
	for(i = 0, jj = 0; jj < MM; i++, jj++)				\
	  {								\
	    ch = REV(buf[bp+jj]);					\
	    ch2 = ch - '0';						\
	    if (ch2 <  0) break;					\
	    if (ch2 >= 10)						\
	      {								\
		ch2 = ch - 'a' + 10;					\
		if (ch2 < 10 || ch2 >= N)				\
		  {							\
		    ch2 = ch - 'A' + 10;				\
		    if (ch2 < 10 || ch2 >= N) break;			\
		  }							\
	      }								\
	  }								\
									\
	if (jj == MM)							\
	  {								\
	    for(; jj < 100; jj++)					\
	      {								\
		ch = REV(buf[bp+jj]);					\
		ch2 = ch - '0';						\
		if (ch2 <  0) break;					\
		if (ch2 >= 10)						\
		  {							\
		    ch2 = ch - 'a' + 10;				\
		    if (ch2 < 10 || ch2 >= N)				\
		      {							\
			ch2 = ch - 'A' + 10;				\
			if (ch2 < 10 || ch2 >= N)			\
			  break;					\
		      }							\
		  }							\
	      }								\
									\
	    if (jj == 100)						\
	      {								\
		*e = 1;							\
		return 0;						\
	      }								\
	  }								\
									\
	if (i)								\
	  {								\
	    j  = i;							\
            sh = (j-1)*nbin;						\
            for (i = 0; i < j; i++, sh -= nbin)				\
	      {								\
                ch = REV(buf[bp+i]);					\
                ch2 = ch - '0';					        \
		if (ch2 >= 10)						\
		  {							\
		    ch2 = ch -'A' + 10;					\
		    if (ch2 >= N) ch2 = ch -'a' + 10;			\
		  }							\
		x2 |= ch2 << sh;					\
	      }								\
	    j3  = j*nbin;						\
	  }								\
									\
        if (jj)								\
	  {								\
	    bp  += jj;							\
	    incr_pos(pos, jj);						\
	  }								\
      }							                \
    									\
    ch = REV(buf[bp]);							\
    									\
    ex = 0;								\
    if (ch=='e' || ch=='E' || ch == 'z' || ch == 'Z')			\
      {									\
	bp ++;								\
	incr_pos(pos,1);						\
	int e2 = 0;							\
	ex = read_b_integer(buf,&bp,pos,&e2,(flags|3|read_sgn_flag))*nbin; \
	ex -= (M%nbin) ? 0 : nbin;					\
	if (!e2)							\
	  {								\
	    *e = 0;							\
	    return 0;							\
	  }								\
      }									\
    									\
    printf("ex %d\n",ex);						\
    if (ex < -M1)							\
      {									\
	x = powf(2,-M1);						\
      }									\
    else if (ex > M1)							\
      {									\
	x = powf(2,M1);							\
      }									\
    else								\
      {									\
	man = 0;							\
	if (x1 > 0)							\
	  {								\
	    int v = M+8-j0;						\
	    int w = v-j2-j3;						\
	    								\
	    man  = x1 << v;						\
	    man |= w >= 0 ? x2 << w : x2 >> -w;				\
	    ex   = ex - v + j1;						\
	  }								\
	else								\
	  {								\
	    man = x2;							\
	    ex  = ex - j2 - j3;						\
	  }								\
	sh  = 64 - __builtin_clzl(man) - M - 1;				\
	man = (sh >= 0) ? (man >> sh) : (man << -sh);			\
	ex += sh;							\
	x = ldexp(man,ex);						\
	printf("ex %d\n",ex);						\
      }									\
    									\
    *bufpt += bp;							\
    *e = 1;								\
    return sgn?-x:x;							\
  }    

#define MKMKRFLOATBIN(tag,int_t,REV)					\
  MKRFLOATBIN(read_b_qdouble_##tag, int_t, REV,				\
	      __float128,  MANBITS128, 1500,				\
	      read_b_integer_s_##tag, uint64_t, ldexpq);		\
									\
  MKRFLOATBIN(read_b_double_##tag, int_t, REV,				\
	      double,  MANBITS64, 1500,					\
	      read_b_integer_s_##tag, uint64_t, ldexpl);		\
  									\
  MKRFLOATBIN(read_b_float_##tag , int_t, REV,				\
	      float, MANBITS32, 150,					\
	      read_b_integer_s_##tag, uint64_t, ldexp);

MKMKRFLOATBIN(utf8     ,  uint8_t, REV0 );
MKMKRFLOATBIN(utf16    , uint16_t, REV0 );
MKMKRFLOATBIN(utf16_rev, uint16_t, rev16);
MKMKRFLOATBIN(utf32    , uint32_t, REV0 );
MKMKRFLOATBIN(utf32_rev, uint32_t, rev32);

#define RDFLT(flt,utf)							\
  if (nbins)								\
    {									\
      return read_b_##flt##_##utf(buf, bufpt, pos, e, flags);		\
    }									\
  else									\
    {									\
      return read_##flt##_##utf(buf, bufpt, pos, e, flags);	\
    }


double c_read_double_(uint8_t *buf_, size_t *bufpt, SCM pos, int *e,
		      uint64_t flags)
{
  int      Sh    = (flags & read_sh_mask  ) >> read_sh_shift  ;  
  uint64_t nbins = (flags & read_bins_mask) >> read_bins_shift;

  if (Sh == 0)
    {
      uint8_t *buf = (uint8_t *)buf_;
      RDFLT(double,utf8);
    }
  else if (Sh == 1)
    {
      uint16_t *buf = (uint16_t *)buf_;
      int d = (flags & read_d_mask  ) >> read_d_shift;
      if (d == 1)
	{
	  if (is_me_big)
	    {
	      RDFLT(double,utf16);
	    }
	  else
	    {
	      RDFLT(double,utf16_rev);
	    }
	}
      else
	{
	  if (is_me_big)
	    {
	      RDFLT(double,utf16_rev);
	    }
	  else
	    {
	      RDFLT(double,utf16);
	    }
	}      
    }
  else if (Sh == 2)
    {
      uint32_t *buf = (uint32_t *)buf_;
      int d = (flags & read_d_mask  ) >> read_d_shift;
      if (d == 3)
	{
	  if (is_me_big)
	    {
	      RDFLT(double,utf32);
	    }
	  else
	    {
	      RDFLT(double,utf32_rev);
	    }
	}
      else
	{
	  if (is_me_big)
	    {
	      RDFLT(double,utf32_rev);
	    }
	  else
	    {
	      RDFLT(double,utf32);
	    }
	}
    }

  scm_misc_error("read","unknown double format",SCM_EOL);
  return 0;
}

float c_read_float_(uint8_t *buf_, size_t *bufpt, SCM pos, int *e,
		      uint64_t flags)
{
  int      Sh    = (flags & read_sh_mask  ) >> read_sh_shift  ;  
  uint64_t nbins = (flags & read_bins_mask) >> read_bins_shift;

  if (Sh == 0)
    {
      uint8_t *buf = (uint8_t *)buf_;
      RDFLT(float,utf8);
    }
  else if (Sh == 1)
    {
      uint16_t *buf = (uint16_t *)buf_;
      int d = (flags & read_d_mask  ) >> read_d_shift;
      if (d == 1)
	{
	  if (is_me_big)
	    {
	      RDFLT(float,utf16);
	    }
	  else
	    {
	      RDFLT(float,utf16_rev);
	    }
	}
      else
	{
	  if (is_me_big)
	    {
	      RDFLT(float,utf16_rev);
	    }
	  else
	    {
	      RDFLT(float,utf16);
	    }
	}      
    }
  else if (Sh == 2)
    {
      uint32_t *buf = (uint32_t *)buf_;
      int d = (flags & read_d_mask  ) >> read_d_shift;
      if (d == 3)
	{
	  if (is_me_big)
	    {
	      RDFLT(float,utf32);
	    }
	  else
	    {
	      RDFLT(float,utf32_rev);
	    }
	}
      else
	{
	  if (is_me_big)
	    {
	      RDFLT(float,utf32_rev);
	    }
	  else
	    {
	      RDFLT(float,utf32);
	    }
	}
    }

  scm_misc_error("read","unknown float format",SCM_EOL);
  return 0;
}


SCM_DEFINE(c_read_double, "c-read-double", 4, 0, 0,
	   (SCM buf_, SCM bufpt_e, SCM pos, SCM flags_),
	   "find a most probable a list match")
#define FUNC_NAME c_read_double_s
{
  uint8_t   *buf   = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_));
  uint64_t  flags  = scm_to_uint64(flags_);
  int       e      = 0;
  size_t    buffpt = scm_to_size_t(SCM_CAR(bufpt_e));

  double n = c_read_double_(buf, &buffpt, pos, &e, flags);
  
  SCM_SETCAR(bufpt_e, scm_from_size_t(buffpt));
  SCM_SETCDR(bufpt_e, scm_from_size_t(e));
  
  return scm_from_double(n);
}
#undef FUNC_NAME

SCM_DEFINE(c_read_float, "c-read-float", 4, 0, 0,
	   (SCM buf_, SCM bufpt_e, SCM pos, SCM flags_),
	   "find a most probable a list match")
#define FUNC_NAME c_read_float_s
{
  uint8_t   *buf   = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_));
  uint64_t  flags  = scm_to_uint64(flags_);
  int       e      = 0;
  size_t    buffpt = scm_to_size_t(SCM_CAR(bufpt_e));

  float n = c_read_float_(buf, &buffpt, pos, &e, flags);
  
  SCM_SETCAR(bufpt_e, scm_from_size_t(buffpt));
  SCM_SETCDR(bufpt_e, scm_from_size_t(e));
  
  return scm_from_double(n);
}
#undef FUNC_NAME

typedef union
{
  float x;
  uint32_t n;
} bits32_t;

typedef union
{
  double x;
  uint64_t n;
} bits64_t;

typedef union
{
  __float128 x;
  uint128_t n;
} bits128_t;
  
void real_init()
{
    {
      bits32_t  xx1;
      bits64_t  xx2;
      bits128_t xx3;

      xx1.x = 1.5;
      MANBITS32 = __builtin_ctz(xx1.n) + 1;
      DIG32     = (int) log10(pow(2,MANBITS32)) + 1;

      xx2.x = 1.5;
      MANBITS64 = __builtin_ctzl(xx2.n) + 1;
      DIG64     = (int) log10(pow(2,MANBITS64)) + 1;
    
      xx3.x = 1.5;
      MANBITS128 = __builtin_ctzll(xx3.n) + 1;
      DIG128     = (int) log10(pow(2,MANBITS128)) + 1;
    }
    
    for (int i = 0; i < 2500; i++)
      {
	long double d = powl(2,i-1023);
      
	if (isnan(d) || isinf(d)) continue;
	
	int exp = (int) log10l(d);
	
	if (exp < 0)
	  {
	    exp--;
	  }
	exp++;
	
	emap[i].d   = powl(10,-exp+17);
	emap[i].exp = exp;
      }

  for (int i = 0; i < 2500; i++)
    {
      __float128 d = powl(2,i-1250);
      
      if (isnan(d) || isinf(d)) continue;

      int exp = (int) log10(d);

      if (exp < 0)
	{
	  exp--;
	}
      exp++;
      
      emap2[i].exp = exp;
      emap2[i].d   = powq(10, -exp + DIG128);
    }

  for (int i = 0; i < 500; i++)
    {
      double d = pow(2,i-250);
      
      if (isnan(d) || isinf(d)) continue;

      int exp = (int) log10(d);

      if (exp < 0)
	{
	  exp--;
	}
      exp++;
      
      fmap[i].exp = exp;
      fmap[i].d   = powl(10,-exp+8);
    }
  
  for (int i = -500; i < 500; i++) mypow[i+500] = powl(10,i);
  for (int i = -500; i < 500; i++) mypowq[i+500] = powq(10,i);
}
