static void
encode_latin1_chars_to_latin1_buf_display
(uint8_t       *buf,
 size_t        *bufpt,
 uint64_t       buflen,
 const uint8_t *chars,
 size_t        *count,
 uint64_t       flags)
{
  int size = buflen - *bufpt - 100;
  int n = (size >= *count ? *count : size);

  buf += *bufpt;
  for (int i  = 0; i < n; i++)
    buf[i] = chars[i];
  
  *count -= n;
  *bufpt += n;
}

static void
encode_latin1_chars_to_latin1_buf_n (uint8_t       *buf,
				     size_t        *bufpt,
				     uint64_t       buflen,
				     const uint8_t *chars,
				     size_t        *count,
				     int            Sh,
				     int            D)
{
  int size = buflen - *bufpt;
  int N = 1 << Sh;
  int M = size/N;
  int n = (M <= *count ? M : *count);
  buf += *bufpt;
  for (int i  = 0; i < n; i++)
    {
      buf[D] = chars[i];
      buf += N;
    }
  
  *count -= n;
  *bufpt += (n<<Sh);
}


static void
encode_latin1_chars_to_utf16_buf_display
(uint8_t       *buf,
 size_t        *bufpt,
 uint64_t       buflen,
 const uint8_t *chars,
 size_t        *count,
 uint64_t       flags,
 int            D)
{
  encode_latin1_chars_to_latin1_buf_n (buf,bufpt,buflen,chars,count,1,D);
}

static void
encode_latin1_chars_to_utf32_buf_display
(uint8_t       *buf,
 size_t        *bufpt,
 uint64_t       buflen,
 const uint8_t *chars,
 size_t        *count,
 uint64_t       flags,
 int            D)
{
  encode_latin1_chars_to_latin1_buf_n (buf,bufpt,buflen,chars,count,2,D);
}


MK_LATIN1(encode_latin1_chars_to_latin1_buf_write, LINE_UTF8_WRITE,
	  fill_write_latin1   , INIT(0,0));

MK_LATIN1(encode_latin1_chars_to_latin1_buf_extended, LINE_UTF8_SYM,
	  fill_extended_latin1, INIT(0,0));

MK_LATIN1(encode_latin1_chars_to_latin1_buf_r7rs, LINE_UTF8_R7RS,
	  fill_r7rs_latin1    , INIT(0,0));


MKUTF32(encode_utf32_chars_to_latin1_buf_write   , fill_write_latin1,
	INIT(0,0));

MKUTF32(encode_utf32_chars_to_latin1_buf_extended, fill_extended_latin1,
	INIT(0,0));

MKUTF32(encode_utf32_chars_to_latin1_buf_r7rs    , fill_r7rs_latin1,
	INIT(0,0));




// READ TOKEN
#define MKMKREADSTR(token,TOKEN)					\
  MK_RLATIN1(read_##token##_latin1_to_latin1, uint8_t, 1,		\
	     LINE_R_LATIN1_##TOKEN,					\
	     read_##token##_latin1_latin1);				\
									\
  MK_RLATIN1(read_##token##_latin1_to_utf32, uint32_t, 2,		\
	     LINE_R_LATIN1_##TOKEN,					\
	     read_##token##_latin1_utf32);				\
  									\
  MK_RLATIN1(read_##token##_utf8_to_latin1, uint8_t, 1,			\
	     LINE_R_UTF8_##TOKEN,					\
	     read_##token##_utf8_latin1);				\
									\
  MK_RLATIN1(read_##token##_utf8_to_utf32, uint32_t, 2,			\
	     LINE_R_UTF8_##TOKEN,					\
	     read_##token##_utf8_utf32);				\
									\
  MKRUTF32(read_##token##_utf16_to_latin1    , uint8_t, uint16_t,	\
	   read_##token##_utf16_latin1,1,REV0);				\
									\
  MKRUTF32(read_##token##_utf16_to_latin1_rev, uint8_t, uint16_t,	\
	   read_##token##_utf16_latin1_rev,1,rev16);			\
									\
  MKRUTF32(read_##token##_utf16_to_utf32      , uint32_t, uint16_t,	\
	   read_##token##_utf16_utf32,1,REV0);				\
									\
  MKRUTF32(read_##token##_utf16_to_utf32_rev , uint32_t, uint16_t,	\
	   read_##token##_utf16_utf32_rev,1,rev16);			\
									\
  MKRUTF32(read_##token##_utf32_to_latin1    , uint8_t,  uint32_t,	\
	   read_##token##_utf32_latin1,1,REV0);				\
									\
  MKRUTF32(read_##token##_utf32_to_latin1_rev, uint8_t,  uint32_t,	\
	   read_##token##_utf32_latin1_rev,1,rev32);			\
									\
  MKRUTF32(read_##token##_utf32_to_utf32     , uint32_t, uint32_t,	\
	   read_##token##_utf32_utf32,2,REV0);				\
									\
  MKRUTF32(read_##token##_utf32_to_utf32_rev , uint32_t, uint32_t,	\
	   read_##token##_utf32_utf32_rev,2,rev32);


MKMKREADSTR(token,TOKEN);
MKMKREADSTR(str  ,STR);
MKMKREADSTR(r7rs ,R7RS);
MKMKREADSTR(ext  ,EXT);

