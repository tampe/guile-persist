SCM print_vector_part(SCM     *v  , size_t start, size_t end,
		      uint8_t *buf, size_t *bufpt   , int buflen,
		      uint64_t flags)
{
  int Sh = 0;
  int D  = 0;
  MKShD();

  int sep = (flags & comma_flag) ? ',' : ' ';

  uint64_t nbin = (flags && binary_mask) >> binary_shift;
  
  size_t pt = *bufpt;
  size_t n  = 0;
  int count = end - start;
  
  for (;pt < buflen && n < count; n++)
    {
      int d = dispatch(v[start + n], buf + pt, flags, Sh, D, nbin);

      if (d < 0)
	{
	  *bufpt = pt;
	  return scm_from_int64(-n);	  
	}
      else
	{
	  pt += d;
	}

      if (n != count - 1)
	pt += put_char(sep,buf+pt,Sh,D);
    }

  *bufpt = pt;
  return scm_from_int64(n);
}

SCM print_list_part(SCM x, uint8_t *buf, size_t *bufpt, int buflen,
		    uint64_t flags, SCM *er)
{
  int Sh = 0;
  int D  = 0;
  MKShD();

  uint64_t nbin = (flags && binary_mask) >> binary_shift;
  
  int sep = (flags & comma_flag) ? ',' : ' ';
  
  size_t pt = *bufpt;

  for (;pt < buflen && SCM_CONSP(x);)
    {
      int d = dispatch(SCM_CAR(x), buf + pt, flags, Sh, D, nbin);
      
      if (d < 0)
	{
	  *bufpt = pt;
	  *er    = SCM_BOOL_T;
	  return x;
	}
      else
	{
	  pt += d;
	}

      x = SCM_CDR(x);
	
      if (SCM_CONSP(x))
	{
	  pt += put_char(sep, buf + pt, Sh, D);
	}
      else if (!SCM_NULLP(x))
	{
	 pt += put_string(" . ", buf + pt, Sh, D);
	 int d = dispatch(x, buf + pt, Sh, D, flags, nbin);

	 if (d < 0)
	   {
	     *bufpt = pt;
	     *er    = SCM_BOOL_T;
	     return x;
	   }
	 else
	   {
	     pt += d;
	   }
	}
    }

  *bufpt = pt;  
  return x;
}

SCM_DEFINE(print_list_c, "c-print-list", 4, 0, 0,
	   (SCM l, SCM buf_, SCM bufpt_count, SCM flags_),
	   "post string to buffer")
#define FUNC_NAME print_string_list_s
{
  size_t   bufpt = scm_to_size_t(SCM_CAR(bufpt_count));
  size_t   len   = scm_c_bytevector_length(buf_) - 598;
  uint64_t flags = scm_to_uint64(flags_);
  uint8_t *buf   = (uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_);
  SCM      er    = SCM_BOOL_F;  
  l = print_list_part (l, buf, &bufpt, len, flags, &er);

  SCM_SETCAR(bufpt_count, scm_from_size_t(bufpt));
  SCM_SETCDR(bufpt_count, er);
  return l;
}

#undef FUNC_NAME
SCM_DEFINE(print_vector_c, "c-print-vector", 5, 0, 0,
	   (SCM v_, SCM start_, SCM buf_, SCM bufpt_count, SCM flags_),
	   "post string to buffer")
#define FUNC_NAME print_string_vector_s
{
  size_t   bufpt = scm_to_size_t(SCM_CAR(bufpt_count));
  size_t   len   = scm_c_bytevector_length(buf_) - 598;
  uint64_t flags = scm_to_uint64(flags_);
  size_t   start = scm_to_size_t(start_);
  size_t   vlen  = scm_c_vector_length(v_);
  uint8_t  *buf  = (uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_);
  SCM      *v    = ((SCM *) v_) + 1;
  SCM      ret   = print_vector_part (v, start, vlen, buf, &bufpt, len, flags);
  
  SCM_SETCAR(bufpt_count, scm_from_size_t(bufpt));
  
  return ret;
}
#undef FUNC_NAME
