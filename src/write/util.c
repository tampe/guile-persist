#include <iconv.h>
#include <uniconv.h>
#include <unictype.h>

#define MKShD()							\
  {								\
    Sh      = ((flags >> sh_shift) & sh_d_mask);		\
    D       = ((flags >> d_shift ) & sh_d_mask);		\
  }

#define MKRShD()						\
  {								\
    Sh      = ((flags & read_sh_mask) >> read_sh_shift);	\
    D       = ((flags & read_d_mask ) >> read_d_shift);		\
  }

#define INITIAL_IDENTIFIER_MASK                                      \
  (UC_CATEGORY_MASK_Lu | UC_CATEGORY_MASK_Ll | UC_CATEGORY_MASK_Lt   \
   | UC_CATEGORY_MASK_Lm | UC_CATEGORY_MASK_Lo | UC_CATEGORY_MASK_Mn \
   | UC_CATEGORY_MASK_Nl | UC_CATEGORY_MASK_No | UC_CATEGORY_MASK_Pd \
   | UC_CATEGORY_MASK_Pc | UC_CATEGORY_MASK_Po | UC_CATEGORY_MASK_Sc \
   | UC_CATEGORY_MASK_Sm | UC_CATEGORY_MASK_Sk | UC_CATEGORY_MASK_So \
   | UC_CATEGORY_MASK_Co)

#define SUBSEQUENT_IDENTIFIER_MASK                                      \
  (INITIAL_IDENTIFIER_MASK                                              \
   | UC_CATEGORY_MASK_Nd | UC_CATEGORY_MASK_Mc | UC_CATEGORY_MASK_Me)


inline uint16_t rev16(uint16_t x)
{
  char *v;
  
  v = (char *)&x;
  
  char t = v[0];
  v[0] = v[1];
  v[1] = t;

  return x;
}

inline uint32_t  rev32(uint32_t x)
{
  char *v;
  
  v= (char *)&x;
  char t = v[0];
  v[0] = v[3];
  v[3] = t;

  t = v[1];
  v[1] = v[2];
  v[2] = t;

  return x;
}
	     
inline int put_char(char ch, uint8_t *buf, int Sh, int D)
{
  buf[D] = ch;
  return 1<<Sh;
}

inline int put_string(const char *str, uint8_t *buf, int Sh, int D)
{
  int n = 0;
  for(; str[n]; n++)
    {
      buf[D] = str[n];
      buf += 1<<Sh;
    }
  
  return n<<Sh;
}

inline const char * get_symbol_chars (SCM sym)
{
  SCM x = scm_symbol_to_string(sym);
  
  int qq = scm_to_int(scm_string_bytes_per_char (x));

  if (qq == 1)
    {     
      return (const char *) i_string_chars (x);
    }
  else
    {
      return (const char *)0;
    }
}

/*
void
port_acquire_iconv_descriptors (SCM port, iconv_t *output_cd)
{
  pthread_mutex_lock (&iconv_lock);   
  SCM      *pt              = (SCM *) SCM_CELL_WORD_2 (port);
  
  *output_cd = o_cd  = *((iconv_t *)(pt + 13 + sz_ic));
}

inline void
port_release_iconv_descriptors ()
{
  pthread_mutex_unlock (&iconv_lock);
}
*/

uint32_t utf8_to_utf32(uint32_t ch, uint8_t *chars, int *i, int *e)
{
  while (ch && (ch & 0xc0) != 0xc0)
    {
      ch  = chars[*i+1];
      *i += 1;
    }

  if (!ch)
    {
      *e = 1;
      return 0;
    }
  
  uint32_t ch2 = chars[*i+1];
  if ((ch & 0xe0) == 0xc0)
    {
      *i += 1;
      return (ch2 & 0x3f) | (ch & 0x1f) << 6;
    }
  else 
    {
      uint32_t ch3 = chars[*i+2];
      if ((ch & 0xf0) == 0xe0)
	{
	  *i += 2;
	  return (ch3 & 0x3f) | ((ch2 & 0x3f) << 6) | ((ch & 0xf) << 12);
	}
      else
	{
	  uint32_t ch4 = chars[*i+3];
	  *i += 3;
	  return (ch4 & 0x3f) | ((ch3 & 0x3f) << 6) | ((ch2 & 0x3f) << 12) |
	    ((ch & 0xf) << 18);
	}
    }  
}

uint32_t utf16_to_utf32(uint32_t ch, uint16_t *chars, int *i, int *e)
{
  if (ch < 0x800)
    {
      return ch;
    }
  else
    {
      uint32_t ch2 = chars[*i+1];
      *i += 1;
      return (ch & 0x3ff) | ((ch2 & 0x7ff) << 10) | 0x10000;
    }
}

uint32_t utf16_to_utf32_rev(uint32_t ch, uint16_t *chars, int *i, int *e)
{
  if (ch < 0x800)
    {
      return ch;
    }
  else
    {
      uint32_t ch2 = rev16(chars[*i+1]);
      *i += 1;
      return (ch & 0x3ff) | ((ch2 & 0x7ff) << 10) | 0x10000;
    }
}

size_t
codepoint_to_utf8 (uint32_t codepoint, uint8_t *utf8)
{
  size_t len;

  if (codepoint <= 0x7f)
    {
      len = 1;
      utf8[0] = codepoint;
    }
  else if (codepoint <= 0x7ffUL)
    {
      len = 2;
      utf8[0] = 0xc0 | (codepoint >> 6);
      utf8[1] = 0x80 | (codepoint & 0x3f);
    }
  else if (codepoint <= 0xffffUL)
    {
      len = 3;
      utf8[0] = 0xe0 | (codepoint >> 12);
      utf8[1] = 0x80 | ((codepoint >> 6) & 0x3f);
      utf8[2] = 0x80 | (codepoint & 0x3f);
    }
  else
    {
      len = 4;
      utf8[0] = 0xf0 | (codepoint >> 18);
      utf8[1] = 0x80 | ((codepoint >> 12) & 0x3f);
      utf8[2] = 0x80 | ((codepoint >> 6) & 0x3f);
      utf8[3] = 0x80 | (codepoint & 0x3f);
    }

  return len;
}

size_t
encode_escape_sequence (scm_t_wchar ch, uint8_t *buf, int Sh, int D,
			uint64_t flags)
{
  /* Represent CH using the in-string escape syntax.  */
  static const char hex[] = "0123456789abcdef";
  static const char escapes[7] = "abtnvfr";
  size_t i = 0;
  int N = 1<<Sh;
  
  buf[i+D] = '\\';
  i += N;
  if (ch >= 0x07 && ch <= 0x0D && ch != 0x0A)
    {
      /* Use special escapes for some C0 controls.  */    
      buf[i+D] = escapes[ch - 0x07];
      i += N;
    }
  else if (flags & 2)
    {
      if (ch <= 0xFF)
        {
          buf[i+D] = 'x';
          buf[i+N+D] = hex[ch / 16];
          buf[i+2*N+D] = hex[ch % 16];
	  i += 3*N;
        }
      else if (ch <= 0xFFFF)
        {
          buf[i+D] = 'u';
          buf[i+N+D] = hex[(ch & 0xF000) >> 12];
          buf[i+2*N+D] = hex[(ch & 0xF00) >> 8];
          buf[i+3*N+D] = hex[(ch & 0xF0) >> 4];
          buf[i+4*N+D] = hex[(ch & 0xF)];
	  i += 5*N;
        }
      else if (ch > 0xFFFF)
        {
          buf[i+D] = 'U';
          buf[i+N+D] = hex[(ch & 0xF00000) >> 20];
          buf[i+2*N+D] = hex[(ch & 0xF0000) >> 16];
          buf[i+3*N+D] = hex[(ch & 0xF000) >> 12];
          buf[i+4*N+D] = hex[(ch & 0xF00) >> 8];
          buf[i+5*N+D] = hex[(ch & 0xF0) >> 4];
          buf[i+6*N+D] = hex[(ch & 0xF)];
	  i += 7*N;
        }
    }
  else
    {
      buf[i+D] = 'x';
      i += N;
      if (ch > 0xfffff) {buf[i+D] = hex[(ch >> 20) & 0xf];i+=N;}
      if (ch > 0x0ffff) {buf[i+D] = hex[(ch >> 16) & 0xf];i+=N;}
      if (ch > 0x00fff) {buf[i+D] = hex[(ch >> 12) & 0xf];i+=N;}
      if (ch > 0x000ff) {buf[i+D] = hex[(ch >>  8) & 0xf];i+=N;}
      if (ch > 0x0000f) {buf[i+D] = hex[(ch >>  4) & 0xf];i+=N;}
      buf[i+D] = hex[ch & 0xf];i += N;
      buf[i+D] = ';';i += N;
    }

  return i;
}

inline int encode_utf16
(
 uint32_t  ch,
 uint8_t  *buf,
 uint64_t  flags,
 int       D
 )
  
{
  if (likely(ch < 0x800))
    {
      buf[D]   = ch & 0xff;
      buf[1-D] = ch >> 8;
      return 2;
    }
  else 
    { 
      int x = ch - 0x10000;
      x  = (x >> 10)    | 0xd800;
      ch = (ch & 0x3ff) | 0xdc00;

      buf[D]   = ch & 0xff;
      buf[1-D] = ch >> 8;

      buf[2+D]   = x & 0xff;
      buf[3-D]   = x >> 8;
      
      return 4;
    }
}

/*
Use this as fallback
int encode_iconv
(
 uint32_t  ch,
 uint8_t  *buf,
 uint64_t  flags,
 ind       D,
 )
{
  uint8_t utf8[1];
  iconv_t output_cd;
  
  SCM port = scm_fluid_ref(portal);
  
  port_acquire_iconv_descriptors (port, &output_cd);
  
  int utf8_len = codepoint_to_utf8 (ch, utf8);
      
  char  *input       = (char *) utf8;
  size_t output_left = 100;
  size_t input_left  = utf8_len;

  size_t res  = iconv (output_cd, &input, &input_left, (char **)&buf,
		       &output_left);

  int n = 100 - output_left;
      
  if (res != (size_t) -1)
    {
      port_release_iconv_descriptors ();
      return n;
    }

  SCM conv = ((SCM *) SCM_CELL_WORD_2 (port))[8];
       
  if (scm_is_eq (conv, sym_escape))
    {
      uint8_t escape[100];
      input       = (char *) escape;
      input_left  = encode_escape_sequence (ch, escape, 0, 0, flags);
      output_left = 100;

      res = iconv (output_cd, &input, &input_left, (char **)&buf, &output_left);
      iconv (output_cd, NULL, NULL, (char **)&buf, &output_left);
    }
  else if (scm_is_eq (conv, sym_substitute))
    {
      uint8_t substitute[2] = "?";
      input                 = (char *) substitute;
      input_left            = 1;
      output_left           = 100;
	  
      res = iconv (output_cd, &input, &input_left, (char **)&buf, &output_left);

      iconv (output_cd, NULL, NULL, (char **)&buf, &output_left);
    }

  scm_port_release_iconv_descriptors (port);

  n = 100 - output_left;

  return n;
}
*/

inline void hexit(uint8_t *buf, uint8_t ch)
{
  buf[0] = '\\';
  buf[1] = 'x';

  int ch1 = 0xf & ch;
  int ch2 = ch >> 4;

  if (ch1 < 10)
    buf[2] = '0' + ch1;
  else
    buf[2] = 'a' + ch1 - 10;

  if (ch2 < 10)
    buf[3] = '0' + ch2;
  else
    buf[3] = 'a' + ch2 - 10;  
}

inline void hexitn(uint8_t *buf, uint8_t ch, int Sh, int D)
{
  buf[0       + D] = '\\';
  buf[(1<<Sh) + D] = 'x';

  int ch1 = 0xf & ch;
  int ch2 = ch >> 4;

  if (ch1 < 10)
    buf[(2<<Sh) + D] = '0' + ch1;
  else
    buf[(2<<Sh) + D] = 'a' + ch1 - 10;

  if (ch2 < 10)
    buf[(3<<Sh) + D] = '0' + ch2;
  else
    buf[(3<<Sh) + D] = 'a' + ch2 - 10;
}

inline int hexit2(uint32_t ch, uint8_t *buf, int Sh, int D)
{
  int N = 1 << Sh;

  buf[D] = '\\';
  buf[N+D] = 'x';
  
  int i = 2*N;

  while (ch)
    {
      char chs[] = {'0','1','2','3','4','5','6','7','8','9',
		    'a','b','c','d','e','f'};
      int d = ch & 0xf;
      buf[i+D] = chs[d];
      ch >>= 4;
      i += N;
    }
  buf[i+D] = ';';

  return i + N;
}

uint32_t swap(uint32_t x_)
{
  uint32_t x = x_;
  uint32_t y = 0;
  char*    vy = (char*)&y;
  char*    vx = (char*)&x;

  vy[0] = vx[3];
  vy[1] = vx[2];
  vy[2] = vx[1];
  vy[0] = vx[3];

  return y;
}

inline void incr_pos(SCM pos, size_t n)
{
  SCM_SETCAR(pos, scm_from_size_t(scm_to_size_t(SCM_CAR(pos)) + n));
}

inline void newline(SCM pos)
{
  SCM_SETCAR(pos, scm_from_size_t(0));
  SCM_SETCDR(pos, scm_from_size_t(scm_to_size_t(SCM_CDR(pos)) + 1));
}

#define MKSPACE(skip_space8, buf_t, REV)			\
  inline int skip_space8(buf_t *buf, size_t pt, int N, SCM pos)	\
  {								\
    int nn = 0;							\
    int q  = 0;							\
    for (;nn < N; nn++, q++)					\
      {								\
	uint32_t ch = REV(buf[pt+nn]);				\
	if (ch == ' ' || ch == '\t')				\
	  {							\
	    continue;						\
	  }							\
	else if (ch == '\n')					\
	  {							\
	    newline(pos);					\
	    q = 0;						\
	    continue;						\
	  }							\
	else if (ch == '\r')					\
	  {							\
	    q--;						\
	    continue;						\
	  }							\
	break;							\
      }								\
								\
    incr_pos(pos, q);						\
								\
    if (nn == N) return -nn;					\
    								\
    return nn;							\
  }

#define REV0
MKSPACE(skip_utf8     , uint8_t , REV0);
MKSPACE(skip_utf16    , uint16_t, REV0);
MKSPACE(skip_utf16_rev, uint16_t, rev16);
MKSPACE(skip_utf32    , uint32_t, REV0);
MKSPACE(skip_utf32_rev, uint32_t, rev32);

#define mymemset(A,B,C,Sh,D)			\
{						\
  for (int i = 0; i < (C); i+=N)		\
    (A)[(i<<Sh) + D]=(B);			\
}

#define mymemmove(A,B,C,Sh,D)			\
  {						\
    for (int i = (C-1)<<Sh; i >= 0; i--)	\
      (A)[i]=(B)[i];				\
  }


#pragma GCC diagnostic ignored "-Wstrict-aliasing"


int is_me_big = 0;

void init_util()
{
  uint16_t xxx = 1;
  if (*((char*)&xxx))
    is_me_big = 0;
  else
    is_me_big = 1;
}

inline int min(int x, int y)
{
  return x < y ? x : y;
}

// This can be 16X faster to scan a lot of spaces for utf8/latin1 through
// SIMD operations

void read_spaces(uint8_t *buff, size_t *buffpt, size_t len, int *e, SCM pos, uint64_t flags, int *fnd)
{
  int Sh,D;
  MKShD();
  int N = 1 << Sh;
  
  size_t pt = *buffpt;

  int i = 0;

  int found = 0;

  if (pt >= len)
    {
      *e      = 1;
      *fnd    = found;
      return;
    }
  
  for(;pt<len;pt+=N,i++)
    {
      uint8_t ch = buff[pt + D];

      if (N > 1)
	{
	  for (int j = 0; j < N; j++)
	    {
	      if (j == D) continue;
	      if (buff[pt+j]) goto out;
	    }
	}
      
      if (ch == ' ' || ch == '\t' || ch == '\r')
	{
	  found = 1;
	  continue;
	}

      if (ch == '\n')
	{
	  found = 1;
	  newline(pos);
	  i = 0;
	  continue;
	}

    out:
      incr_pos(pos,i);
      *buffpt = pt;
      *e      = 1;
      *fnd    = found;
      return;
    }
  
  incr_pos(pos,i);
  *buffpt = pt;
  *e      = 0;
  *fnd    = found;
}

#define read_r6rs_hex_escape(s, chars, j, REV)				\
{									\
  s = 0;								\
  int k = 0;								\
  for(;k < 9;k++)							\
    {									\
      uint32_t ch3 = REV(chars[j+k]);					\
      int64_t  ch4 = ch3 - '0';						\
      if (ch4 < 0) goto err;						\
      if (ch4 > 9)							\
	{								\
	  ch4 = ch3 - 'A' + 10;						\
	  if (ch4 < 10) goto err;					\
	  if (ch4 > 15)							\
	    {								\
	      ch4 = ch3 - 'a' + 10;					\
	      if (ch4 < 10 || ch4 > 15) goto err;			\
	    }								\
	}								\
									\
      s = s << 4 | ch4;							\
      continue;								\
									\
    err:								\
      if (!ch3)								\
	{								\
	  scm_misc_error						\
	    ("read",							\
	     "unexpected end of input in character escape sequence",	\
	     SCM_EOL);							\
	}								\
      if (k == 0)							\
	{								\
	  scm_misc_error("read",					\
			 "invalid character in escape sequence",	\
			 SCM_EOL);					\
	}								\
									\
      if (ch3 == ';')							\
	{								\
	  j += k;							\
	  break;							\
	}								\
									\
      scm_misc_error("read",						\
		     "invalid character in escape sequence",		\
		     SCM_EOL);						\
									\
    }									\
									\
  if (k == 9)								\
    scm_misc_error("read",						\
		   "too long hex sequence in escape sequence",		\
		   SCM_EOL);						\
									\
}

#define read_fixed_hex_escape(s, chars, j, REV, N)			\
{									\
  for(int k = 0; k < N; k++)						\
    {									\
      uint32_t ch3 = REV(chars[j+k]);					\
      int64_t  ch4 = ch3 - '0';						\
      if (ch4 < 0) goto er##N;						\
      if (ch4 > 9)							\
	{								\
	  ch4 = ch3 - 'A' + 10;						\
	  if (ch4 < 10) goto er##N;					\
	  if (ch4 > 15)							\
	    {								\
	      ch4 = ch3 - 'a' + 10;					\
	      if (ch4 < 10 || ch4 > 15) goto er##N;			\
	    }								\
	}								\
									\
      s = s << 4 | ch4;							\
      continue;								\
									\
    er##N:								\
      if (!ch3)								\
	{								\
	  scm_misc_error						\
	    ( "read",							\
	      "unexpected end of input in character escape sequence",	\
	      SCM_EOL);							\
	}								\
      if (k == 0)							\
	{								\
	  scm_misc_error("read",					\
			 "invalid character in escape sequence",	\
			 SCM_EOL);					\
	}								\
    }									\
									\
  j += N;								\
}
