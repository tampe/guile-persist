uint64_t escaped_newline_flag = 1<<0;
uint64_t r6rs_escapes_flag    = 1<<1;
uint64_t write_flag           = 1<<2;
uint64_t utf8_flag            = 1<<22;
uint64_t convert_flags        = 3<<20;
uint64_t CONVERT_REPLACE      = 1<<20;
uint64_t CONVERT_ESCAPE       = 0<<20;
uint64_t d_shift              = 16;
uint64_t sh_shift             = 18;
uint64_t sh_d_mask            = 0x3;
uint64_t r7rs_symbols_flag    = 1<<4;
uint64_t keyword_style_mask   = 3<<12;
uint64_t keyword_style_flag   = 1<<14;
uint64_t keyword_read         = 1<<12;
uint64_t binary_mask          = 7<<23;
uint64_t binary_shift         = 23;
uint64_t comma_flag           = 1<<26;
uint64_t quote_flag           = 1<<27;
uint64_t python_flag          = 1<<28;

void set_write_flags(SCM v_)
{
  SCM *v = (SCM *)v;
  int i  = 1;
  write_flag         = scm_to_uint64(v[i]);i++;
  utf8_flag          = scm_to_uint64(v[i]);i++;
  convert_flags      = scm_to_uint64(v[i]);i++;
  CONVERT_REPLACE    = scm_to_uint64(v[i]);i++;
  CONVERT_ESCAPE     = scm_to_uint64(v[i]);i++;
  d_shift            = scm_to_uint64(v[i]);i++;
  sh_shift           = scm_to_uint64(v[i]);i++;
  r7rs_symbols_flag  = scm_to_uint64(v[i]);i++;
  keyword_style_mask = scm_to_uint64(v[i]);i++;
  keyword_style_flag = scm_to_uint64(v[i]);i++;
  keyword_read       = scm_to_uint64(v[i]);i++;
  binary_mask        = scm_to_uint64(v[i]);i++;
  binary_shift       = scm_to_uint64(v[i]);i++;
  comma_flag         = scm_to_uint64(v[i]);i++;
  quote_flag         = scm_to_uint64(v[i]);i++;
  python_flag        = scm_to_uint64(v[i]);i++;
}

uint64_t read_bits_mask  = 3;
int      read_bits_shift = 0;
uint64_t read_bins_mask  = 7<<2;
int      read_bins_shift = 2;
uint64_t read_sh_mask    = 3<<5;
int      read_sh_shift   = 5;
uint64_t read_d_mask     = 3<<7;
int      read_d_shift    = 7;

uint64_t read_utf8_flag               = 1<<11;
uint64_t read_real_flag               = 1<<9;
uint64_t read_sgn_flag                = 1<<10;
uint64_t read_complex_flag            = 1<<22;
uint64_t read_curly_infix_flag        = 1<<18;
uint64_t read_square_bracket_flag     = 1<<17;
uint64_t read_r6rs_escapes_flag       = 1<<24;
uint64_t read_hungry_eol_escapes_flag = 1<<25;
