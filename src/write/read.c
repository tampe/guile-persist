

SCM_DEFINE(c_read_spaces, "c-read-spaces", 5, 0, 0,
	   (SCM buf_, SCM bufflen_, SCM pt_e, SCM pos, SCM flags_),
	   "read spaces")
#define FUNC_NAME read_spaces_s
{
  uint8_t   *buf    = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_));
  uint64_t  flags   = scm_to_uint64(flags_);
  size_t    bufflen = scm_to_size_t(bufflen_);
  int       e       = 0;
  size_t    buffpt  = scm_to_size_t(SCM_CAR(pt_e));
  int       fnd     = 0;
  read_spaces(buf, &buffpt, bufflen, &e, pos, flags, &fnd);
  
  SCM_SETCAR(pt_e,scm_from_size_t(buffpt));
  SCM_SETCDR(pt_e,scm_from_int(e));
  
  return fnd ? SCM_BOOL_T : SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(butify, "c-butify", 1, 0, 0,
	   (SCM str),
	   "str -> bv")
#define FUNC_NAME butify_c_
{
  SCM sbuf;
  size_t offset = 0;
  get_str_buf_start (&str, &sbuf, &offset);

  uint8_t *chars = ((uint8_t *) CHARS(sbuf)) + offset;

  return scm_from_uint64((uint64_t) chars);
}
#undef FUNC_NAME
