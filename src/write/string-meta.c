#define WWMK(LINE,work64_t,N8)					\
  {								\
    work64_t u   = *((work64_t *)(chars + chn));		\
    work64_t ww2;						\
    ww2.v         = LINE;					\
								\
    if (ww2.y) goto adv;					\
    							        \
    for (int k = 0; k < N8; k++)				\
      *((uint8_t *)(buf + (k<<Sh)  + bufn + D)) = u.v[k ];	\
								\
    bufn   += N8<<Sh;						\
    chn    += N8;						\
    limit2 -= N8;						\
  }

#define WWMK1(LINE,work64_t,N8)					\
  {								\
    work64_t u   = *((work64_t *)(chars + chn));		\
    work64_t ww2;						\
    ww2.v         = LINE;					\
								\
    if (ww2.y) goto adv;					\
    							        \
    for (int k = 0; k < N8; k++)				\
      *((uint8_t *)(buf + (k<<Sh)  + bufn + D)) = u.v;		\
								\
    bufn   += N8<<Sh;						\
    chn    += N8;						\
    limit2 -= N8;						\
  }

#define MK_LATIN1(encode_latin1,LINER,fill_utf8, INIT, ...)		\
  void encode_latin1							\
  (uint8_t       *buf,							\
   size_t        *bufpt,						\
   uint64_t       buflen,						\
   uint8_t       *chars,						\
   size_t        *count,						\
   uint64_t       flags,						\
   ##__VA_ARGS__)							\
  {									\
    INIT;								\
    size_t limit     = *count;						\
    int64_t offset   = ((uint64_t) chars) % 16;				\
  									\
    DB(printf("start count = %d, bufpt = %d offset %d addr %lu\n",	\
	      *count, *bufpt, offset, (uint64_t) count));		\
  									\
    uint64_t  bufn = *bufpt;						\
    uint64_t  chn  = 0;							\
  									\
									\
    int N = (1<<Sh);							\
  									\
    int i, limit2, limit3, size, lim, p;				\
									\
  begin:								\
    size   = buflen - bufn;						\
    size   = ((size % 16) == 0 ? size : (size + 16 - (size % 16)));	\
    limit2 = limit  - chn;						\
    									\
    limit2 = (size < limit2 ? size : limit2);				\
    if (limit2 <= 0) goto exit;						\
    									\
    limit3 = limit2 - (limit2 % 16);					\
    									\
    if (offset >  0) goto adv;						\
    									\
    DB(printf("size16(%d) size %d, limit %d len %d total %d chn %d\n",	\
	      limit3, size, limit - chn, bufn - *bufpt, bufn,chn));	\
    									\
    for (i = 0; i < limit3; i += 16)					\
      {									\
	DB(printf("loop %d\n",i));					\
	work_t u  = *((work_t *)(chars + chn));				\
	work_t ww;							\
	ww.v      = LINER(w);						\
    									\
	if (ww.m[0] || ww.m[1]) goto adv;				\
									\
    									\
	DB(printf("transfer\n"));					\
	if (unlikely((Sh || (((uint64_t)(buf+bufn)) & 0x7))))		\
	  {								\
	    DB(printf("slow\n"));					\
	    for (int k = 0; k < 16; k+=1)				\
	      *((uint8_t *)(buf + k*N  + bufn+D)) = u.v[k];		\
	  }								\
	else								\
	  {								\
	     DB(printf("fast 8 byte aligned\n"));			\
	     *((uint64_t *)(buf + bufn)) = u.m[0];			\
	     *((uint64_t *)(buf + bufn + 8)) = u.m[1];			\
	  }								\
    									\
	bufn   += 16*N;							\
	chn    += 16;							\
	limit2 -= 16;							\
      }									\
    									\
    DB(printf("size8(%d)\n",size));					\
    									\
    if (limit2 < 8) goto do4;						\
    									\
    WWMK(LINER(ww),work64_t,8);						\
    									\
  do4:									\
    if (limit2 < 4) goto do2;						\
 									\
    WWMK(LINER(www),work32_t,4);					\
 									\
  do2:									\
    if (limit2 < 2) goto do1;						\
 									\
    WWMK(LINER(wwww),work16_t,2);					\
 									\
  do1:									\
    if (limit2 <= 0) goto exit;						\
 									\
    WWMK1(LINER(wwwww), work8_t, 1);					\
 									\
    goto exit;								\
 									\
  adv:									\
    p = 0;								\
    									\
    if (offset > 0)							\
      {									\
	lim = (limit2 > offset ? offset : limit2);			\
      }									\
    else								\
      {									\
	lim = (limit2 > 16    ? 16      : limit2);			\
      }									\
 									\
    DB(printf("lim = %d adv(%d) %d %d\n",lim, limit2, bufn - *bufpt, chn)); \
    for (i = 0; i < lim; i++)						\
      {									\
	uint8_t ch = chars[chn+i];					\
     									\
	bufn += fill_utf8(ch,buf+bufn,Sh,D,flags,&p);			\
      }									\
 									\
    limit2  -= i;							\
    chn     += i;							\
 									\
    DB(printf("limit2 %d i %d offset %d\n",limit2,i,offset));		\
 									\
    if (limit2 && (buflen - bufn) > 0)					\
      {									\
	if (offset > 0 ? (i != offset || p < offset) : (i != 16 || p != 16)) \
	  {								\
	    offset -= i;						\
	    goto adv;							\
	  }								\
	else								\
	  {								\
	    offset = 0;							\
	    goto begin;							\
	  }								\
      }									\
									\
  exit:									\
    *bufpt  = bufn;							\
    *count  = limit - chn;						\
									\
    DB(printf("adv end count = %d, bufpt = %d nprocessed %d\n",		\
	      *count,*bufpt,chn));					\
  }

#define WWRMK(LINE,work64_t,N8)					\
  {								\
    work64_t u   = *((work64_t *)(chars + chn));		\
    work64_t ww2;						\
    ww2.v         = LINE;					\
								\
    if (ww2.y) goto adv;					\
    							        \
    for (int k = 0; k < N8; k++)				\
      buf[k + bufn] = u.v[k];					\
								\
    bufn   += N8;						\
    chn    += N8;						\
    limit2 -= N8;						\
    incr_pos(pos,N8);						\
  }

#define WWRMK1(LINE)						\
  {								\
    work8_t u   = *((work8_t *)(chars + chn));			\
    work8_t ww2;						\
    ww2.v         = LINE;					\
								\
    if (ww2.y) goto adv;					\
    							        \
    buf[bufn] = u.v;						\
								\
    bufn   += 1;						\
    chn    += 1;						\
    limit2 -= 1;						\
    incr_pos(pos,1);						\
  }

#define MK_RLATIN1(encode_latin1, buf_t, N, LINER, fill_utf8)		\
  void encode_latin1							\
  (buf_t         *buf,							\
   size_t        *bufpt,						\
   uint64_t      buflen,						\
   uint8_t       *chars,						\
   size_t        *count,						\
   int           *e,							\
   SCM           pos,							\
   uint64_t      flags)							\
  {									\
    size_t limit     = *count;						\
    int64_t offset   = ((uint64_t) chars) % 16;				\
  									\
    DB(printf("start count = %d, bufpt = %d offset %d addr %lu\n",	\
	      *count, *bufpt, offset, (uint64_t) count));		\
  									\
    uint64_t  bufn = *bufpt;						\
    uint64_t  chn  = 0;							\
									\
    int i, limit2, limit3, size, lim, p;				\
									\
  begin:								\
    size   = buflen - bufn;						\
    size   = ((size % 16) == 0 ? size : (size + 16 - (size % 16)));	\
    limit2 = limit  - chn;						\
    									\
    limit2 = (size < limit2 ? size : limit2);				\
    if (limit2 <= 0) goto exit;						\
    									\
    limit3 = limit2 - (limit2 % 16);					\
    									\
    if (offset >  0) goto adv;						\
    									\
    DB(printf("size16(%d) size %d, limit %d len %d total %d chn %d\n",	\
	      limit3, size, limit - chn, bufn - *bufpt, bufn,chn));	\
    									\
    for (i = 0; i < limit3; i += 16)					\
      {									\
	DB(printf("loop %d\n",i));					\
	work_t u  = *((work_t *)(chars + chn));				\
	work_t ww;							\
	ww.v      = LINER(w);						\
    									\
	if (ww.m[0] || ww.m[1]) goto adv;				\
									\
    									\
	DB(printf("transfer\n"));					\
	for (int k = 0; k < 16; k+=1)					\
	  buf[k + bufn] = u.v[k];					\
    									\
	bufn   += 16;							\
	chn    += 16;							\
	limit2 -= 16;							\
	incr_pos(pos,16);						\
      }									\
    									\
    DB(printf("size8(%d)\n",size));					\
    									\
    if (limit2 < 8) goto do4;						\
    									\
    WWRMK(LINER(ww),work64_t,8);					\
    									\
  do4:									\
    if (limit2 < 4) goto do2;						\
 									\
    WWRMK(LINER(www),work32_t,4);					\
 									\
  do2:									\
    if (limit2 < 2) goto do1;						\
 									\
    WWRMK(LINER(wwww),work16_t,2);					\
 									\
  do1:									\
    if (limit2 <= 0) goto exit;						\
 									\
    WWRMK1(LINER(wwwww));						\
 									\
    goto exit;								\
 									\
  adv:									\
    p = 0;								\
    									\
    if (offset > 0)							\
      {									\
	lim = (limit2 > offset ? offset : limit2);			\
      }									\
    else								\
      {									\
	lim = (limit2 > 16    ? 16      : limit2);			\
      }									\
 									\
    DB(printf("lim = %d adv(%d) %d %d\n",lim, limit2, bufn - *bufpt, chn)); \
    for (i = 0; i < lim; i++)						\
      {									\
	uint8_t ch = chars[chn+i];					\
     									\
	int j = i;							\
	bufn += fill_utf8(ch, buf+bufn, chars+chn, &j, flags, &p, e, pos); \
	incr_pos(pos,j-i);						\
	i = j;								\
	if (*e >= 1)							\
	  {								\
	    chn += i;							\
	    goto exit0;							\
	  }								\
      }									\
 									\
    limit2  -= i;							\
    chn     += i;							\
 									\
    DB(printf("limit2 %d i %d offset %d\n",limit2,i,offset));		\
 									\
    if (limit2 && (buflen - bufn) > 0)					\
      {									\
	if (offset > 0 ? (i != offset || p < offset) : (i != 16 || p != 16)) \
	  {								\
	    offset -= i;						\
	    goto adv;							\
	  }								\
	else								\
	  {								\
	    offset = 0;							\
	    goto begin;							\
	  }								\
      }									\
									\
  exit:									\
    *e = 3;								\
  exit0:								\
    *bufpt  = bufn;							\
    *count  = limit - chn;						\
									\
    DB(printf("adv end count = %d, bufpt = %d nprocessed %d\n",		\
	      *count,*bufpt,chn));					\
  }

#define MKUTF32(encode_utf32,fill_utf8,INIT, ...)		\
  void encode_utf32 (uint8_t        *buf,			\
		     size_t         *bufpt,			\
		     uint64_t        buflen,			\
		     const uint32_t *chars,			\
		     size_t         *count,			\
		     uint64_t       flags,			\
		     ##__VA_ARGS__)				\
  {								\
    INIT;							\
    size_t max   = buflen - 100;				\
    size_t limit = *count;					\
    								\
    size_t n = *bufpt;						\
    int    j = 0;						\
    int    p = 0;						\
    for (size_t i = 0; i < limit && n < max; i++, j++)		\
      {								\
	uint32_t ch = chars[i];					\
								\
	n += fill_utf8(ch,buf+n,Sh,D,flags,&p);			\
      }								\
    								\
    *bufpt = n;							\
    *count = *count - j;					\
  }

#define MKRUTF32(encode_utf32, buf_t, char_t, fill_utf8,N,REV)	\
  void encode_utf32 (buf_t          *buf,			\
		     size_t         *bufpt,			\
		     uint64_t        buflen,			\
		     char_t         *chars,			\
		     size_t         *count,			\
		     int	    *e,				\
		     SCM            pos,			\
		     uint64_t       flags)			\
  {								\
    size_t max   = buflen;					\
    size_t limit = *count;					\
								\
    size_t n = *bufpt;						\
    int    j = 0;						\
    int    p = 0;						\
    int    i = 0;						\
    for (; i < limit && n < max; i++)				\
      {									\
	uint32_t ch = REV0(chars[i]);					\
									\
	int j = i;							\
	n += fill_utf8(ch, buf + n, chars, &j, flags, &p, e, pos);	\
	incr_pos(pos,j-i);						\
	i = j;								\
	if (*e >= 1)							\
	  {								\
	    goto out;							\
	  }								\
      }									\
    									\
    *e = 3;								\
  out:									\
    *bufpt = n;								\
    *count = *count - j;						\
  }


#define LINE_UTF8(w) (u.v >= w##128 .v)

#define LINE_UTF8_WRITE(w)			\
  ( (u.v >= w##127 .v)  |			\
    (u.v <  w##32  .v ) |			\
    (u.v == w##92  .v ) |			\
    (u.v == w##39  .v ) |			\
    (u.v == w##34  .v ))			\

#define LINE_UTF8_SYM(w)						\
  (  (u.v <  w##32  .v) |						\
     ((u.v >= w##127 .v) & ((u.v < w##161 .v) | (u.v == w##173 .v))))

#define LINE_UTF8_R7RS(w)						\
  (  (u.v <  w##32  .v) |						\
    ((u.v >= w##127 .v) & ((u.v < w##161 .v) | (u.v == w##173 .v))) |	\
     (u.v == w##92  .v) |						\
     (u.v == w##124 .v))
  
#define MKFILLW(fill_write,LINE,LINE2, ...)				\
  inline int fill_write(uint32_t ch, uint8_t *buf, int Sh, int D,		\
			uint64_t flags, int *p, ##__VA_ARGS__)		\
  {									\
    if (ch < 32)							\
      {									\
	if (ch == 10)							\
	  {								\
	    if (flags & escaped_newline_flag)				\
	      {								\
		buf[D]         = '\\';					\
		buf[(1<<Sh)+D] = 'n';					\
		return 2<<Sh;						\
	      }								\
	    else							\
	      {								\
		p++;							\
		buf[D] = ch;						\
		return 1<<Sh;						\
	      }								\
	  }								\
	else if (ch <= 13 && ch >= 7)					\
	  {								\
	    if (flags & r6rs_escapes_flag)				\
	      {								\
		hexitn(buf,ch,Sh,D);					\
		return 4<<Sh;						\
	      }								\
	    else							\
	      {								\
		char v[7] = {'a','b','t','n','v','f','r'};		\
		buf[D] = '\\';						\
		buf[(1<<Sh)+D] = v[ch-7];				\
		return 2 << Sh;						\
	      }								\
	  }								\
	else								\
	  {								\
	    hexitn(buf,ch,Sh,D);					\
	    return 4 << Sh;						\
	  }								\
      }									\
    else if (ch == 34 && !(flags & quote_flag))				\
      {									\
	buf[D] = '\\';							\
	buf[(1<<Sh) + D] = '"';						\
	return 2 << Sh;							\
      }									\
    else if (ch == 39 && (flags & quote_flag))				\
      {									\
	buf[D] = '\\';							\
	buf[(1<<Sh) + D] = '\'';					\
	return 2 << Sh;							\
      }									\
    else if (ch == 92)							\
      {									\
	buf[D] = buf[(1<<Sh)+D] = '\\';					\
	return 2 << Sh;							\
      }									\
    else if (ch < 256 && ch > 173)					\
      {									\
	return LINE;							\
      }									\
    else if (ch >= 127 && (ch < 161 || ch == 173))			\
      {									\
	hexitn(buf, ch, Sh, D);						\
	return 4 << Sh;							\
      }									\
    else if (ch < 256)							\
      {									\
	buf[D] = ch;							\
	(*p)++;								\
	return 1 << Sh;							\
      }									\
    else if (uc_is_general_category_withtable (ch,			\
					       UC_CATEGORY_MASK_L |	\
					       UC_CATEGORY_MASK_M |	\
					       UC_CATEGORY_MASK_N |	\
					       UC_CATEGORY_MASK_P |	\
					       UC_CATEGORY_MASK_S))	\
      {									\
	return LINE2;							\
      }									\
    else								\
      {									\
	return encode_escape_sequence (ch, buf, Sh, D, flags);		\
      }									\
									\
    return 0;								\
  }

MKFILLW(fill_write_utf8          ,
	(codepoint_to_utf8(ch,buf)),
	(codepoint_to_utf8(ch,buf)));

MKFILLW(fill_write_latin1        ,
	((*p)++,buf[0] = ch, 1),
	(encode_escape_sequence (ch, buf, Sh, D, flags)));

MKFILLW(fill_write_utf32_copy    ,
	((*p)++, buf[D] = ch, 4),
	(*((uint32_t *)buf) = ch, 4));

MKFILLW(fill_write_utf32_rev,
	((*p)++, (*((uint32_t *)buf) = swap(ch)), 4),
	((*((uint32_t *)buf) = swap(ch)), 4));

MKFILLW(fill_write_utf16,
	((*p)++, buf[D] = ch, 2),
	encode_utf16(ch,buf,flags,D))

#define MKFILL_U8(fill,LINE)						\
  int fill(uint8_t ch, uint8_t *buf, int Sh, int D,			\
	   uint64_t flags, int *p)					\
  {									\
    if (ch < 128)							\
      {									\
	buf[D] = ch;							\
	(*p) += 1;							\
	return 1 << Sh;							\
      }									\
    else								\
      {									\
	return LINE;							\
      }									\
  }

MKFILL_U8(fill_utf8, codepoint_to_utf8(ch,buf));

#define MKFILLS(fill_sym,LINE1,LINE2, ...)				\
  inline int fill_sym(uint32_t ch, uint8_t *buf, int Sh, int D,		\
		      uint64_t flags, int *p, ##__VA_ARGS__)		\
									\
  {									\
    int N = 1<<Sh;							\
    if (ch < 32)							\
      {									\
	return hexit2(ch,buf,Sh,D);					\
      }									\
    else if (ch > 173 && ch < 256)					\
    {									\
      int d = LINE1;							\
      if (d == N) (*p)++;						\
      return d;								\
    }									\
    else if (ch >= 127 && (ch < 161 || ch == 173))			\
      {									\
	return hexit2(ch, buf, Sh, D);					\
      }									\
    else if (ch < 256)							\
      {									\
	int d = LINE1;							\
	if (d == N) (*p)++;						\
	return d;							\
      }									\
    else if (uc_is_general_category_withtable (ch,			\
					       SUBSEQUENT_IDENTIFIER_MASK \
					       | UC_CATEGORY_MASK_Zs))	\
      {									\
	return LINE2;							\
      }									\
    else								\
      {									\
	return hexit2(ch, buf, Sh, D);					\
      }									\
									\
    return 0;								\
  }
     

MKFILLS(fill_extended_utf8,
	codepoint_to_utf8(ch, buf),
	codepoint_to_utf8(ch, buf));

MKFILLS(fill_extended_latin1,
	(buf[0] = ch, 1),
	(encode_escape_sequence (ch, buf, Sh, D, flags)));

MKFILLS(fill_extended_utf32_copy,
	(buf[D] = ch, 4),
	(*((uint32_t *)buf) = ch, 4));

MKFILLS(fill_extended_utf32_rev,
	(buf[D] = ch, 4),
	((*((uint32_t *)buf) = swap(ch)), 4));

MKFILLS(fill_extended_utf16,
	(buf[D] = ch, 2),
	encode_utf16(ch,buf,flags,D))

#define MKFILLR(fill_r7rs, LINE1, LINE2,  ...)				\
  int fill_r7rs(uint32_t ch, uint8_t *buf, int Sh, int D,		\
		uint64_t flags, int *p, ##__VA_ARGS__)			\
									\
  {									\
    int N = 1<<Sh;							\
    if (ch < 32)							\
      {									\
	if (ch >= 7 && ch <= 10)					\
	  {								\
	    char chs[] = {'a','b','t','n'};				\
	    buf[D] = '\\';						\
	    buf[N+D] = chs[ch-7];					\
	    return 2*N;							\
	  }								\
	else if (ch == 13)						\
	  {								\
	    buf[D]   = '\\';						\
	    buf[N+D] = 'r';						\
	    return 2*N;							\
	  }								\
	else								\
	  return hexit2(ch,buf,Sh,D);					\
      }									\
    else if (ch == 124)							\
      {									\
	buf[D]   = '\\';						\
	buf[N+D] = '|';							\
	return 2*N;							\
      }									\
    else if (ch == 92)							\
      return hexit2(ch,buf,Sh,D);					\
    else if (ch > 173 && ch < 256)					\
      {									\
	int d = LINE1;							\
	if (d == N) (*p)++;						\
	return d;							\
      }									\
    else if (ch >= 127 && (ch < 161 || ch == 173))			\
      {									\
	return hexit2(ch, buf, Sh, D);					\
      }									\
    else if (ch < 256)							\
      {									\
	int d = LINE1;							\
	if (d == N) (*p)++;						\
	return d;							\
      }									\
    else if (uc_is_general_category_withtable (ch,			\
					       SUBSEQUENT_IDENTIFIER_MASK \
					       | UC_CATEGORY_MASK_Zs))	\
      {									\
	return LINE2;							\
      }									\
    else								\
      {									\
	return hexit2(ch, buf, Sh, D);					\
      }									\
									\
    return 0;								\
  }

MKFILLR(fill_r7rs_utf8,
	codepoint_to_utf8(ch, buf),
	codepoint_to_utf8(ch, buf))

MKFILLR(fill_r7rs_latin1,
	(buf[0] = ch, 1),
	(encode_escape_sequence (ch, buf, Sh, D, flags)));

MKFILLR(fill_r7rs_utf32_copy,
	(buf[D] = ch , 4),
	(*((uint32_t *)buf) = ch , 4));

MKFILLR(fill_r7rs_utf32_rev,
	(buf[D] = ch, 4),
	(*((uint32_t *)buf) = swap(ch), 4));

MKFILLR(fill_r7rs_utf16,
	(buf[D] = ch, 2),
	encode_utf16(ch,buf,flags,D));
	

/*
END AT
 ( 40
 ) 41
 ; 59
 " 34
 space   32
 return  13
 ff      12
 newline 10
 tab     9

 [  133
 ]  135 if square_braket_flag

 {  173 if curly_infix_flag
 }  175

 
*/
#define RLINEUTF8(N)				\
{						\
  int j = *i;					\
  ch = utf8_to_utf32(ch,chars,&j,e);		\
  if (N == 1 && ch >= 256)			\
    {						\
      *e = 2;					\
      return 0;					\
    }						\
						\
  if (*e)					\
    {						\
      *i = j;					\
      return 0;					\
    }						\
						\
  *i = j;					\
  buf[0] = ch;					\
  return 1;					\
}

#define MK_READ_TOK(read_utf8_token, M, LINE, buf_t, char_t, N, REV)	\
  int read_utf8_token(uint32_t ch, buf_t *buf, char_t *chars, int *i,	\
		      uint64_t flags, int *p, int *e, SCM pos)		\
  {									\
    if (ch > M)								\
      {									\
	LINE;								\
      }									\
    else if (ch <= 41)							\
      {									\
	if (ch == 0  || ch == 40 || ch == 41 || ch == 59 || ch == 34 ||	\
	    ch == 32 || ch == 12 || ch == 13 || ch == 10 || ch == 9)	\
	  {								\
	    *e = 1;							\
	    return 0;							\
	  }								\
	else								\
	  {								\
	    buf[0] = ch;						\
	    return 1;							\
	  }								\
      }									\
    else if ((flags & read_curly_infix_flag) && (ch == 123 || ch == 125)) \
      {									\
	*e = 1;								\
	return 0;							\
      }									\
    else if ((flags & read_square_bracket_flag) && (ch == 91 || ch == 93)) \
      {									\
	*e = 1;								\
	return 0;							\
      }									\
    else								\
      {									\
	buf[0] = ch;							\
	return 1;							\
      }									\
  }

#define RLINEUTF16(N,utf16_to_utf32)		\
{						\
  if (N == 1)					\
    {						\
      *e = 2;					\
      return 0;					\
    }						\
						\
  int j = *i;					\
  ch = utf16_to_utf32(ch,chars,&j,e);		\
						\
  if (*e)					\
    {						\
      *i = j;					\
      return 0;					\
    }						\
						\
  *i = j;					\
  buf[0] = ch;					\
  return 1;					\
}

#define RLINEUTF32(N)				\
{						\
  if (N == 1)					\
    {						\
      *e = 2;					\
      return 0;					\
    }						\
						\
  buf[0] = ch;					\
  return 1;					\
}


#define RLINELAT1

#define LINE_R_UTF8_EXT(w)				\
  ( (u.v == w##125 .v ) |				\
    (u.v == w##10  .v ) |				\
    (u.v >  w##127 .v )	|				\
    (u.v == w##0   .v ))

#define LINE_R_UTF8_TOKEN(w)				\
  ( (u.v <= w##41  .v ) |				\
    (u.v == w##0   .v ) |				\
    (u.v == w##91  .v ) |				\
    (u.v == w##93  .v ) |				\
    (u.v == w##123 .v ) |				\
    (u.v == w##125 .v ) |				\
    (u.v >  w##127 .v ))
    

#define LINE_R_LATIN1_EXT(w)				\
  ( (u.v == w##125 .v ) |				\
    (u.v == w##10  .v ) |				\
    (u.v == w##0   .v ))

#define LINE_R_LATIN1_TOKEN(w)				\
  ( (u.v <= w##41  .v ) |				\
    (u.v == w##0   .v ) |				\
    (u.v == w##91  .v ) |				\
    (u.v == w##93  .v ) |				\
    (u.v == w##123 .v ) |				\
    (u.v == w##125 .v ))

#define LINE_R_UTF8_STR(w)				\
  ( (u.v == w##34  .v ) |				\
    (u.v == w##0 .v )   |				\
    (u.v == w##92  .v ) |				\
    (u.v >  w##127 .v ))

#define LINE_R_UTF8_R7RS(w)				\
  ( (u.v == w##124 .v ) |				\
    (u.v == w##0   .v ) |				\
    (u.v == w##92  .v ) |				\
    (u.v >  w##127 .v ))

#define LINE_R_LATIN1_STR(w)				\
  ( (u.v == w##34  .v ) |				\
    (u.v == w##0   .v ) |				\
    (u.v == w##92  .v ))

#define LINE_R_LATIN1_R7RS(w)				\
  ( (u.v == w##124 .v ) |				\
    (u.v == w##0 .v ) |					\
    (u.v == w##92  .v ))			       


#define FIXED_ESCAPE(REV,N)						\
  {									\
    int j = *i + 2;							\
    uint64_t s = 0;							\
    read_fixed_hex_escape(s, chars, j, REV, N);				\
    if (N == 1 && s > 255)						\
      {									\
	*e = 2;								\
	return 0;							\
      }									\
    *i = j;								\
    buf[0] = s;								\
  }									\


#define MK_READ_STR_CH(read_utf8_str, M, LINE, buf_t, char_t, N, REV, CH) \
  inline int read_utf8_str(uint32_t ch, buf_t *buf, char_t *chars, int *i, \
			   uint64_t flags, int *p, int *e, SCM pos)	\
  {									\
    if (ch > M)								\
      {									\
	LINE;								\
      }									\
    else if (ch == CH)							\
      {									\
	*e = 1;								\
	return 0;							\
      }									\
    else if (ch == 0)							\
      {									\
	scm_misc_error("read",						\
		       "unexpected end of input while reading string",	\
		       SCM_EOL);					\
      }									\
    else if (ch == '\\')						\
      {									\
	uint64_t ch2 = chars[*i+1];					\
	if (!ch2)							\
	  scm_misc_error("read","unexpected 0 in string (end of file?)", \
			 SCM_EOL);					\
									\
	if (ch2 == '|' || ch2 == '(' || ch2 == CH || ch2 == '(')	\
	  buf[0] = ch2;							\
	else if (ch2 == '0') buf[0] = 0;				\
	else if (ch2 == 'f') buf[0] = '\f';				\
	else if (ch2 == 'n') buf[0] = '\n';				\
	else if (ch2 == 'r') buf[0] = '\r';				\
	else if (ch2 == 't') buf[0] = '\t';				\
	else if (ch2 == 'a') buf[0] = '\a';				\
	else if (ch2 == 'v') buf[0] = '\v';				\
	else if (ch2 == 'b') buf[0] = '\b';				\
	else if (ch2 == 'x')						\
	  {								\
	    if ((flags & read_r6rs_escapes_flag) || CH == '|')		\
	      {								\
		int j = *i + 2;						\
		uint64_t s = 0;						\
		read_r6rs_hex_escape(s, chars, j, REV);			\
		if (N == 1 && s > 255)					\
		  {							\
		    *e = 2;						\
		    return 0;						\
		  }							\
		*i = j;							\
		buf[0] = s;						\
	      }								\
	    else							\
	      {								\
		FIXED_ESCAPE(REV,2);					\
	      }								\
	  }								\
	else if (ch2 == 'u')						\
	  {								\
	    FIXED_ESCAPE(REV,4);					\
	  }								\
	else if (ch2 == 'U')						\
	  {								\
	    FIXED_ESCAPE(REV,6);					\
	  }								\
	else if (ch2 == '\n')						\
	  {								\
	    newline(pos);						\
	    if (flags & read_hungry_eol_escapes_flag)			\
	      {								\
		*e  = 4;						\
		return 1;						\
	      }								\
	    buf[0] = '\n';						\
	    return 1;							\
	  }								\
	else								\
	  scm_misc_error("read", "unknow escape char", SCM_EOL);	\
									\
	return 1;							\
      }									\
    else								\
      {									\
	buf[0] = ch;							\
	return 1;							\
      }									\
    }									

#define MK_READ_UNTIL_CH(read_utf8_str, M, LINE, buf_t, char_t, N, REV,	\
			 CH1,CH2)					\
  inline int read_utf8_str(uint32_t ch, buf_t *buf, char_t *chars, int *i, \
			   uint64_t flags, int *p, int *e, SCM pos)	\
  {									\
    if (ch > M)								\
      {									\
	LINE;								\
      }									\
    else if (ch == CH1)							\
      {									\
	uint32_t ch2 = REV(chars[*i + 1]);				\
	if (ch2 == CH2)							\
	  {								\
	    *i += 1;							\
	    *e = 1;							\
	    return 0;							\
	  }								\
	buf[0] = CH1;							\
	*i += 1;							\
	return 1;							\
      }									\
    else if (ch == 0)							\
      {									\
	scm_misc_error("read",						\
		       "unexpected end of input while reading string",	\
		       SCM_EOL);					\
      }									\
    else if (ch == '\n')						\
      {									\
	*i += 1;							\
	newline(pos);							\
	buf[0] = '\n';							\
	return 1;							\
      }									\
    else								\
      {									\
	buf[0] = ch;							\
	return 1;							\
      }									\
    }									

#define MK_READ_EXT(read_utf8_str, M, LINE, buf_t, char_t, N, REV)	\
  MK_READ_UNTIL_CH(read_utf8_str, M, LINE, buf_t, char_t, N, REV, '}', '#')

#define MK_READ_STR(read_utf8_str, M, LINE, buf_t, char_t, N, REV)	\
  MK_READ_STR_CH(read_utf8_str, M, LINE, buf_t, char_t, N, REV, '"')

#define MK_READ_R7RS(read_utf8_str, M, LINE, buf_t, char_t, N, REV)	\
  MK_READ_STR_CH(read_utf8_str, M, LINE, buf_t, char_t, N, REV, '|')


#define MKMK_READ_TOK(MK_READ_TOK,token)				\
  MK_READ_TOK(read_##token##_utf8_latin1  , 127, RLINEUTF8(1),		\
	      uint8_t, uint8_t,  1, REV0);				\
									\
									\
  MK_READ_TOK(read_##token##_utf8_utf32  , 127, RLINEUTF8(4),		\
	      uint32_t, uint8_t, 4, REV0);				\
									\
  MK_READ_TOK(read_##token##_utf16_latin1  , 255,			\
	      RLINEUTF16(1,utf16_to_utf32), uint8_t, uint16_t, 1, REV0); \
  									\
  MK_READ_TOK(read_##token##_utf16_utf32  , 255,			\
	      RLINEUTF16(4,utf16_to_utf32), uint32_t, uint16_t, 4, REV0); \
  									\
  MK_READ_TOK(read_##token##_utf16_latin1_rev  , 255,			\
	      RLINEUTF16(1,utf16_to_utf32_rev), uint8_t, uint16_t, 1, rev16); \
									\
  MK_READ_TOK(read_##token##_utf16_utf32_rev  , 255,			\
	      RLINEUTF16(4,utf16_to_utf32_rev), uint32_t, uint16_t, 4, rev16); \
    									\
  MK_READ_TOK(read_##token##_utf32_latin1 , 255, RLINEUTF32(1),		\
	      uint8_t, uint32_t, 1, REV0);				\
									\
  MK_READ_TOK(read_##token##_utf32_utf32 , 255, RLINEUTF32(4),		\
	      uint32_t, uint32_t, 4, REV0);				\
									\
  MK_READ_TOK(read_##token##_utf32_latin1_rev , 255, RLINEUTF32(1),	\
	      uint8_t, uint32_t, 1, rev32);				\
									\
  MK_READ_TOK(read_##token##_utf32_utf32_rev , 255, RLINEUTF32(4),	\
	      uint32_t, uint32_t, 4, rev32);				\
									\
  MK_READ_TOK(read_##token##_latin1_latin1 , -1 , RLINELAT1,		\
	      uint8_t, uint8_t, 1, REV0);				\
									\
  MK_READ_TOK(read_##token##_latin1_utf32 , -1 , RLINELAT1,		\
  	      uint32_t, uint8_t, 1, REV0);



MKMK_READ_TOK(MK_READ_EXT,ext);
MKMK_READ_TOK(MK_READ_TOK,token);
MKMK_READ_TOK(MK_READ_STR,str);
MKMK_READ_TOK(MK_READ_R7RS,r7rs);

