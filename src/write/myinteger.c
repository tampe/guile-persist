#include <math.h>
#include <float.h>

int B[64] = {0, 0, 1, 1, 1, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6,
	     6, 6, 7, 7, 7, 8, 8, 8, 9, 9, 9, 9, 10, 10, 10, 11, 11, 11,
	     12, 12, 12, 12, 13, 13, 13, 14, 14, 14, 15, 15, 15, 15, 16,
	     16, 16, 17, 17, 17, 18, 18, 18, 18, 19};

uint64_t A[20] = {1UL,
		  10UL,
		  100UL,
		  1000UL,
		  10000UL,
		  100000UL,
		  1000000UL,
		  10000000UL,
		  100000000UL,
		  1000000000UL,
		  10000000000UL,
		  100000000000UL,
		  1000000000000UL,
		  10000000000000UL,
		  100000000000000UL,
		  1000000000000000UL,
		  10000000000000000UL,
		  100000000000000000UL,
		  1000000000000000000UL,
		  10000000000000000000UL};

int MM1[20*2];
int MM2[20*2];

uint64_t MU1[] = {(1UL<<8)-1UL, (1UL<<16)-1UL, (1UL<<32)-1UL,
		  ((1UL<<63)-1UL)|(1UL<<63)};

uint64_t MS1[] = {(1UL<<7)-1UL, (1UL<<15)-1UL, (1UL<<31)-1UL,
		  (1UL<<63)-1UL};
uint64_t MS2[] = {1UL<<7      , 1UL<<15      , 1UL<<31      ,
		  1UL<<63      };

uint8_t  MU [] = {4, 6, 11, 21};
uint8_t  MS [] = {4, 6, 11, 20};

uint8_t  PU [] = {0, 0, 0 , 20};
uint8_t  PS [] = {0, 0, 0 , 19};


/* PRINTER PART */
int print_int64(int64_t x_, uint8_t *pt, uint64_t flags,
		int Sh, int D, int prefix)   
{
  int N = 1<<Sh;
  int n = 0;

  if (x_ == 0)
    {
      pt[n+D] = '0';
      n += N;
      return n;
    }

  uint64_t x;
  if (x_ < 0)
    {
      x = (uint64_t)(-x_);
      pt[n+D] = '-';
      n += N;
    }
  else
    {
      x = (uint64_t) x_;
    }

  int sh = B[64-__builtin_clzl(x)+1];

  if (x < A[sh])
    {
      sh--;
      if (x < A[sh]) sh--;
    }

  for(; sh >= 0; sh--)
    {
      int ch = x/A[sh];
      pt[n + D] = '0' + ch;
      n += N;
      x -= ch*A[sh];
    }

  return n;
}

int print_uint64(uint64_t x, uint8_t *pt, uint64_t flags,
		 int Sh, int D, int prefix)   
{
  int N = 1<<Sh;
  int n = 0;

  if (x == 0)
    {
      pt[n+D] = '0';
      n += N;
      return n;
    }

  int sh = B[64-__builtin_clzl(x)+1];

  if (x < A[sh])
    {
      sh--;
      if (x < A[sh]) sh--;
    }

  for(; sh >= 0; sh--)
    {
      int ch = x/A[sh];
      pt[n + D] = '0' + ch;
      n += N;
      x -= ch*A[sh];
    }

  return n;
}

int print_int64_binary(int64_t x_, uint8_t *pt, uint64_t flags,
		       int Sh, int D, int prefix)   
{
  int nbits = (flags & binary_mask) >> binary_shift;
  int       N = 1<<Sh;
  int       n = 0;

  int M = 1 << nbits;
  
  if (x_ == 0)
    {
      pt[n+D] = '0';
      n += N;
      return n;
    }

  uint64_t x;
  if (x_ < 0)
    {
      x = (uint64_t)(-x_);
      pt[n+D] = '-';
      n += N;
    }
  else
    {
      x = (uint64_t) x_;
    }

  if (prefix)
    {
      static const char binpre[6] = {'b','b','q','o','x','y'};
      n += put_char('#'          ,pt + n, Sh, D);
      n += put_char(binpre[nbits],pt + n, Sh, D);
    }

  int sh  = 64 - __builtin_clzl(x);
  int sh2 = ((sh  / nbits) *  nbits);
  if (sh == sh2)
    sh = sh2-nbits;
  else
    sh = sh2;
    
  uint64_t mask = (M-1) << sh;
  
  for(; sh >= 0; mask >>= nbits,  sh -= nbits)
    {
      int ch = (x&mask) >> sh;
      if (ch < 10)
	pt[n + D] = '0' + ch;
      else
	pt[n + D] = 'a' + (ch - 10);
      
      n += N;
    }

  return n;
}


int print_uint64_binary_cut(uint64_t x, uint8_t *pt, uint64_t flags,
			int Sh, int D, int prefix, int cut)   
{
  int nbits = (flags & binary_mask) >> binary_shift;
  int N = 1<<Sh;
  int n = 0;

  uint64_t M = 1 << nbits;
  
  if (x == 0)
    {
      n = put_char('0',pt,Sh,D);
      return n;
    }

  if (prefix)
    {
      static const char binpre[6] = {'b','b','q','o','x','y'};
      n += put_char('#'          ,pt + n, Sh, D);
      n += put_char(binpre[nbits],pt + n, Sh, D);
    }
  

  int sh  = 64 - __builtin_clzl(x);
  int sh2 = ((sh  / nbits) *  nbits);
  if (sh == sh2)
    sh = sh2-nbits;
  else
    sh = sh2;

  uint64_t mask = (M-1) << sh;
  
  for(; mask && (cut ? x : 1); mask >>= nbits, sh -= nbits, n += N)
    {
      uint64_t ch = (x&mask) >> sh;

      if (ch < 10)
	pt[n + D] = '0' + ch;
      else
	pt[n + D] = 'a' + (ch - 10);
      
      x -= (ch<<sh);
    }

  return n;
}

int print_uint128_binary_cut(uint128_t x, uint8_t *pt, uint64_t flags,
			int Sh, int D, int prefix, int cut)   
{
  int nbits = (flags & binary_mask) >> binary_shift;
  int N = 1<<Sh;
  int n = 0;

  uint128_t M = 1 << nbits;
  
  if (x == 0)
    {
      n = put_char('0',pt,Sh,D);
      return n;
    }

  if (prefix)
    {
      static const char binpre[6] = {'b','b','q','o','x','y'};
      n += put_char('#'          ,pt + n, Sh, D);
      n += put_char(binpre[nbits],pt + n, Sh, D);
    }
  

  int sh;
  uint64_t hi = x >> 64;
  if (likely(hi))
    {
      sh = 64 - __builtin_clzl(x) + 64;;
    }
  else
    {      
      sh = 64 - __builtin_clzl((uint64_t) x) + 64;;
    }
  
  int sh2 = ((sh  / nbits) *  nbits);
  if (sh == sh2)
    sh = sh2-nbits;
  else
    sh = sh2;

  uint128_t mask = (M-1) << sh;
  
  for(; mask && (cut ? x : 1); mask >>= nbits, sh -= nbits, n += N)
    {
      uint64_t ch = (x&mask) >> sh;

      if (ch < 10)
	pt[n + D] = '0' + ch;
      else
	pt[n + D] = 'a' + (ch - 10);
      
      x -= (ch<<sh);
    }

  return n;
}

int print_uint64_binary(uint64_t x, uint8_t *pt, uint64_t flags,
			int Sh, int D, int prefix)
{
  return print_uint64_binary_cut(x, pt, flags, Sh, D, prefix, 0);
}

int print_uint64_hex(uint64_t x, uint8_t *pt, uint64_t flags,
		     int Sh, int D, int prefix)
{
  flags = (flags & ~binary_mask) | (4 << binary_shift);
  return print_uint64_binary_cut(x, pt, flags, Sh, D, prefix, 0);
}

int print_int64_hex(uint64_t x, uint8_t *pt, uint64_t flags,
		     int Sh, int D, int prefix)
{
  flags = (flags & ~binary_mask) | (4 << binary_shift);
  return print_int64_binary(x, pt, flags, Sh, D, prefix);
}

// This is the standard algorithm, no tricks, just pure math
int print_int64_base(int64_t x_, uint8_t *pt, uint64_t flags,
		      int Sh, int D, int base)   
{
  int N = 1<<Sh;
  int n = 0;
  
  uint64_t x;
  
  if (x_ == 0)
    {
      pt[D] = '0';
      n += N;
      return n;
    }

  if (x_ < 0)
    {
      x = (uint64_t)(-x_);
      pt[n+D] = '-';
      n += N;
    }
  else
    {
      x = (uint64_t) x_;
    }

  int m = 0;

  // This will lead to the wrong order smallest digit first
  for(; x; m++, n += N, x /= base)
    {
      int ch = x % base;
      if (ch < 10)
	pt[n + D] = '0' + (ch);
      else
	pt[n + D] = 'a' + (ch);
    }

  // Reverse the order
  for (int i = 0; i < m/2; i++)
    {
      for(int j = 0; j<N; j++)
	{
	  int t = pt[i*N+j];	
	  pt[i*N+j] = pt[(m-i-1)+j];
	  pt[(m-i-1)+j] = t;
	}
    }
  
  return n;
}


// This is the standard algorithm, no tricks, just pure math
int print_uint64_base(uint64_t x, uint8_t *pt, uint64_t flags,
		      int Sh, int D, int base)   
{
  int N = 1<<Sh;

  int n = 0;
  
  if (x == 0)
    {
      pt[D] = '0';
      n += N;
      return n;
    }

  int m = 0;

  // This will lead to the wrong order smallest digit first
  for(; x; m++, n += N, x /= base)
    {
      int ch = x % base;
      if (ch < 10)
	pt[n + D] = '0' + (ch);
      else
	pt[n + D] = 'a' + (ch);
    }

  // Reverse the order
  for (int i = 0; i < m/2; i++)
    {
      for(int j = 0; j<N; j++)
	{
	  int t = pt[i*N+j];	
	  pt[i*N+j] = pt[(m-i-1)+j];
	  pt[(m-i-1)+j] = t;
	}
    }
  
  return n;
}

/* READER PART */

#define MKRINT(read_integer, int_t, buf_t, REV, SGN)			\
int_t read_integer(buf_t *buf, size_t *bufpt, SCM pos, int *e, uint64_t flags) \
{									\
  uint64_t nbits_ = (flags & read_bits_mask) >> read_bits_shift;	\
									\
  int      M      = (SGN ? MS [nbits_] : MU [nbits_] );			\
  int      P      = (SGN ? PS [nbits_] : PU [nbits_] );			\
  uint64_t M1     = (SGN ? MS1[nbits_] : MU1[nbits_] );			\
  uint64_t M2     = (SGN ? MS2[nbits_] : MU1[nbits_] );			\
  int     *MM     = (SGN ? MM2         : MM1         );			\
									\
  int j = 0, q = 0, pred = 0, sgn = 0;					\
  									\
  size_t bp = *bufpt;							\
									\
  int_t x = 0;								\
									\
  int64_t ch;								\
									\
  if (SGN)								\
    {									\
      ch = REV(buf[bp]);						\
      if (ch == '-')							\
	{								\
	  bp += 1;							\
	  sgn = 1;							\
	}								\
      									\
      incr_pos(pos,1);							\
    }									\
									\
  for (int k = 0; j < M; j++,k++)					\
    {									\
      ch = REV(buf[bp+k]);						\
									\
      if (ch == '\n' || ch == '\r')					\
	{								\
	  j--;								\
	  continue;							\
	}								\
    									\
      ch -= '0';							\
      if (ch >= 0 && ch <= 9)						\
	{								\
	  continue;							\
	}								\
    									\
      break;								\
    }									\
  									\
  if (j == M || j == 0)							\
    {									\
      *e = 0;								\
      return 0;								\
    }									\
									\
  int_t exp = A[j-1];							\
									\
  q = 0;								\
  pred = (nbits_ == 3 && ((j == P) ? 1 : 0));				\
  for (int k = 0, p = 0; exp; p++, k++, q++, exp /= 10)			\
    {									\
      ch = REV(buf[bp+k]);						\
      if (ch == '\n' || ch == '\r')					\
	{								\
	  if (ch == '\n')						\
	    {								\
	      q = 0;							\
	      newline(pos);						\
	    }								\
	  k--;								\
	  continue;							\
	}								\
									\
      ch -= '0';							\
	    								\
      if (pred && ch >= MM[((j-k-1)<<1)+sgn])				\
	{								\
	  if (ch > MM[((j-k-1)<<1)+sgn])				\
	    scm_misc_error("read",					\
			   "numerical overflow in integer",		\
			   SCM_EOL);					\
	}								\
      else								\
	{								\
	  pred = 0;							\
	}								\
									\
      x += ch*exp;							\
    }									\
									\
  incr_pos(pos,q);							\
									\
  bp += j;								\
  if (SGN)								\
    {									\
      if (sgn == 1)							\
	{								\
	  if (x > M2)							\
	    {								\
	      scm_misc_error						\
		("read",						\
		 "numerical overflow/underflow in integer read ~a",	\
		 scm_list_1(scm_from_int64(-x)));			\
	    }								\
	}								\
      else								\
	{								\
	  if (x > M1)							\
	    {								\
	      scm_misc_error						\
		("read",						\
		 "numerical overflow/underflow in integer read ~a",	\
		 scm_list_1(scm_from_int64(x)));			\
	    }								\
	}								\
    }									\
  else									\
    {									\
      if (x > M1)							\
	{								\
	  scm_misc_error						\
	    ("read",							\
	     "numerical overflow/underflow in integer read ~a",		\
	     scm_list_1(scm_from_uint64(x)));				\
	}								\
    }									\
									\
  *bufpt = bp;								\
  *e     = 1;								\
									\
  return sgn ? -x : x;							\
}



#define MKMKRINT(tag, buf_t, REV)					\
  MKRINT(read_integer_u##tag , uint64_t, buf_t, REV, 0);		\
  MKRINT(read_integer_s##tag ,  int64_t, buf_t, REV, 1);  

MKMKRINT(_utf8     , uint8_t , REV0);
MKMKRINT(_utf16    , uint16_t, REV0);
MKMKRINT(_utf16_rev, uint16_t, rev16);
MKMKRINT(_utf32    , uint32_t, REV0);
MKMKRINT(_utf32_rev, uint32_t, rev32);


#define MKRINTBIN(read_integer, int_t, buf_t, REV, SGN)			\
  int_t read_integer(buf_t *buf, size_t *bufpt, SCM pos, int *e,	\
		     uint64_t flags)					\
  {									\
    uint64_t nbits_ = (flags & read_bits_mask) >> read_bits_shift;	\
    uint64_t nbits2 = 8 * (1 << nbits_);				\
    uint64_t nbits  = nbits2 - (SGN ? 1 : 0);				\
									\
    uint64_t nbin  = (flags & read_bins_mask) >> read_bins_shift;	\
    int j = 0, q = 0, pred = 0, sgn = 0;				\
    int64_t ch, ch2;							\
    size_t bp = *bufpt;							\
    int N  = 1<<nbin;							\
    int M0 = nbits % nbin;						\
    int NK = N-10;							\
    int M  = (nbits / nbin) + (M0?1:0) + 1;				\
    int P  = nbits2 == 64 ? ((1<<M0) - 1) : 0;				\
    uint64_t M1 = (nbits2 == 64) ? ((1UL<<63) | ((1UL<<63)-1UL))	\
                                 : ((1UL<<nbits)-1UL);			\
    uint64_t M2 = (1UL<<nbits);						\
    									\
    /* printf("M = %d, M0 = %d, P = %d, M1 = %lu, M2=%lu\n",M,M0,P,M1,M2);*/ \
									\
    for (j = 0; j < 100; j++)						\
      {									\
	ch = REV(buf[bp]);						\
									\
	if (ch == '-')							\
	  {								\
	    bp += 1;							\
	    sgn = sgn ? 0 : 1;						\
	    incr_pos(pos,1);						\
	    continue;							\
	  }								\
	else if (ch == '+')						\
	  {								\
	    bp++;							\
	    incr_pos(pos,1);						\
	    continue;							\
	  }								\
									\
	break;								\
      }									\
    									\
    if (j == 100)							\
      {									\
	*e = 0;								\
	return 0;							\
      }									\
    j = 0;								\
    									\
    if (sgn)								\
      {									\
	if (!SGN)							\
	  {								\
	    *e = 0;							\
	    return 0;							\
	  }								\
	nbits++;							\
	M0  = nbits % nbin;						\
	M   = (nbits/nbin) + (M0?1:(nbin?1:0))+1;			\
	P   = nbits2 == 64 ? (1<<M0) : 0;				\
      }									\
									\
    for (int k = 0; j < M; j++, k++)					\
      {									\
	ch = REV(buf[bp+k]);						\
	   								\
	if (ch == '\n' || ch == '\r')					\
	  {								\
	    j--;							\
	    continue;							\
	  }								\
	   								\
	ch2 = ch - '0';							\
	if (ch2 >= 0 && ch2 <= 9 && ch2 < N)				\
	  {								\
	    continue;							\
	  }								\
									\
	ch2 = ch -'a';							\
	if (ch2 >= 0 && ch2 < NK)					\
	  {								\
	    continue;							\
	  }								\
									\
	break;								\
      }									\
									\
    if (j == M)								\
      scm_misc_error("read", "wrong rank in integer", SCM_EOL);	\
									\
    if (j == 0)								\
     {									\
       *e = 0;								\
       return 0;							\
     }									\
									\
    int_t x = 0;							\
    int exp = (j-1)*nbin;						\
									\
    q = 0;								\
    pred = P && ((j == (M-1)) ? 1 : 0);					\
    for (int k = 0, p = 0; exp >=0; p++, k++, q++, exp -= nbin)		\
      {									\
	ch = REV(buf[bp+k]);						\
	if (ch == '\n' || ch == '\r')					\
	  {								\
	    if (ch == '\n')						\
	      {								\
		q = 0;							\
		newline(pos);						\
	      }								\
	    k--;							\
	    continue;							\
	  }								\
	    								\
	ch2 = buf[bp+p]-'0';						\
	ch  = (ch2 < 10 ? ch2 : (ch - 'a' + 10));			\
	if (pred && ch > P)						\
	  {								\
	    scm_misc_error("read",					\
			   "numerical overflow in integer read",	\
			   SCM_EOL);					\
	  }								\
	else								\
	  {								\
	    if (sgn)							\
	      {								\
		P = 0;							\
	      }								\
	    else							\
	      pred = 0;							\
	  }								\
	    								\
	x += ch<<exp;							\
      }									\
    									\
    incr_pos(pos,q);							\
									\
    bp += j;								\
    if (SGN)								\
      {									\
	if (sgn == 1)							\
	  {								\
	    if (x > M2)							\
	      scm_misc_error						\
		("read",						\
		 "numerical overflow/underflow in integer read",	\
		 SCM_EOL);						\
	    								\
	  }								\
	else								\
	  {								\
	    if (x > M1)							\
	      scm_misc_error						\
		("read",						\
		 "numerical overflow/underflow in integer read",	\
		 SCM_EOL);						\
	  }								\
      }									\
    else								\
      {									\
	if (x > M1)							\
	  scm_misc_error						\
	    ("read",							\
	     "numerical overflow/underflow in integer read",		\
	     SCM_EOL);							\
      }									\
									\
    *bufpt = bp;							\
    *e     = 1;								\
    return sgn ? -x : x;						\
  }

#define MKMKRINTBIN(tag, buf_t, REV)			      \
  MKRINTBIN(read_b_integer_u##tag , uint64_t, buf_t, REV, 0); \
  MKRINTBIN(read_b_integer_s##tag ,  int64_t, buf_t, REV, 1);

MKMKRINTBIN(_utf8     , uint8_t , REV0);
MKMKRINTBIN(_utf16    , uint16_t, REV0);
MKMKRINTBIN(_utf16_rev, uint16_t, rev16);
MKMKRINTBIN(_utf32    , uint32_t, REV0);
MKMKRINTBIN(_utf32_rev, uint32_t, rev32);

#define RDINT(u,utf)							\
  if (nbins)								\
    {									\
      return read_b_integer_##u##_##utf(buf, bufpt, pos, e, flags);	\
    }									\
  else									\
    {									\
      return read_integer_##u##_##utf(buf, bufpt, pos, e, flags);	\
    }

uint64_t c_read_integer_u_(uint8_t *buf_, size_t *bufpt, SCM pos, int *e,
			   uint64_t flags)
{
  int      Sh    = (flags & read_sh_mask  ) >> read_sh_shift  ;  
  uint64_t nbins = (flags & read_bins_mask) >> read_bins_shift;

  if (Sh == 0)
    {
      uint8_t *buf = (uint8_t *)buf_;
      RDINT(u,utf8);
    }
  else if (Sh == 1)
    {
      uint16_t *buf = (uint16_t *)buf_;
      int d = (flags & read_d_mask  ) >> read_d_shift;
      if (d == 1)
	{
	  if (is_me_big)
	    {
	      RDINT(u,utf16);
	    }
	  else
	    {
	      RDINT(u,utf16_rev);
	    }
	}
      else
	{
	  if (is_me_big)
	    {
	      RDINT(u,utf16_rev);
	    }
	  else
	    {
	      RDINT(u,utf16);
	    }
	}      
    }
  else if (Sh == 2)
    {
      uint32_t *buf = (uint32_t *)buf_;
      int d = (flags & read_d_mask  ) >> read_d_shift;
      if (d == 3)
	{
	  if (is_me_big)
	    {
	      RDINT(u,utf32);
	    }
	  else
	    {
	      RDINT(u,utf32_rev);
	    }
	}
      else
	{
	  if (is_me_big)
	    {
	      RDINT(u,utf32_rev);
	    }
	  else
	    {
	      RDINT(u,utf32);
	    }
	}      
    }

  scm_misc_error("read","unknown integer format",SCM_EOL);
  return 0;
}

int64_t c_read_integer_s_(uint8_t *buf_, size_t *bufpt, SCM pos, int *e,
			  uint64_t flags)
{
  int      Sh    = (flags & read_sh_mask  ) >> read_sh_shift  ;  
  uint64_t nbins = (flags & read_bins_mask) >> read_bins_shift;

  if (Sh == 0)
    {
      uint8_t *buf = (uint8_t *)buf_;
      RDINT(s,utf8);
    }
  else if (Sh == 1)
    {
      uint16_t *buf = (uint16_t *)buf_;
      int d = (flags & read_d_mask  ) >> read_d_shift;
      if (d == 1)
	{
	  if (is_me_big)
	    {
	      RDINT(s,utf16);
	    }
	  else
	    {
	      RDINT(s,utf16_rev);
	    }
	}
      else
	{
	  if (is_me_big)
	    {
	      RDINT(s,utf16_rev);
	    }
	  else
	    {
	      RDINT(s,utf16);
	    }
	}      
    }
  else if (Sh == 2)
    {
      uint32_t *buf = (uint32_t *)buf_;
      int d = (flags & read_d_mask  ) >> read_d_shift;
      if (d == 3)
	{
	  if (is_me_big)
	    {
	      RDINT(s,utf32);
	    }
	  else
	    {
	      RDINT(s,utf32_rev);
	    }
	}
      else
	{
	  if (is_me_big)
	    {
	      RDINT(s,utf32_rev);
	    }
	  else
	    {
	      RDINT(s,utf32);
	    }
	}
    }

  scm_misc_error("read","unknown integer format",SCM_EOL);
  return 0;
}


SCM_DEFINE(c_read_integer_u, "c-read-unsigned-integer", 4, 0, 0,
	   (SCM buf_, SCM bufpt_e, SCM pos, SCM flags_),
	   "find a most probable a list match")
#define FUNC_NAME c_read_u_integer_s
{
  uint8_t   *buf   = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_));
  uint64_t  flags  = scm_to_uint64(flags_);
  int       e      = 0;
  size_t    buffpt = scm_to_size_t(SCM_CAR(bufpt_e));

  uint64_t n = c_read_integer_u_(buf, &buffpt, pos, &e, flags);
  
  SCM_SETCAR(bufpt_e, scm_from_size_t(buffpt));
  SCM_SETCDR(bufpt_e, scm_from_size_t(e));
  
  return scm_from_uint64(n);
}
#undef FUNC_NAME

SCM_DEFINE(c_read_integer_s, "c-read-integer", 4, 0, 0,
	   (SCM buf_, SCM bufpt_e, SCM pos, SCM flags_),
	   "find a most probable a list match")
#define FUNC_NAME c_read_integer_s
{
  uint8_t   *buf   = ((uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_));
  uint64_t  flags  = scm_to_uint64(flags_);
  int       e      = 0;
  size_t    buffpt = scm_to_size_t(SCM_CAR(bufpt_e));

  int64_t n = c_read_integer_s_(buf, &buffpt, pos, &e, flags);
  
  SCM_SETCAR(bufpt_e, scm_from_size_t(buffpt));
  SCM_SETCDR(bufpt_e, scm_from_size_t(e));
  
  return scm_from_int64(n);
}
#undef FUNC_NAME

void init_integer()
{
  MM1[0]    = 5;
  MM1[1*2]  = 1;
  MM1[2*2]  = 6;
  MM1[3*2]  = 1;
  MM1[4*2]  = 5;
  MM1[5*2]  = 5;
  MM1[6*2]  = 9;
  MM1[7*2]  = 0;
  MM1[8*2]  = 7;
  MM1[9*2]  = 3;
  MM1[10*2] = 7;
  MM1[11*2] = 0;
  MM1[12*2] = 4;
  MM1[13*2] = 4;
  MM1[14*2] = 7;
  MM1[15*2] = 6;
  MM1[16*2] = 4;
  MM1[17*2] = 4;
  MM1[18*2] = 8;
  MM1[19*2] = 1;

  MM2[0]    = 7;
  MM2[1*2]  = 0;
  MM2[2*2]  = 8;
  MM2[3*2]  = 5;
  MM2[4*2]  = 7;
  MM2[5*2]  = 7;
  MM2[6*2]  = 4;
  MM2[7*2]  = 5;
  MM2[8*2]  = 8;
  MM2[9*2]  = 6;
  MM2[10*2] = 3;
  MM2[11*2] = 0;
  MM2[12*2] = 2;
  MM2[13*2] = 7;
  MM2[14*2] = 3;
  MM2[15*2] = 3;
  MM2[16*2] = 2;
  MM2[17*2] = 2;
  MM2[18*2] = 9;

  MM2[0*2+1]  = 8;
  MM2[1*2+1]  = 0;
  MM2[2*2+1]  = 8;
  MM2[3*2+1]  = 5;
  MM2[4*2+1]  = 7;
  MM2[5*2+1]  = 7;
  MM2[6*2+1]  = 4;
  MM2[7*2+1]  = 5;
  MM2[8*2+1]  = 8;
  MM2[9*2+1]  = 6;
  MM2[10*2+1] = 3;
  MM2[11*2+1] = 0;
  MM2[12*2+1] = 2;
  MM2[13*2+1] = 7;
  MM2[14*2+1] = 3;
  MM2[15*2+1] = 3;
  MM2[16*2+1] = 2;
  MM2[17*2+1] = 2;
  MM2[18*2+1] = 9;
}
