int quote_keywordish_symbols (uint64_t flags)
{
  uint64_t option = (flags & keyword_style_mask);

  if (!option)
    return 0;
  
  if (option == keyword_read)
    return ((flags & keyword_style_flag)) ? 1 : 0;
  
  return 1;
}


SCM quote_symbol_latin1_p(uint8_t *buf, size_t pt, size_t len, uint64_t flags, SCM str)
{
  if (len == 0) return SCM_BOOL_T;
  
  int i = 0;
  
  if (pt == 0)
    {
      int kind = quote_table1[buf[0]];
      
      if (kind)
	{
	  switch (kind)
	    {
	    case 1:
	      return SCM_BOOL_T;
	  
	    case 2:
	      if ((flags & r7rs_symbols_flag) > 0)
		return SCM_BOOL_T;
	      else
		break;
	  
	    case 3:
	      if (quote_keywordish_symbols (flags))
		return SCM_BOOL_T;
	      break;
	      
	    case 4:
	      if (len == 1) return SCM_BOOL_T;
	      break;

	    case 5:
	      if (scm_is_true (scm_string_to_number (str, scm_from_int(10))))
		return SCM_BOOL_T;
	    }
	}

      if (buf[len-1] == ':')
	if (quote_keywordish_symbols (flags))
	  {
	    return SCM_BOOL_T;
	  }

      i++;
    }

  len -= pt;
  
  for (; i < (1<<12) && i < len; i++)
    {
      int kind = quote_table2[buf[i]];

      if (kind)
	{
	  if (kind == 1)
	    return SCM_BOOL_T;
	  else
	    if (flags & r7rs_symbols_flag)
	      return SCM_BOOL_T;
	}
    }

  return scm_from_int(i);
}

SCM quote_symbol_utf32_p(uint32_t *buf, size_t pt, size_t len, uint64_t flags, SCM str)
{
  if (len == 0) return SCM_BOOL_T;
  
  int i = 0;
  
  if (pt == 0)
    {
      uint32_t ch = buf[0];

      if (ch < 256)
	{
	  int kind = quote_table1[ch];
  
	  if (kind)
	    {
	      switch (kind)
		{
		case 1:
		  return SCM_BOOL_T;
	  
		case 2:
		  if (flags & r7rs_symbols_flag)
		    return SCM_BOOL_T;
		  else
		    break;
	  
		case 3:
		  if (quote_keywordish_symbols (flags))
		    return SCM_BOOL_T;
		  break;
	      
		case 4:
		  if (len == 1) return SCM_BOOL_T;
		  break;

		case 5:
		  if (scm_is_true (scm_string_to_number (str,scm_from_int(10))))
		    return SCM_BOOL_T;
		}
	    }
	}
      else
	{
	  if (!uc_is_general_category_withtable (ch, INITIAL_IDENTIFIER_MASK))
	    return SCM_BOOL_T;
	}
      if (buf[len-1] == ':')
	if (quote_keywordish_symbols (flags))
	  {
	    return SCM_BOOL_T;
	  }

      i++;
    }

  buf += pt;
    
  for (; (i < (1<<12) && i < len); i++)
    {
      uint32_t ch = buf[i];
      
      if (ch < 256)
	{
	  int kind = quote_table2[ch];

	  if (kind)
	    {
	      if (kind == 1)
		return SCM_BOOL_T;
	      else
		if (flags & r7rs_symbols_flag)
		  return SCM_BOOL_T;
	    }
	}
      else
	{
	  if (!uc_is_general_category_withtable (ch,
						 SUBSEQUENT_IDENTIFIER_MASK))
	    return SCM_BOOL_T;
	}
    }

  return scm_from_int(i);
}


int print_symbol_adv(SCM str, uint8_t *buf, size_t len, uint64_t flags, int Sh,
		     int D)
{
  SCM p;
  SCM sbuf;
  size_t offset = 0;
  get_str_buf_start (&str, &sbuf, &offset);

  int d = 0;
  
  uint32_t q = scm_to_uint32 (scm_string_bytes_per_char (str));
  if (q == 1)
    {
      uint8_t  *chars = ((uint8_t  *) CHARS(sbuf)) + offset;
      p = quote_symbol_latin1_p(chars, 0, len, flags, str);
    }
  else
    {
      uint32_t *chars = ((uint32_t *) CHARS(sbuf)) + offset;
      p = quote_symbol_utf32_p(chars, 0, len, flags, str);
    }
	      
  if (scm_is_eq(p, SCM_BOOL_T))
    {
      if (flags & r7rs_symbols_flag)
	{
	  d  = put_char('|', buf, Sh, D);
	  d += port_encode_dispatch_r7rs(str, buf + d, flags, Sh, D);
	  d += put_char('|', buf + d, Sh, D);
	}
      else
	{
	  d  = put_string("#{", buf, Sh, D);
	  d += port_encode_dispatch_extended(str, buf + d, flags, Sh, D);
	  d += put_string("}#", buf + d, Sh, D);
	}
    }
  else
    {
      d += port_encode_dispatch_display(str, buf + d, flags, Sh, D);
    }

  return d;
}

int print_symbol_unint(SCM sym, SCM str, uint8_t *buf, size_t len,
		       uint64_t flags, int Sh, int D)
{
  int n = put_string("#<uninterned-symbol ", buf, Sh, D);
  n += print_symbol_adv(str, buf + n, len, flags, Sh, D);
  n += put_string(" 0x", buf + n, Sh, D);
  n += print_uint64_hex(SCM_UNPACK(sym),buf+n,flags,Sh,D,0);
  return n + put_char('>',buf+n,Sh,D);
}

int dispatch_symbol(SCM sym, uint8_t *buf, size_t maxlen,
		 uint64_t flags, int Sh, int D)
{
  SCM     str = scm_symbol_to_string(sym);
  size_t  len = scm_to_size_t(scm_string_length(str));
	  
  if (len <= maxlen)
    {
      if (SCM_SYMBOL_INTERNED_P(sym))
	{
	  return print_symbol_adv(str, buf, len, flags, Sh, D);
	}
      else
	{
	  return print_symbol_unint(sym, str, buf, len, flags, Sh, D);
	}
    }

  return -1;
}

SCM_DEFINE(symbol_quoted_p, "c-symbol-quoted?", 4, 0, 0,
	   (SCM str, SCM start_, SCM count_, SCM flags_),
	   "post string to buffer")
#define FUNC_NAME symbol_quoted_p_s
{
  SCM sbuf;
  size_t offset = 0;
  get_str_buf_start (&str, &sbuf, &offset);
  
  size_t   start = scm_to_size_t(start_);
  size_t   count = scm_to_size_t(count_);
  uint64_t flags = scm_to_uint64(flags_);
  
  uint32_t q = scm_to_uint32 (scm_string_bytes_per_char (str));
  if (q == 1)
    {
      uint8_t  *chars = ((uint8_t  *) CHARS(sbuf)) + offset + start;
      return quote_symbol_latin1_p(chars, start, count, flags, str);
    }
  else
    {
      uint32_t *chars = ((uint32_t *) CHARS(sbuf)) + offset + start;
      return quote_symbol_utf32_p(chars, start, count, flags, str);
    }
}
#undef FUNC_NAME


SCM_DEFINE(print_string_extended_c, "c-print-string-extended", 5, 0, 0,
	   (SCM str, SCM start_,
	    SCM buf_,   SCM bufpt_count, SCM flags_),
	   "post string to buffer")
#define FUNC_NAME print_string_write_s
{
  size_t bufpt = scm_to_size_t(SCM_CAR(bufpt_count));
  size_t count = scm_to_size_t(SCM_CDR(bufpt_count));
  size_t len   = scm_c_bytevector_length(buf_) - 600;
  size_t start = scm_to_size_t(start_);
  uint64_t flags = scm_to_uint64(flags_);
  uint8_t *buf = (uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_);

  int Sh = 0;
  int D  = 0;
  MKShD();

  port_encode_extended
    (buf,
     &bufpt,
     len,
     str,
     start,
     &count,
     flags,
     Sh,
     D);

  SCM_SETCAR(bufpt_count, scm_from_size_t(bufpt));
  SCM_SETCDR(bufpt_count, scm_from_size_t(count));

  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(print_string_r7rs_c, "c-print-string-r7rs", 5, 0, 0,
	   (SCM str, SCM start_,
	    SCM buf_,   SCM bufpt_count, SCM flags_),
	   "post string to buffer")
#define FUNC_NAME print_string_write_s
{
  size_t bufpt = scm_to_size_t(SCM_CAR(bufpt_count));
  size_t count = scm_to_size_t(SCM_CDR(bufpt_count));
  size_t len   = scm_c_bytevector_length(buf_) - 600;
  size_t start = scm_to_size_t(start_);
  uint64_t flags = scm_to_uint64(flags_);
  uint8_t *buf = (uint8_t *) SCM_BYTEVECTOR_CONTENTS(buf_);

  int Sh = 0;
  int D  = 0;
  MKShD();

  port_encode_r7rs
    (buf,
     &bufpt,
     len,
     str,
     start,
     &count,
     flags,
     Sh,
     D);

  SCM_SETCAR(bufpt_count, scm_from_size_t(bufpt));
  SCM_SETCDR(bufpt_count, scm_from_size_t(count));

  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME
