#define GP_GETREF(x) ((SCM *) (SCM_UNPACK(x)))
#define GP_UNREF(x)  ((SCM)   (x))

#include <fcntl.h>

struct mapped_elf_image
{
  char *start;
  char *end;
  char *frame_maps;
};

#if (SCM_MAJOR_VERSION == 2 && SCM_MINOR_VERSION < 9)
#define GET_VRA(x) ((x)->ra)
#define SET_VRA(x,v) ((x)->ra = (v))
#define JIT 0
#else
#define GET_VRA(x) ((x)->vra)
#define SET_VRA(x,v) ((x)->vra = (v))
#define JIT 1
#endif

SCM_DEFINE(gp_find_elf_relative_adress, "gp-bv-address", 1, 0, 0, 
	   (SCM bv),
	   "dives down in a gp variable one time")
#define FUNC_NAME s_gp_find_elf_relative_adress
{
  uint64_t ref = (uint64_t) SCM_BYTEVECTOR_CONTENTS(bv);
  return scm_from_uintptr_t(ref);
}
#undef FUNC_NAME

SCM_DEFINE(gp_make_null_procedure, "gp-make-null-procedure", 2, 0, 0, (SCM n, SCM def), 
	     "reuse a variable and make a new one")
#define FUNC_NAME s_gp_make_null_procedure
{
  int i,nfree;
  scm_t_bits *x;
  uintptr_t a = scm_to_uintptr_t(def);
  nfree = scm_to_int(n);
  x     = (scm_t_bits *) scm_gc_malloc (sizeof(SCM)*(nfree + 2), "program");  
  x[0] = (scm_t_bits) (scm_tc7_program | nfree << 16);
  x[1] = a;
  for(i = 0; i < nfree; i++)
    {
      x[2+i] = SCM_UNPACK(SCM_UNSPECIFIED);
    }
  return GP_UNREF(x);
}
#undef FUNC_NAME

SCM_DEFINE(gp_fill_null_procedure, "gp-fill-null-procedure", 3, 0, 0, (SCM proc, SCM addr, SCM l), 
	     "reuse a variable and make a new one")
#define FUNC_NAME s_gp_fill_null_procedure
{
  uintptr_t a = scm_to_uintptr_t(addr);
  int i = 0;
  SCM_SET_CELL_WORD_1 (proc, a);
  for(;SCM_CONSP(l);l = SCM_CDR(l),i++)
    {
      SCM_PROGRAM_FREE_VARIABLE_SET(proc,i,SCM_CAR(l));
    }
  return SCM_UNSPECIFIED;
}

#undef FUNC_NAME
SCM_DEFINE(gp_make_struct, "gp-make-struct", 2, 0, 0, 
	   (SCM vtable_data, SCM n), 
	   "")
#define FUNC_NAME s_gp_make_struct
{
  SCM ret;
  int i,nn = scm_to_int(n);
  ret = scm_words ((scm_t_bits)vtable_data | scm_tc3_struct, scm_to_int(n) + 2);
  SCM_SET_CELL_WORD_1 (ret, (scm_t_bits)SCM_CELL_OBJECT_LOC (ret, 2));
  for(i = 0; i < nn; i++)
    GP_GETREF(ret)[i+2] = SCM_UNSPECIFIED;
  return ret;
}
#undef FUNC_NAME


SCM_DEFINE(gp_set_struct, "gp-set-struct", 2, 0, 0, 
	   (SCM s, SCM l), 
	   "")
#define FUNC_NAME s_gp_set_struct  
{
  int i;
  SCM *v = GP_GETREF(s);
  SCM vtable = SCM_CAR(l);
  l = SCM_CDR(l);
  v[0] = SCM_PACK(((scm_t_bits) SCM_STRUCT_DATA(vtable)) | scm_tc3_struct);
  v    = GP_GETREF(v[1]);
  for(i=0;SCM_CONSP(l);l=SCM_CDR(l),i++)
    {
      v[i] = SCM_CAR(l);
    }
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(gp_code_to_int, "code-to-int", 1, 0, 0, (SCM x), 
	   "get the gp id tag")
#define FUNC_NAME s_gp_code_to_int
{
  scm_t_bits y = SCM_UNPACK(x);
  if((y & 7) == 4)
    return SCM_PACK((y&~7) | 2);
  else
    return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(gp_int_to_code, "int-to-code", 1, 0, 0, (SCM x), 
	   "get the gp id tag")
#define FUNC_NAME s_gp_int_to_code
{
  scm_t_bits y = SCM_UNPACK(x);
  return SCM_PACK((y&~7) | 4);
}
#undef FUNC_NAME

SCM_DEFINE(gp_get_var_var, "gp-get-var-var", 1, 0, 0, (SCM x), 
	   "take out the gp var data part")
#define FUNC_NAME s_gp_get_var_var
{  
  SCM *ref = GP_GETREF(x);
  return ref[1];
}
#undef FUNC_NAME

#if (SCM_MAJOR_VERSION == 2 && SCM_MINOR_VERSION < 9)
struct scm_vm_cont {
  uint32_t *ra;
  ptrdiff_t fp_offset;
  ptrdiff_t stack_size;
  union scm_vm_stack_element *stack_bottom;
  scm_t_dynstack *dynstack;
  uint32_t flags;
};
#endif
SCM scm_list_7(SCM x1,SCM x2,SCM x3,SCM x4,SCM x5,SCM x6,SCM x7)
{
  return scm_cons(x1,scm_cons(x2,scm_cons(x3,scm_list_4(x4,x5,x6,x7))));
}

SCM 
serialize_dynstack (scm_t_dynstack *in)
{
  size_t len = in->limit - in->base;

  SCM x = SCM_EOL;
  scm_t_bits *ret = (scm_t_bits *) in->base;
  int i  = 0;

  if(i < len)
    {
      while(i < len)
	{
	  i++;
	  scm_t_bits t = ret[i];

	  switch (SCM_DYNSTACK_TAG_TYPE(t))
	    {
	    case SCM_DYNSTACK_TYPE_NONE:
	      {
		x = scm_cons(scm_list_2
			     (scm_from_int
			      (SCM_DYNSTACK_TYPE_NONE),
			      scm_from_int
			      (SCM_DYNSTACK_TAG_FLAGS(t))),
			     x);
		i += 1;
		break;	
	      }
	    case SCM_DYNSTACK_TYPE_FRAME:
	      {
		x = scm_cons(scm_list_3(scm_from_int(SCM_DYNSTACK_TYPE_FRAME),
					scm_from_int(SCM_DYNSTACK_TAG_FLAGS(t)),
					scm_from_int64(ret[i+1])),
			     x);
		i += 2;
		break;
	      }
	      
	    case SCM_DYNSTACK_TYPE_REWINDER:
	      {
		x = scm_cons(scm_list_4(scm_from_int(SCM_DYNSTACK_TYPE_REWINDER),
					scm_from_int(SCM_DYNSTACK_TAG_FLAGS(t)),
					scm_from_int64(ret[i+1]),
					scm_from_int64(ret[i+2])),
			     x);
		i += 3;
		break;
	      }

	    case SCM_DYNSTACK_TYPE_UNWINDER:
	      {
		x = scm_cons(scm_list_4(scm_from_int(SCM_DYNSTACK_TYPE_UNWINDER),
					scm_from_int(SCM_DYNSTACK_TAG_FLAGS(t)),
					scm_from_int64(ret[i+1]),
					scm_from_int64(ret[i+2])),
			     x);
		i += 3;
		break;
	      }

	    case SCM_DYNSTACK_TYPE_WITH_FLUID:
	      {
		x = scm_cons(scm_list_4(scm_from_int(SCM_DYNSTACK_TYPE_WITH_FLUID),
					scm_from_int(SCM_DYNSTACK_TAG_FLAGS(t)),
					SCM_PACK(ret[i+1]),
					SCM_PACK(ret[i+2])),
			     x);
		i += 3;
		break;
	      }

	    case SCM_DYNSTACK_TYPE_PROMPT:
	      {
		x = scm_cons(scm_list_7(scm_from_int(SCM_DYNSTACK_TYPE_PROMPT),
					scm_from_int(SCM_DYNSTACK_TAG_FLAGS(t)),
					SCM_PACK(ret[i+1]),
					scm_from_int64(ret[i+2]),
					scm_from_int64(ret[i+3]),
					scm_from_int64(ret[i+4]),
					scm_from_int64(ret[i+5])),
			     x);
		i += 6;
		break;
	      }

	    case SCM_DYNSTACK_TYPE_DYNWIND:
	      {
		x = scm_cons(scm_list_4(scm_from_int(SCM_DYNSTACK_TYPE_DYNWIND),
					scm_from_int(SCM_DYNSTACK_TAG_FLAGS(t)),
					SCM_PACK(ret[i+1]),
					SCM_PACK(ret[i+2])),
			       x);
		i += 3;
		break;
	      }

	    case SCM_DYNSTACK_TYPE_DYNAMIC_STATE:
	      {
		x = scm_cons(scm_list_3(scm_from_int(SCM_DYNSTACK_TYPE_DYNAMIC_STATE),
					scm_from_int(SCM_DYNSTACK_TAG_FLAGS(t)),
					SCM_PACK(ret[i+1])),
			     x);
		i += 2;
		break;
	      }
	    }
	}
    }
  
  return scm_cons(scm_from_int(len), scm_reverse(x));
}

scm_t_dynstack * deserialize_dynstack(SCM x)
{
  int len = scm_to_int(SCM_CAR(x));

  scm_t_dynstack *ret;
  void *mem = scm_gc_malloc (sizeof (*ret) + len * sizeof(scm_t_bits), "dynstack");
  ret = (scm_t_dynstack *) mem;
  ret->base = (scm_t_bits *) (mem + sizeof (*ret));
  ret->limit = ret->base + len;
  ret->top = ret->base + len;

  scm_t_bits* pt = (scm_t_bits *) ret->base;
  
  x = SCM_CDR(x);

  int old = 0;  
  for(;SCM_CONSP(x);x=SCM_CDR(x))
    {
      pt[0] = (scm_t_bits) old;
      pt++;
      SCM y = SCM_CAR(x);
      int flags = scm_to_int(SCM_CADR(y));
      int type = scm_to_int(SCM_CAR(y));
      y = SCM_CDDR(y);

      switch (SCM_DYNSTACK_TAG_TYPE(type))
	{
	case SCM_DYNSTACK_TYPE_NONE:
	  {
	    pt[0] = SCM_MAKE_DYNSTACK_TAG (type, flags, 0);
	    old = 2;
	    pt += 1;

	    break;
	  }
	case SCM_DYNSTACK_TYPE_FRAME:
	  {
	    pt[0] = SCM_MAKE_DYNSTACK_TAG (type, flags, 1);
	    pt[1] = (scm_t_bits) scm_to_int64(SCM_CAR(y));

	    old = 3;
	    pt += 2;
	    break;
	  }
	      
	case SCM_DYNSTACK_TYPE_REWINDER:
	  {
	    pt[0] = SCM_MAKE_DYNSTACK_TAG (type, flags, 2);
	    pt[1] = (scm_t_bits) scm_to_int64(SCM_CAR(y));
	    pt[2] = (scm_t_bits) scm_to_int64(SCM_CDAR(y));

	    old = 4;
	    pt += 3;
	    break;
	  }

	case SCM_DYNSTACK_TYPE_UNWINDER:
	  {
	    pt[0] = SCM_MAKE_DYNSTACK_TAG (type, flags, 2);
	    pt[1] = (scm_t_bits) scm_to_int64(SCM_CAR(y));
	    pt[2] = (scm_t_bits) scm_to_int64(SCM_CDAR(y));
	    
	    pt += 3;
	    old = 4;
	    break;
	  }

	case SCM_DYNSTACK_TYPE_WITH_FLUID:
	  {
	    pt[0] = SCM_MAKE_DYNSTACK_TAG (type, flags, 2);
	    pt[1] = SCM_UNPACK(SCM_CAR(y));
	    pt[2] = SCM_UNPACK(SCM_CDAR(y));

	    pt += 3;
	    old = 4;
	    break;
	  }

	case SCM_DYNSTACK_TYPE_PROMPT:
	  {
	    pt[0] = SCM_MAKE_DYNSTACK_TAG (type, flags, 5);
	    pt[1] = SCM_UNPACK(SCM_CAR(y));
	    y = SCM_CDR(y);
	    pt[2] = (scm_t_bits) scm_to_int64(SCM_CAR(y));
	    pt[3] = (scm_t_bits) scm_to_int64(SCM_CADR(y));
	    pt[4] = (scm_t_bits) scm_to_int64(SCM_CADDR(y));
	    pt[5] = (scm_t_bits) scm_to_int64(SCM_CADDDR(y));

	    pt += 6;
	    old = 7;
	    break;
	  }

	case SCM_DYNSTACK_TYPE_DYNWIND:
	  {
	    pt[0] = SCM_MAKE_DYNSTACK_TAG (type, flags, 2);
	    pt[1] = SCM_UNPACK(SCM_CAR(y));
	    pt[2] = SCM_UNPACK(SCM_CDAR(y));

	    pt += 3;
	    old = 4;
	    break;
	  }

	case SCM_DYNSTACK_TYPE_DYNAMIC_STATE:
	  {
	    pt[0] = SCM_MAKE_DYNSTACK_TAG (type, flags, 1);
	    pt[1] = SCM_UNPACK(SCM_CAR(y));
		
	    pt += 2;
	    old = 3;
	    break;
	  }
	}
    }

  return ret;
}

SCM serialize_stack(union scm_vm_stack_element *sp, int size)
{
  SCM ret = scm_c_make_vector(size, SCM_BOOL_F);
  SCM *vp = SCM_I_VECTOR_WELTS(ret);
       
  int i  = 0;
  int ii = 0;
  
  int k = SCM_UNPACK(sp[i+(JIT?2:1)].as_scm);
  
  vp[i+0] = scm_from_int64(SCM_UNPACK(sp[i+0].as_scm));
  vp[i+1] = scm_from_int64(SCM_UNPACK(sp[i+1].as_scm));
  if(JIT)
    {
      vp[i+2] = scm_from_int64(SCM_UNPACK(sp[i+2].as_scm));
    }
  i += (JIT?3:2);
  ii = ii + k;
  
  for(;ii <= size;)
    {
      for(; i < ii; i++)
	{
	  vp[i] = sp[i].as_scm;
	}

      if(i == size)
	break;
      else
	k = SCM_UNPACK(sp[i+(JIT?2:1)].as_scm);
      
      vp[i+0] = scm_from_int64(SCM_UNPACK(sp[i+0].as_scm));
      vp[i+1] = scm_from_int64(SCM_UNPACK(sp[i+1].as_scm));
      if(JIT)
        {
          vp[i+2] = scm_from_int64(SCM_UNPACK(sp[i+2].as_scm));
        }
      i  += (JIT?3:2);
      ii += k;
    }
  return ret;
}
union scm_vm_stack_element * deserialize_stack(SCM x)
{
  int size = scm_c_vector_length(x);
  SCM  *sp = SCM_I_VECTOR_WELTS(x);
  union scm_vm_stack_element *vp
    = (union scm_vm_stack_element *) scm_gc_malloc (size * sizeof (union scm_vm_stack_element),
						    "unserilaized_vm_cont");
  int i  = 0;
  int ii = 0;
  
  vp[i+0].as_scm = SCM_PACK((scm_t_bits) scm_to_int64(sp[i+0]));
  vp[i+1].as_scm = SCM_PACK((scm_t_bits) scm_to_int64(sp[i+1]));
  if(JIT)
    {
      vp[i+2].as_scm = SCM_PACK((scm_t_bits) scm_to_int64(sp[i+2]));
    }
  int k = SCM_UNPACK(vp[i+(JIT?2:1)].as_scm);

  i  += (JIT?3:2);
  ii += k;
  
  for(;ii <= size;)
    {
      for(;i < ii; i++)
	{
	  vp[i].as_scm = sp[i];
	}

      if(i == size)
	break;
      else
	{
	  vp[i+0].as_scm = SCM_PACK((scm_t_bits)
				    scm_to_int64(sp[i+0]));
	  vp[i+1].as_scm = SCM_PACK((scm_t_bits)
				    scm_to_int64(sp[i+1]));

          if(JIT)
            {
              vp[i+2].as_scm = SCM_PACK((scm_t_bits)
                                        scm_to_int64(sp[i+2]));
            }
	  k = SCM_UNPACK(vp[i+(JIT?2:1)].as_scm);
	}

      i  += (JIT?3:2);
      ii += k;
    }
  
  return vp;
}

SCM make_cont (uint32_t *ra,
	       ptrdiff_t fp_offset,
	       ptrdiff_t stack_size,
	       union scm_vm_stack_element *stack_bottom,
	       scm_t_dynstack *dynstack,
	       uint32_t flags)
{
  struct scm_vm_cont *p;
  p = scm_gc_malloc (sizeof (*p), "unserialize_vm_cont");
  p->stack_size = stack_size;
  p->stack_bottom = scm_gc_malloc (p->stack_size * sizeof (*p->stack_bottom),
                                   "unserilaized_vm_cont");
  SET_VRA(p, ra);
  p->fp_offset = fp_offset;
  
  p->stack_bottom = stack_bottom;
  p->dynstack = dynstack;
  p->flags = flags;
  return scm_cell (scm_tc7_vm_cont, (scm_t_bits) p);
}

SCM make_cont2 (uint32_t *ra,
	       ptrdiff_t fp_offset,
	       ptrdiff_t stack_size,
	       uint32_t flags)
{
  struct scm_vm_cont *p;
  p = scm_gc_malloc (sizeof (*p), "unserialize_vm_cont");
  p->stack_size = stack_size;
  p->stack_bottom = scm_gc_malloc (p->stack_size * sizeof (*p->stack_bottom),
                                   "unserilaized_vm_cont");
  SET_VRA(p, ra);
  p->fp_offset = fp_offset;
  
  p->stack_bottom = NULL;
  p->dynstack = NULL;
  p->flags = flags;
  return scm_cell (scm_tc7_vm_cont, (scm_t_bits) p);
}

SCM_DEFINE(vm_continuation_p, "vm-continuation?", 1, 0, 0, (SCM x),
	   "vm continuation?")
#define FUNC_NAME s_vm_continuation_p
{
  if (SCM_HAS_TYP7 (x, scm_tc7_vm_cont))
    return SCM_BOOL_T;
  else
    return SCM_BOOL_F;
}
#undef FUNC_NAME


SCM_DEFINE(deserialize_continuation, "deserialize-vm-continuation", 1, 0, 0, (SCM x),
	   "deserialize a vm continuation")
#define FUNC_NAME s_deserialize_continuatio
{
  SCM x0 = scm_c_vector_ref(x,0);
  SCM x1 = scm_c_vector_ref(x,1);
  SCM x2 = scm_c_vector_ref(x,2);
  SCM x3 = scm_c_vector_ref(x,3);
  SCM x4 = scm_c_vector_ref(x,4);
  SCM x5 = scm_c_vector_ref(x,5);

  return make_cont((uint32_t *) scm_to_int64(x0),
		   scm_to_ptrdiff_t(x1),
		   scm_to_ptrdiff_t(x2),
		   deserialize_stack(x3),
		   deserialize_dynstack(x4),
		   scm_to_uint32(x5));		   
}
#undef FUNC_NAME

SCM_DEFINE(gp_make_vm_continuation, "gp-make-vm-continuation", 1, 0, 0, (SCM x),
	   "make a vm continuation")
#define FUNC_NAME s_gp_make_vm_continuation
{
  SCM x0 = scm_c_vector_ref(x,0);
  SCM x1 = scm_c_vector_ref(x,1);
  SCM x2 = scm_c_vector_ref(x,2);
  SCM x3 = scm_c_vector_ref(x,3);

  return make_cont2((uint32_t *) scm_to_int64(x0),
		   scm_to_ptrdiff_t(x1),
		   scm_to_ptrdiff_t(x2),
		   scm_to_uint32(x3));		   
}
#undef FUNC_NAME

SCM_DEFINE(gp_set_vm_continuation, "gp-set-vm-continuation!", 2, 0, 0, (SCM cont, SCM data),
	   "make a vm continuation")
#define FUNC_NAME s_gp_set_vm_continuation
{
  SCM x0 = SCM_CAR(data);
  SCM x1 = SCM_CDR(data);

  struct scm_vm_cont *vmcont = ((struct scm_vm_cont *)
				SCM_CELL_WORD_1 (cont));  

  vmcont->stack_bottom = deserialize_stack(x0);
  vmcont->dynstack     = deserialize_dynstack(x1);

  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME
	   
SCM_DEFINE(serialize_continuation, "serialize-vm-continuation", 1, 0, 0, (SCM x),
	   "serialize a vm continuation")
#define FUNC_NAME s_serialize_continuation
{
  if (SCM_HAS_TYP7 (x, scm_tc7_vm_cont))
    {
      struct scm_vm_cont *data = ((struct scm_vm_cont *) SCM_CELL_WORD_1 (x));
      SCM x0 = scm_from_int64((scm_t_bits)GET_VRA(data));
      SCM x1 = scm_from_ptrdiff_t(data->fp_offset);
      SCM x2 = scm_from_ptrdiff_t(data->stack_size);
      SCM x3 = serialize_stack(data->stack_bottom, data->stack_size);
      SCM x4 = serialize_dynstack(data->dynstack);
      SCM x5 = scm_from_uint32(data->flags);

      SCM ret = scm_c_make_vector(6,SCM_BOOL_F);
      
      scm_c_vector_set_x(ret,0,x0);
      scm_c_vector_set_x(ret,1,x1);
      scm_c_vector_set_x(ret,2,x2);
      scm_c_vector_set_x(ret,3,x3);
      scm_c_vector_set_x(ret,4,x4);
      scm_c_vector_set_x(ret,5,x5);
      
      return ret;
    }

  scm_misc_error("serialize-vm-continuation","not a vm continuation",SCM_EOL);
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(serialize_ra, "serialize-vm-ra", 1, 0, 0, (SCM x),
	   "serialize to ra")
#define FUNC_NAME s_serialize_ra
{
  if (SCM_HAS_TYP7 (x, scm_tc7_vm_cont))
    {
      struct scm_vm_cont *data = ((struct scm_vm_cont *) SCM_CELL_WORD_1 (x));
      return scm_from_int64((scm_t_bits)GET_VRA(data));
    }

  scm_misc_error("serialize-vm-continuation","not a vm continuation",SCM_EOL);
  return SCM_BOOL_F;
}
#undef FUNC_NAME

SCM_DEFINE(fcntl2, "fcntl2", 2, 0, 0, (SCM x, SCM y),
	   "serialize to ra")
#define FUNC_NAME s_fcntl2
{
  return scm_from_int(fcntl(scm_to_int(x),scm_to_int(y)));
}
#undef FUNC_NAME

SCM_DEFINE(fcntl3, "fcntl3", 3, 0, 0, (SCM x, SCM y, SCM z),
	   "serialize to ra")
#define FUNC_NAME s_fcntl3
{
  return scm_from_int(fcntl(scm_to_int(x),scm_to_int(y),scm_to_int(z)));
}
#undef FUNC_NAME
