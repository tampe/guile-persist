#include <libguile.h>
#include <termios.h>
#include "gmp.h"
#include "guile-2.2.c"

#define likely(x)    __builtin_expect (!!(x), 1)
#define unlikely(x)  __builtin_expect (!!(x), 0)

int gper_tcgetattr(int x, struct termios *v)
{
  return tcgetattr(x,v);
}

int gper_tcsetattr(int x, int y, struct termios *v)
{
  return tcsetattr(x,y,v);
}

SCM_DEFINE(q_address, "address", 1, 0, 0,
	   (SCM x),
	   "find a most probable a list match")
#define FUNC_NAME q_address_c
{
  return scm_from_uint64(SCM_UNPACK(x));
}
#undef FUNC_NAME

SCM_DEFINE(q_lookup1, "lookup-1", 2, 0, 0,
	   (SCM x, SCM soffset),
	   "find a most probable a list match")
#define FUNC_NAME q_lookup_c
{
  SCM *pt         = (SCM *)SCM_UNPACK(x);
  uint64_t offset = scm_to_uint64(soffset); 
  return scm_from_uint64(SCM_UNPACK(*(pt+offset)));
}
#undef FUNC_NAME


SCM_DEFINE(q_lookup2, "lookup-2", 3, 0, 0,
	   (SCM x, SCM soffset, SCM soffset2),
	   "find a most probable a list match")
#define FUNC_NAME q_lookup_c
{

  SCM *pt          = (SCM *)SCM_UNPACK(x);
  uint64_t offset  = scm_to_uint64(soffset);
  uint64_t offset2 = scm_to_uint64(soffset2);
  
  pt = (SCM *) (*(pt + offset));
  
  return scm_from_uint64(SCM_UNPACK(*(pt+offset2)));
}
#undef FUNC_NAME

#include "atom.c"

#include "hash/resize.c"

SCM_DEFINE(put_bytevector_few, "put-bytevector-few", 4, 0, 0,
	   (SCM port, SCM bv, SCM start, SCM count),
	   "find a most probable a list match")
#define FUNC_NAME print_bytevector_few_c
{
  int    s = scm_to_int(start);
  int    c = scm_to_int(count);
  char *pt = (char *) SCM_BYTEVECTOR_CONTENTS(bv);

  scm_c_write (port, pt+s, c);

  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME


#include "write/flags.c"
#include "write/util.c"
#include "write/myinteger.c"
#include "write/myreal.c"
#include "write/mynumber.c"
#include "write/string-meta.c"
#include "write/mystring.c"
#include "write/mysymbol.c"
#include "write/mybytevector.c"
#include "write/dispatch.c"
#include "write/myseq.c"
#include "write/read.c"

#undef FUNC_NAME

void persist_init()
{
#include "persist.x"
  serializer_init();
  deserialize_init();
  stis_hash_init();
  init_util();
  string_init();
  init_integer();
  real_init();
}

