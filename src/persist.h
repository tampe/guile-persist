#include <termios.h>	 

SCM_API SCM gp_find_elf_relative_adress(SCM bv);
SCM_API SCM gp_make_null_procedure(SCM n, SCM def);
SCM_API SCM gp_fill_null_procedure(SCM proc, SCM addr, SCM l);
SCM_API SCM gp_make_struct(SCM vtable_data, SCM n);
SCM_API SCM gp_set_struct(SCM s, SCM l);
SCM_API SCM gp_code_to_int(SCM x);
SCM_API SCM gp_int_to_code(SCM x);
SCM_API SCM gp_get_var_var(SCM x);
SCM_API SCM vm_continuation_p(SCM x);
SCM_API SCM deserialize_continuation(SCM x);
SCM_API SCM serialize_continuation(SCM x);
SCM_API SCM gp_make_vm_continuation(SCM x);
SCM_API SCM gp_set_vm_continuation(SCM cont, SCM data);
SCM_API SCM serialize_ra(SCM x);
SCM_API SCM fcntl2(SCM x, SCM y);
SCM_API SCM fcntl3(SCM x, SCM y, SCM z);
SCM_API SCM atom_c_serializer(SCM bv , SCM i,
			      SCM j, SCM stack, SCM data, SCM first);

SCM_API SCM atom_c_deserializer(SCM bv, SCM i, SCM j, SCM n, SCM stack,
				SCM map, SCM flags, SCM data, SCM final);

SCM_API SCM atom_c_serializer_info(SCM a, SCM b, SCM c, SCM d, SCM e, SCM f);
	  
extern int gper_tcgetattr(int x, struct termios *v);
extern int gper_tcsetattr(int x, struct termios *v);

extern SCM scm_mp_fac(SCM x);
