#ifdef CONTENTS
#undef CONTENTS
#endif

#define CONTENTS(_bv) ((uint128_t *) SCM_BYTEVECTOR_CONTENTS(_bv))

void stis_hash_init(void);
SCM  stis_mark(SCM obj);
SCM  stis_amark(SCM obj);
SCM  c_stis_b_make(uint64_t n, uint64_t m);
SCM  c_stis_a_make(uint64_t n);
void calk_k(uint64_t n, uint64_t h,uint64_t *k1, uint64_t *k2, uint64_t mask, int shift);

typedef __int128 uint128_t;
typedef uint8_t   v16si __attribute__ ((vector_size (16)));
typedef uint64_t  v2si  __attribute__ ((vector_size (16)));
typedef uint32_t  v4si  __attribute__ ((vector_size (16)));
typedef uint16_t  v8si  __attribute__ ((vector_size (16)));
typedef uint128_t v1si;

typedef double    w4di  __attribute__ ((vector_size (32)));

typedef union 
{
  v16si  v;
  v2si   m;
  v4si   n;
  v8si   o;
  v1si   x;
  SCM    scm[2];
} work_t;

typedef struct {
  work_t h1;
  work_t h2;
  SCM    x[16];
} awork_t;

typedef struct {
  work_t h;
  work_t x;
} bwork_t;

const uint64_t c0 = 0x0101010101010101UL;
const int      ca = 32 + 16*8;
scm_t_bits stis_smob  = 0;
scm_t_bits stis_asmob = 0;

work_t c,yy;

SCM stis_mark(SCM obj);

void stis_hash_init(void)
{
  c.m[0]  = c0;
  c.m[1]  = c0;

  yy.m[0] = 0UL;
  yy.m[1] = 0UL;

  stis_smob = scm_make_smob_type ("stis-hash",1);
  scm_set_smob_mark (stis_smob, stis_mark);

  stis_asmob = scm_make_smob_type ("stis-ahash",1);
  scm_set_smob_mark (stis_asmob, stis_amark);
}

SCM stis_amark(SCM obj)
{
  SCM bv     = SCM_PACK(SCM_SMOB_DATA(obj));
  scm_gc_mark(bv);
  uint64_t n = scm_c_bytevector_length(bv)/ca;
  awork_t *w  = (awork_t *) SCM_BYTEVECTOR_CONTENTS(bv);
  for (int i = 0; i < n; i++)
    {
      for (int j = 0; j < 16; j++)
	{
	  scm_gc_mark(w[i].x[j]);
	}
    }

    return SCM_BOOL_T;
}

SCM stis_mark(SCM obj)
{
  SCM bv     = SCM_PACK(SCM_SMOB_DATA(obj));
  scm_gc_mark(bv);
  
  uint64_t n = scm_c_bytevector_length(bv)/(2*16);
  bwork_t *w  = (bwork_t *) SCM_BYTEVECTOR_CONTENTS(bv);
  for (int i = 0; i < n; i++)
    {
      SCM x0 = w[i].x.scm[0];
      if (scm_is_true(x0))
	{
	  scm_gc_mark(x0);
	}
      
      SCM x1 = w[i].x.scm[1];
      if (scm_is_true(x1))
	{
	  scm_gc_mark(x1);
	}
    }
  
  return SCM_BOOL_T;
}


SCM c_stis_a_make(uint64_t n)
{
  SCM bv  = scm_c_make_bytevector(n*ca);
  scm_gc_protect_object(bv);
  
  awork_t *w  = (awork_t *) SCM_BYTEVECTOR_CONTENTS(bv);

  for(int i = 0; i < n; i+=1)
    {
      for (int j = 0; j < 2; j++)
	{
	  w[i].x[j] = SCM_BOOL_F;
	}
    }
  
  scm_t_bits xx = SCM_UNPACK(bv);
  SCM ret = scm_new_smob(stis_asmob, xx);
  
  scm_gc_unprotect_object(bv);

  return ret;
}

SCM c_stis_b_make(uint64_t n, uint64_t mm)
{
  SCM bv  = scm_c_make_bytevector(n*16*2);
  scm_gc_protect_object(bv);
  
  bwork_t *w  = (bwork_t *) SCM_BYTEVECTOR_CONTENTS(bv);

  SCM guard = scm_cons(SCM_BOOL_F,SCM_EOL);
  scm_gc_protect_object(guard);
  mm = 1 << mm;
  
  for(int i = 0; i < n; i++)
    {
      w[i].h.m[0]   = 0ul;
      w[i].h.m[1]   = 0ul;
      w[i].x.scm[0] = SCM_BOOL_F;
      SCM x = scm_c_make_vector(mm,SCM_BOOL_F);
      scm_c_vector_set_x(x,mm-1,SCM_EOL);      
      w[i].x.scm[1] = x;
      SCM_SETCDR(guard,scm_cons(x,SCM_CDR(guard)));
    }

  
  scm_t_bits xx = SCM_UNPACK(bv);
  SCM ret = scm_new_smob(stis_smob, xx);
  
  scm_gc_unprotect_object(bv);
  scm_gc_unprotect_object(guard);

  return ret;
}

SCM c_stis_b_search(bwork_t *x, uint64_t h, uint64_t  n);
SCM c_stis_b_add(bwork_t *x, uint64_t h, uint64_t n, SCM handle);
SCM c_stis_b_alist_ref(bwork_t *x, uint64_t h, uint64_t n);
SCM c_stis_b_alist_set_x(bwork_t *x, uint64_t h, uint64_t n, SCM l);
SCM c_stis_b_remove(bwork_t *x, uint64_t h, uint64_t n);

SCM  c_stis_a_re_search(awork_t *x, uint64_t h, uint64_t n, uint64_t m);
SCM  c_stis_a_search(awork_t *x, uint64_t h, uint64_t n);
void c_stis_a_add(awork_t *x, uint64_t h, uint64_t n, SCM handle);
SCM  c_stis_a_ref(awork_t *x, uint64_t n);
SCM  c_stis_a_set_x(awork_t *x, uint64_t n, SCM val);

inline void calk_k(uint64_t n, uint64_t h,uint64_t *k1, uint64_t *k2, uint64_t mask, int shift)
{
  int m = __builtin_ctzl(n);
  h %= (n<<8);
  
  *k2   = (h & (n-1));
  int k = (h >> m) & mask;
  if (k == 0)
    {
      k = *k2 & mask;
      if (k == 0)
      {
	k = (*k2 >> shift) & mask;
	if (k == 0)
	{
	  k = (*k2 >> (2*shift)) & mask;
	  if (k == 0)
	  {
	    k = 1;
	  }
	}
      }
    }
  
  *k1   = k;
}

work_t c;

inline int8_t getInd(work_t zz)
{
  int8_t ind = -1;
  if(zz.m[0])
    {
      ind = __builtin_ctzl(zz.m[0]) >> 3;
    }
  else if (zz.m[1])
    {
      ind = __builtin_ctzl(zz.m[1]) >> 3;
      ind  += 8;
    }

  return ind;
}


/*
[M=[M1,M2],C,V]
M : small hashed keys
C : lru front
V : rest vector
*/

inline SCM c_stis_b_remove_(bwork_t *x, uint64_t h, uint64_t n, uint64_t mask, int shift)
{
  uint64_t k1,k2;
  work_t hh;
  work_t xx;

  calk_k(n,h,&k1,&k2, mask, shift);

  hh.x = c.x * k1;
  
  xx.v = (hh.v == x[k2].h.v);

  int8_t ind = getInd(xx);

  if (ind >= 0)
    {
      if (ind == 0)
	{
	  x[k2].h.v[0] = 0;
	  x[k2].x.scm[0] = SCM_BOOL_F;
	  return SCM_BOOL_T;
	}
      else 
	{
	  x[k2].h.v[ind] = 0;
	  SCM *v = (SCM *)(x[k2].x.scm[1]);
	  v[ind] = SCM_BOOL_F;
	}
    }
  
  return SCM_UNSPECIFIED;
}

inline SCM c_stis_b_remove(bwork_t *x, uint64_t h, uint64_t n)
{
  return c_stis_b_remove_(x, h, n, 0xff, 8);
}

inline SCM c_stis_cache_b_remove(bwork_t *x, uint64_t h, uint64_t n)
{
  uint64_t mask  = scm_c_vector_length(x[0].x.scm[1]) - 1;
  int      shift = __builtin_clz(~mask);

  return c_stis_b_remove_(x, h, n, mask, shift);
}


inline SCM c_stis_b_add_(bwork_t *x, uint64_t h, uint64_t n, SCM val, uint64_t mask, int shift)
{
  work_t zz,xx;

  uint64_t k1,k2;

  calk_k(n,h,&k1,&k2, mask, shift);
  
  xx = x[k2].h;

  zz.v = (xx.v == yy.v);
  
  int8_t ind = getInd(zz);

  if (ind == -1)
    {
      return SCM_BOOL_F;
    }
  
  if (ind == 0)
    {
      x[k2].h.v[0]   = k1;
      x[k2].x.scm[0] = val;
    }
  else if (ind <= 15)
    {
      x[k2].h.v[ind] = k1;
      
      SCM v = x[k2].x.scm[1];
      int n;

      if (scm_is_false(v))
	{
	  v = scm_c_make_vector(4, SCM_BOOL_F);
	  scm_gc_protect_object(v);
	  scm_c_vector_set_x(v,3,SCM_EOL);
	  
	  n = 4;
	  x[k2].x.scm[1] = v;
	  scm_gc_unprotect_object(v);
	}
      else
	{
	  n = scm_c_vector_length(v);
	}
      
      if (ind > n - 1)
	{
	  SCM w   = scm_c_make_vector(2*n, SCM_BOOL_F);
	  scm_gc_protect_object(w);
	  scm_gc_protect_object(v);
	  SCM *vv = (SCM*) SCM_UNPACK(v);
	  SCM *ww = (SCM*) SCM_UNPACK(w);

	  for (int i = 1; i < n; i++)
	    {
	      ww[i] = vv[i];
	    }

	  ww[2*n] = vv[n];
	  x[k2].x.scm[1] = w;
	  scm_gc_unprotect_object(v);
	  scm_gc_unprotect_object(w);
	  
	  v=w;
	}
	      
      scm_c_vector_set_x(v,ind-1,val);
    }
  else
    {
      SCM v = x[k2].x.scm[1];
      int n = scm_c_vector_length(v);
      SCM x = scm_cons(val,scm_c_vector_ref(v,n-1));
      scm_c_vector_set_x(v,n-1,x);
    }
      
  return SCM_BOOL_T;
}

inline SCM c_stis_b_add(bwork_t *x, uint64_t h, uint64_t n, SCM val)
{
  return c_stis_b_add_(x, h, n, val, 0xff, 8);
}

inline SCM c_stis_cache_b_add(bwork_t *x, uint64_t h, uint64_t n, SCM val)
{
  uint64_t mask  = scm_c_vector_length(x[0].x.scm[1]) - 1;
  int      shift = __builtin_clz(~mask);
  return c_stis_b_add_(x, h, n, val, mask, shift);
}

inline SCM c_stis_b_search_(bwork_t *x, uint64_t h, uint64_t n,
			    uint64_t mask, int shift)
{
  work_t hh,xx;
  uint64_t k1,k2;
  
  calk_k(n,h,&k1,&k2,mask,shift);
  
  hh.x = c.x * k1;

  xx.v = (hh.v == x[k2].h.v);

  int ind = getInd(xx);
  
  if(ind >= 0)
    {
      uint64_t ind = __builtin_ctzl(xx.m[0]) >> 3;
  
      if (ind == 0)
	{
	first:
	  return x[k2].x.scm[0];
	}
      else 
	{
	  x[k2].h.v[ind] = x[k2].h.v[0];
	  x[k2].h.v[0  ] = k1;
  
	  SCM *z         = (SCM *) (x[k2].x.m[1]);
	  SCM v          = z[ind];
	  z[ind]         = x[k2].x.scm[0];
	  x[k2].x.scm[0] = v;

	  goto first;
	}
    }

  {
    SCM v = x[k2].x.scm[1];
    if (scm_is_false(v))
      {
	return SCM_BOOL_F;
      }

    int n = scm_c_vector_length(v);
    SCM x = scm_c_vector_ref(v,n-1);

    return scm_is_eq(x, SCM_EOL) ? SCM_BOOL_F : SCM_BOOL_T;
  }
}

inline SCM c_stis_b_search(bwork_t *x, uint64_t h, uint64_t n)
{
  return c_stis_b_search_(x, h, n, 0xff, 8);
}

inline SCM c_stis_cache_b_search(bwork_t *x, uint64_t h, uint64_t n)
{
  uint64_t mask  = scm_c_vector_length(x[0].x.scm[1]) - 1;
  int      shift = __builtin_clz(~mask);
  return c_stis_b_search_(x, h, n, mask, shift);
}

inline SCM c_stis_b_alist_ref(bwork_t *x, uint64_t h, uint64_t n)
{
  uint64_t k1,k2;
  calk_k(n,h,&k1,&k2, 0xffff, 16);

  SCM v = x[k2].x.scm[1];

  if (scm_is_false(v))
    {
      v = scm_c_make_vector(4,SCM_BOOL_F);
      scm_c_vector_set_x(v,3,SCM_EOL);
      x[k2].x.scm[1] = v;
    }
  
  int m = scm_c_vector_length(v);    
  return scm_c_vector_ref(v,m-1);
}

inline SCM c_stis_b_alist_set_x(bwork_t *x, uint64_t h, uint64_t n, SCM l)
{
  uint64_t k1,k2;
  calk_k(n,h,&k1,&k2,0xffff,16);

  SCM v = x[k2].x.scm[1];
  int m = scm_c_vector_length(v);
  scm_c_vector_set_x(v,m-1,l);
  return SCM_UNSPECIFIED;
}
  
void c_stis_a_add(awork_t *x, uint64_t h, uint64_t n, SCM handle)
{
  int index = n/16;
  int k     = n & 0xf;
  x[index].h1.v[k] = (h & 0xff);
  x[index].h2.v[k] = (h & 0xff00)>>8;
  x[index].x   [k] = handle;
}

inline SCM c_stis_a_search(awork_t *x, uint64_t h, uint64_t n)
{
  return c_stis_a_re_search(x, h, n, 0);
}

inline SCM c_stis_a_re_search(awork_t *x, uint64_t h, uint64_t n, uint64_t m)
{
  work_t hh;

  uint8_t h16 = (0xff & h); 
  hh.x = c.x * h16; 
    
  int ind;
  m /= 16;
  n /= 16;
  for (uint64_t i = m; i < n; i += 1)
    {
      work_t xx;
      xx.v = (hh.v == x[i].h1.v);

      if(!xx.x) continue;

      if(xx.m[0])
	{
	  ind = __builtin_ctzl(xx.m[0]) >> 3;
	  
	  do
	    {
	      uint8_t h16 = (0xff00 & h) >> 8; 
	      if (x[i].h2.v[ind] == h16)
		{
		  return x[i].x[ind];
		}
	      xx.v[ind] = 0;
	      ind = __builtin_ctzl(xx.m[0]) >> 3;
	      
	    } while (xx.m[0]);
	}

      if (xx.m[1])
	{
	  ind   = __builtin_ctzl(xx.m[1]) >> 3;

	  do
	    {
	      uint8_t h16 = (0xff00 & h) >> 8; 
	      if (x[i].h2.v[ind + 8] == h16)
		{
		  return x[i].x[ind + 8];
		}
	      else
		{
		  xx.v[ind+8] = 0;
		}
	      ind = __builtin_ctzl(xx.m[1]) >> 3;

	    } while (xx.m[1]);
	}       
    }
  
  return SCM_BOOL_F;
}

SCM c_stis_a_ref(awork_t *x, uint64_t n)
{
  return x[n>>4].x[n & 0xf];
}

SCM c_stis_a_set_x(awork_t *x, uint64_t n, SCM val)
{
  x[n>>4].x[n & 0xf] = val;

  return SCM_UNSPECIFIED;
}

#undef CONTENTS

