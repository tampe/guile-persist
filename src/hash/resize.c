#include "work.c"

SCM_DEFINE(stis_b_make, "stis-b-make", 2, 0, 0,
	   (SCM n_, SCM m_),
	   "make a stis b hash")
#define FUNC_NAME stis_b_search_c
{
  uint64_t     n  = SCM_UNPACK(n_) >> 2;
  uint64_t     m  = SCM_UNPACK(m_) >> 2;
 
  return c_stis_b_make(n,m);
}
#undef FUNC_NAME


SCM_DEFINE(stis_b_add, "stis-b-add!", 3, 0, 0,
	   (SCM bv, SCM h_, SCM handle),
	   "find a most probable a list match")
#define FUNC_NAME stis_b_add_c
{
  uint64_t     n      = SCM_BYTEVECTOR_LENGTH(bv) >> 5;
  uint64_t     h      = SCM_UNPACK(h_) >> 2;
  bwork_t     *x      = (bwork_t *) SCM_BYTEVECTOR_CONTENTS(bv);

  return c_stis_b_add(x, h, n, handle);
}
#undef FUNC_NAME

SCM_DEFINE(stis_cache_b_add, "stis-cache-b-add!", 3, 0, 0,
	   (SCM bv, SCM h_, SCM handle),
	   "find a most probable a list match")
#define FUNC_NAME stis_cache_b_add_c
{
  uint64_t     n      = SCM_BYTEVECTOR_LENGTH(bv) >> 5;
  uint64_t     h      = SCM_UNPACK(h_) >> 2;
  bwork_t     *x      = (bwork_t *) SCM_BYTEVECTOR_CONTENTS(bv);

  return c_stis_cache_b_add(x, h, n, handle);
}
#undef FUNC_NAME

#undef FUNC_NAME
SCM_DEFINE(stis_b_remove_x, "stis-b-remove!", 2, 0, 0,
	   (SCM bv, SCM h_),
	   "remove a hash item")
#define FUNC_NAME stis_b_add_c
{
  uint64_t     n      = SCM_BYTEVECTOR_LENGTH(bv) >> 5;
  uint64_t     h      = SCM_UNPACK(h_) >> 2;
  bwork_t     *x      = (bwork_t *) SCM_BYTEVECTOR_CONTENTS(bv);

  return c_stis_b_remove(x, h, n);
}
#undef FUNC_NAME
  
SCM_DEFINE(stis_b_search, "stis-b-search", 2, 0, 0,
	   (SCM bv, SCM h_),
	   "find a most probable a list match")
#define FUNC_NAME stis_b_search_c
{
  uint64_t     n      = SCM_BYTEVECTOR_LENGTH(bv) >> 5;
  uint64_t     h      = SCM_UNPACK(h_) >> 2;
  bwork_t     *x      = (bwork_t *) SCM_BYTEVECTOR_CONTENTS(bv);

  return c_stis_b_search(x, h, n);
}
#undef FUNC_NAME

SCM_DEFINE(stis_cache_b_search, "stis-cache-b-search", 2, 0, 0,
	   (SCM bv, SCM h_),
	   "find a most probable a list match")
#define FUNC_NAME stis_cache_b_search_c
{
  uint64_t     n      = SCM_BYTEVECTOR_LENGTH(bv) >> 5;
  uint64_t     h      = SCM_UNPACK(h_) >> 2;
  bwork_t     *x      = (bwork_t *) SCM_BYTEVECTOR_CONTENTS(bv);

  return c_stis_cache_b_search(x, h, n);
}
#undef FUNC_NAME

SCM_DEFINE(stis_b_alist_ref, "stis-b-alist-ref", 2, 0, 0,
	   (SCM bv, SCM h_),
	   "find the assoc list is stis hash")
#define FUNC_NAME stis_b_alist_c
{
  uint64_t     n      = SCM_BYTEVECTOR_LENGTH(bv) >> 5;
  bwork_t     *x      = (bwork_t *) SCM_BYTEVECTOR_CONTENTS(bv);
  uint64_t     h  = SCM_UNPACK(h_) >> 2;
  
  return c_stis_b_alist_ref(x, h, n);
}
#undef FUNC_NAME

SCM_DEFINE(stis_b_alist_set_x, "stis-b-alist-set!", 3, 0, 0,
	   (SCM bv, SCM h_, SCM l),
	   "set the assoc part of stis hash")
#define FUNC_NAME stis_b_alist_c
{
  uint64_t     n      = SCM_BYTEVECTOR_LENGTH(bv) >> 5;
  bwork_t     *x      = (bwork_t *) SCM_BYTEVECTOR_CONTENTS(bv);
  uint64_t     h  = SCM_UNPACK(h_) >> 2;  
  
  return c_stis_b_alist_set_x(x, h, n, l);
}
#undef FUNC_NAME

SCM_DEFINE(stis_a_make, "stis-a-make", 1, 0, 0,
	   (SCM n_),
	   "make a stis a hash")
#define FUNC_NAME stis_a_make_c
{
  uint64_t     n  = SCM_UNPACK(n_) >> 2;
 
  return c_stis_a_make(n);
}
#undef FUNC_NAME

SCM_DEFINE(stis_bv_ref, "stis-bv-ref", 1, 0, 0,
	   (SCM a),
	   "get smob bv")
#define FUNC_NAME stis_bv_c
{
  return SCM_PACK(SCM_SMOB_DATA(a));
}
#undef FUNC_NAME

SCM_DEFINE(stis_bv_set_x, "stis-bv-set!", 2, 0, 0,
	   (SCM a, SCM v),
	   "get smob bv")
#define FUNC_NAME stis_bv_set_x_c
{
  SCM_SET_SMOB_DATA(a,v);
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(stis_a_search, "stis-a-search", 3, 0, 0,
	   (SCM a_, SCM h_, SCM n_),
	   "find a most probable a list match")
#define FUNC_NAME stis_a_search_c
{
  uint64_t     n  = SCM_UNPACK(n_) >> 2;
  uint64_t     h  = SCM_UNPACK(h_) >> 2;
  awork_t     *x  = (awork_t *) SCM_BYTEVECTOR_CONTENTS(a_);
  
  return c_stis_a_search(x, h, n);
}
#undef FUNC_NAME


SCM_DEFINE(stis_a_add, "stis-a-add!", 4, 0, 0,
	   (SCM a_, SCM h_, SCM n_, SCM handle),
	   "find a most probable a list match")
#define FUNC_NAME stis_a_add_c
{
  uint64_t     n  = SCM_UNPACK(n_) >> 2;
  uint64_t     h  = SCM_UNPACK(h_) >> 2;
  awork_t     *x  = (awork_t *) SCM_BYTEVECTOR_CONTENTS(a_);
  
  c_stis_a_add(x, h, n, handle);
  
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(stis_a_ref, "stis-a-ref", 2, 0, 0,
	   (SCM a_, SCM n_),
	   "find a most probable a list match")
#define FUNC_NAME stis_a_ref_c
{
  uint64_t     n  = SCM_UNPACK(n_) >> 2;
  awork_t     *x  = (awork_t *) SCM_BYTEVECTOR_CONTENTS(a_);
  
  c_stis_a_ref(x,n);
  
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(stis_a_set_x, "stis-a-set!", 3, 0, 0,
	   (SCM a_, SCM n_, SCM val),
	   "find a most probable a list match")
#define FUNC_NAME stis_a_set_x_c
{
  uint64_t     n  = SCM_UNPACK(n_) >> 2;
  awork_t     *x  = (awork_t *) SCM_BYTEVECTOR_CONTENTS(a_);
  
  c_stis_a_set_x(x,n,val);
  
  return SCM_UNSPECIFIED;
}
#undef FUNC_NAME

SCM_DEFINE(stis_a_re_search, "stis-a-re-search", 4, 0, 0,
	   (SCM a_, SCM h_, SCM n_, SCM m_),
	   "find a most probable a list match")
#define FUNC_NAME stis_a_re_search_c
{
  uint64_t     n  = SCM_UNPACK(n_) >> 2;
  uint64_t     m  = SCM_UNPACK(m_) >> 2;
  uint64_t     h  = SCM_UNPACK(h_) >> 2;
  awork_t     *x  = (awork_t *) SCM_BYTEVECTOR_CONTENTS(a_);

  return c_stis_a_re_search(x, h, n, m);
}
#undef FUNC_NAME
