;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (persist elf-mod)
  #:use-module (system vm debug)
  #:export (find-program-file))

(define-syntax-rule (D x) (@@ (system vm dwarf) x))
(define-syntax-rule (U x) (@@ (system vm debug) x))

(set! (@@ (system vm dwarf) find-die-by-pc)
  (lambda (roots pc)
  (define (skip? ctx offset abbrev)
    (case ((D abbrev-tag) abbrev)
      ((subprogram compile-unit) #f)
      (else #t)))
  (define (recurse? die)
    (case ((D die-tag) die)
      ((compile-unit)
       (not (or (and=> ((D die-low-pc) die)
                       (lambda (low) (< pc low)))
                (and=> ((D die-high-pc) die)
                       (lambda (high) (<= high pc))))))
      (else #f)))
  ((D find-die) roots
   (lambda (die)
     (and (eq? ((D die-tag) die) 'subprogram)
	  (and (<= ((D die-low-pc)  die) pc)
	       (>= ((D die-high-pc) die) pc))))
   #:skip? skip? #:recurse? recurse?)))

(define (get-file die context)
  (let* ((base ((U debug-context-base) context))
	 (low-pc ((U die-ref) die 'low-pc))
	 (high-pc ((U die-high-pc) die))
	 (prog (let line-prog ((die die))
		 (and die
		      (or ((U die-line-prog) die)
			  (line-prog ((U ctx-die) ((U die-ctx) die))))))))
    (cond
     ((and low-pc high-pc prog)
      (call-with-values (lambda ()
			  ((U line-prog-scan-to-pc) prog low-pc))       
	(lambda (pc file line col)
	  file)))
     (else #f))))

(define* (find-die-file addr #:optional
                           (context (find-debug-context addr)))
  (and=> (and context
              ((U false-if-exception)
               ((U elf->dwarf-context) ((U debug-context-elf) context))))
         (lambda (dwarf-ctx)
           (find-die-file0 ((U read-die-roots) dwarf-ctx)
                           (- addr (debug-context-base context))
			   context))))


(define find-die-file0
  (lambda (roots pc context)

    (define (skip? ctx offset abbrev)
      (case ((D abbrev-tag) abbrev)
	((subprogram compile-unit) #f)
	(else #t)))
    #;
    (define (recurse? die)
      (case ((D die-tag) die)
	((compile-unit)
	 (not (or (and=> ((D die-low-pc) die)
			 (lambda (low) (< pc low)))
		  (and=> ((D die-high-pc) die)
			 (lambda (high) (<= high pc))))))
	(else #f)))

    
    (define (recurse? die)
      (case ((D die-tag) die)
	((compile-unit)
	 #t)
	(else #f)))

    ((D find-die) roots
     (lambda (die)
       (and (eq? ((D die-tag) die) 'subprogram)
	    (get-file die context)))
    
     #:skip? skip? #:recurse? recurse?)))


(define* (find-program-file addr #:optional
                               (context (find-debug-context addr)))
  (cond
   ((find-die-file addr)
    => (lambda (die) (get-file die context)))
   (else #f)))
