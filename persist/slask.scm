;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (persist slask)
  #:use-module (ice-9 vlist)
  #:replace (set-object-property!
             object-property
             set-procedure-property!
             procedure-property
             procedure-name)
  #:export (gp-cons?
            gp-cons-1
            gp-cons-2
            gp-make-pure-cons
            gp-cons-set-1!
            gp-cons-set-2!
            curstack?
            get-curstack
            vlist-null*
            gp-make-var
            gp-clobber-var
            gp-get-id-data
            gp?
            slask))

(define set-object-property!    (@ (guile) set-object-property!))
(define object-property         (@ (guile) object-property))
(define set-procedure-property! (@ (guile) set-procedure-property!))
(define procedure-property      (@ (guile) procedure-property))
(define procedure-name          (@ (guile) procedure-name))
(define gp-cons?          (lambda x #f))
(define gp-cons-1         #f)
(define gp-cons-2         #f)
(define gp-make-pure-cons #f)
(define gp-cons-set-1!    #f)
(define gp-cons-set-2!    #f)
(define (curstack? x)     #f)
(define (get-curstack)    #f)
(define vlist-null*       vlist-null)
(define gp-make-var       (lambda x #f))
(define gp-clobber-var    (lambda x #f))
(define gp-get-id-data    (lambda x #f))
(define gp?               (lambda x #f))
(define (slask x) x)
