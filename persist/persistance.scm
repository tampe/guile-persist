;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (persist persistance)
  #:use-module (system vm loader)
  #:use-module (system vm program)
  #:use-module (system vm debug)
  #:use-module (system vm dwarf)
  #:use-module (ice-9 match)
  #:use-module (ice-9 vlist)
  #:use-module (ice-9 control)
  #:use-module (ice-9 pretty-print)
  #:use-module (oop goops)
  #:use-module (persist primitive)
  #:use-module (persist elf-mod)
  #:use-module (persist slask)
  #:use-module (ice-9 pretty-print)
  #:use-module (rnrs bytevectors)
  
  #:export  (make-persister 
	     load-persists
	     save-persists
	     persist-set!
	     persist-ref
	     
	     make-shallow
	     associate-getter-setter
	     
	     name-object
             name-object-mod
	     name-object-deep
             name-object-deep-mod
	     define-named-object
	     define-shallow-object
	     define-fluid-object
	     
	     pcopyable?
	     deep-pcopyable?
	     pcopy
	     deep-pcopy
	     
	     repersist
	     
             fcntl2
             fcntl3

	     atom-c-serialize
	     atom-c-deserialize
	     atom-c-deserialize-unwind
	     atom-dump-bv
	     atom-load-bv
	     kind-of-hash
	     create-atom-validation-data
	     
	     stis-b-make
	     stis-b-search
	     stis-b-add!
	     stis-b-remove!
	     stis-b-alist-ref
	     stis-b-alist-set!

	     stis-cache-b-search
	     stis-cache-b-add!
	     
	     stis-bv-ref
	     stis-bv-set!
	     
	     stis-a-make
	     stis-a-search
	     stis-a-add!
	     stis-a-ref
	     stis-a-set!
	     stis-a-re-search

	     print-fixnum
	     print-real
	     put-bytevector-few

	     c-print-string
	     c-print-string-write
	     c-print-string-r7rs
	     c-print-string-extended
	     c-print-char
	     c-print-vector
	     c-print-list
	     c-symbol-quoted?
	     c-bom-write

	     c-read-integer
	     c-read-unsigned-integer
	     c-read-float
	     c-read-double
	     c-read-bytevector
	     c-butify
	     c-read-spaces
	     c-numerati
	     c-read-string
	     
	     c-print-u8
	     c-print-s8
	     c-print-u16
	     c-print-s16
	     c-print-u16
	     c-print-s16
	     c-print-u32
	     c-print-s32
	     c-print-u64
	     c-print-s64
	     c-print-f32
	     c-print-f64
	     
	     address
	     lookup-1
	     lookup-2
	     ))

(define *copying* (make-fluid #f))
(define (copy?) (fluid-ref *copying*))

(define jit?
  (or
   (and (=  (string->number (major-version)) 2)
        (>= (string->number (minor-version)) 9))
   (>= (string->number (major-version)) 3)))

(define (contproc? x) (equal? cont-addr (program-code x)))

(cond-expand
 (guile-3.0)
 ((or guile-2.0 guile-2.2)
  (define gp-bv-address          #f)
  (define gp-fill-null-procedure #f)
  (define gp-make-null-procedure #f)
  (define gp-set-struct          #f)
  (define gp-make-struct         #f)
  (define int-to-code            #f)
  (define code-to-int            #f)
  (define gp-get-var-var         #f)

  ;; Sielence the compiler
  (define serialize-vm-ra           #f)
  (define serialize-vm-continuation #f)
  (define gp-set-vm-continuation    #f)
  (define gp-make-vm-continuation   #f)
  (define vm-continuation?          #f)
  (define gp-set-vm-continuation!   #f)
  (define serialize-vm-ra           #f)))

(catch #t
  (lambda ()
    (load-extension "libguile-persist" "persist_init"))
  (lambda x
    (pk x)
    (backtrace)
    (let ((file  
           (%search-load-path "src/.libs/libguile-persist.so")))
      (if 
       file
       (catch #t
         (lambda ()          
           (load-extension file "persist_init"))
         (lambda x
           (warn
            "libguile-persist is not loadable!")))
       (warn 
        "libguile-persist is not present, did you forget to make it?")))))

(define-syntax-rule (aif it p . x) (let ((it p)) (if it . x)))


(define ref program-free-variable-ref)
(define (get-k)
  (call-with-prompt 'a
    (lambda () (abort-to-prompt 'a))
    (lambda (k . l) k)))

(define (get-kk)
  (call-with-prompt 'a
    (lambda ()
      (let/ec ret
	(abort-to-prompt 'a)
	(ret)))
    (lambda (k . l) k)))

(define *vm* #f)

(define (get-vm)
  (let* ((k (ref (get-kk) 0))
	 (l (serialize-vm-continuation k)))
    (list-ref (cadr (vector-ref l 4)) 6)))

(define default-ra (serialize-vm-ra (ref (get-k) 0)))

(define abort-addr (program-code abort-to-prompt))


(define-method (pcopyable? x)      #f)
(define-method (deep-pcopyable? x) #f)
(define-method (pcopy x)           (error "could not copy"))
(define-method (deep-pcopy x)      (error "could not deep copy"))


(define (default-print-func struct port)
  (format port "<pre-struct>"))

(define default-struct
  (make-vtable "pw" default-print-func))

(define (struct->data s)
  (let lp ((n 0) (r (list (struct-vtable s))))
    (catch #t
      (lambda ()
	(lp (+ n 1) (cons (struct-ref s n) r)))
      (lambda x
	(reverse r)))))

(define default-code (program-code format))

(define-syntax aif
  (syntax-rules ()
    ((_ (it . l) p a b)
     (call-with-values (lambda () p)
       (lambda (it . l)
	 (if it a b))))
    
    ((_ it p a b)
     (let ((it p))
       (if it a b)))))

(define lookup-table (make-hash-table))

(define cont-addr
  (program-code
   (call-with-prompt 'a
     (lambda () (abort-to-prompt 'a))
     (lambda (k . l) k))))


(define (find-module-from-path path)
  (aif (it) (hash-ref lookup-table path #f)
       it
       (let lp ((maps (all-mapped-elf-images)))
	 (if (pair? maps)
	     (let* ((m (car maps))
		    (b (gp-bv-address m))
		    (x (find-program-file b)))
	       (if x
		   (if (equal? x path)
		       (begin
			 (hash-set! lookup-table path m)
			 m)
		       (lp (cdr maps)))
		   (lp (cdr maps))))
	     (error "could not lookup function - did you have all modules loaded?")))))

(define (find-fkn-adress path offset)
  (aif (it) (hash-ref lookup-table path #f)
       (+ (gp-bv-address it) offset)
       (let lp ((maps (all-mapped-elf-images)))
	 (if (pair? maps)
	     (let* ((m (car maps))
		    (b (gp-bv-address m))
		    (a (+ b offset))
		    (x (find-program-file b)))
	       (if x
		   (if (equal? x path)
		       (begin
			 (hash-set! lookup-table path m)
			 a)
		       (lp (cdr maps)))
		   (lp (cdr maps))))
	     (error "could not lookup function - did you have all modules loaded?")))))

(define (load-persists log)        (log 'load))
(define (save-persists log)        (log 'save))
(define (persist-set! log tag val) (register-tag log tag val))
(define (persist-ref  log tag)     (log 'get-tag tag))
(define (make-shallow val)
  (set-object-property! val 'shallow val)
  val)

(define (associate-getter-setter o set get)
  (set-object-property! o 'get-accessor get)
  (set-object-property! o 'set-accessor set)
  o)

(define num2name (make-hash-table))
(define-syntax-rule (definex nm nr)
  (begin
    (hash-set! num2name nr 'nm)
    (define nm nr)))

(define (M x) (lambda (y) (eq? x y)))

(definex gp-var    0)
(definex var       1)
(definex fluid     2)
(definex pair      3)
(definex vector    4)
(definex procedure 5)
(definex struct    6)
(definex named     7)
(definex gp-cons   8)
(definex atom      9)
(definex code      10)
(definex standard-vtable 11)
(definex primitive       12)
(definex vlist-null-i    13)
(definex curstack        14)
(definex vm-cont         15)
(definex hashtable       16)

(definex set-gp-var      20)
(definex set-var         21)
(definex set-fluid       22)
(definex set-pair        23)
(definex set-vector      24)
(definex set-procedure   25)
(definex set-struct      26)
(definex set-gp-cons     28)
(definex set-accessor    29)
(definex make-object     30)
(definex make-reducer    31)
(definex set-reducer     32)
(definex set-vm-cont     33)
(definex make-vm-cont    34)
(definex set-hashtable   35)

(define (pr x)
  (let ((x1 (car x))
	(x2 (cadr x))
	(x3 (caddr x))
	(l  (cdddr x)))
    (pretty-print
     (cons*
      x1
      x2
      (let lp ((u x3) (r '()))
	(if (pair? u)
	    (let ((xx (car u)))
	      (if (pair? xx)
		  (lp (cdr u) (cons (cons (hash-ref num2name (car xx) #f) (cdr xx))
				    r))
		  (lp (cdr u) (cons xx r))))
	    (reverse r)))
      l))
    x))

(define (serialize-ip log addr)
  (let* ((elf (find-mapped-elf-image addr))
	 (elfaddr  (gp-bv-address elf))
	 (reladdr  (- addr elfaddr))
	 (path     (find-program-file elfaddr))
	 (icode    (log 'reg-code path)))
    (cons icode reladdr)))

(define (deserialize-ip log x)
  (let* ((icode   (car x))
	 (reladdr (cdr x))
	 (path    (log 'interface-lookup icode))
	 (m       (find-module-from-path path))
	 (b       (gp-bv-address m)))
    (+ b reladdr)))


(define (hash->assoc h)
  (hash-fold
   (lambda (k v l) (cons (cons k v) l))
   '() h))

(define (assoc->hash l)
  (let lp ((h (make-hash-table)) (l l))
    (if (pair? l)
	(let ((k.v (car l)))
	  (hash-set! h (car k.v) (cdr k.v))
	  (lp h (cdr l)))
	h)))

(define-syntax-rule (ket self l ...)
  (letrec ((self (let l ...))) self))

(define (copy x)
  (let ((p1 (make-persister))
	(p2 (make-persister)))
    (register-tag-shallow p1 'x x)
    (p2 'load-repr
        (with-fluids ((*copying* #t))
          (p1 'repr)))
    (p2 'get-tag 'x)))

(define (dump x f)
  (let ((p (make-persister #:file f)))
    (register-tag p 'x x)
    (p 'save)))

(define (dumps x)
  (let ((p (make-persister)))
    (register-tag p 'x x)
    (p 'save-str)))

(define (load file)
  (let ((p (make-persister #:file file)))
    (p 'load)
    (p 'get-tag 'x)))

(define (loads s)
  (let ((p (make-persister)))
    (p 'load-str s)
    (p 'get-tag 'x)))

(define (deep-copy x)
  (let ((p1 (make-persister))
	(p2 (make-persister)))
    (register-tag-deep p1 'x x)
    (p2 'load-repr
        (with-fluids ((*copying* #t))
          (p1 'repr)))
    (p2 'get-tag 'x)))

(define (unserialize log)
  (define lmap (make-hash-table))
  (let lp ((l (reverse (log 'data))))
    ;(pr (list 1 1 (list (car l))))
    (if (pair? l)
	(begin
	  (match (car l)
	    (((? (M vlist-null-i)) i)
	     (let ((v vlist-null*))
	       (log 'reg-obj i v)))

	    (((? (M curstack)) i)
	     (let ((v (get-curstack)))
	       (log 'reg-obj i v)))

	    (((? (M gp-var)) i)
	     (let ((v (gp-make-var)))
	       (log 'reg-obj i v)))

	    (((? (M var)) i)
	     (let ((v (make-variable 1)))
	       (log 'reg-obj i v)))

	    (((? (M fluid)) i)
	     (let ((v (make-fluid)))
	       (log 'reg-obj i v)))

	    (((? (M pair)) i)
	     (let ((v (cons 0 0)))
	       (log 'reg-obj i v)))

	    (((? (M make-object)) i x)
	     (log 'reg-obj i x))
	    
	    (((? (M make-reducer)) i x)
	     (let* ((f (log 'rev-lookup x))
		    (o (f)))
	       (log 'reg-obj i o)))

	    (((? (M make-vm-cont)) i x)
	     (let ((y (vector-ref x 0)))
	       (if (pair? y)
		   (let* ((relfaddr (cadr  y)) 
			  (path     (log 'interface-lookup (car y)))
			  (faddr    (find-fkn-adress path relfaddr))
			  (reladdr  (cddr y)))
		     (vector-set! x 0 (+ faddr reladdr)))
		   (vector-set! x 0 default-ra)))
	     (let* ((cont (gp-make-vm-continuation x)))
	       (log 'reg-obj i cont)))
	       	    
	    (((? (M struct)) i n)
	     (let ((v (gp-make-struct default-struct n)))
	       (log 'reg-obj i v)))
	   
	    (((? (M vector)) i n)
	     (let ((v (make-vector n)))
	       (log 'reg-obj i v)))

            (((? (M hashtable)) i)
	     (let ((v (make-hash-table)))
	       (log 'reg-obj i v)))

	    (((? (M procedure)) i n)
	     (let ((v (gp-make-null-procedure n default-code)))
	       (log 'reg-obj i v)))

	    (((? (M named)) i globdata)
	     (call-with-values (lambda () (log 'get-global globdata))
	       (lambda (path name i)                 
		 (aif (mod) (resolve-module path)
		      (aif (f) (aif (f) (if (module-defined? mod name)
                                            (module-ref mod name)
                                            #f)
                                    f
                                    (aif (f) (get-slask-name mod name i)
                                         f
                                         #f))
                           
			   (log 'reg-obj i f)
			   (error 
			    (format 
			     #f 
			     "symbol ~a not present in module ~a at unserializing" name path)))
		      (format 
		       #f 
		       "module ~a is not present at unserializing" path)))))

		   
            (((? (M standard-vtable)) i)
	     (log 'reg-obj i <standard-vtable>))

            (((? (M gp-cons)) i)
	     (let ((v (gp-make-pure-cons)))
	       (log 'reg-obj i v)))
	     
            (((? (M code)) i a)
	     (log 'reg-obj i (int-to-code a)))

            (((? (M primitive)) i a)
	     (log 'reg-obj i (get-primitive a)))

	    (((? (M atom)) i a)
	     (log 'reg-obj i a)
	     a)

	    (((? (M set-gp-var)) i j k)
	     (let ((var (log 'rev-lookup i))
		   (val (log 'rev-lookup k)))
	       (gp-clobber-var var j val)))

	    (((? (M set-vm-cont)) i j)
	     (let ((cont (log 'rev-lookup i))
		   (val  (log 'rev-lookup j)))
	       (let ((x (car val)))
		 (let lp ((i 0))
                   (define (ad   i) (if jit? (+ i 1) i))
                   (define (next i) (if jit? (+ i 2) (+ i 1)))
		   (if (< (+ i 1) (vector-length x))
		       (let* ((y        (vector-ref x (ad i)))
                              (tag      (car y))
			      (relfaddr (cadr  y)) 
			      (path     (if tag
                                            (log 'interface-lookup (car y))
                                            #f))
			      (faddr    (if tag
                                            (find-fkn-adress path relfaddr)
                                            (+ default-ra relfaddr)))
			      (reladdr  (cddr y)))
                         (if (and jit? (not (copy?)))
                             (vector-set! x i 0))
			 (vector-set! x (ad i) (+ faddr reladdr))
			 (lp (+ i (vector-ref x (next i))))))))
	       (let ((x (car val))
		     (y
		      (let* ((y (cdr val))
			     (n (car y))
			     (y (cdr y)))
			(let lp ((l y) (r '()))
			  (if (pair? l)
			      (let* ((x (car l))
				     (t (car x)))
				(case t
				  ((0 1 2 3 4 6 7)
				   (lp (cdr l) (cons x r)))
				  ((5)
				   (let ((x1 (list-ref x 0))
					 (x2 (list-ref x 1))
					 (x3 (list-ref x 2))
					 (x4 (list-ref x 3))
					 (x5 (list-ref x 4))
					 (x6 (deserialize-ip log
                                                             (list-ref x 5)))
					 (x7 (let ((y (list-ref x 6)))
					       (if y
						   y
						   (get-vm)))))
				     (lp (cdr l)
					 (cons (list x1 x2 x3 x4 x5 x6 x7)
                                               r))))))
			      (cons n (reverse r)))))))
	       (gp-set-vm-continuation! cont (cons x y)))))

	    (((? (M set-var)) i j)
	     (let ((v (log 'rev-lookup i))
		   (x (log 'rev-lookup j)))
	       (variable-set! v x)))

	    (((? (M set-fluid)) i j)
	     (let ((v (log 'rev-lookup i))
		   (x (log 'rev-lookup j)))
	       (fluid-set! v x)))

	    (((? (M set-pair)) i j k)
	     (let ((v (log 'rev-lookup i))
		   (x (log 'rev-lookup j))
		   (y (log 'rev-lookup k)))
	       (set-car! v x)
	       (set-cdr! v y)))

	    (((? (M set-reducer)) io iv)
	     (let ((o (log 'rev-lookup io))
		   (v (log 'rev-lookup iv)))
	       (for-each
		(lambda (x)
		  (apply (car x) o (cdr x)))
		v)))
	     
	    (((? (M set-vector)) i l)
	     (let ((v (log 'rev-lookup i)))
	       (let lp ((l l) (n 0))
		 (if (pair? l)
		     (let ((x (log 'rev-lookup (car l))))
		       (vector-set! v n x)
		       (lp (cdr l) (+ n 1)))))))

	    (((? (M set-hashtable)) i l)
	     (let ((x (log 'rev-lookup i)))
	       (let lp ((l l))
		 (if (pair? l)
		     (let ((k (log 'rev-lookup (caar l)))
                           (v (log 'rev-lookup (cdar l))))
		       (hash-set! x k v)
		       (lp (cdr l)))))))

	    (((? (M set-struct)) i l)
	     (let ((v (log 'rev-lookup i)))
	       (let lp ((l l) (n 0) (r '()))
		 (if (pair? l)
		     (let ((x (log 'rev-lookup (car l))))
		       (lp (cdr l) (+ n 1) (cons x r)))
		     (gp-set-struct v (reverse r))))))

	    (((? (M set-procedure)) i interface addr l)
	     (if interface
		 (let ((proc (log 'rev-lookup i))
		       (path (log 'interface-lookup interface))
		       (l (let lp ((l l))
			    (if (pair? l)
				(cons (log 'rev-lookup (car l))
				      (lp (cdr l)))
				'()))))
		   (gp-fill-null-procedure 
		    proc (find-fkn-adress path addr) l))
                 (case addr
                   ((0) ; cont proc
                    (let ((proc (log 'rev-lookup i))
                          (l (let lp ((l l))
                               (if (pair? l)
                                   (cons (log 'rev-lookup (car l))
                                         (lp (cdr l)))
                                   '()))))
                      (gp-fill-null-procedure 
                       proc cont-addr l)))
                   ((1) ; abort proc
                    (let ((proc (log 'rev-lookup i))
                          (l '()))
                      (gp-fill-null-procedure 
                       proc abort-addr l))))))

	       
            (((? (M set-accessor)) obj data)
             (let* ((obj  (log 'rev-lookup obj))
                    (data (log 'rev-lookup data))
		    (set  (object-property obj 'set-accessor)))
	       (if set
		   (set data)
		   (error
		    (format 
		     #f "was not able to get an accessor set from ~a"
		     obj)))))

	    (((? (M set-struct) i l)) 1)
	    
	    (((? (M set-gp-cons)) i id j k)
	     (let ((v (log 'rev-lookup i))
		   (x (log 'rev-lookup j))
		   (y (log 'rev-lookup k)))	       
	       (gp-cons-set-1! v x)
	       (gp-cons-set-2! v y))))
	       
	  
	  (lp (cdr l))))))

;;(define (vnull) (make-hash-table))
;;(define (vhash-consv- k v h) (begin (hashv-set! h k v) h))

;;(define none (list 'none))
#;(define (vhash-assv-  k h  ) (let ((r (hashv-ref h k none)))
			       (if (eq? r none)
				   #f
				   (cons k r))))

(define (vnull) vlist-null)
(define vhash-consv- vhash-consv)
(define vhash-assv-  vhash-assv)

(define* (make-persister #:key (file "persist.scm"))
  (ket self
       ((globmap  (make-hash-table))
	(mapglob  (make-hash-table))
	(iglob    0)
	(namemap  (make-hash-table))
	(mapname  (make-hash-table))
	(iname    0)
	(maps     (make-hash-table))
	(rev-maps (make-hash-table))
	(imap     0)
	(res     '())
	(i        0)
	(tags     '())
	(i->x     (make-hash-table))
	(i->obj   (vnull))
	(obj->i   (vnull))
	(atom->i  (make-hash-table)))

    (define (reg-global name path)
      (cons
       (aif (it) (hash-ref globmap path #f)
	    it
	    (let ((i iglob))
	      (set! iglob (+ iglob 1))
	      (hash-set! globmap path i)
	      (hash-set! mapglob i path)
	      i))
       (aif (it) (hash-ref namemap name #f)
	    it
	    (let ((i iname))
	      (set! iname (+ iname 1))
	      (hash-set! namemap name i)
	      (hash-set! mapname i name)
	      i))))
    
    (define inc
      (case-lambda 
	(()
	 (let ((res i))
	   (set! i (+ i 1))
	   i))
	((x)
	 (let ((i (inc)))
	   (set! obj->i (vhash-consv- (object-address x) i obj->i))
	   (set! i->obj (vhash-consv- i                  x i->obj))
	   i))))

    (define (update x) (set! res (cons x res)))
    
    (define-syntax-rule (mk-obj i obj code ...)
      (let ((i  (vhash-assv- (object-address obj) obj->i)))
	(if i
	    (cdr i)
	    (let ((i (inc obj)))
	      (update (begin code ...))
	      i))))

    (define-syntax-rule (mk-atom i obj code ...)
      (let ((i (hash-ref atom->i obj #f)))
	(if i
	    i
	    (let ((i (inc)))
	      (hash-set! atom->i obj i)
	      (hash-set! i->x    i   obj)
	      (update (begin code ...))
	      i))))
      
    (define (repr)
      (list tags (hash->assoc maps) (reverse res) imap i
	    (hash->assoc globmap) iglob
	    (hash->assoc namemap) iname))

    (define (f x)
      (map (lambda (x) (cons (cdr x) (car x))) x))
	 
    (define (load-data data)
      (set! tags (list-ref data 0))
      (set! maps (assoc->hash (list-ref data 1)))
      (set! rev-maps (assoc->hash (f (list-ref data 1))))
      (set! res  (reverse (list-ref data 2)))
      (set! imap (list-ref data 3))
      (set! i    (list-ref data 4))
      (set! globmap (assoc->hash (list-ref data 5)))
      (set! mapglob (assoc->hash (f (list-ref data 5))))
      (set! iglob   (list-ref data 6))
      (set! namemap (assoc->hash (list-ref data 7)))
      (set! mapname (assoc->hash (f (list-ref data 7))))
      (set! iname   (list-ref data 8)))

    (case-lambda
      ((kind)
       (case kind
	 ((inc)
	  (inc))

	 ((data)
	  res)

	 ((repr) (repr))
	 
	 ((save)	
	  (let ((s (open-file file "w")))
	    (write (repr) s)
	    (close s)))

	 ((save-str)	
	  (call-with-output-string
	   (lambda (port)
	     (write (repr) port))))
	  
	 ((load)
	  (let* ((s    (open-file file "r"))
		 (data (read s)))
	    (close s)
	    (load-data data)
	    (unserialize self)
	    ))

	 ((print)
	  (pretty-print (repr)))))

      ((kind i j k l)
       (case kind
	 ((set-gp-cons)
	  (update `(,set-gp-cons ,i ,j ,k ,l)))
	 ((set-procedure)
	  (update `(,set-procedure ,i ,j ,k ,l)))))

      ((kind i j k)
       (case kind
	 ((set-gp-var)
	  (update `(,set-gp-var ,i ,j ,k)))
	 ((set-pair)
	  (update `(,set-pair   ,i ,j ,k)))))
       
      ((kind i j)
       (case kind
	 ((make-procedure)
	  (mk-obj n i 
	    `(,procedure ,n ,j)))

	 ((make-struct)
	  (mk-obj n i 
	    `(,struct ,n ,j)))

	 ((make-object)
	  (mk-obj n i
		  `(,make-object ,n ,j)))

	 ((make-reducer)
	  (mk-obj n i
		  `(,make-reducer ,n ,j)))


	 ((make-vector)
	  (mk-obj n i
		  `(,vector ,n ,j)))

	 ((make-vm-cont)
	  (mk-obj n i
		  `(,make-vm-cont ,n ,j)))
	
	 ((reg-obj)
	  (let ((ja (object-address j)))
	    (set! i->obj (vhash-consv- i  j i->obj))
	    (set! obj->i (vhash-consv- ja i obj->i))))

	 ((store-tag)
	  (set! tags (cons (cons i j) tags)))

	 ((set-var)
	  (update `(,set-var ,i ,j)))

	 ((set-reducer)
	  (update (list set-reducer i j)))
	 
	 ((set-fluid)
	  (update `(,set-fluid ,i ,j)))
	 ((set-accessor)
	  (update `(,set-accessor ,i ,j)))
	 ((set-struct)
	  (update `(,set-struct ,i ,j)))
	 ((set-vm-cont)
	  (update `(,set-vm-cont ,i ,j)))
	 ((set-vector)
	  (update `(,set-vector ,i ,j)))
	 ((set-hashtable)
	  (update `(,set-hashtable ,i ,j)))))

      ((kind x)
       (case kind
         ((make-hashtable)
	  (mk-obj n i
		  `(,hashtable ,n)))

	 ((load-repr)
	  (load-data x)
	  (unserialize self))

	 ((load-str)
	  (let ((data (call-with-input-string x read)))
	    (load-data data)
	    (unserialize self)))
	 
	 ((get-global)
	  (values 
	   (hash-ref mapglob (car x) #f)
	   (hash-ref mapname (cadr x) #f)
           (caddr x)))

	 ((get-tag)
	  (let ((r (assoc x tags)))
	    (if (pair? r)
		(let ((x (vhash-assv- (cdr r)  i->obj)))
		  (if (pair? x)
		      (cdr x)
		      #f))
		#f)))
	 ((reg-code)
	  (let ((r (hash-ref maps x #f)))
	    (if r
		r
		(let ((i imap))
		  (set! imap (+ i 1))
		  (hash-set! maps x i)
		  (hash-set! rev-maps i x)
		  i))))

	 ((interface-lookup)
	  (hash-ref rev-maps x #f))
	 ((lookup)
	  (let ((x (vhash-assv- (object-address x) obj->i)))
	    (if x
		(cdr x)
		#f)))

	 ((rev-lookup)
	  (let ((x (vhash-assv- x i->obj)))
	    (if x
		(cdr x)
		#f)))

	 ((named)
	  (let ()
	    (define (mk procedure-property)
	      (let ((path (procedure-property x 'module)))
		(if path
		    (let ((name (aif (it) (object-property x 'name)
				     it
				     (if (procedure? x)
					 (procedure-name x)
					 #f))))
				    
		      (if name
			  (let* ((i (inc x))
				 (o (reg-global name path)))
			    (update `(,named ,i ,o
                                             (object-property
                                              x
                                              'persist-i 0)))
			    i)
			  #f))
		    #f)))

	    (aif (it) (mk object-property)
		 it
		 (if (procedure? x)
		     (mk procedure-property)
		     #f))))
		

	 ((make-accessor)
	  #t)

	 ((make-atom)
	  (mk-atom i x
	    `(,atom ,i ,x)))

	 ((make-code)
	  (mk-atom i x
	    `(,code ,i ,(code-to-int x))))

	 ((make-primitive)
	  (mk-atom i x
	    `(,primitive ,i ,(procedure-name x))))

	 ((make-standard-vtable)
	  (mk-obj i x
	    `(,standard-vtable ,i))) 

	 ((make-vlist-null)
	  (mk-obj i x
	    `(,vlist-null-i ,i))) 

	 ((make-curstack)
	  (mk-obj i x
	    `(,curstack ,i))) 

	 ((make-gp-var)
	  (mk-obj i x
	    `(,gp-var ,i))) 

	 ((make-gp-cons)
	  (mk-obj i x
	    `(,gp-cons ,i))) 

	 ((make-var)
	  (mk-obj i x
	    `(,var ,i))) 

	 ((make-fluid)
	  (mk-obj i x 
            `(,fluid ,i)))

	 ((make-pair)
	  (mk-obj i x 
            `(,pair ,i)))

	 ((make-gp-pair)
	  (mk-obj i x 
            `(,gp-cons ,i))))))))

(define (register-tag log tag x)
  (let ((i (persist log x)))
    (log 'store-tag tag i)))

(define (register-tag-shallow log tag x)
  (let ((i (persist log x #:shallow? #t)))
    (log 'store-tag tag i)))

(define (register-tag-deep log tag x)
  (let ((i (persist log x #:deep? #t)))
    (log 'store-tag tag i)))

(define log-log (make-fluid #f))
(define* (repersist . l)
  (define log (fluid-ref log-log))
  (if log
      (apply persist log l)
      (error "Assumes a persist have been executed")))

(define* (persist log x #:key (pred #t) (shallow? #f) (deep? #f))
(with-fluids ((log-log log))
(define* (upersist log x #:optional (pred #t))
  (define-syntax-rule (mk-name make-var x l ...)
    (let ((i (log 'named x)))
      (if i
	  i
	  (log make-var x l ...))))
  
  (define-syntax-rule (mk-name-f make-var x f)
    (let ((i (log 'named x)))
      (if i
	  i
	  (log make-var x (f)))))

  (define (dpersist log x . l)
    (if shallow?
	(list #:shallow x)	
	(apply upersist log x l)))
  
  (define-syntax-rule (do-if-deep x i code)
    (let ()
      (define (f n) (not (n x 'shallow)))
      (define (p)
	(if (f object-property)
	    (if (procedure? x)
		(f procedure-property)
		#t)
	    #f))
      (if (p) code)
      i))

  (define (make-atom)
    (let ((i (log 'make-atom x)))
      i))

  (define (make-a-code)
    (let ((i (log 'make-code x)))
      i))

  (define (make-a-var)
    (let ((i (mk-name 'make-var x)))
      (do-if-deep x i
	(let ((j (dpersist log (variable-ref x))))
	  (log 'set-var i j)))))

  (define (make-a-fluid)
    (let ((i (mk-name 'make-fluid x)))
      (do-if-deep x i
	(let ((j (dpersist log (fluid-ref x))))
	  (log 'set-fluid i j)))))

  (define (make-a-gp-var)
    (let ((i (mk-name 'make-gp-var x)))
      (do-if-deep x i
	(let* ((id (gp-get-id-data x))
	       (v  (gp-get-var-var x))
	       (j  (dpersist log v)))
	  (log 'set-gp-var i id j)))))

  (define (make-a-gp-cons)
    (let ((i (mk-name 'make-gp-cons x)))
      (do-if-deep x i
	(let* ((id (gp-get-id-data x))
	       (u  (gp-cons-1 x))
	       (v  (gp-cons-2 x))
	       (ju (dpersist log u))
	       (jv (dpersist log v)))
	  (log 'set-gp-cons i id ju jv)))))

  (define (make-vlist-null)
    (let ((i (mk-name 'make-vlist-null x)))
      i))

  (define (make-curstack)
    (let ((i (mk-name 'make-curstack x)))
      i))
    
  (define (make-standard-vtable)
    (let ((i (mk-name 'make-standard-vtable x)))
      i))

  (define (make-a-pair)
    (let ((i (mk-name 'make-pair x)))
      (do-if-deep x i
	(let* ((h (car x))
	       (t (cdr x))
	       (j (dpersist log h))
	       (k (dpersist log t)))
	  (log 'set-pair i j k)))))

  (define (make-an-access x y)
    (let ((i (mk-name 'make-accessor x)))
      (let* ((data (dpersist log (y)))
	     (obj  (dpersist log x #f)))	       
	(log 'set-accessor obj data)
	i)))
    
  (define (make-a-struct)
    (let* ((l (struct->data x))
	   (i (mk-name 'make-struct x (- (length l) 1))))
      (do-if-deep x i
		  (let ((l (map (lambda (x) (dpersist log x)) l)))
		    (log 'set-struct i l)))))

  (define (make-a-vector)
    (let* ((n (vector-length x))
	   (i (mk-name 'make-vector x n)))
      (do-if-deep x i
		  (let ((l (map (lambda (x) (dpersist log x)) 
				(vector->list x))))
		    (log 'set-vector i l)))))

  (define (make-a-hashtable)
    (let* ((i (mk-name 'make-hashtable x)))
      (do-if-deep x i
		  (let ((l (hash-fold (lambda (k v s)
                                        (cons (cons
                                               (dpersist log k)
                                               (dpersist log v))
                                              s))
                                      '()
                                      x)))
		    (log 'set-hashtable i l)))))

  (define (make-a-cont)
    (let* ((v  (serialize-vm-continuation x))
	   (vm (get-vm))
	   (x0 (vector-ref v 0))
	   (x1 (vector-ref v 1))
	   (x2 (vector-ref v 2))
	   (x3 (vector-ref v 3))
	   (x4 (vector-ref v 4))
	   (x5 (vector-ref v 5))
	   (x4
	    (let ((n  (car x4))
		  (x4 (cdr x4)))
	      (let lp ((l x4) (r '()))
		(if (pair? l)
		    (let* ((x   (car l))
			   (tag (car x)))
		      (case tag
			((0 1 2 3 4 6 7)
			 (lp (cdr l) (cons x r)))
			((5)
			 (let ((x1 (list-ref x 0))
			       (x2 (list-ref x 1))
			       (x3 (list-ref x 2))
			       (x4 (list-ref x 3))
			       (x5 (list-ref x 4))
			       (x6 (serialize-ip log (list-ref x 5)))
			       (x7 (let ((z (list-ref x 6)))
				     (if (= z vm)
					 #f
					 z))))
				
			   (lp (cdr l)
			       (cons (list x1 x2 x3 x4 x5 x6 x7) r))))))
				  
		    (cons n (reverse r))))))

	   (i
	    (catch #t
	      (lambda ()
		(let* ((addr     x0)
		       (faddr    (+ addr (die-low-pc
					  ((@@ (system vm debug) find-program-die) addr))))
		       (elf      (find-mapped-elf-image addr))
		       (elfaddr  (gp-bv-address elf))
		       (freladdr (- faddr elfaddr))
		       (reladdr  (- addr faddr))
		       (path     (find-program-file elfaddr))
		       (icode    (log 'reg-code path)))
		  (mk-name 'make-vm-cont x
			   ((@ (guile) vector) (cons* icode freladdr reladdr) x1 x2 x5))))
	      (lambda z
		  (mk-name 'make-vm-cont x
			   ((@ (guile) vector) 0 x1 x2 x5))))))
      
      (let lp ((i 0))
        (define (next i) (if jit? (+ i 2) (+ i 1)))
        (define (ad   i) (if jit? (+ i 1) i))
        (if (< i (vector-length x3))
            (begin
              (catch #t
                (lambda ()
                  (let* ((addr     (vector-ref x3 (ad i)))
                         (faddr    (+ addr
                                      (die-low-pc
                                       ((@@ (system vm debug)
                                            find-program-die) addr))))
                         (elf      (find-mapped-elf-image addr))
                         (elfaddr  (gp-bv-address elf))
                         (freladdr (- faddr elfaddr))
                         (reladdr  (- addr faddr))
                         (path     (find-program-file elfaddr))
                         (icode    (log 'reg-code path)))
                    (vector-set! x3 (ad i) (cons* icode freladdr reladdr))))
                (lambda xx
                  (let* ((addr     (vector-ref x3 (ad i)))
                         (reladdr  (- addr default-ra)))
                    (vector-set! x3 (ad i) (cons* #f reladdr 0)))))
	      (lp (+ i (vector-ref x3 (next i)))))))
      
      (do-if-deep x i
		  (let ((l1 (dpersist log (cons x3 x4))))
		    (log 'set-vm-cont i l1)))))

  (define (make-copy)
    (mk-name-f 'make-object x (lambda () (pcopy x))))
  
  (define (make-deep-copy)
    (let ((i (log 'named x)))
      (if i
	  (do-if-deep x i
	       (match (deep-pcopy x deep?)
		      ((#:obj x2)
		       (mk-name-f 'make-object x (lambda () x2)))
		      ((#:reduce make data)
		       (let ((id (dpersist log data)))
			 (log 'set-reducer i id)
			 i))))
		      
	  (match (deep-pcopy x deep?)
		 ((#:obj x2)
		  (mk-name-f 'make-object x (lambda () x2)))
		 ((#:reduce make data)
		  (let ((i (mk-name-f 'make-reducer x
				      (lambda () (dpersist log make)))))
		    (let ((id (dpersist log data)))
		      (log 'set-reducer i id)
		      i)))))))
			
	  
  (define (make-a-primitive)
    (let ((i (mk-name 'make-primitive x)))
      i))

  (define (make-a-procedure)
    (let* ((prim?   (primitive? x))
	   (free    (if prim? '() (program-free-variables x)))
	   (nfree   (length free))
	   (i       (mk-name 'make-procedure x nfree)))
      (do-if-deep x i
        (let* ((addr (program-code x)))
	  (cond
           ((= cont-addr addr)
            (let ((l (map (lambda (x) (dpersist log x)) free)))
              (log 'set-procedure i #f 0 l)))
           ((= abort-addr addr)
            (let ((l (map (lambda (x) (dpersist log x)) free)))
              (log 'set-procedure i #f 1 l)))
           (else            
            (let* ((elf     (find-mapped-elf-image addr))
                   (elfaddr (gp-bv-address elf))
                   (reladdr (- addr elfaddr))
                   (source  (find-program-file elfaddr))
                   (path   source
                           #;(if source
                           (match (string-split source #\/)
                           ((x ... e) (append 
                           (map string->symbol x) 
                           (list
                           (match (string-split e #\.)
                           ((x ... scm)
                           (string->symbol
                           (string-join x ".")))))))
                           (error "could not translate a procedure"))))
                   (icode   (log 'reg-code path))
                   (l       (map (lambda (x) (dpersist log x))
                                 free)))
              (log 'set-procedure i icode reladdr l))))))))
    

    
   
  (define-syntax-rule (mk code)
    (let ((i (log 'lookup x)))
      (if i
	  i
	  (code))))

  (cond   
   ((and pred (object-property x 'get-accessor))
    =>
    (lambda (y)
      (mk (lambda () (make-an-access x y)))))

   ((and shallow? (object-property x 'copy))
    =>
    (lambda (f)
      (mk (f x))))

   ((and shallow? (pcopyable? x))
    (mk make-copy))
 
   ((deep-pcopyable? x)
    (mk make-deep-copy))
	   	      
   ((curstack? x)
    (mk make-curstack))

   ((eq? x vlist-null*)
    (mk make-vlist-null))

   ((eq? x <standard-vtable>)
    (mk make-standard-vtable))  

   ((gp-cons? x)
    (mk make-a-gp-cons))

   ((gp? x)
    (mk make-a-gp-var))

   ((variable? x)
    (mk make-a-var))

   ((fluid? x)
    (mk make-a-fluid))

   ((pair? x)
    (mk make-a-pair))

   ((vector? x)
    (mk make-a-vector))

   ((struct? x)
    (mk make-a-struct))

   ((procedure? x)
    (if (and (primitive? x)
             (not (= abort-addr (program-code x)))
             (not (= cont-addr  (program-code x))))
	(aif (it) (if jit?
                      (let ((code (program-code x)))
                        (if (primitive-code? code)
                            code
                            #f))
                      (get-primitive x))
	     (mk make-a-primitive)
	     (error "non registred primitive"))
	(mk make-a-procedure)))

   ((hash-table? x)
    (mk make-a-hashtable))
   
   ((code-to-int x)
    (mk make-a-code))

   ((vm-continuation? x)
    (mk make-a-cont))
   
   (else
    (mk make-atom))))
(upersist log x pred)))

(define (name-object-f mn f nm val)
  (when (object-property val 'persist-i)
    (let lp ((i 0))
      (aif (it) (hash-ref hslask (cons* i f mn) #f)
           (lp (+ i 1))
           (begin
             (hash-set! hslask (cons* i f mn) val)
             (make-shallow val)
             (set-object-property! val 'persist-i      i)
             (set-object-property! val nm              f)
             (set-object-property! val 'module         mn))))))
  
(define-syntax name-object
  (lambda (x)
    (define (mod)
      (datum->syntax x (module-name (current-module))))
    (syntax-case x ()
      ((_ (n f)) #`(name-object-f '#,(mod) 'f 'n    f))
      ((_ f    ) #`(name-object-f '#,(mod) 'f 'name f))
      ((_ g f ...) #'(begin (name-object g) (name-object f) ...)))))

(define hslask (make-hash-table))


(define (name-object-mod-f mn f val)
  (when (object-property val 'persist-i)
    (let lp ((i 0))
      (aif (it) (hash-ref hslask (cons* i f mn) #f)
           (lp (+ i 1))
           (begin
             (hash-set! hslask (cons* i f mn) val)
             (make-shallow val)
             (set-object-property! val 'persist-i i)
             (set-object-property! val 'name    f)
             (set-object-property! val 'module mn))))))

(define-syntax name-object-mod
  (lambda (x)
    (define (mod)
      (datum->syntax x (module-name (current-module))))
    
    (syntax-case x ()
      ((_ (mod f val)) #`(name-object-mod-f 'mod     'f val))
      ((_ (    f val)) #`(name-object-mod-f '#,(mod) 'f val))
      ((_      f     ) #`(name-object-mod-f '#,(mod) 'f f))
      ((_ f ...)   #'(begin (name-object-mod f) ...)))))

(define (get-slask-name modname sym i)
  (if hslask (hash-ref hslask (cons* i 'f modname) #f)))

(define-syntax-rule (name-object-deep f)
  (let ((y (slask f)))
    (set-object-property! y 'name   'f)
    (set-object-property! y 'module (module-name (current-module)))))
  
(define-syntax-rule (name-object-deep-mod f)
  (let* ((m (current-module))
         (y (module-ref m 'f)))
    (set-object-property! y 'name   'f)
    (set-object-property! y 'module (module-name m))))
  
(define-syntax-rule (define-named-object f x)
  (define f
    (let ((y (slask x)))
      (set-object-property! y 'name   'f)
      (set-object-property! y 'module (module-name (current-module)))
      y)))

(define-syntax-rule (define-shallow-object name val)
  (define-named-object name
    (make-shallow val)))

(define-syntax-rule (define-fluid-object name val)
  (define-named-object name
    (associate-getter-setter (make-fluid val) fluid-ref fluid-set!)))


;;; THESE FAST BUT RUDIMENTARY SERIALIZING ROUTINES WORKS WELL WITH
;;; GUILES FIBERS AND IS REASONABLE SUSPENDABLE

#|
the vtables/classes defined the objects that are allowed to be serialized
the paths defines from which source files lambdas are allowed to be serialized

returns (values eq-validate equal-validate)
|#

(define (create-atom-validation-data list-of-classes/vtables list-of-valid-code-paths)
  (define map3 (make-hash-table))
  (define map4 (make-hash-table))
  
  (let lp ((j 0) (l list-of-classes/vtables))
    (if (pair? l)
	(begin
	  (hashq-set! map3 (car l) j      )
	  (hashq-set! map3 j       (car l))
	  
	  (lp (+ j 1) (cdr l)))
	
	(let lp ((j j) (l list-of-valid-code-paths))
	  (if (pair? l)
	      (begin
		(hash-set!  map4 (car l) j      )
		(hashq-set! map3 j       (car l))
		
		(lp (+ j 1) (cdr l)))
	      
	      (values map3 map4))))))
    

(define* (atom-dump-bv scm
		       #:key
		       (rsym #t)   (rbv #f)       (rstr #f) (rref #f)
		       (eq-map #f) (equal-map #f) (do-lam #f)
		       (eq-validate #f) (equal-validate #f)
		       (j 0))
  
  (define data (make-hash-table))
  (hashq-set! data 0 (if eq-map
			 eq-map
			 (if (or rsym rref)
			     (make-hash-table)
			     #f)))
  (hashq-set! data 1 (if equal-map
			 equal-map
			 (if (or rbv  rstr) (make-hash-table) #f)))
  (hashq-set! data 2  rstr)
  (hashq-set! data 3  rsym)
  (hashq-set! data 4  rref)
  (hashq-set! data 5  rbv)
  (hashq-set! data 6  rsym)
  (hashq-set! data 7  rref)
  (hashq-set! data 8  rref)
  (hashq-set! data 9  do-lam)
  (hashq-set! data 10 (if eq-validate    eq-validate    (make-hash-table)))
  (hashq-set! data 11 (if equal-validate equal-validate (make-hash-table)))
  
  (let lp ((bv (make-bytevector 20)) (i 0) (j j) (stack (list scm)))
    (match (atom-c-serialize bv i j stack data #t)
     ((p ok bv i j stack)
      (if p
	  ;; Roundtrip to enable asynchs (fibers)
	  (lp bv i j stack)

	  (if ok
	      (values bv i)
	      (error "Could not serialize")))))))
  

(define* (atom-load-bv bv #:key
		       (n              (bytevector-length bv))
		       (map            #f)
		       (map3           #f)
		       (do-lam         #f)
		       (j              0)
		       (j-validate     #f)
		       (equal-validate #f))
  (define data (make-hash-table))
  (hashq-set! data 9 do-lam)
  (hashq-set! data 10 (if j-validate j-validate (make-hash-table)))
  
  (let lp ((bv bv) (n n) (i 0) (j j) (stack '()) (map map) (flags 0))
    (match (atom-c-deserialize bv i j n stack map flags data #t)
      ((p ok i j stack map flags)
       (if p
	   (lp bv n i j stack map flags)
	   (if ok
	       stack
	       (error "problem in deserializer")))))))
	      

#|
serializing hashes will generally use a inexact method where we scan
the hash to check if there is keys that differs among hasq hasv and hash.
Then the selection is in the order(eq,equal,eqv). To gurantee that the 
hash is of the right kind, add a hash value with key 'kind-of-hash'
below and values.

1 -> eq
2 -> eqv
4 -> equal
|#  

(define kind-of-hash (gensym "hashkind"))

(atom-c-serialize-info abort-addr
		       cont-addr
		       find-program-file
		       find-mapped-elf-image
		       find-fkn-adress
		       kind-of-hash)
