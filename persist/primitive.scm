;;HACK
(define-syntax-rule (define-module- name args ...)
  (cond-expand
   (guile-3
    (define-module name
      #:declarative? #f
      args ...))
   (guile
    (define-module name args ...))))


(define-module- (persist primitive)
  #:use-module (system vm program)
  #:export (get-primitive primitive? primitive-module))

(define m (make-hash-table))

(define (primitive? x)
  (and (procedure? x)
       (program? x)
       (primitive-code? (program-code x))))

(define f
  (lambda (k v)
    (if (and (variable? v) (variable-bound? v) (primitive? (variable-ref v)))
	(hash-set! m k (variable-ref v)))))

(define (primitive-module module)  
  (module-for-each f (resolve-module module)))

(primitive-module '(guile))


(define (get-primitive x)
  (define nm (if (procedure? x) (procedure-name x) x))
  (hash-ref m nm #f))
      
